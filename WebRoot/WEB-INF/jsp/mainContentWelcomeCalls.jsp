
<c:set var="now" value="<%=new java.util.Date()%>"/>

<br />

<table class="tabDetailView" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
		
		<tr>
	      	<td class="tabDetailViewDL" valign="top" width="15%">Created Time :&nbsp;</td>
	     	<td class="tabDetailViewDF" valign="top" width="20%"><c:out value="${WelcomeCall.createdTime}"/></td>	      	      
	    </tr>
		<tr>
	      	<td class="tabDetailViewDL" valign="top" width="15%">Created By :&nbsp;</td>
	      	<td class="tabDetailViewDF" valign="top" width="20%"><c:out value="${WelcomeCall.createdBy}"/></td>
	      	
	    </tr>
	    <tr>
	      <td class="tabDetailViewDL" valign="top" width="15%">Member Name :&nbsp;</td>
	      	<td class="tabDetailViewDF" valign="top" width="20%"><c:out value="${WelcomeCall.memberName}"/></td>
	    </tr>
	    <tr>
	      	<td class="tabDetailViewDL" valign="top" width="15%">Call Status :&nbsp;</td>  
	      <td class="tabDetailViewDF" valign="top" width="20%">
	      	<c:if test="${WelcomeCall.status eq 0}">
	      		OPEN
	      	</c:if>
	      	<c:if test="${WelcomeCall.status eq 1}">
	      		CLOSED
	      	</c:if>
	      </td>
	    </tr>
	    <tr>
	      	<td class="tabDetailViewDL" valign="top" width="15%">Telephone Number :&nbsp;</td>
	     	<td class="tabDetailViewDF" valign="top" width="20%"><c:out value="${WelcomeCall.telephoneNumber}"/></td>	      	      
	    </tr>
		<tr>
	      <td class="tabDetailViewDL" valign="top" width="15%">Log Type Group :&nbsp;</td>
	      <td class="tabDetailViewDF" valign="top" width="20%">
	      <c:if test="${WelcomeCall.logType eq 0}">
	      	WELCOME CALL
	      </c:if>
	      	
	      </td>
	    </tr>    
		</tbody>
		
</table>

