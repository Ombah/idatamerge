<%@page import="java.util.Collection"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="com.ametis.cms.util.WebUtil"%>
<%@ taglib prefix="spring" uri="/WEB-INF/spring.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>

<%
	String alert = (String) request.getAttribute("alert");
	int index = 0;
	int totalIndex = 0;
	int count = 0;
	int countSet = 0;

	try {
		index = ((Integer) request.getAttribute("index")).intValue();
		count = ((Integer) request.getAttribute("count")).intValue();
		countSet = ((Integer) request.getAttribute("countSet")).intValue();
		totalIndex = ((Integer) request.getAttribute("halAkhir")).intValue();
	} catch (Exception e) {
	}
	if (alert != null && !alert.trim().equals("")) {
%>
<div id="warning" align="center">
	<c:out value="${alert}"></c:out>
</div>
<%
	}
%>

<%
	String rowclass = "";
	int i = 0;
	int indexint = 0;
	WebUtil.getAttributeInteger(request, "index", 0).intValue();
%>

<!-- Search Container Start -->

<form name="form1" action="receivedocumentclaim" method="POST"
	enctype="multipart/form-data">
	<input type="hidden" name="navigation">
	 <input type="hidden" name="sortcolumn" value="<c:out value="${sortcolumn}" />"> 
	 <input	type="hidden" name="sortorder" value="<c:out value="${sortorder}" />">
	<input type="hidden" name="columntosort"value="<c:out value="${columntosort}" />">
	 <input	type="hidden" name="arah" value="">
	 <input	type="hidden" name="newDocumentStatus" value="">
	 <input	type="hidden" name="searchDocumentStatus" value="">  
	 <input type="hidden"name="index" value="<c:out value="${index}" />"> 
	 <input	type="hidden" name="documentId"	value="<c:out value="${document.documentId}" />">

	<%
		Collection Documents = (Collection) request.getAttribute("Documents");
	%>

	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody>
			<tr>
				<td nowrap="nowrap">
					<h3>
						<img src="images/h3Arrow.gif" border="0">&nbsp;Receive
						Document Claim
					</h3>
				</td>
				<td width="100%"><img src="images/blank.gif" height="1"
					width="1"></td>
			</tr>
		</tbody>
	</table>
	<table class="tabForm" border="0" cellpadding="0" cellspacing="0"
		width="100%">
		<tbody>
			<tr id="trRow">
				<td class="dataLabel" nowrap="nowrap">Search Keyword:
					&nbsp;&nbsp; 
					<input size="20" name="searchtext"
					value="<c:out value="${searchtext}"/>" type="text">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					Upload Date : &nbsp;&nbsp; 
					<input name="upload_date"
					id="jscal_field" tab="1" maxlength="10" size="11"
					readonly="readonly" value="<c:out value="${upload_date}" />"
					type="text"> <img src="images/jscalendar.gif"
					alt="Enter Date" id="jscal_trigger" align="absmiddle" height="18"
					width="18"> <script type="text/javascript">
						Calendar.setup({
							inputField : "jscal_field", // id of the input field
							ifFormat : "%Y-%m-%d", // format of the input field
							button : "jscal_trigger", // trigger for the calendar (button ID)
							align : "Tl", // alignment (defaults to "Bl")
							singleClick : true
						});
					</script> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
					
					Document Status : &nbsp;&nbsp; 
					<Select id="statusSelector">
					<option value="-1">ALL</option>
					<option value="0">Belum Dikerjakan</option>
					<option value="1">Sedang Dikerjakan</option>
					<option value="2">Sudah Selesai Dikerjakan</option>
					</Select>
					
					<input title="Search"
					accesskey="C"
					class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus"
					onClick="javascript:cari();" name="searchButton" value=" Search "
					type="button">
					
				</td>
			</tr>
		</tbody>
	</table>


	<table id="myTable" class="listView" width="100%" cellspacing="0" cellpadding="0">
		<tbody>
			<tr>
				<td colspan="20" align="right"><table border="0"
						cellpadding="0" cellspacing="0" width="100%">
						<tbody>
							<tr>
								<td class="listViewPaginationTdS1" align="left"></td>
								<td class="listViewPaginationTdS1" align="right" nowrap="nowrap">

									<%
										if (index == 1) {
									%> <img src="images/start_off.gif" alt="Start"
									align="absmiddle" border="0" height="10" width="9">
									Start&nbsp; <img src="images/previous_off.gif" alt="Previous"
									align="absmiddle" border="0" height="10" width="4">
									Previous&nbsp;&nbsp; <%
 	} else if ((index - 1) > 0) {
 %> <img src="images/start.gif" alt="Start" align="absmiddle" border="0"
									height="10" width="9"> <a href="javascript:goleftbgt()"
									class="listViewPaginationLinkS1"> Start&nbsp; </a> <img
									src="images/previous.gif" alt="Previous" align="absmiddle"
									border="0" height="10" width="4"> <a
									href="javascript:goleft()" class="listViewPaginationLinkS1">
										Previous&nbsp;&nbsp; </a> <%
 	}
 %> <span class="pageNumbers">(<c:out value="${minIndex}" /> - <c:out
											value="${maxIndex}" /> of <c:out value="${count}" />)
								</span>&nbsp;&nbsp; <%
 	if (totalIndex > index) {
 %> <a href="javascript:goright()" class="listViewPaginationLinkS1">Next&nbsp;
										<img src="images/next.gif" alt="Next" align="absmiddle"
										border="0" height="10" width="4">
								</a>&nbsp;&nbsp; <a href="javascript:gorightbgt()"
									class="listViewPaginationLinkS1">End&nbsp; <img
										src="images/end.gif" alt="End" align="absmiddle" border="0"
										height="10" width="9">
								</a> <%
 	} else {
 %> Next&nbsp; <img src="images/next_off.gif" alt="Next"
									align="absmiddle" border="0" height="10" width="4">
									&nbsp;&nbsp; End&nbsp; <img src="images/end_off.gif" alt="End"
									align="absmiddle" border="0" height="10" width="9"> <%
 	}
 %>
								</td>
							</tr>
						</tbody>
					</table></td>
			</tr>
			<tr height="20">
				<td width="2%" nowrap="nowrap" class="listViewThS1" scope="col">No.</td>
				<td style="display:none;" scope="col" class="listViewThS1" nowrap="nowrap" width="10%">ID 
-					File</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="20%">Nama
					File</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Upload
					By</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Upload
					Date</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Status</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Action</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Download</td>
			</tr>


			<c:forEach items="${Documents}" var="document" varStatus="status">
				<%
					if (i % 2 == 0) {
							rowclass = "col1";
						} else if (i % 2 != 0) {
							rowclass = "col2";
						}
						i++;
				%>
				<tr height="20">
					<td class="oddListRowS1" align="center" bgcolor="#e7f0fe"
						nowrap="nowrap" valign="top">
						<%-- <%=(i + ((indexint - 1) * countSet))--%>
						 <c:out	value="${status.count}" />
					</td>
					<td style="display:none;" class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out value="${document.documentId}" />
					</td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out value="${document.originalDocName}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out value="${document.createdBy}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out value="${document.createdTime}" /></td>

					<td id='${status.count-1}' class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top">
						<c:if test="${document.status == 0}">
						<c:out value="Belum Dikerjakan" />
						</c:if>
						<c:if test="${document.status == 1 }">
						<c:out value="Sedang Dikerjakan" />
						</c:if>
						<c:if test="${document.status == 2 }">
						<c:out value="Sudah Selesai Dikerjakan" />
						</c:if>
						</td>
						
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"	valign="top">
						<a class=linkDetail href="javascript:editStatus(${status.count-1})">
							Edit Status</a></td>	

					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						align="center" valign="top">
							<a href="javascript:detil('<c:out value="${document.documentId}" />')">
							<img src="images/view.gif" class="action_icon" alt="View" title="View">
							
							<a href="javascript:download('<c:out value="${document.documentId}" />')">
							<img src="images/download.gif" class="action_icon" alt="Download" title="Download"></a>
					</td>
				</tr>

				<tr>
					<td colspan="20" class="listViewHRS1"></td>
				</tr>

			</c:forEach>

			<tr>
				<td class="listViewPaginationTdS1" align="left"></td>
				<td class="listViewPaginationTdS1" align="right" nowrap="nowrap"
					colspan="20">
					<%
						if (index == 1) {
					%> <img src="images/start_off.gif" alt="Start" align="absmiddle"
					border="0" height="10" width="9"> Start&nbsp; <img
					src="images/previous_off.gif" alt="Previous" align="absmiddle"
					border="0" height="10" width="4"> Previous&nbsp;&nbsp; <%
 	} else if ((index - 1) > 0) {
 %> <img src="images/start.gif" alt="Start" align="absmiddle" border="0"
					height="10" width="9"> <a href="javascript:goleftbgt()"
					class="listViewPaginationLinkS1"> Start&nbsp; </a> <img
					src="images/previous.gif" alt="Previous" align="absmiddle"
					border="0" height="10" width="4"> <a
					href="javascript:goleft()" class="listViewPaginationLinkS1">
						Previous&nbsp;&nbsp; </a> <%
 	}
 %> <span class="pageNumbers">(<c:out value="${minIndex}" /> - <c:out
							value="${maxIndex}" /> of <c:out value="${count}" />)
				</span>&nbsp;&nbsp; <%
 	if (totalIndex > index) {
 %> <a href="javascript:goright()" class="listViewPaginationLinkS1">Next&nbsp;
						<img src="images/next.gif" alt="Next" align="absmiddle" border="0"
						height="10" width="4">
				</a>&nbsp;&nbsp; <a href="javascript:gorightbgt()"
					class="listViewPaginationLinkS1">End&nbsp; <img
						src="images/end.gif" alt="End" align="absmiddle" border="0"
						height="10" width="9">
				</a> <%
 	} else {
 %> Next&nbsp; <img src="images/next_off.gif" alt="Next"
					align="absmiddle" border="0" height="10" width="4">
					&nbsp;&nbsp; End&nbsp; <img src="images/end_off.gif" alt="End"
					align="absmiddle" border="0" height="10" width="9"> <%
 	}
 %>
				</td>
			</tr>
		</tbody>
	</table>
</form>

<script language="Javascript">
	var fileId = 0;
	var totalRow = 1;
	var arr = [];
	
	
	var table = document.getElementById('myTable');
	if(table!=null){

	}
	
	function getval(cell) {

            //alert(cell.innerHTML);
		alert(cell);
        } 
	
	function editStatus(rowId){
		
		var cellAction = table.rows[(rowId+1)*2].cells[6];
		var cellStatus = table.rows[(rowId+1)*2].cells[5];
		var statusCurrent = cellStatus.innerHTML.trim();
		
		if(statusCurrent == "Belum Dikerjakan"){
			
			statusCurrent =0;
			cellStatus.innerHTML ='<select id="select_'+rowId+'"><option selected value=0>Belum Dikerjakan</option><option value=1>Sedang Dikerjakan</option><option value=2>Sudah Selesai Dikerjakan</option></select>	';
		}else if(statusCurrent == "Sedang Dikerjakan"){
		
			statusCurrent =1;
			cellStatus.innerHTML ='<select id="select_'+rowId+'"><option value=0>Belum Dikerjakan</option><option selected value=1>Sedang Dikerjakan</option><option value=2>Sudah Dikerjakan</option></select>	';
		}else if(statusCurrent == "Sudah Selesai Dikerjakan"){
		
			statusCurrent =2;
			cellStatus.innerHTML ='<select id="select_'+rowId+'"><option value=0>Belum Dikerjakan</option><option value=1>Sedang Dikerjakan</option><option selected value=2>Sudah Selesai Dikerjakan</option></select>	';
		}
		
		cellAction.innerHTML ='<a class=linkDetail href="javascript:saveStatus('+cellStatus.id+')">Save</a>   <a class=linkDetail href="javascript:cancelStatus('+cellStatus.id+','+statusCurrent+')">Cancel</a>';

	}
	
	function cancelStatus(rowId,statusBefore){	
		var cellAction = table.rows[(rowId+1)*2].cells[6];
		var cellStatus = table.rows[(rowId+1)*2].cells[5];
		cellAction.innerHTML ='<a class=linkDetail href="javascript:editStatus('+cellStatus.id+')">EditStatus</a> ';
		if(statusBefore==0){
			cellStatus.innerHTML ="Belum Dikerjakan";
		}else if(statusBefore==1){
			cellStatus.innerHTML ="Sedang Dikerjakan";
		}else if(statusBefore==2){
			cellStatus.innerHTML ="Sudah Selesai Dikerjakan";
		}
		
		
	}
	
	function saveStatus(rowId){
		
		var docId = table.rows[(rowId+1)*2].cells[1].innerHTML;
		
		var isiStatus = document.getElementById("select_"+rowId).value;
		
		document.form1.method = "POST";
		document.form1.documentId.value = docId;
		document.form1.newDocumentStatus.value = isiStatus;
		document.form1.action = "receivedocumentclaim";
		document.form1.navigation.value = "updateStatus";
		document.form1.submit();
		
	}

	function addFile() {
		fileId++;
		var html = '<tr> '
				+ '<td class="dataLabel" nowrap="nowrap">Input Raw '
				+ 'File: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; '
				+ '<input type="file" id= "files_'+fileId+'" name="files['+fileId+']"> '
				+ '<button onclick="javascript:addFile(); return false;">Tambah File</button>	'
				+ '<button id="'
				+ fileId
				+ '" onclick="javascript:hapus(this.id); return false;">Hapus</button> '
				+ '</td>' + '</tr>';
		add('tBody', 'tr', fileId, html);
	}

	function add(parentId, elementTag, elementId, html) {
		var p = document.getElementById(parentId);
		var newElement = document.createElement(elementTag);
		newElement.setAttribute('id', "trRow" + elementId);
		newElement.innerHTML = html;
		p.appendChild(newElement);
		totalRow++;
		getChildId();
	}

	function hapus(elementId) {
		if (totalRow > 1) {
			var element = document.getElementById("trRow" + elementId);
			element.parentNode.removeChild(element);
			totalRow--;
			getChildId();
		}
	}
	function hapus2(elementId) {
		if (totalRow > 1) {
			var element = document.getElementById(elementId);
			element.parentNode.removeChild(element);
			totalRow--;
			getChildId();
		}
	}
	function simpan() {
		isAllSelected = 1;

		for (i = 0; i <= fileId; i++) {
			var file = document.getElementById("files_" + i);
			if (file != null) {
				var namaFile = file.value;
				if (namaFile == "") {

					isAllSelected = 0;
				}
			}
		}
		if (!isAllSelected) {
			alert("Mohon lengkapi pilihan file yang akan diupload");
		} else {
			document.form1.method = "POST";
			document.form1.action = "receivedocumentclaim";
			document.form1.submit();
		}

	}
	function cancel() {
		//document.form1.navigation.value = "back";
		//document.form1.action = "receivedocumentclaim";
		//document.form1.submit();
		window.location.reload();
	}
	function getChildId() {
		var str = "";
		var parent = document.getElementById('tBody'), child;
		for (i = 0; i < parent.childNodes.length; i++) {
			child = parent.childNodes[i];
			if (child.nodeName == 'TR') {
				str = str + child.id;
				//alert(child.id);
			}
		}
		var hidden = document.getElementById("hiddenArr");
		hidden.value = str;
		//alert(str);
	}
	
	function cari() {
		document.form1.navigation.value = "gosearch";
		var searchDocStatus = document.getElementById("statusSelector").value;
		document.form1.searchDocumentStatus.value = searchDocStatus;
		document.form1.action = "receivedocumentclaim";
		document.form1.method = "POST";
		document.form1.submit();
	}
	function download (idx){
		window.open ("document?navigation=preview&action=download&documentId="+idx+"&url=claim-form","Download","width=1024, height=768, menubar=yes, status=no, toolbar=no, scrollbars=yes, resizable=yes");
	}
	function detil(idx) {
		window
				.open(
						"document?navigation=preview&documentId=" + idx
								+ "&url=receivedocumentclaim",
						"Search",
						"width=1024, height=768, menubar=yes, status=no, toolbar=no, scrollbars=yes, resizable=yes");

	}
	function hapusdoc(idx) {
		var delConfirm = window
				.confirm("Are You Sure Want To Delete This Entry ?");

		if (delConfirm) {
			document.form1.method = "POST";
			document.form1.documentId.value = idx;
			document.form1.action = "receivedocumentclaim";
			document.form1.navigation.value = "delete";
			document.form1.submit();
		}
	}
	<%
	String nav="";
	if(request.getAttribute("navigation").equals("gosearch")||request.getAttribute("navigation").equals("golookup")){
		nav = (String)request.getAttribute("navigation");
	}
	%>
	function goleft(){
		document.form1.navigation.value = "<%=nav%>";
		document.form1.arah.value="kiri";
		document.form1.method = "POST";
		document.form1.submit();
	}
	function goleftbgt(){
		document.form1.navigation.value = "<%=nav%>";
		document.form1.arah.value="kiribgt";
		document.form1.method = "POST";
		document.form1.submit();
	}
	function goright(){
		document.form1.navigation.value = "<%=nav%>";
		document.form1.arah.value="kanan";
		document.form1.method = "POST";
		//alert(document.form1.arah.value);
		document.form1.submit();
	}
	function gorightbgt(){
		document.form1.navigation.value = "<%=nav%>";
		document.form1.arah.value="kananbgt";
		document.form1.method = "POST";
		document.form1.submit();
	}
	function go(){
		document.form1.navigation.value = "<%=nav%>";
		document.form1.submit();
	}
</script>