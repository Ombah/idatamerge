<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="com.ametis.cms.util.WebUtil"%>
<%@ taglib prefix="spring" uri="/WEB-INF/spring.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>


<%
	String navigation = WebUtil.getAttributeString(request, "navigation", "");
%>

<!-- Page Title Start // Should be put on <Title> tag-->

<!-- Page Title Stop-->
<%
	String alert = (String) request.getAttribute("alert");
	int index = 0;
	int totalIndex = 0;
	int count = 0;
	int countSet = 0;

	try {
		index = ((Integer) request.getAttribute("index")).intValue();
		count = ((Integer) request.getAttribute("count")).intValue();
		countSet = ((Integer) request.getAttribute("countSet")).intValue();
		totalIndex = ((Integer) request.getAttribute("halAkhir")).intValue();

	} catch (Exception e) {
	}

	if (alert != null && !alert.trim().equals("")) {
%>
<div id="warning" align="center">
	<c:out value="${alert}"></c:out>
</div>
<%
	}
%>

<%
	String rowclass = "";
	int i = 0;
	int indexint = Integer.parseInt(request.getAttribute("index").toString());
	WebUtil.getAttributeInteger(request, "index", 0).intValue();
%>
<!-- Search Container Start -->

<form name="form1" action="consultation" method="POST">
	 <input type="hidden" name="navigation" value="gosearch">
	 <input type="hidden" name="arah" value=""> 
	 <input type="hidden" name="id" value="">	 
	<input type="hidden" name="index" value="<c:out value="${index}" />">
	 <input type="hidden" name="consultationId" value="">
	 
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody>
			<tr>
				<td nowrap="nowrap">
					<h3>
						<img src="images/h3Arrow.gif" border="0"> &nbsp;Search Consultation
					</h3>
				</td>
				<td width="100%"><img src="images/blank.gif" height="1"	width="1"></td>
			</tr>
		</tbody>
	</table>
	<table class="tabForm" border="0" cellpadding="0" cellspacing="0"
		width="100%">
		<tbody>
			<tr>
				<td><table border="0" cellpadding="0" cellspacing="0"
						width="100%">
						<tbody>
							<tr>
								<form></form>
								<td class="dataLabel" nowrap="nowrap">Search Keyword:
									&nbsp;&nbsp; <input size="20" name="searchtext"
									class="dataField" value="<c:out value="${searchtext}"/>"
									type="text">
								</td>
								<td class="dataLabel" nowrap="nowrap">Search Category:
									&nbsp;&nbsp; <select name="searchby" class="inputbox">
										<option value="title"<c:if test="${searchby eq \"title\"}">selected="true"</c:if>>Title</option>
										<option value="customerFullName"<c:if test="${searchby eq \"customerFullName\"}">selected="true"</c:if>>Customer Full Name</option>
								</select>

								</td>
								<td class="dataLabel" nowrap="nowrap">Status: &nbsp;&nbsp;

									<select name="status">
										<option value="-1">-- All Status --</option>
										<option value="1">Read</option>
										<option value="0">Not Read</option>
								</select>

								</td>
								<td class="dataLabel"><input title="Search [Alt+Shift+Q]"
									accesskey="Q"
									class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus"
									name="button" value="Search" type="submit"></td>
							</tr>
						</tbody>
					</table></td>
			</tr>
		</tbody>
	</table>
	<div class="table_container">
		<!-- Table Toolbar Start -->
		<%
			String nampak = "";
			if (navigation != null && navigation.equals("gosearch")) {
			} else {
				nampak = " style=\"display: none;\"";
			}
		%>

	</div>
	<br />

	<table class="listView" cellspacing="0" cellpadding="0" width="100%">
		<tbody>
		<tr>

				<td class="listViewPaginationTdS1" align="left"></td>
            	<td class="listViewPaginationTdS1" align="right" nowrap="nowrap" colspan=20>

					<%if (index == 1) {

			%>
					<img src="images/start_off.gif" alt="Start" align="absmiddle" border="0" height="10" width="9">
					Start&nbsp;

					<img src="images/previous_off.gif" alt="Previous" align="absmiddle" border="0" height="10" width="4">
					Previous&nbsp;&nbsp;
					<%} else if ((index - 1) > 0) {

			%>
					<img src="images/start.gif" alt="Start" align="absmiddle" border="0" height="10" width="9">
					<a href="javascript:goleftbgt()" class="listViewPaginationLinkS1"> Start&nbsp; </a>

					<img src="images/previous.gif" alt="Previous" align="absmiddle" border="0" height="10" width="4">
					<a href="javascript:goleft()" class="listViewPaginationLinkS1"> Previous&nbsp;&nbsp; </a>
					<%}

			%>
					<span class="pageNumbers">(<c:out value="${minIndex}" /> - <c:out value="${maxIndex}" /> of <c:out value="${count}" />)</span>&nbsp;&nbsp;

					<%if (totalIndex > index) {

			%>

					<a href="javascript:goright()" class="listViewPaginationLinkS1">Next&nbsp; <img src="images/next.gif" alt="Next" align="absmiddle" border="0" height="10" width="4"> </a>&nbsp;&nbsp; <a href="javascript:gorightbgt()" class="listViewPaginationLinkS1">End&nbsp;
						<img src="images/end.gif" alt="End" align="absmiddle" border="0" height="10" width="9"> </a>
					<%} else {

			%>
					Next&nbsp;
					<img src="images/next_off.gif" alt="Next" align="absmiddle" border="0" height="10" width="4">
					&nbsp;&nbsp; End&nbsp;
					<img src="images/end_off.gif" alt="End" align="absmiddle" border="0" height="10" width="9">
					<%}

			%>
				</td>
			</tr>
			<tr height="20">
				<td width="6%" nowrap="nowrap" class="listViewThS1" scope="col">
					<img src="images/blank.gif" alt="asd" height="1" width="1">
					No.
				</td>

				<!-- ini default generated table from database -->

				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Title &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Customer Number &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Customer Full Name &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Username &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Type &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Created By &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Created Time &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Modified By &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Modified Time &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Image &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Status &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Action &nbsp;</td>
			</tr>


			<c:forEach items="${Consultations}" var="consultation" varStatus="status">
				<%
					if (i % 2 == 0) {
							rowclass = "col1";
						} else if (i % 2 != 0) {
							rowclass = "col2";
						}
						i++;
				%>
				<c:if test="${consultation.status==0}">
						<tr onMouseOver="" onMouseOut="" onMouseDown="" height="20" >
						<td class="oddListRowS1StatusUnread" align="center" bgcolor="#e7f0fe"
							nowrap="nowrap" valign="top"><%=(i + ((indexint - 1) * countSet))%>
						</td>
						
						<td class="oddListRowS1StatusUnread" bgcolor="#e7f0fe" nowrap="nowrap"
							valign="top"><a href="javascript:detil('<c:out value="${consultation.consultationId}" />')"><c:out
								value="${consultation.title}" /></a></td>
								<td class="oddListRowS1StatusUnread" bgcolor="#e7f0fe" nowrap="nowrap"
							valign="top"><c:out
								value="${consultation.memberId.customerNumber}" /></td>
								<td class="oddListRowS1StatusUnread" bgcolor="#e7f0fe" nowrap="nowrap"
							valign="top"><c:out
								value="${consultation.memberId.fullName}" /></td>
						<td class="oddListRowS1StatusUnread" bgcolor="#e7f0fe" nowrap="nowrap"
							valign="top"><c:out
								value="${consultation.userId.username}" /></td>
						<td class="oddListRowS1StatusUnread" bgcolor="#e7f0fe" nowrap="nowrap"
							valign="top">
							<c:if test="${consultation.type==0}">

							Claim

							</c:if>
	
							<c:if test="${consultation.type==1}">
	
								Complaint
	
							</c:if>
							<c:if test="${consultation.type==2}">
	
								Question	
							</c:if>
							
							</td>
						<td class="oddListRowS1StatusUnread" bgcolor="#e7f0fe" nowrap="nowrap"
							valign="top"><c:out
								value="${consultation.createdBy}" /></td>
						<td class="oddListRowS1StatusUnread" bgcolor="#e7f0fe" nowrap="nowrap"
							valign="top"><c:out	value="${consultation.createdTime}" /></td>
						<td class="oddListRowS1StatusUnread" bgcolor="#e7f0fe" nowrap="nowrap"
							valign="top"><c:out value="${consultation.modifiedBy}" /></td>
						<td class="oddListRowS1StatusUnread" bgcolor="#e7f0fe" nowrap="nowrap"
							valign="top"><c:out	value="${consultation.modifiedTime}" /></td>
							<td class="oddListRowS1StatusUnread" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${consultation.originalDocName}" />
					<a href="javascript:detilGambar('<c:out value="${consultation.documentId.documentId}" />')">
								<img src="images/view.gif" class="action_icon" alt="View" title="View">
					</td>
							<td class="oddListRowS1StatusUnread" bgcolor="#e7f0fe" nowrap="nowrap" valign="top">
	
							
	
								MESSAGE NOT READ
	
							</td>
	
						<td class="oddListRowS1StatusUnread" bgcolor="#e7f0fe" nowrap="nowrap"
							valign="top">
					<a href="javascript:detil('<c:out value="${consultation.consultationId}" />')">
					<img src="images/view.gif" class="action_icon" alt="View" title="View"></a>
					</td>
					</tr>
	
					<tr>
						<td colspan="20" class="listViewHRS1"></td>
					</tr>
				</c:if>
				<c:if test="${consultation.status!=0}">
				
				<tr onMouseOver="" onMouseOut="" onMouseDown="" height="20" >
					<td class="oddListRowS1" align="center" bgcolor="#e7f0fe"
						nowrap="nowrap" valign="top"><%=(i + ((indexint - 1) * countSet))%>
					</td>
					
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><a style="color:#000000" href="javascript:detil('<c:out value="${consultation.consultationId}" />')"><c:out
							value="${consultation.title}" /></a></td>
							<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out
							value="${consultation.memberId.customerNumber}" /></td>
							<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out
							value="${consultation.memberId.fullName}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out
							value="${consultation.userId.username}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top">
						<c:if test="${consultation.type==0}">

							Claim

							</c:if>
	
							<c:if test="${consultation.type==1}">
	
								Complaint
	
							</c:if>
							<c:if test="${consultation.type==2}">
	
								Question	
							</c:if>
						</td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out
							value="${consultation.createdBy}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out	value="${consultation.createdTime}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out value="${consultation.modifiedBy}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out	value="${consultation.modifiedTime}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${consultation.originalDocName}" />
					<a href="javascript:detilGambar('<c:out value="${consultation.documentId.documentId}" />')">
								<img src="images/view.gif" class="action_icon" alt="View" title="View">
					</td>
						<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top">

						<c:if test="${consultation.status==1}">

							MESSAGE READ

						</c:if>

						<c:if test="${consultation.status==0}">

							MESSAGE NOT READ

						</c:if>
						<c:if test="${consultation.status==2}">

							MESSAGE SEND BY ${consultation.modifiedBy}

						</c:if>
						</td>

					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top">
				<a href="javascript:detil('<c:out value="${consultation.consultationId}" />')">
				<img src="images/view.gif" class="action_icon" alt="View" title="View"></a>
				</td>
				</tr>

				<tr>
					<td colspan="20" class="listViewHRS1"></td>
				</tr>
			</c:if>

			</c:forEach>
			<tr>
				<td class="listViewPaginationTdS1" align="left"></td>
            	<td class="listViewPaginationTdS1" align="right" nowrap="nowrap" colspan=20>

					<%if (index == 1) {

			%>
					<img src="images/start_off.gif" alt="Start" align="absmiddle" border="0" height="10" width="9">
					Start&nbsp;

					<img src="images/previous_off.gif" alt="Previous" align="absmiddle" border="0" height="10" width="4">
					Previous&nbsp;&nbsp;
					<%} else if ((index - 1) > 0) {

			%>
					<img src="images/start.gif" alt="Start" align="absmiddle" border="0" height="10" width="9">
					<a href="javascript:goleftbgt()" class="listViewPaginationLinkS1"> Start&nbsp; </a>

					<img src="images/previous.gif" alt="Previous" align="absmiddle" border="0" height="10" width="4">
					<a href="javascript:goleft()" class="listViewPaginationLinkS1"> Previous&nbsp;&nbsp; </a>
					<%}

			%>
					<span class="pageNumbers">(<c:out value="${minIndex}" /> - <c:out value="${maxIndex}" /> of <c:out value="${count}" />)</span>&nbsp;&nbsp;

					<%if (totalIndex > index) {

			%>

					<a href="javascript:goright()" class="listViewPaginationLinkS1">Next&nbsp; <img src="images/next.gif" alt="Next" align="absmiddle" border="0" height="10" width="4"> </a>&nbsp;&nbsp; <a href="javascript:gorightbgt()" class="listViewPaginationLinkS1">End&nbsp;
						<img src="images/end.gif" alt="End" align="absmiddle" border="0" height="10" width="9"> </a>
					<%} else {

			%>
					Next&nbsp;
					<img src="images/next_off.gif" alt="Next" align="absmiddle" border="0" height="10" width="4">
					&nbsp;&nbsp; End&nbsp;
					<img src="images/end_off.gif" alt="End" align="absmiddle" border="0" height="10" width="9">
					<%}

			%>
				</td>
			</tr>
			
		</tbody>
	</table>
	<input title="Add Message" accesskey="S" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" onClick="javascript:addMessage()" name="AddInfo" value=" Add Message" type="Button">
	
</form>

<!-- Table Container Stop -->
<script language="Javascript">

<%
String nav="";
if(request.getAttribute("navigation").equals("gosearch")||request.getAttribute("navigation").equals("golookup")){
	nav = (String)request.getAttribute("navigation");
}
%>
function addMessage(){
	document.form1.navigation.value = "tambah";
	document.form1.action = "consultation-form";
	document.form1.method = "GET";
	document.form1.submit();
}
function detil(consultationId){
	document.form1.method = "GET";
	document.form1.navigation.value="detail";
	document.form1.action="consultationdetail-form";
	document.form1.id.value=(consultationId);
	document.form1.consultationId.value=(consultationId);
	document.form1.submit();
}

function goleft(){
	document.form1.navigation.value = "<%=nav%>";
	document.form1.arah.value="kiri";
	//document.form1.searchtext.value="<c:out value="${searchtext}" />";
	//document.form1.searchby.value="<c:out value="${searchby}" />";
	document.form1.action="consultation";
	document.form1.method = "POST";
	document.form1.submit();
}
function goleftbgt(){
	document.form1.navigation.value = "<%=nav%>";
	document.form1.arah.value="kiribgt";
	//document.form1.searchtext.value="<c:out value="${searchtext}" />";
	//document.form1.searchby.value="<c:out value="${searchby}" />";
	document.form1.action="consultation";
	document.form1.method = "POST";
	document.form1.submit();
}
function goright(){
	document.form1.navigation.value = "<%=nav%>";
	document.form1.arah.value="kanan";
//	document.form1.searchtext.value="<c:out value="${searchtext}" />";
	//document.form1.searchby.value="<c:out value="${searchby}" />";
	document.form1.action="consultation";
	document.form1.method = "POST";
	document.form1.submit();
}
function gorightbgt(){
	document.form1.navigation.value = "<%=nav%>";
	document.form1.arah.value="kananbgt";
	//document.form1.searchtext.value="<c:out value="${searchtext}" />";
	//document.form1.searchby.value="<c:out value="${searchby}" />";
	document.form1.action="consultation";
	document.form1.method = "POST";
	document.form1.submit();
}

function detilGambar(idx) {
	window
			.open(
					"document?navigation=preview&documentId=" + idx
							+ "&url=searchTheme",
					"Search",
					"width=1024, height=768, menubar=yes, status=no, toolbar=no, scrollbars=yes, resizable=yes");

}

</script>
