<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="/WEB-INF/spring.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>


<link rel="stylesheet" type="text/css" href="css/autocomplete.css">
<link rel="stylesheet" type="text/css" href="css/button.css">

<link rel="stylesheet" type="text/css" href="css/jquery/autocomplete/jquery.autocomplete.css"/>

<script type="text/javascript" src="scripts/jquery/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="scripts/jquery/plugin/jquery.autocomplete.pack.js"></script>


<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
     	<c:choose>
		<c:when test="${navigation eq 'tambah'}">
      		<td nowrap="nowrap"><h3><img src="images/h3Arrow.gif"  border="0">&nbsp;Create Consultation</h3></td>
      	</c:when>
		<c:when test="${navigation eq 'editConsultation'}">
      		<td nowrap="nowrap"><h3><img src="images/h3Arrow.gif"  border="0">&nbsp;Edit Consultation</h3></td>
      	</c:when>
      </c:choose>
      <td width="100%"><img src="images/blank.gif"  height="1" width="1"></td>
    </tr>
  </tbody>
</table>

<form action="consultation-form" method="POST"  name="form1" id="form_layout" enctype="multipart/form-data">
<input type="hidden" name="navigation" value="${navigation }">

<%
String alert = (String) request.getAttribute("alert");
if (alert != null && !alert.trim().equals("")) {%>
	<div id="warning">
		<c:out value="${alert}"></c:out>
	</div>
	<%}%>


<table class="tabForm" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tbody>
						<spring:bind path="consultationForm.id">
							<input type="hidden" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}" />" >
							<div id="fredcaption">
								<c:out value="${status.errorMessage}" />
							</div>
						</spring:bind>
						<tr style="display:none">	  
							<td class="dataLabel"> Member : </td>
							<td class="dataField">
								<spring:bind path="consultationForm.memberName">
										<input type="text" size="35" id="memberName" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}" />" />
										<div id="fredcaption">
											<c:out value="${status.errorMessage}" />
										</div>
										<input  type="hidden" id="memberId" name="memberId" value="" />
									</spring:bind>
							</td>	
						</tr>
						<tr>
							<td class="dataLabel"> Username receiver: </td>
							<td class="dataField">
								<input type="text" size="35" id="username" name="username" value="" />
										<div id="fredcaption">
										</div>
										<input type="hidden" id="userId" name="userId" value="" />
							</td> 		
						</tr>
						<tr>
							<td class="dataLabel">Title : </td>		
							<td class="dataField">
								<spring:bind path="consultationForm.title">
									<input type="text" name="<c:out value="${status.expression}" />" value="<c:out value="${status.value}" />" id="title">
								</spring:bind>
							</td>
						</tr>
						<tr>
						    <td class="dataLabel">Question : </td>		
							<td class="dataField">
								<spring:bind path="consultationForm.message">
									<textarea rows="10" cols="200" id="question" name="<c:out value="${status.expression}" />" ><c:out value="${status.value}" /></textarea>
									<br><br>
								</spring:bind>								
							</td>
						</tr>
						<tr>
						    <td class="dataLabel">type : </td>		
							<td class="dataField">
								<spring:bind path="consultationForm.type">
									 <select name="<c:out value="${status.expression}"/>">
										  <option value="notification">Notification</option>
										  <option value="info">Info</option>
										  <option value="message">Message</option>
										</select> 
								</spring:bind>								
							</td>
						</tr>
						<tr>
								<td class="dataLabel">Image :</td>
								<td class="dataField">
									<spring:bind path="consultationForm.file">
										<input type="file"	name="<c:out value="${status.expression}" />" accept="image/jpg,image/png,image/jpeg">
									</spring:bind>
								</td>
						</tr>
								
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
<input title="Save Consultation" accesskey="S" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" onClick="javascript:saveConsultation()" name="SaveConsultation" value=" Save Consultation " type="button">
<input title="Cancel" accesskey="S" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" onClick="javascript:cancel()" name="cancel" value=" Cancel " type="button">
</form>

<script language="javascript">
	
function saveConsultation (){
	document.form1.method = "POST";
	document.form1.action = "consultation-form";
	document.form1.submit();
}
function cancel (){
	window.location.reload();
}	

var temp=false;
$(document).ready(function() {
	$("#memberName").autocomplete("member?navigation=lookupjson", {
		max : 7,
		dataType : "json",
		parse : function(data) {
			return $.map(data, function(row) {
				return {
					data : row,
					value : row.name,
					result : row.name
				}
			});
		},
		formatItem : function(row) {
			return "<font color='#000' >" + row.name + "</font>";
		}
	}).bind("result", function(data, value) {
		$(this).parents("dd").find(".error_message").hide();
		//include the countryId for filtering
		//alert(value.toSource());
		$("#memberId").val(value.id);
		temp=false;
//		alert(value.id);
	});
	
	$("#username").autocomplete("user?navigation=lookupjson", {
		max : 7,
		dataType : "json",
		parse : function(data) {
			return $.map(data, function(row) {
				return {
					data : row,
					value : row.name,
					result : row.name
				}
			});
		},
		formatItem : function(row) {
			return "<font color='#000' >" + row.name + "</font>";
		}
	}).bind("result", function(data, value) {
		$(this).parents("dd").find(".error_message").hide();
		//include the countryId for filtering
		//alert(value.toSource());
		$("#userId").val(value.id);
		temp=false;
		//alert(value.id);
	});
});



</script>