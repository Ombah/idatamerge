<%@page import="java.util.Collection"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="com.ametis.cms.util.WebUtil"%>
<%@ taglib prefix="spring" uri="/WEB-INF/spring.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>

<%
	String rowclass = "";
	int i = 0;
	int indexint = 0;
	WebUtil.getAttributeInteger(request, "index", 0).intValue();
%>

<!-- Search Container Start -->

<form name="form1" action="settingrulesproduct" method="POST"
	enctype="multipart/form-data">
	<input type="hidden" name="navigation">
	<input type="hidden" name="productId">
	 <input type="hidden"name="index" value="<c:out value="${index}" />"> 

	<%
		Collection Products = (Collection) request.getAttribute("Products");
	%>

	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody>
			<tr>
				<td nowrap="nowrap">
					<h3>
						<img src="images/h3Arrow.gif" border="0">&nbsp;Pilih Product
					</h3>
				</td>
				<td width="100%"><img src="images/blank.gif" height="1"
					width="1"></td>
			</tr>
		</tbody>
	</table>
	<table id="myTable" class="listView" width="100%" cellspacing="0" cellpadding="0">
		<tbody>
			
			<tr height="20">
				<td width="2%" nowrap="nowrap" class="listViewThS1" scope="col">No</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Product Code</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">ID	Product</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Product Name</td>
				
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Action</td>
			</tr>

			<c:forEach items="${Products}" var="product" varStatus="status">
				<%
					if (i % 2 == 0) {
							rowclass = "col1";
						} else if (i % 2 != 0) {
							rowclass = "col2";
						}
						i++;
				%>
				<tr height="20">
					<td class="oddListRowS1" align="center" bgcolor="#e7f0fe"
						nowrap="nowrap" valign="top">
						<%-- <%=(i + ((indexint - 1) * countSet))--%>
						 <c:out	value="${status.count}" />
					</td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out value="${product.productCode}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out value="${product.productId}" />
					</td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out value="${product.productName}" /></td>
					
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
							valign="top"><input type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" value="Setting" onclick="javascript:setting('<c:out value="${product.productId}" />')" ></td>
				</tr>

				<tr>
					<td colspan="20" class="listViewHRS1"></td>
				</tr>

			</c:forEach>

			
		</tbody>
	</table>
</form>

<script language="Javascript">
function setting(productId){
	alert("setting"+productId);
	document.form1.action="settingrulesproduct";
	document.form1.navigation.value="gosetting";
	document.form1.productId.value=productId;
	document.form1.submit();
	alert("submit done");
}
</script>