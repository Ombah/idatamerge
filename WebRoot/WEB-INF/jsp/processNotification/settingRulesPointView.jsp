<%@page import="java.util.Collection"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="com.ametis.cms.util.WebUtil"%>
<%@ taglib prefix="spring" uri="/WEB-INF/spring.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>

<!-- Search Container Start -->

<form name="form1" action="settingrulespoint" method="POST"
	enctype="multipart/form-data">
	<input type="hidden" name="navigation">
	<input type="hidden" name="productId" value="<c:out value="${ProductId}" />">
	<input type="hidden" name="arrEmailMember">
	<input type="hidden" name="arrEmailClient">
	<input type="hidden" name="arrEmailProvider">
	<input type="hidden" name="arrSmsMember">
	 <input type="hidden"name="index" value="<c:out value="${index}" />"> 

	<%
		Collection Points = (Collection) request.getAttribute("Points");
	%>

	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody>
			<tr>
				<td nowrap="nowrap">
					<h3>
						<img src="images/h3Arrow.gif" border="0">&nbsp;Client ${ClientName}<br>
						<img src="images/h3Arrow.gif" border="0">&nbsp;Policy Number ${PolicyNumber} <br>
						<img src="images/h3Arrow.gif" border="0">&nbsp;Setting Notifikasi Untuk Produk ${ProductName}
					</h3>
				</td>
				<td width="100%"><img src="images/blank.gif" height="1"
					width="1"></td>
			</tr>
		</tbody>
	</table>
	<table id="myTable" class="listView" width="100%" cellspacing="0" cellpadding="0">
		<tbody>
			
			<tr height="20">
				<td width="2%" nowrap="nowrap" class="listViewThS1" scope="col">No.</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Process Id</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Email Member</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Email Client</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Email Provider</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">SMS Member</td>
				
				
			</tr>

			<c:forEach items="${Points}" var="point" varStatus="status">

				<tr height="20">
					<td class="oddListRowS1" align="center" bgcolor="#e7f0fe"
						nowrap="nowrap" valign="top">
						 <c:out	value="${status.count}" />
					</td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out value="${point.processId}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top">
						<c:set var="isMemberEmail" scope="session" value="${point.isMemberEmail}"/>
						<c:if test="${isMemberEmail==1}">
							<input class="cbMemberEmail" type="checkbox" checked value="${point.processId}">
						</c:if>
						<c:if test="${(isMemberEmail==0) || (isMemberEmail=='')}">
							<input class="cbMemberEmail" type="checkbox" value="${point.processId}">
						</c:if>
					</td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top">
						<c:set var="isClientEmail" scope="session" value="${point.isClientEmail}"/>
						<c:if test="${isClientEmail==1}">
							<input class="cbClientEmail" type="checkbox" checked value="${point.processId}">
						</c:if>
						<c:if test="${(isClientEmail==0) || (isClientEmail=='')}" >
							<input class="cbClientEmail" type="checkbox" value="${point.processId}">
						</c:if>
						</td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top">
						<c:set var="isProviderEmail" scope="session" value="${point.isProviderEmail}"/>
						<c:if test="${isProviderEmail==1}">
							<input class="cbProviderEmail" type="checkbox" checked value="${point.processId}">
						</c:if>
						<c:if test="${(isProviderEmail==0) || (isProviderEmail=='')}">
							<input class="cbProviderEmail" type="checkbox"  value="${point.processId}">
						</c:if></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top">
						<c:set var="isMemberSms" scope="session" value="${point.isMemberSms}"/>
						<c:if test="${isMemberSms==1}">
							<input class="cbMemberSms" type="checkbox" checked  value="${point.processId}">
						</c:if>
						<c:if test="${(isMemberSms==0) || (isMemberSms=='')}">
							<input class="cbMemberSms" type="checkbox" value="${point.processId}">
						</c:if></td>
				</tr>
				<tr>
					<td colspan="20" class="listViewHRS1"></td>
				</tr>

			</c:forEach>

			
		</tbody>
	</table>
	<table class="tabForm" border="0" cellpadding="0" cellspacing="0"
		width="100%">
		<tbody>
			<tr id="trRow">
				<td class="dataLabel" nowrap="nowrap">
					<input title="Save"	accesskey="C" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus"
					onClick="javascript:save();" name="saveButton" value=" Save "	type="button">
					&nbsp;&nbsp;
					<input title="Cancel" accesskey="C" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus"
					onClick="javascript:cancel();" name="cancelButton" value=" Cancel " type="button">
				</td>
			</tr>
		</tbody>
	</table>
</form>

<script language="Javascript">
function setting(productId){
	document.form1.action="settingrulespoint";
	document.form1.navigation.value="gosetting";
	document.form1.productId.value=productId;
	document.form1.submit();
	
}

function cancel(){
	window.location.reload();
}

function save(){
	var rawEmailMember = document.getElementsByClassName("cbMemberEmail");
	var rawEmailClient = document.getElementsByClassName("cbClientEmail");
	var rawEmailProvider = document.getElementsByClassName("cbProviderEmail");
	var rawSmsMember = document.getElementsByClassName("cbMemberSms");
	
	var arrEmailMember= "";
	var arrEmailClient= "";
	var arrEmailProvider = "";
	var arrSmsMember = "";
	
	for(i=0;i<rawEmailMember.length;i++){
		arrEmailMember+=rawEmailMember[i].value+"_"+rawEmailMember[i].checked+"--";
		arrEmailClient+=rawEmailClient[i].value+"_"+rawEmailClient[i].checked+"--";
		arrEmailProvider+=rawEmailProvider[i].value+"_"+rawEmailProvider[i].checked+"--";
		arrSmsMember+=rawSmsMember[i].value+"_"+rawSmsMember[i].checked+"--";
	}
	
	document.form1.arrEmailMember.value=arrEmailMember;
	document.form1.arrEmailClient.value=arrEmailClient;
	document.form1.arrEmailProvider.value=arrEmailProvider;
	document.form1.arrSmsMember.value=arrSmsMember;
	document.form1.action="settingrulespoint";
	document.form1.navigation.value="gosave";
	document.form1.submit();
}
</script>
