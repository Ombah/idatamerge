<%@page import="java.util.Collection"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="com.ametis.cms.util.WebUtil"%>
<%@ taglib prefix="spring" uri="/WEB-INF/spring.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>

<%
	String rowclass = "";
	int i = 0;
	int indexint = 0;
	WebUtil.getAttributeInteger(request, "index", 0).intValue();
%>

<!-- Search Container Start -->

<form name="form1" action="settingrulesproduct" method="POST"
	enctype="multipart/form-data">
	<input type="hidden" name="navigation"> 
	<input type="hidden"name="policyId">
	<input type="hidden"name="clientName" value="<c:out value="${ClientName}"/>"> 
		<input type="hidden" name="index" value="<c:out value="${index}" />">

	<%
		Collection Policies = (Collection) request.getAttribute("Policies");
	%>

	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody>
			<tr>
				<td nowrap="nowrap">
					<h3>
						<img src="images/h3Arrow.gif" border="0">&nbsp;Setting
						Notifikasi Untuk Client ${ClientName}
					</h3>
				</td>
				<td width="100%"><img src="images/blank.gif" height="1"
					width="1"></td>
			</tr>
		</tbody>
	</table>
	<table id="myTable" class="listView" width="100%" cellspacing="0"
		cellpadding="0">
		<tbody>

			<tr height="20">
				<td width="2%" nowrap="nowrap" class="listViewThS1" scope="col">No.</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Policy
					Number</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Policy
					Id</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Email
					Member</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Email
					Client</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Email
					Provider</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">SMS
					Member</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Action</td>
			</tr>

			<c:forEach items="${Policies}" var="policy" varStatus="status">
				<%
					if (i % 2 == 0) {
							rowclass = "col1";
						} else if (i % 2 != 0) {
							rowclass = "col2";
						}
						i++;
				%>
				<tr height="20">
					<td class="oddListRowS1" align="center" bgcolor="#e7f0fe"
						nowrap="nowrap" valign="top">
						<%-- <%=(i + ((indexint - 1) * countSet))--%> <c:out
							value="${status.count}" />
					</td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out value="${policy.policyNumber}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out value="${policy.policyId}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:set var="isMemberEmail" scope="session"
							value="${policy.isMemberEmail}" /> <c:if
							test="${isMemberEmail==1}">
							<input type="checkbox" checked>
						</c:if> <c:if test="${(isMemberEmail==0) || (isMemberEmail=='')}">
							<input type="checkbox">
						</c:if></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:set var="isClientEmail" scope="session"
							value="${policy.isClientEmail}" /> <c:if
							test="${isClientEmail==1}">
							<input type="checkbox" checked>
						</c:if> <c:if test="${(isClientEmail==0) || (isClientEmail=='')}">
							<input type="checkbox">
						</c:if></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:set var="isProviderEmail" scope="session"
							value="${policy.isProviderEmail}" /> <c:if
							test="${isProviderEmail==1}">
							<input type="checkbox" checked>
						</c:if> <c:if test="${(isProviderEmail==0) || (isProviderEmail=='')}">
							<input type="checkbox">
						</c:if></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:set var="isMemberSms" scope="session"
							value="${policy.isMemberSms}" /> <c:if test="${isMemberSms==1}">
							<input type="checkbox" checked>
						</c:if> <c:if test="${(isMemberSms==0) || (isMemberSms=='')}">
							<input type="checkbox">
						</c:if></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><input type="button"
						class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus"
						value="See All Product"
						onclick="javascript:setting('<c:out value="${policy.policyId}" />')">
					</td>
				</tr>

				<tr>
					<td colspan="20" class="listViewHRS1"></td>
				</tr>

			</c:forEach>


		</tbody>
	</table>
	<table class="tabForm" border="0" cellpadding="0" cellspacing="0"
		width="100%">
		<tbody>
			<tr id="trRow">
				<td class="dataLabel" nowrap="nowrap"><input title="Save"
					accesskey="C"
					class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus"
					onClick="javascript:cari();" name="saveButton" value=" Save "
					type="button"> &nbsp;&nbsp; <input title="Cancel"
					accesskey="C"
					class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus"
					onClick="javascript:cari();" name="cancelButton" value=" Cancel "
					type="button"></td>
			</tr>
		</tbody>
	</table>
</form>

<script language="Javascript">
	function setting(policyId) {
		document.form1.action = "settingrulesproduct";
		document.form1.navigation.value = "gosetting";
		document.form1.policyId.value = policyId;
		document.form1.submit();
		
	}
</script>