<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="spring" uri="/WEB-INF/spring.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>


<link rel="stylesheet" type="text/css" href="css/autocomplete.css">
<link rel="stylesheet" type="text/css" href="css/button.css">

<link rel="stylesheet" type="text/css"
	href="css/jquery/autocomplete/jquery.autocomplete.css" />

<script type="text/javascript" src="scripts/jquery/jquery-1.4.4.min.js"></script>
<script type="text/javascript"
	src="scripts/jquery/plugin/jquery.autocomplete.pack.js"></script>


<form action="info-form" method="POST"  name="form1" id="form_layout" enctype="multipart/form-data" >
<input type="hidden" name="navigation" value="${navigation }">

<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<c:choose>
				<c:when test="${navigation eq 'tambah'}">
					<td nowrap="nowrap"><h3>
							<img src="images/h3Arrow.gif" border="0">&nbsp;Create Info
						</h3></td>
				</c:when>
				<c:when test="${navigation eq 'editInfo'}">
					<td nowrap="nowrap"><h3>
							<img src="images/h3Arrow.gif" border="0">&nbsp;Edit Info
						</h3></td>
				</c:when>
			</c:choose>
			<td width="100%"><img src="images/blank.gif" height="1"
				width="1"></td>
		</tr>
	</tbody>
</table>


	<%
		String alert = (String) request.getAttribute("alert");
		if (alert != null && !alert.trim().equals("")) {
	%>
	<div id="warning">
		<c:out value="${alert}"></c:out>
	</div>
	<%}%>


<table class="tabForm" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tbody>
						<spring:bind path="infoForm.id">
							<input type="hidden" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}" />" >
							<div id="fredcaption">
								<c:out value="${status.errorMessage}" />
							</div>
						</spring:bind>
						<tr>
							<td class="dataLabel">Title : </td>		
							<td class="dataField">
								<spring:bind path="infoForm.title">
									<input type="text" name="<c:out value="${status.expression}" />" value="<c:out value="${status.value}" />" id="title">
								</spring:bind>
							</td>
						</tr>
						<tr>	  
							<td class="dataLabel"> Content : </td>
							<td class="dataField">
							<spring:bind path="infoForm.content">
								<textarea rows="10" cols="200" id="content" name="<c:out value="${status.expression}" />" ><c:out value="${status.value}" /></textarea>
									<div id="fredcaption">
										<c:out value="${status.errorMessage}" />
									</div>
								</spring:bind>
							</td>	 		
						</tr>		
								<tr>
								<td class="dataLabel">Expired Date :</td>
								<td class="dataField">
								<spring:bind path="infoForm.endDate">
										<INPUT type="text" size="10" name="<c:out value="${status.expression}" />" value="<c:out value="${status.value}" />" id="endDate" maxlength="19">
										
										<img src="images/jscalendar.gif" alt="Enter Date"
											id="<c:out value="${status.expression}"/>_trigger"
											align="absmiddle" height="20" width="20">
										<script type="text/javascript">
											Calendar
													.setup({
														inputField : "<c:out value="${status.expression}"/>", // id of the input field
														ifFormat : "%d-%m-%Y", // format of the input field
														button : "<c:out value="${status.expression}"/>_trigger", // trigger for the calendar (button ID)
														align : "Bl", // alignment (defaults to "Bl")
														singleClick : true
													});
										</script>
									</spring:bind>
								</td>
							</tr>	
						<tr>
								<td class="dataLabel">Image :</td>
								<td class="dataField">
									<spring:bind path="infoForm.file">
										<input type="file"	name="<c:out value="${status.expression}" />" accept="image/jpg,image/png,image/jpeg">
									</spring:bind>
								</td>
						</tr>
						<c:if test="${navigation eq 'editInfo'}">
							<tr>
							<td class="dataLabel">Current : </td>		
							<td class="dataField">
								<spring:bind path="infoForm.originalFilename">
									<c:out value="${status.value}" />
								</spring:bind>
								<a href="javascript:detil('<c:out value="${fileId}" />')">
								<img src="images/view.gif" class="action_icon" alt="View" title="View">
							</td>

						</tr>
						</c:if>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
<input title="Save Info" accesskey="S" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" onClick="javascript:saveInfo()" name="SaveInfo" value=" Save Info " type="button">
<input title="Cancel" accesskey="S" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" onClick="javascript:batal()" name="cancel" value=" Cancel " type="button">

	
</form>


<script language="javascript">
	function saveInfo() {
		var judulString = document.getElementById("title").value;
		var contentString = document.getElementById("content").value;
		if (judulString == "" || contentString == "") {
			alert(" Gagal Input , Judul dan Konten tidak boleh kosong ");
		} else {
			document.form1.method = "POST";
			document.form1.action = "info-form";
			document.form1.submit();
		}
	}
	
	function cancel() {
		window.location.reload();
	}

	function batal() {
		window.location.reload();
	}
	
	function detil(idx) {
		window
				.open(
						"document?navigation=preview&documentId=" + idx
								+ "&url=editInfo",
						"Search",
						"width=1024, height=768, menubar=yes, status=no, toolbar=no, scrollbars=yes, resizable=yes");

	}

	$(function() {
		$("input:file")
				.change(
						function() {
							var ftype = this.files[0].type;
							var allowed = true;
							switch (ftype) {
							case 'image/png':
							case 'image/jpeg':
							case 'image/pjpeg':
								break;
							default: {
								allowed = false;
								alert('Unsupported File! Only accept for PNG, JPG/JPEG');
								$(this)
										.replaceWith(
												$(this).val('').clone(true));
							}
							}
							if (allowed) {
								console.log('This file size is: '
										+ (this.files[0].size / 1024 / 1024)
												.toFixed(2) + " MB");
								if ((this.files[0].size / 1024 / 1024)
										.toFixed(2) > 1) { //file size limit 1MB
									allowed = false;
									alert('File size exceed 1MB limit!');
									$(this).replaceWith(
											$(this).val('').clone(true));
								}
							}
						});
	});
</script>