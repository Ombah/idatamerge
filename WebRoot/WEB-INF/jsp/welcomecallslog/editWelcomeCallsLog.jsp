<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="spring" uri="/WEB-INF/spring.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>


<link rel="stylesheet" type="text/css" href="css/autocomplete.css">
<link rel="stylesheet" type="text/css" href="css/button.css">

<link rel="stylesheet" type="text/css"
	href="css/jquery/autocomplete/jquery.autocomplete.css" />

<script type="text/javascript" src="scripts/jquery/jquery-1.4.4.min.js"></script>
<script type="text/javascript"
	src="scripts/jquery/plugin/jquery.autocomplete.pack.js"></script>


<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<c:choose>
				<c:when test="${navigation eq 'tambah'}">
					<td nowrap="nowrap"><h3>
							<img src="images/h3Arrow.gif" border="0">&nbsp;Create
							WelcomeCallsLog
						</h3></td>
				</c:when>
				<c:when test="${navigation eq 'editWelcomeCallsLog'}">
					<td nowrap="nowrap"><h3>
							<img src="images/h3Arrow.gif" border="0">&nbsp;Edit
							WelcomeCallsLog
						</h3></td>
				</c:when>
			</c:choose>
			<td width="100%"><img src="images/blank.gif" height="1"
				width="1"></td>
		</tr>
	</tbody>
</table>

<form action="welcomecallslog-form" method="POST" name="form1"	id="form_layout">
	<input type="hidden" name="navigation" value="${navigation }">
	<input type="hidden" name="refId" value="${welcomeCallsId }">
	<%
		String alert = (String) request.getAttribute("alert");
		if (alert != null && !alert.trim().equals("")) {
	%>
	<div id="warning">
		<c:out value="${alert}"></c:out>
	</div>
	<%
		}
	%>


	<table class="tabForm" border="0" cellpadding="0" cellspacing="0"
		width="100%">
		<tbody>
			<tr>
				<td>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tbody>
							<spring:bind path="welcomeCallsLogForm.id">
								<input type="hidden"
									name="<c:out value="${status.expression}"/>"
									value="<c:out value="${status.value}" />">
								<div id="fredcaption">
									<c:out value="${status.errorMessage}" />
								</div>
							</spring:bind>
							<tr>
								<td class="dataLabel">Call Time :</td>
								<td class="dataField">
								<spring:bind path="welcomeCallsLogForm.date">
										<INPUT type="text" size="10" name="<c:out value="${status.expression}" />" value="<c:out value="${status.value}" />" id="date" maxlength="19">
										
										<img src="images/jscalendar.gif" alt="Enter Date"
											id="<c:out value="${status.expression}"/>_trigger"
											align="absmiddle" height="20" width="20">
											
										<script type="text/javascript">
											Calendar
													.setup({
														inputField : "<c:out value="${status.expression}"/>", // id of the input field
														ifFormat : "%d-%m-%Y", // format of the input field
														button : "<c:out value="${status.expression}"/>_trigger", // trigger for the calendar (button ID)
														align : "Bl", // alignment (defaults to "Bl")
														singleClick : true
													});
										</script>
										
									</spring:bind>
									 <spring:bind path="welcomeCallsLogForm.hour">
										<select name="<c:out value="${status.expression}" />"
											tabindex="1">
											<c:forEach items="${hours}" var="h">
												<option value="<c:out value="${h}" />"
													<c:if test="${status.value eq h}">selected</c:if>><c:out
														value="${h}" /></option>
											</c:forEach>
										</select>
									</spring:bind> : <spring:bind path="welcomeCallsLogForm.minute">
										<select name="<c:out value="${status.expression}" />"
											tabindex="2">
											<c:forEach items="${minutes}" var="m">
												<option value="<c:out value="${m}" />"
													<c:if test="${status.value eq m}">selected</c:if>><c:out
														value="${m}" /></option>
											</c:forEach>
										</select>
									</spring:bind></td>
							</tr>
							<tr>
								<td class="dataLabel">Call Description :</td>
								<td class="dataField"><spring:bind
										path="welcomeCallsLogForm.callDescription">
										<textarea rows="10" cols="200" id="callDescription"
											name="<c:out value="${status.expression}" />"><c:out
												value="${status.value}" /></textarea>
										<div id="fredcaption">
											<c:out value="${status.errorMessage}" />
										</div>
									</spring:bind></td>
							</tr>
							<br>
							<tr>
								<td class="dataLabel">Call Category :</td>
								<td class="dataField"><spring:bind
										path="welcomeCallsLogForm.callCategory">
										<select name="<c:out value="${status.expression}"/>"tabindex="3">
											<c:forEach items="${Categories}" var="cc">
												<option value="<c:out value="${cc.id}"/>"
													<c:if test="${status.value eq cc.id}">selected</c:if>>
													<c:out value="${cc.welcomeCallCategoryName}" />
												</option>
											</c:forEach>
										</select>
										<div id="fredcaption">
											<c:out value="${status.errorMessage}" />
										</div>
									</spring:bind></td>
							</tr>
							<tr>
								<td class="dataLabel">Call Status :</td>
								<td class="dataField"><spring:bind path="welcomeCallsLogForm.statusCall">
										<select name="<c:out value="${status.expression}"/>"tabindex="3">
											<c:forEach items="${Statuses}" var="cc">
												<option value="<c:out value="${cc.id}"/>"
													<c:if test="${status.value eq cc.id}">selected</c:if>>
													<c:out value="${cc.welcomeCallStatusName}" />
												</option>
											</c:forEach>
										</select>
										<div id="fredcaption">
											<c:out value="${status.errorMessage}" />
										</div>
									</spring:bind></td>
							</tr>
							<tr>
								<td class="dataLabel">Followup Needed :</td>
								<td class="dataField"><spring:bind
										path="welcomeCallsLogForm.followupNeeded">
										<input type="checkbox" name="<c:out value="${status.expression}" />" value="1" <c:if test="${status.value eq 1}" >checked</c:if>>

										<div id="fredcaption">
											<c:out value="${status.errorMessage}" />
										</div>
									</spring:bind></td>
							</tr>
							<tr>
								<td class="dataLabel">Telephone Number : </td>		
								<td class="dataField">
										<input type="text" readonly="readonly" "telephoneNumber" value="<c:out value="${telephoneNumber}" />" id="telephoneNumber">
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
	<input title="Save WelcomeCallsLog" accesskey="S"
		class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus"
		onClick="javascript:saveWelcomeCallsLog()" name="SaveWelcomeCallsLog"
		value=" Save WelcomeCallsLog " type="submit"> 
		<input title="Cancel" accesskey="S"	class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" onClick="javascript:batal()" name="cancel" value=" Cancel "
		type="button">
</form>

<script language="javascript">
	function saveWelcomeCallsLog() {
		if(document.getElementById("date").value == ""){
			alert(" Gagal Entri , Tanggal harus di isi");
		}else{
			document.form1.method = "POST";
			document.form1.action = "welcomecallslog-form";
			document.form1.navigation.value="tambah";
			document.form1.submit();			
		}
	}
	function batal() {
		window.location.reload();
	}
</script>