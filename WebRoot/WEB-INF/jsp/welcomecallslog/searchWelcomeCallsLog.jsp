<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="com.ametis.cms.util.WebUtil"%>
<%@ taglib prefix="spring" uri="/WEB-INF/spring.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>


<%
	String navigation = WebUtil.getAttributeString(request, "navigation", "");
%>

<!-- Page Title Start // Should be put on <Title> tag-->

<!-- Page Title Stop-->
<%
	String alert = (String) request.getAttribute("alert");
	int index = 0;
	int totalIndex = 0;
	int count = 0;
	int countSet = 0;

	try {
		index = ((Integer) request.getAttribute("index")).intValue();
		count = ((Integer) request.getAttribute("count")).intValue();
		countSet = ((Integer) request.getAttribute("countSet")).intValue();
		totalIndex = ((Integer) request.getAttribute("halAkhir")).intValue();

	} catch (Exception e) {
	}

	if (alert != null && !alert.trim().equals("")) {
%>
<div id="warning" align="center">
	<c:out value="${alert}"></c:out>
</div>
<%
	}
%>

<%
	String rowclass = "";
	int i = 0;
	int indexint = Integer.parseInt(request.getAttribute("index").toString());
	WebUtil.getAttributeInteger(request, "index", 0).intValue();
%>
<!-- Search Container Start -->

<form name="form1" action="welcomecallslog" method="POST">
	<input type="hidden" name="navigation" value=""> 
	<input type="hidden" name="arah" value=""> 
	<input type="hidden" name="id" value="">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody>
			<tr>
				<td nowrap="nowrap">
					<h3>
						<img src="images/h3Arrow.gif" border="0"> &nbsp;Search Welcome Call Log
					</h3>
				</td>
				<td width="100%"><img src="images/blank.gif" height="1" width="1"></td>
			</tr>
		</tbody>
	</table>

	<div class="table_container">
		<!-- Table Toolbar Start -->
		<%
			String nampak = "";
			if (navigation != null && navigation.equals("gosearch")) {
			} else {
				nampak = " style=\"display: none;\"";
			}
		%>

	</div>
	<br />

	<table class="listView" cellspacing="0" cellpadding="0" width="100%">
		<tbody>
			<tr height="20">
				<td width="6%" nowrap="nowrap" class="listViewThS1" scope="col">
					<img src="images/blank.gif" alt="asd" height="1" width="1">
					No.
				</td>
				<!-- ini default generated table from database -->

				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Call
					Time &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Call
					Description &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Call
					Category&nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Followup
					Needed&nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Created
					By &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Modified
					By &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Created
					Time &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Modified
					Time &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Status
					&nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Action
					&nbsp;</td>

			</tr>
			<c:forEach items="${WelcomeCallsLogs}" var="welcomeCallsLog"
				varStatus="status">
				<%
					if (i % 2 == 0) {
							rowclass = "col1";
						} else if (i % 2 != 0) {
							rowclass = "col2";
						}
						i++;
				%>
				<tr onMouseOver="" onMouseOut="" onMouseDown="" height="20">
					<td class="oddListRowS1" align="center" bgcolor="#e7f0fe"
						nowrap="nowrap" valign="top"><%=(i + ((indexint - 1) * countSet))%>
					</td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out value="${welcomeCallsLog.callTime}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out
							value="${welcomeCallsLog.callDescription}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:if
							test="${welcomeCallsLog.callCategoryId.id == 2}">
							<c:out value="Welcome Calls" />
						</c:if> <c:if test="${welcomeCallsLog.callCategoryId.id == 1}">
							<c:out value="Information" />
						</c:if></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:if
							test="${welcomeCallsLog.followupNeeded == 0}">
							<c:out value="No" />
						</c:if> <c:if test="${welcomeCallsLog.followupNeeded == 1}">
							<c:out value="Yes" />
						</c:if></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out value="${welcomeCallsLog.createdBy}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out value="${welcomeCallsLog.modifiedBy}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out value="${welcomeCallsLog.createdTime}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out value="${welcomeCallsLog.modifiedTime}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:if
							test="${welcomeCallsLog.statusCallId.id == 1}">
							<c:out value="No Active" />
						</c:if> <c:if test="${welcomeCallsLog.statusCallId.id == 2}">
							<c:out value="Wrong Number" />
						</c:if> <c:if test="${welcomeCallsLog.statusCallId.id == 3}">
							<c:out value="Pick Up" />
						</c:if> <c:if test="${welcomeCallsLog.statusCallId.id == 4}">
							<c:out value="Others" />
						</c:if></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						align="center" valign="top">
						
						<!-- ini default edit table for each data --> <a
						href="javascript:ubah('<c:out value="${welcomeCallsLog.id}" />')"> <img
							src="images/edit.gif" class="action_icon" alt="Edit" title="Edit"></a>

						<!-- ini default delete table for each data --> 
						<a href="javascript:hapus('<c:out value="${welcomeCallsLog.id}" />')">
							<img src="images/delete.gif" class="action_icon" alt="Delete" title="Delete"></a>
					</td>
				</tr>
				<tr>
					<td colspan="20" class="listViewHRS1"></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>

</form>

<!-- Table Container Stop -->
<script language="Javascript">

	
	function ubah(welcomeCallsLogId){
		document.form1.method = "GET";
		document.form1.action="welcomecallslog-form";
		document.form1.id.value=welcomeCallsLogId;
		document.form1.navigation.value="ubah";
		document.form1.submit();
	}
	
	function hapus(welcomeCallsLogId){
		var conf = window.confirm ("Are You Sure Want To Delete This Call Log ?");
		if(conf){
			document.form1.id.value=welcomeCallsLogId;
			document.form1.navigation.value="hapus";
			document.form1.submit();
		}
	}
</script>

