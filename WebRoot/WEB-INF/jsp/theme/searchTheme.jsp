<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="com.ametis.cms.util.WebUtil"%>
<%@ taglib prefix="spring" uri="/WEB-INF/spring.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>


<%
	String navigation = WebUtil.getAttributeString(request, "navigation", "");
%>

<!-- Page Title Start // Should be put on <Title> tag-->

<!-- Page Title Stop-->
<%
	String alert = (String) request.getAttribute("alert");
	int index = 0;
	int totalIndex = 0;
	int count = 0;
	int countSet = 0;

	try {
		index = ((Integer) request.getAttribute("index")).intValue();
		count = ((Integer) request.getAttribute("count")).intValue();
		countSet = ((Integer) request.getAttribute("countSet")).intValue();
		totalIndex = ((Integer) request.getAttribute("halAkhir")).intValue();

	} catch (Exception e) {
	}

	if (alert != null && !alert.trim().equals("")) {
%>
<div id="warning" align="center">
	<c:out value="${alert}"></c:out>
</div>
<%
	}
%>

<%
	String rowclass = "";
	int i = 0;
	int indexint = Integer.parseInt(request.getAttribute("index").toString());
	WebUtil.getAttributeInteger(request, "index", 0).intValue();
%>
<!-- Search Container Start -->

<form name="form1" action="theme" method="POST">
	<input type="hidden" name="navigation" value="gosearch"> 
	<input type="hidden" name="arah" value="">
	<input type="hidden" name="id" value="">
	<input type="hidden" name="index" value="<c:out value="${index}" />">
	
	
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody>
			<tr>
				<td nowrap="nowrap">
					<h3>
						<img src="images/h3Arrow.gif" border="0"> &nbsp;Search Theme
					</h3>
				</td>
				<td width="100%"><img src="images/blank.gif" height="1"
					width="1"></td>
			</tr>
		</tbody>
	</table>
	<table class="tabForm" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td><table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tbody>
          <tr>
            <form>
            </form>
            <td class="dataLabel" nowrap="nowrap">Search Keyword:
              &nbsp;&nbsp;
              
				<input size="20" name="searchtext" class="dataField" value="<c:out value="${searchtext}"/>" type="text">
              </td>
            <td class="dataLabel" nowrap="nowrap">Search Category:
              &nbsp;&nbsp;
              
                
				<select name="searchby" class="inputbox">
	 		   			<option value="memberGroupName" <c:if test="${searchby eq \"memberGroupName\"}">selected="true"</c:if> >Member Group Name</option>
	 		   			<option value="createdBy" <c:if test="${searchby eq \"createdBy\"}">selected="true"</c:if> >Created By</option>
	 		   			<option value="modifiedBy" <c:if test="${searchby eq \"modifiedBy\"}">selected="true"</c:if> >Modified By</option>
			   </select>
				
              </td>
			
            <td class="dataLabel">
              <input title="Search [Alt+Shift+Q]" accesskey="Q" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" name="button" value="Search" type="submit">
                          
			</td>
            </tr>
            <c:if test="${theUser.userType eq '2'}">
			<tr>
            
            <td class="dataLabel" nowrap="nowrap"> Minimum Date :
              &nbsp;&nbsp;&nbsp;&nbsp;
              
                <input name="minimum_date" id="jscal_field" tab="1" maxlength="10" size="11" readonly="readonly" value="" type="text">
				<img src="images/jscalendar.gif" alt="Enter Date" id="jscal_trigger" align="absmiddle" height="18" width="18">
				
				<script type="text/javascript">
    					Calendar.setup({
        					inputField     :    "jscal_field",     // id of the input field
        					ifFormat       :    "%Y-%m-%d",      // format of the input field
        					button         :    "jscal_trigger",  // trigger for the calendar (button ID)
        					align          :    "Bl",           // alignment (defaults to "Bl")
        					singleClick    :    true
    					});
				 	</script>
              
			</td>
            <td class="dataLabel" nowrap="nowrap"> Maximum Date :
              &nbsp;&nbsp;&nbsp;&nbsp;
              
            <input name="maximum_date" id="jscal_field_max" tabindex="1" maxlength="10" size="11" readonly="readonly" value="" type="text">
			<img src="images/jscalendar.gif" alt="Enter Date" id="jscal_max_trigger" align="absmiddle" height="18" width="18">
			
					<script type="text/javascript">
    					Calendar.setup({
        					inputField     :    "jscal_field_max",     // id of the input field
        					ifFormat       :    "%Y-%m-%d",      // format of the input field
        					button         :    "jscal_max_trigger",  // trigger for the calendar (button ID)
        					align          :    "Bl",           // alignment (defaults to "Bl")
        					singleClick    :    true
    					});
				 	</script>
			</td>
            <td class="dataLabel">&nbsp;&nbsp;
              </td>
            <td align="right">
            </td>
          </tr>
          </c:if>
        </tbody>
      </table></td>
    </tr>
  </tbody>
</table>

	<div class="table_container">
		<!-- Table Toolbar Start -->
		<%
			String nampak = "";
			if (navigation != null && navigation.equals("gosearch")) {
			} else {
				nampak = "";
			}
		%>

	</div>
	<br />

	<table class="listView" cellspacing="0" cellpadding="0" width="100%">
		<tbody>
		
<tr>
				<td class="listViewPaginationTdS1" align="left"></td>
            	<td class="listViewPaginationTdS1" align="right" nowrap="nowrap" colspan=20>

					<%if (index == 1) {

			%>
					<img src="images/start_off.gif" alt="Start" align="absmiddle" border="0" height="10" width="9">
					Start&nbsp;

					<img src="images/previous_off.gif" alt="Previous" align="absmiddle" border="0" height="10" width="4">
					Previous&nbsp;&nbsp;
					<%} else if ((index - 1) > 0) {

			%>
					<img src="images/start.gif" alt="Start" align="absmiddle" border="0" height="10" width="9">
					<a href="javascript:goleftbgt()" class="listViewPaginationLinkS1"> Start&nbsp; </a>

					<img src="images/previous.gif" alt="Previous" align="absmiddle" border="0" height="10" width="4">
					<a href="javascript:goleft()" class="listViewPaginationLinkS1"> Previous&nbsp;&nbsp; </a>
					<%}

			%>
					<span class="pageNumbers">(<c:out value="${minIndex}" /> - <c:out value="${maxIndex}" /> of <c:out value="${count}" />)</span>&nbsp;&nbsp;

					<%if (totalIndex > index) {

			%>

					<a href="javascript:goright()" class="listViewPaginationLinkS1">Next&nbsp; <img src="images/next.gif" alt="Next" align="absmiddle" border="0" height="10" width="4"> </a>&nbsp;&nbsp; <a href="javascript:gorightbgt()" class="listViewPaginationLinkS1">End&nbsp;
						<img src="images/end.gif" alt="End" align="absmiddle" border="0" height="10" width="9"> </a>
					<%} else {

			%>
					Next&nbsp;
					<img src="images/next_off.gif" alt="Next" align="absmiddle" border="0" height="10" width="4">
					&nbsp;&nbsp; End&nbsp;
					<img src="images/end_off.gif" alt="End" align="absmiddle" border="0" height="10" width="9">
					<%}

			%>
				</td>
			</tr>
			<tr height="20">
				<td width="6%" nowrap="nowrap" class="listViewThS1" scope="col">
					<img src="images/blank.gif" alt="asd" height="1" width="1">
					No.
				</td>
				<!-- ini default generated table from database -->
				
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Member Group Name &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Created By &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Modified By &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Created Time &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Modified Time &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Logo &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Background &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Action &nbsp;</td>
			</tr>
			<c:forEach items="${Themes}" var="theme" varStatus="status">
				<%
					if (i % 2 == 0) {
							rowclass = "col1";
						} else if (i % 2 != 0) {
							rowclass = "col2";
						}
						i++;
				%>
				<tr onMouseOver="" onMouseOut="" onMouseDown="" height="20">
					<td class="oddListRowS1" align="center" bgcolor="#e7f0fe"
						nowrap="nowrap" valign="top"><%=(i + ((indexint - 1) * countSet))%>
					</td>
					
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${theme.membergroupId.groupName}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${theme.createdBy}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${theme.modifiedBy}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${theme.createdTime}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${theme.modifiedTime}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${theme.logoId.originalDocName}" />
					<a href="javascript:detil('<c:out value="${theme.logoId.documentId}" />')">
								<img src="images/view.gif" class="action_icon" alt="View" title="View">
					</td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${theme.bgId.originalDocName}" />
					<a href="javascript:detil('<c:out value="${theme.bgId.documentId}" />')">
								<img src="images/view.gif" class="action_icon" alt="View" title="View">
								</td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top">
						<a href="javascript:edit('<c:out value="${theme.themeId}" />')">
							<img src="images/edit.gif" class="action_icon" alt="Edit" title="Edit"></a>
						<a href="javascript:hapus('<c:out value="${theme.themeId}" />')">
							<img src="images/delete.gif" class="action_icon" alt="Delete" title="Delete"></a>
					</td>
				</tr>
				<tr>
					<td colspan="20" class="listViewHRS1"></td>
				</tr>
			</c:forEach>
			
<tr>
				<td class="listViewPaginationTdS1" align="left"></td>
            	<td class="listViewPaginationTdS1" align="right" nowrap="nowrap" colspan=20>

					<%if (index == 1) {

			%>
					<img src="images/start_off.gif" alt="Start" align="absmiddle" border="0" height="10" width="9">
					Start&nbsp;

					<img src="images/previous_off.gif" alt="Previous" align="absmiddle" border="0" height="10" width="4">
					Previous&nbsp;&nbsp;
					<%} else if ((index - 1) > 0) {

			%>
					<img src="images/start.gif" alt="Start" align="absmiddle" border="0" height="10" width="9">
					<a href="javascript:goleftbgt()" class="listViewPaginationLinkS1"> Start&nbsp; </a>

					<img src="images/previous.gif" alt="Previous" align="absmiddle" border="0" height="10" width="4">
					<a href="javascript:goleft()" class="listViewPaginationLinkS1"> Previous&nbsp;&nbsp; </a>
					<%}

			%>
					<span class="pageNumbers">(<c:out value="${minIndex}" /> - <c:out value="${maxIndex}" /> of <c:out value="${count}" />)</span>&nbsp;&nbsp;

					<%if (totalIndex > index) {

			%>

					<a href="javascript:goright()" class="listViewPaginationLinkS1">Next&nbsp; <img src="images/next.gif" alt="Next" align="absmiddle" border="0" height="10" width="4"> </a>&nbsp;&nbsp; <a href="javascript:gorightbgt()" class="listViewPaginationLinkS1">End&nbsp;
						<img src="images/end.gif" alt="End" align="absmiddle" border="0" height="10" width="9"> </a>
					<%} else {

			%>
					Next&nbsp;
					<img src="images/next_off.gif" alt="Next" align="absmiddle" border="0" height="10" width="4">
					&nbsp;&nbsp; End&nbsp;
					<img src="images/end_off.gif" alt="End" align="absmiddle" border="0" height="10" width="9">
					<%}

			%>
				</td>
			</tr>
		</tbody>
	</table>
	<input title="Add Theme" accesskey="S" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" onClick="javascript:addTheme()" name="AddTheme" value=" Add Theme " type="Button">
</form>

<!-- Table Container Stop -->
<script language="Javascript">
function addTheme(){
	document.form1.navigation.value = "tambah";
	document.form1.action = "theme-form";
	document.form1.method = "GET";
	document.form1.submit();
}

function hapus(themeId){
	var delConfirm = window.confirm ("Are You Sure Want To Delete This Entry ?");
	if (delConfirm){
		document.form1.navigation.value="delete";
		document.form1.id.value=themeId;
		document.form1.submit();
	}
}
function detil(idx) {
	window
			.open(
					"document?navigation=preview&documentId=" + idx
							+ "&url=searchTheme",
					"Search",
					"width=1024, height=768, menubar=yes, status=no, toolbar=no, scrollbars=yes, resizable=yes");

}
function edit(themeId){
	document.form1.method="GET";
	document.form1.navigation.value="edit";
	document.form1.id.value=themeId;
	document.form1.action="theme-form";
	document.form1.submit();
}
<%
String nav="";
if(request.getAttribute("navigation").equals("gosearch")||request.getAttribute("navigation").equals("golookup")){
	nav = (String)request.getAttribute("navigation");
}
%>

function goleft(){
	document.form1.navigation.value = "<%=nav%>";
	document.form1.arah.value="kiri";
	//document.form1.searchtext.value="<c:out value="${searchtext}" />";
	//document.form1.searchby.value="<c:out value="${searchby}" />";
	document.form1.action="info";
	document.form1.method = "POST";
	document.form1.submit();
}
function goleftbgt(){
	document.form1.navigation.value = "<%=nav%>";
	document.form1.arah.value="kiribgt";
	//document.form1.searchtext.value="<c:out value="${searchtext}" />";
	//document.form1.searchby.value="<c:out value="${searchby}" />";
	document.form1.action="theme";
	document.form1.method = "POST";
	document.form1.submit();
}
function goright(){
	document.form1.navigation.value = "<%=nav%>";
	document.form1.arah.value="kanan";
//	document.form1.searchtext.value="<c:out value="${searchtext}" />";
	//document.form1.searchby.value="<c:out value="${searchby}" />";
	document.form1.action="theme";
	document.form1.method = "POST";
	document.form1.submit();
}
function gorightbgt(){
	document.form1.navigation.value = "<%=nav%>";
	document.form1.arah.value="kananbgt";
	//document.form1.searchtext.value="<c:out value="${searchtext}" />";
	//document.form1.searchby.value="<c:out value="${searchby}" />";
	document.form1.action="theme";
	document.form1.method = "POST";
	document.form1.submit();
}

</script>

