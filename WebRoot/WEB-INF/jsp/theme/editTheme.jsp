<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib prefix="spring" uri="/WEB-INF/spring.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>


<link rel="stylesheet" type="text/css" href="css/autocomplete.css">
<link rel="stylesheet" type="text/css" href="css/button.css">

<link rel="stylesheet" type="text/css"
	href="css/jquery/autocomplete/jquery.autocomplete.css" />

<script type="text/javascript" src="scripts/jquery/jquery-1.4.4.min.js"></script>
<script type="text/javascript"
	src="scripts/jquery/plugin/jquery.autocomplete.pack.js"></script>


<form action="theme-form" method="POST"  name="form1" id="form_layout" enctype="multipart/form-data" >
<input type="hidden" name="navigation" value="${navigation }">


<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<c:choose>
				<c:when test="${navigation eq 'tambah'}">
					<td nowrap="nowrap"><h3>
							<img src="images/h3Arrow.gif" border="0">&nbsp;Create Theme
						</h3></td>
				</c:when>
				<c:when test="${navigation eq 'edit'}">
					<td nowrap="nowrap"><h3>
							<img src="images/h3Arrow.gif" border="0">&nbsp;Edit Theme
						</h3></td>
				</c:when>
			</c:choose>
			<td width="100%"><img src="images/blank.gif" height="1"
				width="1"></td>
		</tr>
	</tbody>
</table>


	<%
		String alert = (String) request.getAttribute("alert");
		if (alert != null && !alert.trim().equals("")) {
	%>
	<div id="warning">
		<c:out value="${alert}"></c:out>
	</div>
	<%}%>


<table class="tabForm" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tbody>
						<spring:bind path="themeForm.themeId">
							<input type="hidden" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}" />" >
							<div id="fredcaption">
								<c:out value="${status.errorMessage}" />
							</div>
						</spring:bind>
						
						<tr>	  
							<td class="dataLabel"> Member Group : </td>
							<td class="dataField">
								<spring:bind path="themeForm.memberGroupName">
										<input type="text" size="35" id="memberGroupName" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}" />" />
										<div id="fredcaption">
											<c:out value="${status.errorMessage}" />
										</div>
										<input type="hidden" id="memberGroupId" name="memberGroupId" value="" />
									</spring:bind>
							</td>	 		
						</tr>				
						<tr>
							<td class="dataLabel">Logo :</td>
							<td class="dataField">
								<spring:bind path="themeForm.logo">
										<input type="file"	name="<c:out value="${status.expression}" />" accept="image/jpg,image/png,image/jpeg">
								</spring:bind>
							</td>
						</tr>
						<tr>
							<td class="dataLabel">Background :</td>
							<td class="dataField">
									<spring:bind path="themeForm.bg">
										<input type="file"	name="<c:out value="${status.expression}" />" accept="image/jpg,image/png,image/jpeg">
									</spring:bind>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
<input title="Save Theme" accesskey="S" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" onClick="javascript:saveTheme()" name="SaveTheme" value=" Save Theme " type="button">
<input title="Cancel" accesskey="S" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" onClick="javascript:batal()" name="cancel" value=" Cancel " type="button">

	
</form>


<script language="javascript">
var temp=false;
$(document).ready(function() {
	$("#memberGroupName").autocomplete("membergroup?navigation=lookupjson", {
		max : 7,
		dataType : "json",
		parse : function(data) {
			return $.map(data, function(row) {
				return {
					data : row,
					value : row.name,
					result : row.name
				}
			});
		},
		formatItem : function(row) {
			return "<font color='#000' >" + row.name + "</font>";
		}
	}).bind("result", function(data, value) {
		$(this).parents("dd").find(".error_message").hide();
		//include the countryId for filtering
		//alert(value.toSource());
		$("#memberGroupId").val(value.id);
		temp=false;
		//alert(value.id);
	});
	});
	
	
	function saveTheme() {
		document.form1.method = "POST";
		document.form1.action = "theme-form";
		document.form1.submit();
	}

	function batal() {
		window.location.reload();
	}

</script>