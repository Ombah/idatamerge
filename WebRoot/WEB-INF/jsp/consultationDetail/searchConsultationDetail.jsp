<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="com.ametis.cms.util.WebUtil"%>
<%@ taglib prefix="spring" uri="/WEB-INF/spring.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>


<%
	String navigation = WebUtil.getAttributeString(request, "navigation", "");
%>

<!-- Page Title Start // Should be put on <Title> tag-->

<!-- Page Title Stop-->
<%
	String alert = (String) request.getAttribute("alert");
	int index = 0;
	int totalIndex = 0;
	int count = 0;
	int countSet = 0;

	try {
		index = ((Integer) request.getAttribute("index")).intValue();
		count = ((Integer) request.getAttribute("count")).intValue();
		countSet = ((Integer) request.getAttribute("countSet")).intValue();
		totalIndex = ((Integer) request.getAttribute("halAkhir")).intValue();

	} catch (Exception e) {
	}

	if (alert != null && !alert.trim().equals("")) {
%>
<div id="warning" align="center">
	<c:out value="${alert}"></c:out>
</div>
<%
	}
%>

<%
	String rowclass = "";
	int i = 0;
	int indexint = Integer.parseInt(request.getAttribute("index").toString());
	WebUtil.getAttributeInteger(request, "index", 0).intValue();
%>
<!-- Search Container Start -->

<form name="form1" action="consultationdetail-form" method="POST">
	 <input type="hidden" name="navigation" value="gosearch">
	 <input type="hidden" name="arah" value=""> 
	 <input type="hidden" name="id" value="">
	 
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody>
			<tr>
				<td nowrap="nowrap">
					<h3>
						<img src="images/h3Arrow.gif" border="0"> &nbsp;Search Consultation
					</h3>
				</td>
				<td width="100%"><img src="images/blank.gif" height="1"	width="1"></td>
			</tr>
		</tbody>
	</table>

	<div class="table_container">
		<!-- Table Toolbar Start -->
		<%
			String nampak = "";
			if (navigation != null && navigation.equals("gosearch")) {
			} else {
				nampak = " style=\"display: none;\"";
			}
		%>

	</div>
	<br />

	<table class="listView" cellspacing="0" cellpadding="0" width="100%">
		<tbody>
			<tr height="20">
				<td width="6%" nowrap="nowrap" class="listViewThS1" scope="col">
					<img src="images/blank.gif" alt="asd" height="1" width="1">
					No.
				</td>

				<!-- ini default generated table from database -->

				<td scope="col" class="listViewThS1"  width="19%">Message &nbsp;</td>				
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Created By &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Modified By &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Created Time &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Modified Time &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Action &nbsp;</td>
			</tr>


			<c:forEach items="${ConsultationDetails}" var="consultation" varStatus="status">
				<%
					if (i % 2 == 0) {
							rowclass = "col1";
						} else if (i % 2 != 0) {
							rowclass = "col2";
						}
						i++;
				%>
				<tr onMouseOver="" onMouseOut="" onMouseDown="" height="20">
					<td class="oddListRowS1" align="center" bgcolor="#e7f0fe"
						nowrap="nowrap" valign="top"><%=(i + ((indexint - 1) * countSet))%>
					</td>
					
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><textarea readonly cols="60"><c:out
							value="${consultation.message}" /></textarea></td>
					
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out
							value="${consultation.createdBy}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out value="${consultation.modifiedBy}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out	value="${consultation.createdTime}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out	value="${consultation.modifiedTime}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top">
						<a href="javascript:edit('<c:out value="${consultation.consultationId}" />')">
				<img src="images/edit.gif" class="action_icon" alt="Edit" title="Edit"></a></td>
				</tr>

				<tr>
					<td colspan="20" class="listViewHRS1"></td>
				</tr>

			</c:forEach>

			
		</tbody>
	</table>
</form>
<form action="consultationdetail-form" method="POST"  name="form2" id="form_layout">
<input type="hidden" name="navigation" value="${navigation }">



<table class="tabForm" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tbody>
						<tr>	  
						<spring:bind path="consultationDetailForm.id">
							<input type="hidden" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}" />" >
							<div id="fredcaption">
								<c:out value="${status.errorMessage}" />
							</div>
						</spring:bind>
							<td class="dataLabel"> Message : </td>
							<td class="dataField">
							<spring:bind path="consultationDetailForm.message">
								
								<textarea rows="10" cols="200" id="message" name="<c:out value="${status.expression}" />" ><c:out value="${status.value}" /></textarea>
								</spring:bind>
									<div id="fredcaption">
										<c:out value="${status.errorMessage}" />
									</div>
							
							</td>	 		
						</tr>				
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
<input title="Save ConsultationDetail" accesskey="S" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" onClick="javascript:saveConsultationDetail()" name="SaveConsultationDetail" value=" Save ConsultationDetail " type="submit">
<input title="Cancel" accesskey="S" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" onClick="javascript:cancel()" name="cancel" value=" Cancel " type="submit">
</form>

<script language="javascript">
	
function saveConsultationDetail (){
	document.form2.method = "POST";
	document.form2.action = "consultationdetail-form";
	document.form2.submit();
}
function cancel (){
	document.form2.navigation.value = "";
	document.form2.action = "consultationdetail";
	document.form2.submit();
}	




</script>
<!-- Table Container Stop -->
<script language="Javascript">
function addConsultation(){
	document.form1.navigation.value = "tambah";
	document.form1.method = "GET";
	document.form1.submit();
}

function edit(consultationId){
	document.form1.method = "GET";
	document.form1.navigation.value="editConsultation";
	document.form1.id.value=(consultationId);
	document.form1.submit();
}

</script>
