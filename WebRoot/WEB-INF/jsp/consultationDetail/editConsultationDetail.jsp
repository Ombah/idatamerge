<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="/WEB-INF/spring.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>


<link rel="stylesheet" type="text/css" href="css/autocomplete.css">
<link rel="stylesheet" type="text/css" href="css/button.css">

<link rel="stylesheet" type="text/css" href="css/jquery/autocomplete/jquery.autocomplete.css"/>

<script type="text/javascript" src="scripts/jquery/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="scripts/jquery/plugin/jquery.autocomplete.pack.js"></script>

<%
	String rowclass = "";
	int i = 0;
%>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
     	<c:choose>
		<c:when test="${navigation eq 'tambah'}">
      		<td nowrap="nowrap"><h3><img src="images/h3Arrow.gif"  border="0">&nbsp;Create ConsultationDetail</h3></td>
      	</c:when>
		<c:when test="${navigation eq 'editConsultationDetail'}">
      		<td nowrap="nowrap"><h3><img src="images/h3Arrow.gif"  border="0">&nbsp;Edit ConsultationDetail</h3></td>
      	</c:when>
      </c:choose>
      <td width="100%"><img src="images/blank.gif"  height="1" width="1"></td>
    </tr>
  </tbody>
</table>

<form action="consultationdetail-form" method="POST"  name="form1" id="form_layout">
<input type="hidden" name="navigation" value="${navigation }">
<input type="hidden" name="consultationId" value="${consultationId }">

<%
String alert = (String) request.getAttribute("alert");
if (alert != null && !alert.trim().equals("")) {%>
	<div id="warning">
		<c:out value="${alert}"></c:out>
	</div>
	<%}%>

	<table class="listView" cellspacing="0" cellpadding="0" width="100%">
		<tbody>
			<tr height="20">
				<td width="6%" nowrap="nowrap" class="listViewThS1" scope="col">
					<img src="images/blank.gif" alt="asd" height="1" width="1">
					No.
				</td>

				<!-- ini default generated table from database -->

				<td scope="col" class="listViewThS1"  width="19%">Message &nbsp;</td>				
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Created By &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Modified By &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Created Time &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Modified Time &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Image &nbsp;</td>
			</tr>


			<c:forEach items="${ConsultationDetails}" var="consultation" varStatus="status">
				<%
					if (i % 2 == 0) {
							rowclass = "col1";
						} else if (i % 2 != 0) {
							rowclass = "col2";
						}
						i++;
				%>
				<tr onMouseOver="" onMouseOut="" onMouseDown="" height="20">
					<td class="oddListRowS1" align="center" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><%=(i)%>
					</td>
					
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><textarea readonly cols="60"><c:out
							value="${consultation.message}" /></textarea></td>
					
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out
							value="${consultation.createdBy}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out value="${consultation.modifiedBy}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out	value="${consultation.createdTime}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out	value="${consultation.modifiedTime}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top">
						<%if (i == 1) {%>
					<c:out value="${consultation.consultationId.documentId.originalDocName}" />
					<a href="javascript:detilGambar('<c:out value="${consultation.consultationId.documentId.documentId}" />')">
								<img src="images/view.gif" class="action_icon" alt="View" title="View"></a>
								<%} %>
					</td>
					
				</tr>

				<tr>
					<td colspan="20" class="listViewHRS1"></td>
				</tr>

			</c:forEach>

			
		</tbody>
	</table>

<table class="tabForm" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tbody>
						<tr>	  
							<td class="dataLabel"> Message : </td>
							<td class="dataField">
							
							<spring:bind path="consultationDetailForm.message">
							
								<textarea rows="10" cols="200" id="message" name="<c:out value="${status.expression}" />" ><c:out value="${status.value}" /></textarea>
							</spring:bind>
									<div id="fredcaption">
										<c:out value="${status.errorMessage}" />
									</div>
							
							</td>	 		
						</tr>				
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
<input title="Save ConsultationDetail" accesskey="S" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" onClick="javascript:saveConsultationDetail(${consultationId })" name="SaveConsultationDetail" value=" Save ConsultationDetail " type="button">
<input title="Cancel" accesskey="S" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" onClick="javascript:batal()" name="cancel" value=" Cancel " type="button">
</form>

<script language="javascript">
	
function saveConsultationDetail (consultationId){
	document.form1.method = "POST";
	document.form1.action = "consultationdetail-form";
	document.form1.consultationId.value=(consultationId);
	document.form1.submit();
}

function view(consultationDetailId){
	document.form1.method = "GET";
	document.form1.action
}

function batal (){
	window.location.reload();
}	

function detilGambar(idx) {
	window
			.open(
					"document?navigation=preview&documentId=" + idx
							+ "&url=searchTheme",
					"Search",
					"width=1024, height=768, menubar=yes, status=no, toolbar=no, scrollbars=yes, resizable=yes");

}

</script>