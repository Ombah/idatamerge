<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="com.ametis.cms.util.WebUtil"%>
<%@ taglib prefix="spring" uri="/WEB-INF/spring.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>


<%
	String navigation = WebUtil.getAttributeString(request, "navigation", "");
%>

<!-- Page Title Start // Should be put on <Title> tag-->

<!-- Page Title Stop-->
<%
	String alert = (String) request.getAttribute("alert");
	int index = 0;
	int totalIndex = 0;
	int count = 0;
	int countSet = 0;

	try {
		index = ((Integer) request.getAttribute("index")).intValue();
		count = ((Integer) request.getAttribute("count")).intValue();
		countSet = ((Integer) request.getAttribute("countSet")).intValue();
		totalIndex = ((Integer) request.getAttribute("halAkhir")).intValue();

	} catch (Exception e) {
	}

	if (alert != null && !alert.trim().equals("")) {
%>
<div id="warning" align="center">
	<c:out value="${alert}"></c:out>
</div>
<%
	}
%>

<%
	String rowclass = "";
	int i = 0;
	int indexint = Integer.parseInt(request.getAttribute("index").toString());
	WebUtil.getAttributeInteger(request, "index", 0).intValue();
%>
<!-- Search Container Start -->

<form name="form1" action="registercall" method="POST">
	<input type="hidden" name="navigation" value=""> 
	<input type="hidden" name="arah" value="">
	<input type="hidden" name="id" value="">
	<input type="hidden" name="newStatus" value="">
	<input type="hidden" name="message" value="">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody>
			<tr>
				<td nowrap="nowrap">
					<h3>
						<img src="images/h3Arrow.gif" border="0"> &nbsp;Search Register Call
					</h3>
				</td>
				<td width="100%"><img src="images/blank.gif" height="1"
					width="1"></td>
			</tr>
		</tbody>
	</table>
	<table class="tabForm" border="0" cellpadding="0" cellspacing="0"
		width="100%">
		<tbody>
			<tr>
				<td><table border="0" cellpadding="0" cellspacing="0"
						width="100%">
						<tbody>
							<tr>
								<form></form>
								<td class="dataLabel" nowrap="nowrap">Search Keyword:
									&nbsp;&nbsp; <input size="20" name="searchtext"
									class="dataField" value="<c:out value="${searchtext}"/>"
									type="text">
								</td>
								<td class="dataLabel" nowrap="nowrap">Search Category:
									&nbsp;&nbsp; <select name="searchby" class="inputbox">
										<option value="username"<c:if test="${searchby eq \"username\"}">selected="true"</c:if>>Username</option>
										<option value="customerNumber"<c:if test="${searchby eq \"customerNumber\"}">selected="true"</c:if>>Customer Number</option>
										<option value="providerName"<c:if test="${searchby eq \"providerName\"}">selected="true"</c:if>>Provider Name</option>
										<option value="telephoneNumber"<c:if test="${searchby eq \"telephoneNumber\"}">selected="true"</c:if>>Telephone Number</option>
										<option value="customerFullName"<c:if test="${searchby eq \"customerFullName\"}">selected="true"</c:if>>Customer Full Name</option>
										<option value="callcenterUsername"<c:if test="${searchby eq \"callcenterUsername\"}">selected="true"</c:if>>Callcenter Username</option>
								</select>

								</td>
								<td class="dataLabel" nowrap="nowrap">Status: &nbsp;&nbsp;

								<select name="status">
										<option value="-1">-- All Status --</option>
										<option value="0">NOT SOLVED</option>
										<option value="1">PROCESS</option>
										<option value="2">SOLVED</option>
								</select>

								</td>
								<td class="dataLabel"><input title="Search [Alt+Shift+Q]"
									accesskey="Q"
									class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus"
									name="button" value="Search" type="submit"></td>
							</tr>
						</tbody>
					</table></td>
			</tr>
		</tbody>
	</table>
	<div class="table_container">
		<!-- Table Toolbar Start -->
		<%
			String nampak = "";
			if (navigation != null && navigation.equals("gosearch")) {
			} else {
				nampak = " style=\"display: none;\"";
			}
		%>

	</div>
	<br />

	<table class="listView" cellspacing="0" cellpadding="0" width="100%">
		<tbody>
		<tr>
				<td class="listViewPaginationTdS1" align="left"></td>
            	<td class="listViewPaginationTdS1" align="right" nowrap="nowrap" colspan=20>

					<%if (index == 1) {

			%>
					<img src="images/start_off.gif" alt="Start" align="absmiddle" border="0" height="10" width="9">
					Start&nbsp;

					<img src="images/previous_off.gif" alt="Previous" align="absmiddle" border="0" height="10" width="4">
					Previous&nbsp;&nbsp;
					<%} else if ((index - 1) > 0) {

			%>
					<img src="images/start.gif" alt="Start" align="absmiddle" border="0" height="10" width="9">
					<a href="javascript:goleftbgt()" class="listViewPaginationLinkS1"> Start&nbsp; </a>

					<img src="images/previous.gif" alt="Previous" align="absmiddle" border="0" height="10" width="4">
					<a href="javascript:goleft()" class="listViewPaginationLinkS1"> Previous&nbsp;&nbsp; </a>
					<%}

			%>
					<span class="pageNumbers">(<c:out value="${minIndex}" /> - <c:out value="${maxIndex}" /> of <c:out value="${count}" />)</span>&nbsp;&nbsp;

					<%if (totalIndex > index) {

			%>

					<a href="javascript:goright()" class="listViewPaginationLinkS1">Next&nbsp; <img src="images/next.gif" alt="Next" align="absmiddle" border="0" height="10" width="4"> </a>&nbsp;&nbsp; <a href="javascript:gorightbgt()" class="listViewPaginationLinkS1">End&nbsp;
						<img src="images/end.gif" alt="End" align="absmiddle" border="0" height="10" width="9"> </a>
					<%} else {

			%>
					Next&nbsp;
					<img src="images/next_off.gif" alt="Next" align="absmiddle" border="0" height="10" width="4">
					&nbsp;&nbsp; End&nbsp;
					<img src="images/end_off.gif" alt="End" align="absmiddle" border="0" height="10" width="9">
					<%}

			%>
				</td>
			</tr>
			<tr height="20">
				<td width="6%" nowrap="nowrap" class="listViewThS1" scope="col">
					<img src="images/blank.gif" alt="asd" height="1" width="1">
					No.
				</td>
				<!-- ini default generated table from database -->
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Username &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Customer Number &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Customer Full Name &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Provider Name &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Status&nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Telephone Number &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Created By &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Modified By &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Created Time &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Modified Time &nbsp;</td>
				
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Callcenter Username &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Message &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Action &nbsp;</td>
			</tr>
			
			<c:forEach items="${RegisterCalls}" var="registerCall" varStatus="status">
				<%
					if (i % 2 == 0) {
							rowclass = "col1";
						} else if (i % 2 != 0) {
							rowclass = "col2";
						}
						i++;
				%>
				<tr onMouseOver="" onMouseOut="" onMouseDown="" height="20">
					<td class="oddListRowS1" align="center" bgcolor="#e7f0fe"
						nowrap="nowrap" valign="top"><%=(i + ((indexint - 1) * countSet))%>
					</td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${registerCall.username}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out	value="${registerCall.memberId.customerNumber}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out	value="${registerCall.memberId.fullName}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out	value="${registerCall.providerId.providerName}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top">
						<c:if test="${registerCall.status == 0 }">
							Not Solved
						</c:if>
						<c:if test="${registerCall.status == 1 }">
							Process
						</c:if>
						<c:if test="${registerCall.status == 2 }">
							Solved
						</c:if>
					</td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${registerCall.telephoneNumber}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${registerCall.createdBy}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${registerCall.modifiedBy}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${registerCall.createdTime}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${registerCall.modifiedTime}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${registerCall.callcenterUsername}" /></td>
						<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap"
						valign="top"><c:out	value="${registerCall.message}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top">
					<c:if test="${registerCall.status == 0 }">
						<a class=linkDetail href="javascript:changeStatus('<c:out value="${registerCall.id}" />',1)">Process</a>
						<a class=linkDetail href="javascript:changeStatus('<c:out value="${registerCall.id}" />',2)">Solve</a>
					</c:if>
					<c:if test="${registerCall.status == 1 }">
						<a class=linkDetail href="javascript:changeStatus('<c:out value="${registerCall.id}" />',2)">Solve</a>
					</c:if>
					<c:if test="${registerCall.status == 2 }">
						SOLVED
					</c:if>
					<a class=linkDetail href="javascript:alertgcm('<c:out value="${registerCall.id}" />')">SEND ALERT</a>				
					<a href="javascript:hapus('<c:out value="${registerCall.id}" />')"><img src="images/delete.gif" class="action_icon" alt="Delete" title="Delete"></a></td>
				</tr>
				<tr>
					<td colspan="20" class="listViewHRS1"></td>
				</tr>
			</c:forEach>
			<tr>
				<td class="listViewPaginationTdS1" align="left"></td>
            	<td class="listViewPaginationTdS1" align="right" nowrap="nowrap" colspan=20>

					<%if (index == 1) {

			%>
					<img src="images/start_off.gif" alt="Start" align="absmiddle" border="0" height="10" width="9">
					Start&nbsp;

					<img src="images/previous_off.gif" alt="Previous" align="absmiddle" border="0" height="10" width="4">
					Previous&nbsp;&nbsp;
					<%} else if ((index - 1) > 0) {

			%>
					<img src="images/start.gif" alt="Start" align="absmiddle" border="0" height="10" width="9">
					<a href="javascript:goleftbgt()" class="listViewPaginationLinkS1"> Start&nbsp; </a>

					<img src="images/previous.gif" alt="Previous" align="absmiddle" border="0" height="10" width="4">
					<a href="javascript:goleft()" class="listViewPaginationLinkS1"> Previous&nbsp;&nbsp; </a>
					<%}

			%>
					<span class="pageNumbers">(<c:out value="${minIndex}" /> - <c:out value="${maxIndex}" /> of <c:out value="${count}" />)</span>&nbsp;&nbsp;

					<%if (totalIndex > index) {

			%>

					<a href="javascript:goright()" class="listViewPaginationLinkS1">Next&nbsp; <img src="images/next.gif" alt="Next" align="absmiddle" border="0" height="10" width="4"> </a>&nbsp;&nbsp; <a href="javascript:gorightbgt()" class="listViewPaginationLinkS1">End&nbsp;
						<img src="images/end.gif" alt="End" align="absmiddle" border="0" height="10" width="9"> </a>
					<%} else {

			%>
					Next&nbsp;
					<img src="images/next_off.gif" alt="Next" align="absmiddle" border="0" height="10" width="4">
					&nbsp;&nbsp; End&nbsp;
					<img src="images/end_off.gif" alt="End" align="absmiddle" border="0" height="10" width="9">
					<%}

			%>
				</td>
			</tr>
		</tbody>
	</table>
</form>

<!-- Table Container Stop -->
<script language="Javascript">
<%
String nav="";
if(request.getAttribute("navigation").equals("gosearch")||request.getAttribute("navigation").equals("golookup")){
	nav = (String)request.getAttribute("navigation");
}
%>
function hapus(registerCallId){
	var delConfirm = window.confirm ("Are You Sure Want To Delete This Entry ?");
	if (delConfirm){
		document.form1.navigation.value="delete";
		document.form1.id.value=registerCallId;
		document.form1.action = "registercall"
		document.form1.submit();
	}
}

function changeStatus(registerCallId,newStatus){
	var confirm = prompt("Please enter the message", "Message");
	if(confirm!=null){
		document.form1.id.value=registerCallId;
		document.form1.navigation.value="changestatus";
		document.form1.newStatus.value=newStatus;
		document.form1.message.value = confirm;
		document.form1.action = "registercall"
		document.form1.submit();		
	}
}

function alertgcm(id){
	var confirm = window.confirm("Are You Sure Want To Send Alert ?");
	if(confirm){
		document.form1.id.value = id;
		document.form1.navigation.value = "alertgcm"
		//TODO FARHAN ARAHKAN ACTION KE CONTROLLER YG DI INGINKAN
		document.form1.action = "registercall"
		//alert("not yet");
		document.form1.submit();
	}
}

function goleft(){
	document.form1.navigation.value = "<%=nav%>";
	document.form1.arah.value="kiri";
	//document.form1.searchtext.value="<c:out value="${searchtext}" />";
	//document.form1.searchby.value="<c:out value="${searchby}" />";
	document.form1.action="registercall";
	document.form1.method = "POST";
	document.form1.submit();
}
function goleftbgt(){
	document.form1.navigation.value = "<%=nav%>";
	document.form1.arah.value="kiribgt";
	//document.form1.searchtext.value="<c:out value="${searchtext}" />";
	//document.form1.searchby.value="<c:out value="${searchby}" />";
	document.form1.action="registercall";
	document.form1.method = "POST";
	document.form1.submit();
}
function goright(){
	document.form1.navigation.value = "<%=nav%>";
	document.form1.arah.value="kanan";
//	document.form1.searchtext.value="<c:out value="${searchtext}" />";
	//document.form1.searchby.value="<c:out value="${searchby}" />";
	document.form1.action="registercall";
	document.form1.method = "POST";
	document.form1.submit();
}
function gorightbgt(){
	document.form1.navigation.value = "<%=nav%>";
	document.form1.arah.value="kananbgt";
	//document.form1.searchtext.value="<c:out value="${searchtext}" />";
	//document.form1.searchby.value="<c:out value="${searchby}" />";
	document.form1.action="registercall";
	document.form1.method = "POST";
	document.form1.submit();
}
</script>

