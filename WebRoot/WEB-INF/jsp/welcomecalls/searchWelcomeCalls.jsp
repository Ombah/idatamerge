<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="com.ametis.cms.util.WebUtil"%>
<%@ taglib prefix="spring" uri="/WEB-INF/spring.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>


<%
	String navigation = WebUtil.getAttributeString(request, "navigation", "");
%>

<!-- Page Title Start // Should be put on <Title> tag-->

<!-- Page Title Stop-->
<%
	String alert = (String) request.getAttribute("alert");
	int index = 0;
	int totalIndex = 0;
	int count = 0;
	int countSet = 0;

	try {
		index = ((Integer) request.getAttribute("index")).intValue();
		count = ((Integer) request.getAttribute("count")).intValue();
		countSet = ((Integer) request.getAttribute("countSet")).intValue();
		totalIndex = ((Integer) request.getAttribute("halAkhir")).intValue();

	} catch (Exception e) {
	}

	if (alert != null && !alert.trim().equals("")) {
%>
<div id="warning" align="center">
	<c:out value="${alert}"></c:out>
</div>
<%
	}
%>

<%
	String rowclass = "";
	int i = 0;
	int indexint = Integer.parseInt(request.getAttribute("index").toString());
	WebUtil.getAttributeInteger(request, "index", 0).intValue();
%>
<!-- Search Container Start -->

<form name="form1" action="welcomecalls" method="POST">
	<input type="hidden" name="navigation" value=""> 
	<input type="hidden" name="arah" value="">
	<input type="hidden" name="id" value="">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody>
			<tr>
				<td nowrap="nowrap">
					<h3>
						<img src="images/h3Arrow.gif" border="0"> &nbsp;Search Welcome Call
					</h3>
				</td>
				<td width="100%"><img src="images/blank.gif" height="1"
					width="1"></td>
			</tr>
		</tbody>
	</table>

	<div class="table_container">
		<!-- Table Toolbar Start -->
		<%
			String nampak = "";
			if (navigation != null && navigation.equals("gosearch")) {
			} else {
				nampak = " style=\"display: none;\"";
			}
		%>

	</div>
	<br />

	<table class="listView" cellspacing="0" cellpadding="0" width="100%">
		<tbody>
			<tr height="20">
				<td width="6%" nowrap="nowrap" class="listViewThS1" scope="col">
					<img src="images/blank.gif" alt="asd" height="1" width="1">
					No.
				</td>
				<!-- ini default generated table from database -->
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Member Name &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Telephone Number &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Created By &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Modified By &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Created Time &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Modified Time &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Status &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="10%">Log Type &nbsp;</td>
				
			</tr>
			<c:forEach items="${WelcomeCalls}" var="welcomeCalls" varStatus="status">
				<%
					if (i % 2 == 0) {
							rowclass = "col1";
						} else if (i % 2 != 0) {
							rowclass = "col2";
						}
						i++;
				%>
				<tr onMouseOver="" onMouseOut="" onMouseDown="" height="20">
					<td class="oddListRowS1" align="center" bgcolor="#e7f0fe"
						nowrap="nowrap" valign="top"><%=(i + ((indexint - 1) * countSet))%>
					</td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top">
						<a href="welcomecalls?navigation=welcomeCallDetail&welcomeCallId=<c:out value="${welcomeCalls.id}" />" class="linkDetail">	<c:out value="${welcomeCalls.memberName}" /></a>
					</td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${welcomeCalls.telephoneNumber}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${welcomeCalls.createdBy}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${welcomeCalls.modifiedBy}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${welcomeCalls.createdTime}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${welcomeCalls.modifiedTime}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top">
					<c:if test="${welcomeCalls.status == 0}">
						<c:out value="OPEN" />
					</c:if>
					<c:if test="${welcomeCalls.status == 1}">
						<c:out value="CLOSE" />
					</c:if>
					</td>
					
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top">
					<c:if test="${welcomeCalls.logType == 0}">
						<c:out value="Welcome Call" />
					</c:if>
					</td>
										
				</tr>
				<tr>
					<td colspan="20" class="listViewHRS1"></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<c:if test="${theUser.roleId.roleId == 1}">
		<c:if test="${navigation.equals('') || navigation.equals('gosave')}">
		<table class="tabForm" border="0" cellpadding="0" cellspacing="0"
			width="100%">
			<tbody>
				<tr id="trRow">
					<td class="dataLabel" nowrap="nowrap">
						<input title="Add Member"	accesskey="C" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus"
						onClick="javascript:addMember();" name="addMemberButton" value=" Add Member "	type="button">
					</td>
				</tr>
			</tbody>
		</table>
		</c:if>
	</c:if>	
</form>

<!-- Table Container Stop -->
<script language="Javascript">
function addMember(){
		document.form1.navigation.value="add";
		document.form1.submit();
}
</script>

