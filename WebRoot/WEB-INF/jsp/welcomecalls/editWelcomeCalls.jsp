<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="/WEB-INF/spring.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>


<link rel="stylesheet" type="text/css" href="css/autocomplete.css">
<link rel="stylesheet" type="text/css" href="css/button.css">

<link rel="stylesheet" type="text/css" href="css/jquery/autocomplete/jquery.autocomplete.css"/>

<script type="text/javascript" src="scripts/jquery/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="scripts/jquery/plugin/jquery.autocomplete.pack.js"></script>


<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
     	  	<c:choose>
		<c:when test="${navigation eq 'tambah'}">
      		<td nowrap="nowrap"><h3><img src="images/h3Arrow.gif"  border="0">&nbsp;Create WelcomeCalls</h3></td>
      	</c:when>
		<c:when test="${navigation eq 'editWelcomeCalls'}">
      		<td nowrap="nowrap"><h3><img src="images/h3Arrow.gif"  border="0">&nbsp;Edit WelcomeCalls</h3></td>
      	</c:when>
      </c:choose>
      <td width="100%"><img src="images/blank.gif"  height="1" width="1"></td>
    </tr>
  </tbody>
</table>

<form action="consultation-form" method="POST"  name="form1" id="form_layout">
<input type="hidden" name="navigation" value="${navigation }">
<input type="hidden" name="welcomeCallsId" value="">

<%
String alert = (String) request.getAttribute("alert");
if (alert != null && !alert.trim().equals("")) {%>
	<div id="warning">
		<c:out value="${alert}"></c:out>
	</div>
	<%}%>


<table class="tabForm" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tbody>
						<spring:bind path="welcomeCallsForm.id">
							<input type="hidden" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}" />" >
							<div id="fredcaption">
								<c:out value="${status.errorMessage}" />
							</div>
						</spring:bind>
						<tr>
							<td class="dataLabel">Status : </td>		
							<td class="dataField">
								<spring:bind path="welcomeCallsForm.status">
									<select name="<c:out value="${status.expression}"/>"tabindex="3">
									<c:if test="${status.value eq 0}">
									
										<option value="1" >CLOSED</option>
										<option value="0" selected>OPEN</option>
									</c:if>
									<c:if test="${status.value eq 1}">
										<option value="1" selected>CLOSED</option>
										<option value="0" >OPEN</option>									
									</c:if>
									</select>
								</spring:bind>
							</td>
						</tr>
						<tr>	  
							<td class="dataLabel"> Telephone Number : </td>
							<td class="dataField">
							<spring:bind path="welcomeCallsForm.telephoneNumber">
								<input type="text" name="<c:out value="${status.expression}" />" value="<c:out value="${status.value}" />" id="telephoneNumber">
							</spring:bind>
							</td>	 		
						</tr>				
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
<input title="Save WelcomeCalls" accesskey="S" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" onClick="javascript:saveWelcomeCalls(${welcomeCallsId})" name="SaveWelcomeCalls" value=" Save WelcomeCalls " type="submit">
<input title="Cancel" accesskey="S" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" onClick="javascript:batal()" name="cancel" value=" Cancel " type="button">
</form>

<script language="javascript">
	
function saveWelcomeCalls (id){
	alert(id);
	document.form1.method = "POST";
	document.form1.welcomeCallsId.value=id;
	document.form1.action = "welcomecalls-form";
	document.form1.submit();
}
function batal (){
	window.location.reload();
}	




</script>