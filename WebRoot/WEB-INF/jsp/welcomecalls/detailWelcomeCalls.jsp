<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ taglib prefix="spring" uri="/WEB-INF/spring.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>


<form action="welcomecalls" name="form1" id="form_layout">
<input type="hidden" name="caseType" value="">

	<input type="hidden" name="navigation" value="">
	<input type="hidden" name="welcomeCallsId" value="<c:out value="${welcomeCallsId}" />" >
	
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td nowrap="nowrap"><h3><img src="images/h3Arrow.gif"  border="0">&nbsp;Detail Welcome Calls</h3></td>
      <td width="100%"><img src="images/blank.gif"  height="1" width="1"></td>
      <input title="Update [Alt+Shift+U]" accesskey="U" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" onClick="javascript:ubah(<c:out value="${welcomeCallsId}" />)" name="Update" value=" Update " type="button">
			
	  <input title="Close" accesskey="D" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus" onClick="javascript:tutup(<c:out value="${welcomeCallsId}" />)" name="Tutup" value=" Close " type="button">
      <c:if test="${theUser.roleId.roleId eq 5 or theUser.roleId.roleId eq 28}">
	      <td align="right">
	      	<input title="Call Log"  name="callLog" id="callLog" value=" Call Log " class="errorLog" type="button" onClick="javascript:printCallLog(<c:out value="${welcomeCallsId}" />)">
	      	
	      </td>
	      <td align="right">
	      	<input title="Add Call Log"  name="addCallLog" value=" Add Call Log " class="errorLog" type="button" onClick="javascript:tambahCallLog(<c:out value="${welcomeCallsId}" />)">
	      </td>
      </c:if>
    </tr>
  </tbody>
</table>

<c:set var="now" value="<%=new java.util.Date()%>"/>

<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr> 
      	<td style="padding-bottom: 2px; padding-top: 5px;">
      	
		
			
		</td>
    </tr>
  </tbody>
</table>

<%@ include file="../mainContentWelcomeCalls.jsp" %>
	

<br />
 	

</form>
<script language="javascript">
	function tutup(id){
		var conf = window.confirm ("Are You Sure Want To Close This Call  ?");
		if(conf){
		document.form1.navigation.value="tutup";
		document.form1.welcomeCallsId.value = id;
		document.form1.method = "POST";
		document.form1.submit();			
		}
	}
	
	function ubah(id){
		document.form1.action = "welcomecalls-form";
		document.form1.welcomeCallsId.value = id;
		document.form1.method = "GET";
		document.form1.navigation.value="ubah";
		document.form1.submit();
	}
	
	function tambahCallLog(welcomeCallsId){
		
	 	document.form1.action="welcomecallslog-form";
	 	document.form1.navigation.value="tambah";
	 	document.form1.welcomeCallsId.value = welcomeCallsId;
	 	document.form1.submit();
	}
	
	function printCallLog(welcomeCallsId){
		window.location.href = "welcomecallslog?navigation=searchWelcomeCallsLog&welcomeCallsId="+welcomeCallsId;		
	}
	
</script>
