<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page import="com.ametis.cms.util.WebUtil"%>
<%@ taglib prefix="spring" uri="/WEB-INF/spring.tld"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"%>


<%
	String navigation = WebUtil.getAttributeString(request, "navigation", "");
%>

<!-- Page Title Start // Should be put on <Title> tag-->

<!-- Page Title Stop-->
<%
	String alert = (String) request.getAttribute("alert");
	int index = 0;
	int totalIndex = 0;
	int count = 0;
	int countSet = 0;

	try {
		index = ((Integer) request.getAttribute("index")).intValue();
		count = ((Integer) request.getAttribute("count")).intValue();
		countSet = ((Integer) request.getAttribute("countSet")).intValue();
		totalIndex = ((Integer) request.getAttribute("halAkhir")).intValue();

	} catch (Exception e) {
	}

	if (alert != null && !alert.trim().equals("")) {
%>
<div id="warning" align="center">
	<c:out value="${alert}"></c:out>
</div>
<%
	}
%>

<%
	String rowclass = "";
	int i = 0;
	int indexint = Integer.parseInt(request.getAttribute("index").toString());
	WebUtil.getAttributeInteger(request, "index", 0).intValue();
%>
<!-- Search Container Start -->

<form id="myForm" name="form1" action="registercall" method="POST">
	<input type="hidden" name="navigation" value=""> 
	<input type="hidden" name="arah" value="">
	<input type="hidden" name="arrAddedMember" value="">
	<input type="hidden" name="id" value="">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tbody>
			<tr>
				<td nowrap="nowrap">
					<h3>
						<img src="images/h3Arrow.gif" border="0"> &nbsp;Add Member for Welcome Calls
					</h3>
				</td>
				<td width="100%"><img src="images/blank.gif" height="1"
					width="1"></td>
			</tr>
		</tbody>
	</table>

	<div class="table_container">
		<!-- Table Toolbar Start -->
		<%
			String nampak = "";
			if (navigation != null && navigation.equals("gosearch")) {
			} else {
				nampak = " style=\"display: none;\"";
			}
		%>

	</div>
	<br />

	<table class="listView" cellspacing="0" cellpadding="0" width="100%">
		<tbody>
			<tr height="20">
				<td width="6%" nowrap="nowrap" class="listViewThS1" scope="col">
					<img src="images/blank.gif" alt="asd" height="1" width="1">
					No.
				</td>
				<!-- ini default generated table from database -->
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Member Name &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Member Number &nbsp;</td>
				<td scope="col" class="listViewThS1" nowrap="nowrap" width="5%">Add For Welcome Calls &nbsp;</td>
			</tr>
			<c:forEach items="${Members}" var="member" varStatus="status">
				<%
					if (i % 2 == 0) {
							rowclass = "col1";
						} else if (i % 2 != 0) {
							rowclass = "col2";
						}
						i++;
				%>
				<tr onMouseOver="" onMouseOut="" onMouseDown="" height="20">
					<td class="oddListRowS1" align="center" bgcolor="#e7f0fe"
						nowrap="nowrap" valign="top"><%=(i + ((indexint - 1) * countSet))%>
					</td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${member.fullName}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top"><c:out value="${member.customerNumber}" /></td>
					<td class="oddListRowS1" bgcolor="#e7f0fe" nowrap="nowrap" valign="top">
					<c:set var="isAdded" scope="session" value="${member.welcomeCall}"/>
						<c:if test="${isAdded==0}">
							<input class="cbMemberAdded" type="checkbox" value="${member.memberId}">
						</c:if>
						<c:if test="${isAdded==1}">
							Already Added
						</c:if>
					</td>
				</tr>
				<tr>
					<td colspan="20" class="listViewHRS1"></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<table class="tabForm" border="0" cellpadding="0" cellspacing="0"
		width="100%">
		<tbody>
			<tr id="trRow">
				<td class="dataLabel" nowrap="nowrap">
					<input title="Save"	accesskey="C" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus"
					onClick="javascript:save();" name="saveButton" value=" Save "	type="button">
				</td>
				<td class="dataLabel" nowrap="nowrap">
					<input title="Cancel"	accesskey="C" class="ui-button ui-widget ui-state-default ui-corner-all ui-state-focus"
					onClick="javascript:cancel();" name="cancelButton" value=" Cancel "	type="button">
				</td>
			</tr>
		</tbody>
	</table>	
</form>

<!-- Table Container Stop -->
<script language="Javascript">
function save(){
	var raw = document.getElementsByClassName("cbMemberAdded");
	
	var arrAddedMember= "";
	for(i=0;i<raw.length;i++){
		arrAddedMember+=raw[i].value+"_"+raw[i].checked+"--";

	}
	
	document.form1.arrAddedMember.value=arrAddedMember;
	document.form1.action="welcomecalls";
	document.form1.navigation.value="gosave";
	document.form1.submit();
}
function cancel(){
	document.getElementById("myForm").reset();
}
</script>


