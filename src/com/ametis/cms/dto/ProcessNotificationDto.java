package com.ametis.cms.dto;

public class ProcessNotificationDto {
	private Integer id;
	private Integer processId;
	private Integer productId;
	private Integer ClientId;
	private Integer policyId;
	private Integer isMemberEmail;
	private Integer isClientEmail;
	private Integer isProviderEmail;
	private Integer isMemberSMS;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getProcessId() {
		return processId;
	}
	public void setProcessId(Integer processId) {
		this.processId = processId;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Integer getClientId() {
		return ClientId;
	}
	public void setClientId(Integer clientId) {
		ClientId = clientId;
	}
	public Integer getPolicyId() {
		return policyId;
	}
	public void setPolicyId(Integer policyId) {
		this.policyId = policyId;
	}
	public Integer getIsMemberEmail() {
		return isMemberEmail;
	}
	public void setIsMemberEmail(Integer isMemberEmail) {
		this.isMemberEmail = isMemberEmail;
	}
	public Integer getIsClientEmail() {
		return isClientEmail;
	}
	public void setIsClientEmail(Integer isClientEmail) {
		this.isClientEmail = isClientEmail;
	}
	public Integer getIsProviderEmail() {
		return isProviderEmail;
	}
	public void setIsProviderEmail(Integer isProviderEmail) {
		this.isProviderEmail = isProviderEmail;
	}
	public Integer getIsMemberSMS() {
		return isMemberSMS;
	}
	public void setIsMemberSMS(Integer isMemberSMS) {
		this.isMemberSMS = isMemberSMS;
	}
	
	
}
