package com.ametis.cms.webservice;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.jws.WebService;

import org.jgroups.Message;

import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Consultation;
import com.ametis.cms.datamodel.ConsultationDetail;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.dto.CaseCategoryDto;
import com.ametis.cms.dto.MemberProductDto;
import com.ametis.cms.dto.ConsultationDetailDto;
import com.ametis.cms.service.ClaimService;
import com.ametis.cms.service.ConsultationDetailService;
import com.ametis.cms.service.ConsultationService;
import com.ametis.cms.service.MemberProductService;
import com.ametis.cms.service.MemberService;
import com.ametis.cms.service.ProductService;
import com.ametis.cms.service.UserService;

@WebService(name = "ConsultationDetailWebService", endpointInterface = "com.ametis.cms.webservice.IConsultationDetailWebService")

public class ConsultationDetailWebServiceImpl implements IConsultationDetailWebService {
	private ConsultationDetailService consultationDetailService;
	private ConsultationService consultationService;
	private UserService userService;
	
	public ConsultationService getConsultationService() {
		return consultationService;
	}

	public void setConsultationService(ConsultationService consultationService) {
		this.consultationService = consultationService;
	}

	public ConsultationDetailService getConsultationDetailService() {
		return consultationDetailService;
	}

	public void setConsultationDetailService(ConsultationDetailService consultationDetailService) {
		this.consultationDetailService = consultationDetailService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public Collection<ConsultationDetailDto> getAllMessage(String consultationId, String userId){
		System.out.println("masuk"+consultationId);
		Collection<ConsultationDetailDto> result = null;
		
		ActionUser user = new ActionUser();
		User theUser = null;
		
		try {
			theUser = userService.get(Integer.parseInt(userId));
			user.setUser(theUser);

			String param[] = { "deletedStatus","consultationId.consultationId"};
			Object[] value = { Integer.valueOf(0),Integer.valueOf(consultationId) };

			int total = consultationDetailService.getTotal(null, null, param, value);
//			Collection<ConsultationDetail> consultationDetails = consultationDetailService.getAll();
//			Collection<ConsultationDetail> consultationDetails = consultationDetailService.search(null, null,	param, value,false,"consultationDetailId", 0, total);

			Vector vSortP = new Vector();
			vSortP.add("createdTime");
			
			String sSortP[] = new String[vSortP.size()];
			vSortP.toArray(sSortP);
			Collection<ConsultationDetail> consultationDetails = consultationDetailService.search(null, null,	param, value,true, sSortP,null, 0, total);
//			Collection<ConsultationDetail> consultationDetails = consultationDetailService.getAll();

			if (consultationDetails != null) {
				System.out.println("consultationDetail not null");
				result = new Vector();

				Iterator<ConsultationDetail> iterator = consultationDetails.iterator();

				ConsultationDetailDto dto = null;

				while (iterator.hasNext()) {

					dto = new ConsultationDetailDto();

					ConsultationDetail consultationDetail= iterator.next();
					consultationDetail.setIsRead(ConsultationDetail.STATUS_READED);
					consultationDetailService.update(consultationDetail,user);
					if (consultationDetail != null) {
						dto.setId(consultationDetail.getId());
						dto.setMessage(consultationDetail.getMessage());
						dto.setCreatedBy(consultationDetail.getCreatedBy());
						dto.setCreatedById(""+consultationDetail.getCreatedById());
						System.out.println("farhand debug consul id "+consultationDetail.getId()+consultationDetail.getMessage()+"\n"+consultationDetail.getMessage().length());
						System.out.println("farhand debug consul id consultationDetail.getCreatedById()"+consultationDetail.getCreatedById());
//						dto.setQuestion(consultationDetail.getQuestion());
//						dto.setAnswer(consultationDetail.getAnswer());
						dto.setCreatedBy(consultationDetail.getCreatedBy());
//						dto.setModifiedBy(consultationDetail.getModifiedBy());
						dto.setCreatedTime(String.valueOf(consultationDetail.getCreatedTime()));
//						dto.setModifiedTime(String.valueOf(consultationDetail.getModifiedTime()));
						if(consultationDetail.getDocumentId()!=null){
							dto.setDocumentOriginalDocName(consultationDetail.getDocumentId().getOriginalDocName());							
						}
						result.add(dto);
					}
				}
			}
		} catch (Exception e) {

			System.out.println("consultationDetail service kena"+e.getMessage());
			e.printStackTrace();
		}
		System.out.println("coba kita lihat sizenya "+result.size());
		return result;
	}

	public boolean synchronize(Integer memberProductId, ActionUser actionUser) {
		// TODO Auto-generated method stub
		return false;
	}

	
	public Collection<CaseCategoryDto> getGroupCaseCategories(Integer groupId) {
		// TODO Auto-generated method stub
		System.out.println("tes masuk");
		Collection<CaseCategoryDto> result = new Vector<CaseCategoryDto>();
		try {
			String[] eqParam = {"deletedStatus","isProductActive"};
			Object[] eqValue = {};
			
			
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	public ConsultationDetailDto sentMessage(String message, String consultationId, String userId ){
		ConsultationDetailDto messageDto = new ConsultationDetailDto();
		ConsultationDetail newConsultationDetail = new ConsultationDetail();
		newConsultationDetail.setDeletedStatus(0);
		newConsultationDetail.setMessage(message);
		newConsultationDetail.setIsRead(ConsultationDetail.STATUS_READED);
		
		Consultation cc = null;
		try{
			cc = consultationService.get(Integer.parseInt(consultationId));
		}catch(Exception ee){
			
		}
		newConsultationDetail.setConsultationId(cc);
		ActionUser user = new ActionUser();
		User theUser = null;
		try{
			theUser = userService.get(Integer.parseInt(userId));
			user.setUser(theUser);

//			System.out.println("userservice sukses mas");
		}catch(Exception e){

//			System.out.println("userservice gagal mas"+e.getMessage());
			e.printStackTrace();
		}
		
		try{
			ConsultationDetail createdConsultationDetail = consultationDetailService.create(newConsultationDetail,user);
			Consultation modifiedConsultation = consultationService.get(Integer.valueOf(consultationId));
			StringTokenizer st = new StringTokenizer(message, System.getProperty("line.separator"));
			String preview = st.nextToken();
			if(preview.length()>90){
				modifiedConsultation.setPreviewContent(preview.substring(0,85)+"...");
			}else{
				modifiedConsultation.setPreviewContent(preview);
			}
			modifiedConsultation.setModifiedBy(theUser.getUsername());
			Date date = new Date();
			modifiedConsultation.setModifiedTime(new Timestamp(date.getTime()));
			consultationService.update(modifiedConsultation, user);
			messageDto.setMessage("masuk");
			System.out.println("sukses mas");
		}catch(Exception e){
			messageDto.setMessage("ga masuk");
			System.out.println("gagal mas "+e.getMessage());
			e.printStackTrace();
		}
		return messageDto;
	}
}
