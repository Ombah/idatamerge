package com.ametis.cms.webservice;

import java.util.Collection;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.jws.WebService;

import org.apache.xerces.impl.dv.util.Base64;
import org.jgroups.Message;

import com.ametis.cms.datamodel.ActionResult;
import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Member;
import com.ametis.cms.datamodel.Consultation;
import com.ametis.cms.datamodel.ConsultationDetail;
import com.ametis.cms.datamodel.Document;
import com.ametis.cms.datamodel.DocumentCategory;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.dto.CaseCategoryDto;
import com.ametis.cms.dto.ConsultationDetailDto;
import com.ametis.cms.dto.MemberProductDto;
import com.ametis.cms.dto.ConsultationDto;
import com.ametis.cms.service.ClaimService;
import com.ametis.cms.service.ConsultationDetailService;
import com.ametis.cms.service.ConsultationService;
import com.ametis.cms.service.DocumentService;
import com.ametis.cms.service.MemberProductService;
import com.ametis.cms.service.MemberService;
import com.ametis.cms.service.ProductService;
import com.ametis.cms.service.UserService;
import com.ametis.cms.util.StringUtil;

@WebService(name = "ConsultationWebService", endpointInterface = "com.ametis.cms.webservice.IConsultationWebService")

public class ConsultationWebServiceImpl implements IConsultationWebService {
	private ConsultationService consultationService;
	private ConsultationDetailService consultationDetailService;
	private UserService userService;
	private MemberService memberService;
	private DocumentService documentService;
	
	public DocumentService getDocumentService() {
		return documentService;
	}

	public void setDocumentService(DocumentService documentService) {
		this.documentService = documentService;
	}

	public MemberService getMemberService() {
		return memberService;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	public ConsultationDetailService getConsultationDetailService() {
		return consultationDetailService;
	}

	public void setConsultationDetailService(ConsultationDetailService consultationDetailService) {
		this.consultationDetailService = consultationDetailService;
	}

	public ConsultationService getConsultationService() {
		return consultationService;
	}

	public void setConsultationService(ConsultationService consultationService) {
		this.consultationService = consultationService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public Collection<ConsultationDto> getAllMessage(String userId){
		System.out.println("masuk"+userId);
		Collection<ConsultationDto> result = null;

		try {

			String param[] = { "deletedStatus","userId.userId"};
			Object[] value = { Integer.valueOf(0),Integer.valueOf(userId) };

			int total = consultationService.getTotal(null, null, param, value);
			
//			Collection<Consultation> consultations = consultationService.getAll();
			Vector vSortP = new Vector();
			vSortP.add("modifiedTime");
			
			String sSortP[] = new String[vSortP.size()];
			vSortP.toArray(sSortP);
			Collection<Consultation> consultations = consultationService.search(null, null,	param, value,false, sSortP,null, 0, total);


			if (consultations != null) {
				System.out.println("consultation not null");
				result = new Vector();

				Iterator<Consultation> iterator = consultations.iterator();

				ConsultationDto dto = null;

				while (iterator.hasNext()) {

					dto = new ConsultationDto();

					Consultation consultation= iterator.next();
					String paramUnread[] = { "deletedStatus","consultationId.consultationId","isRead"};
					Object[] valueUnread = { Integer.valueOf(0),consultation.getConsultationId(),ConsultationDetail.STATUS_UNREAD };

					int totalUnread = consultationDetailService.getTotal(null, null, paramUnread, valueUnread);

					if (consultation != null) {
						dto.setTitle(consultation.getTitle());
						dto.setId(consultation.getConsultationId());
//						System.out.println("farhand debug consul id "+consultation.getConsultationId());
//						dto.setQuestion(consultation.getQuestion());
//						dto.setAnswer(consultation.getAnswer());
						
//						dto.setCreatedBy(consultation.getCreatedBy());
						dto.setModifiedBy(consultation.getModifiedBy());
						dto.setPreviewContent(""+consultation.getPreviewContent());
						dto.setCounter(totalUnread);
						System.out.println("farhand debug consul preview contetn "+consultation.getPreviewContent()+" counter = "+totalUnread);
//						dto.setCreatedTime(String.valueOf(consultation.getCreatedTime()));
						dto.setModifiedTime(String.valueOf(consultation.getModifiedTime()));
						if(consultation.getDocumentId()!=null){
							dto.setDocumentId(consultation.getDocumentId().getDocumentId());							
							dto.setDocumentOriginalDocName(consultation.getOriginalDocName());
						}
						result.add(dto);
					}
				}
			}
		} catch (Exception e) {

			System.out.println("consultation service kena"+e.getMessage());
			e.printStackTrace();
		}
		System.out.println("coba kita lihat sizenya "+result.size());
		return result;
	}

	public boolean synchronize(Integer memberProductId, ActionUser actionUser) {
		// TODO Auto-generated method stub
		return false;
	}

	
	public Collection<CaseCategoryDto> getGroupCaseCategories(Integer groupId) {
		// TODO Auto-generated method stub
		System.out.println("tes masuk");
		Collection<CaseCategoryDto> result = new Vector<CaseCategoryDto>();
		try {
			String[] eqParam = {"deletedStatus","isProductActive"};
			Object[] eqValue = {};
			
			
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	public ConsultationDto sentMessage(String message, String judulPesan,  String userId,String memberId, String type,String imageLocalName, String decoded64 ){
		ConsultationDto messageDto = new ConsultationDto();
//		System.out.println("sentmessage : "+decoded64);
		System.out.println("sentmessage : "+imageLocalName);
		Consultation newConsultation = new Consultation();
		newConsultation.setTitle(judulPesan);
		newConsultation.setDeletedStatus(0);
		newConsultation.setStatus(Consultation.MESSAGE_NOT_READ);
		System.out.println("consultationtype =>"+type);
		if(type.equalsIgnoreCase("claim")){
			newConsultation.setType(Consultation.TYPE_CLAIM);
		}else if(type.equalsIgnoreCase("complaint")){
			newConsultation.setType(Consultation.TYPE_COMPLAINT);
			
		}else{
			newConsultation.setType(Consultation.TYPE_QUESTION);

		}
		//newConsultation.setCreatedById(Integer.parseInt(userId));
		
		try{
			Member member = memberService.get(Integer.parseInt(memberId));
			newConsultation.setMemberId(member);			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		StringTokenizer st = new StringTokenizer(message, System.getProperty("line.separator"));
		String preview = st.nextToken();
		if(preview.length()>90){
			newConsultation.setPreviewContent(preview.substring(0,85)+"...");
		}else{
			newConsultation.setPreviewContent(preview);
		}
		ActionUser user = new ActionUser();
		try{
			User theUser = userService.get(Integer.parseInt(userId));
			user.setUser(theUser);
			newConsultation.setUserId(theUser);
			System.out.println("userservice sukses mas");
		}catch(Exception e){

			System.out.println("userservice gagal mas"+e.getMessage());
			e.printStackTrace();
		}
		
		try{
			Collection<Document> documentList = new Vector<Document>();
			Collection<byte[]> contentList = new Vector<byte[]>();
			
			String ext = "";
			StringTokenizer tokenizer = new StringTokenizer(imageLocalName,".");
			while(tokenizer.hasMoreTokens()){
				ext = tokenizer.nextToken();
			}
			
			String url = StringUtil.hash(System.currentTimeMillis()+"") + "." + ext;
			
			
			Document doc = new Document();
			doc.setDocumentUrl(url);
			doc.setCreatedBy(user.getUser().getUsername());
			doc.setDeletedStatus(0);
			doc.setStatus(0);
			doc.setOriginalDocName(imageLocalName);
			DocumentCategory category = new DocumentCategory();
			category.setDocumentCategoryId(9);
			doc.setDocumentCategoryId(category);
			documentList.add(doc);
			byte[] tempByte = Base64.decode(decoded64);
			contentList.add(tempByte);
			ActionResult ac = documentService.create(documentList, contentList, user);
			Document docRes = (Document) ac.getResultObject();
			newConsultation.setDocumentId(docRes);
			newConsultation.setOriginalDocName(docRes.getOriginalDocName());
			System.out.println("sukses mas");
		}catch(Exception e){
			e.printStackTrace();
		}
		

		try{
			Consultation createdConsultation = consultationService.create(newConsultation,user);
			messageDto.setId(createdConsultation.getConsultationId());
			
			createdConsultation = consultationService.update(createdConsultation,user); //for sorting purpose
			createFirstDetail(message, createdConsultation, user.getUser().getUserId());
			messageDto.setContent("masuk");
			System.out.println("sukses mas");
		}catch(Exception e){
			messageDto.setContent("ga masuk");
			System.out.println("gagal mas "+e.getMessage());
			e.printStackTrace();
		}
		return messageDto;
		
	}

	
	public boolean createFirstDetail(String message, Consultation consultationId, Integer userId){
		ConsultationDetailDto messageDto = new ConsultationDetailDto();
		ConsultationDetail newConsultationDetail = new ConsultationDetail();
		newConsultationDetail.setDeletedStatus(0);
		newConsultationDetail.setMessage(message);
		newConsultationDetail.setDocumentId(consultationId.getDocumentId());
		newConsultationDetail.setIsRead(ConsultationDetail.STATUS_READED);
		Consultation cc = null;
//		try{
//			cc = consultationService.get(consultationId);
//		}catch(Exception ee){
//			
//		}
//		newConsultationDetail.setConsultationId(cc);
		newConsultationDetail.setConsultationId(consultationId);
		ActionUser user = new ActionUser();
		try{
			User theUser = userService.get(userId);
			user.setUser(theUser);

//			System.out.println("userservice sukses mas");
		}catch(Exception e){

//			System.out.println("userservice gagal mas"+e.getMessage());
			e.printStackTrace();
		}
		
		try{
			ConsultationDetail createdConsultationDetail = consultationDetailService.create(newConsultationDetail,user);
			messageDto.setMessage("masuk");
			System.out.println("sukses mas");
		}catch(Exception e){
			messageDto.setMessage("ga masuk");
			System.out.println("gagal mas "+e.getMessage());
			e.printStackTrace();
		}
		return true;
	}
//	

	@Override
	public ConsultationDto downloadImage(String documentId) {
		// TODO Auto-generated method stub
	    System.out.println("download IMage masuk "+documentId);
		ConsultationDto dto = new ConsultationDto();
		try{
//			System.out.println("cc adalah" +cc==null);

//			System.out.println("cc documentid"+cc.getDocumentId().getDocumentId());
			Document doc = documentService.get(Integer.parseInt(documentId)); 
			byte[] imageByte = documentService.getDocumentContent(doc);
			
//			dto.setImageByte(imageByte);
			String base64Image= new String(Base64.encode(imageByte));
	        dto.setBase64Image(base64Image);
	        dto.setDocumentOriginalDocName(doc.getOriginalDocName());
	        System.out.println("original doc name "+doc.getOriginalDocName());
	        System.out.println("original doc base 64 "+base64Image);
		}catch(Exception ee){
			ee.printStackTrace();
		}
		return dto;
	}

}
	