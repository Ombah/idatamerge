package com.ametis.cms.webservice;

import java.sql.Date;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import javax.jws.WebService;

import org.apache.xerces.impl.dv.util.Base64;
import org.jgroups.Message;

import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Consultation;
import com.ametis.cms.datamodel.Info;
import com.ametis.cms.dto.CaseCategoryDto;
import com.ametis.cms.dto.MemberProductDto;
import com.ametis.cms.dto.ConsultationDto;
import com.ametis.cms.dto.InfoDto;
import com.ametis.cms.service.ClaimService;
import com.ametis.cms.service.ConsultationService;
import com.ametis.cms.service.DocumentService;
import com.ametis.cms.service.InfoService;
import com.ametis.cms.service.MemberProductService;
import com.ametis.cms.service.MemberService;
import com.ametis.cms.service.ProductService;
import com.ametis.cms.util.WebUtil;

@WebService(name = "InfoWebService", endpointInterface = "com.ametis.cms.webservice.IInfoWebService")

public class InfoWebServiceImpl implements IInfoWebService {
	private InfoService infoService;
	private DocumentService documentService;
	
	
	public DocumentService getDocumentService() {
		return documentService;
	}

	public void setDocumentService(DocumentService documentService) {
		this.documentService = documentService;
	}

	@Override
	public Collection<InfoDto> getAll(String userId) {
		// TODO Auto-generated method stub
		System.out.println("masuk info"+userId);
		Collection<InfoDto> result = null;

		try {

			String param[] = { "deletedStatus"};
			Object[] value = { Integer.valueOf(0) };

//			int total = consultationService.getTotal(null, null, param, value);
			int total = 10;
			Vector vSortP = new Vector();
			vSortP.add("createdTime");
			
			String sSortP[] = new String[vSortP.size()];
			vSortP.toArray(sSortP);
			
			Date minDate = new Date(System.currentTimeMillis());
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.YEAR, 1); 
			java.util.Date nextYear = cal.getTime();
			Date maxDate = new Date(nextYear.getTime());
//			String dateBy = WebUtil.getParameterString(request, "dateBy", "");

			String[] betweenColumn = { "endDate" };
			Object[] minColumn = { minDate };
			Object[] maxColumn = { maxDate };

//			count = memberService.getTotal(sLikeP, sLikeQ, sEqP, sEqQ, betweenColumn, minColumn, maxColumn);
//			Collection<Consultation> consultations = consultationService.getAll();
			Collection<Info> infos = infoService.search(null, null,	param, value,betweenColumn, minColumn, maxColumn,false, sSortP,null, 0, total);
//			Collection<Info> infos = infoService.getAll();

			if (infos != null) {
				System.out.println("infos not null "+infos.size());
				result = new Vector();

				Iterator<Info> iterator = infos.iterator();

				InfoDto dto = null;

				while (iterator.hasNext()) {

					dto = new InfoDto();

					Info info= iterator.next();

					if (info!= null) {
						dto.setTitle(info.getTitle());
						dto.setId(info.getId());
//						dto.setQuestion(consultation.getQuestion());
//						dto.setAnswer(consultation.getAnswer());
//						dto.setCreatedBy(consultation.getCreatedBy());
//						dto.setModifiedBy(consultation.getModifiedBy());
						dto.setCreatedTime(String.valueOf(info.getCreatedTime()));
						if(info.getContent().length()>75){
							dto.setContent(info.getContent().substring(0,80)+" ...");						
						}else{
							dto.setContent(info.getContent());
						}
						byte[] imageByte = documentService.getDocumentContent(info.getDocumentId());
//						dto.setImageByte(imageByte);
						String imageName = new String(Base64.encode(imageByte));
		                dto.setImageName(imageName);
						System.out.println("tes url "+info.getDocumentId().getDocumentUrl());
//						dto.setDocumentId(info.getDocumentId().getDocumentId());
						
//						dto.setModifiedTime(consultation.getModifiedTime());
						result.add(dto);
					}
				}
			}
		} catch (Exception e) {

			System.out.println("info service kena"+e.getMessage());
			e.printStackTrace();
		}
		System.out.println("coba kita lihat sizenya "+result.size());
		return result;
	}

	@Override
	public boolean synchronize(Integer clientId, ActionUser actionUser) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public InfoDto getDetail(String id) {
		// TODO Auto-generated method stub
		System.out.println("masuk info"+id);
        InfoDto dto = new InfoDto();
        
        try {

            Info info = infoService.get(Integer.valueOf(id));
            if (info != null) {
                dto.setTitle(""+info.getTitle());
                dto.setContent(""+info.getContent());
//                dto.setImage(info.getImage());
                dto.setCreatedTime(String.valueOf(info.getCreatedTime()));
                byte[] imageByte = documentService.getDocumentContent(info.getDocumentId());
                String imageName = new String(Base64.encode(imageByte));
                dto.setImageName(imageName);
                System.out.println("ets url"+dto.getImageName());
            }
            
        } catch (Exception e) {

            System.out.println("info service kena"+e.getMessage());
            e.printStackTrace();
        }
        return dto;
	}

	@Override
	public InfoDto getLatestInfo() {
		// TODO Auto-generated method stub
		System.out.println("masuk info");
        InfoDto dto = new InfoDto();
        try {

//            Info info = infoService.getAll(1,true);
        	String param[] = { "deletedStatus"};
			Object[] value = { Integer.valueOf(0) };

        	Info info = infoService.getLastElement(param, value);
        	if (info != null) {
                dto.setTitle(""+info.getTitle());
                dto.setContent(""+info.getContent());
//                dto.setImage(info.getImage());
                dto.setCreatedTime(String.valueOf(info.getCreatedTime()));
                dto.setImageName(info.getDocumentId().getDocumentUrl());
            }
            
        } catch (Exception e) {

            System.out.println("info service kena"+e.getMessage());
            e.printStackTrace();
        }
        return dto;
	}
	
	
	public InfoService getInfoService() {
		return infoService;
	}

	public void setInfoService(InfoService infoService) {
		this.infoService = infoService;
	}

}
