package com.ametis.cms.webservice;

import java.util.Collection;

import javax.jws.WebService;

import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Diagnosis;
import com.ametis.cms.dto.MemberBenefitDto;
import com.ametis.cms.dto.ConsultationDto;
import com.ametis.cms.dto.InfoDto;
@WebService (targetNamespace="http://ametis.co.id/services/MessageWebService")

public interface IInfoWebService {

	public Collection<InfoDto> getAll(String clientId);
	public boolean synchronize (Integer clientId, ActionUser actionUser);
	public InfoDto getDetail(String id);
	public InfoDto getLatestInfo();
}
