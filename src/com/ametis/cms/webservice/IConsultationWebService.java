package com.ametis.cms.webservice;

import java.util.Collection;

import javax.jws.WebService;

import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Diagnosis;
import com.ametis.cms.dto.MemberBenefitDto;
import com.ametis.cms.dto.ConsultationDto;
@WebService (targetNamespace="http://ametis.co.id/services/MessageWebService")

public interface IConsultationWebService {

	public Collection<ConsultationDto> getAllMessage (String userId);
	public boolean synchronize (Integer clientId, ActionUser actionUser);
	public ConsultationDto sentMessage(String isiPesan, String judul, String userId,String memberId,String type, String imageLocalName,String base64Img );
	public ConsultationDto downloadImage(String consultationId);
}
