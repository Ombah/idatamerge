package com.ametis.cms.webservice;

import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import javax.jws.WebService;

import org.jgroups.Message;

import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Member;
import com.ametis.cms.datamodel.Provider;
import com.ametis.cms.service.ProviderService;
import com.ametis.cms.datamodel.RegisterCall;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.dto.CaseCategoryDto;
import com.ametis.cms.dto.MemberProductDto;
import com.ametis.cms.dto.RegisterCallDto;
import com.ametis.cms.service.ClaimService;
import com.ametis.cms.service.RegisterCallService;
import com.ametis.cms.service.MemberProductService;
import com.ametis.cms.service.MemberService;
import com.ametis.cms.service.PoliklinikService;
import com.ametis.cms.service.ProductService;
import com.ametis.cms.service.ProductTypePoliklinikService;
import com.ametis.cms.service.ProductTypeService;
import com.ametis.cms.service.ProviderPoliklinikService;
import com.ametis.cms.service.UserService;

@WebService(name = "RegisterCallWebService", endpointInterface = "com.ametis.cms.webservice.IRegisterCallWebService")

public class RegisterCallWebServiceImpl implements IRegisterCallWebService {
	private RegisterCallService registerCallService;
	private ProviderService providerService;
	private UserService userService;
	private MemberService memberService;
	private PoliklinikService poliklinikService;
	private ProductTypePoliklinikService productTypePoliklinikService;
	private ProductTypeService productTypeService;
	private ProviderPoliklinikService providerPoliklinikService;
	
	
	public ProviderPoliklinikService getProviderPoliklinikService() {
		return providerPoliklinikService;
	}

	public void setProviderPoliklinikService(ProviderPoliklinikService providerPoliklinikService) {
		this.providerPoliklinikService = providerPoliklinikService;
	}

	public ProductTypeService getProductTypeService() {
		return productTypeService;
	}

	public void setProductTypeService(ProductTypeService productTypeService) {
		this.productTypeService = productTypeService;
	}

	public ProductTypePoliklinikService getProductTypePoliklinikService() {
		return productTypePoliklinikService;
	}

	public void setProductTypePoliklinikService(ProductTypePoliklinikService productTypePoliklinikService) {
		this.productTypePoliklinikService = productTypePoliklinikService;
	}

	public PoliklinikService getPoliklinikService() {
		return poliklinikService;
	}

	public void setPoliklinikService(PoliklinikService poliklinikService) {
		this.poliklinikService = poliklinikService;
	}

	public ProviderService getProviderService() {
		return providerService;
	}

	public void setProviderService(ProviderService providerService) {
		this.providerService = providerService;
	}

	public MemberService getMemberService() {
		return memberService;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public RegisterCallService getRegisterCallService() {
		return registerCallService;
	}

	public void setRegisterCallService(RegisterCallService registerCallService) {
		this.registerCallService = registerCallService;
	}

	
	public boolean synchronize(Integer memberProductId, ActionUser actionUser) {
		// TODO Auto-generated method stub
		return false;
	}

	
	public Collection<CaseCategoryDto> getGroupCaseCategories(Integer groupId) {
		// TODO Auto-generated method stub
		System.out.println("tes masuk");
		Collection<CaseCategoryDto> result = new Vector<CaseCategoryDto>();
		try {
			String[] eqParam = {"deletedStatus","isProductActive"};
			Object[] eqValue = {};
			
			
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	public RegisterCallDto sentMessage(String telephoneNumber){
		System.out.println(telephoneNumber);
		RegisterCallDto dto = new RegisterCallDto();
		RegisterCall newRegisterCall = new RegisterCall();
		newRegisterCall.setTelephoneNumber(telephoneNumber);
		
		try{
			RegisterCall createdRegisterCall = registerCallService.create(newRegisterCall);
			dto.setMessage("success");
		}catch(Exception e){
			dto.setMessage("failed");
			System.out.println("gagal mas "+e.getMessage());
			e.printStackTrace();
		}
		return dto;
	}
	
	public RegisterCallDto sentMessage(String telephoneNumber, String userId, String memberId, String providerId){
		System.out.println(telephoneNumber+" "+userId+" "+memberId+" "+providerId);
		RegisterCallDto dto = new RegisterCallDto();
		RegisterCall newRegisterCall = new RegisterCall();
//		newRegisterCall.setTelephoneNumber(telephoneNumber);
		
		Member member = new Member();
		ActionUser user = new ActionUser();
		
		try{
			User theUser = userService.get(Integer.parseInt(userId));
			if(theUser.getDeletedStatus()==0){
				user.setUser(theUser);				
			}
			if(!providerId.equalsIgnoreCase("")){
				Provider theProvider  = providerService.get(Integer.parseInt(providerId));
				if(theProvider.getDeletedStatus()==0){
					newRegisterCall.setProviderId(theProvider);
				}
			}
			Member theMember = memberService.get(Integer.parseInt(memberId));
			if(!telephoneNumber.equalsIgnoreCase("")){
				newRegisterCall.setTelephoneNumber(telephoneNumber);
			}else if(!theMember.getTelephone().equalsIgnoreCase("")){
				newRegisterCall.setTelephoneNumber(theMember.getTelephone());				
			}else if(!theUser.getMobilePhone().equalsIgnoreCase("")){
				newRegisterCall.setTelephoneNumber(theUser.getMobilePhone());
			}else if(!theUser.getTelephone().equalsIgnoreCase("")){
				newRegisterCall.setTelephoneNumber(theUser.getTelephone());
			}else if(!theUser.getTelephoneExt().equalsIgnoreCase("")){
				newRegisterCall.setTelephoneNumber(theUser.getTelephoneExt());		
			}
			
			newRegisterCall.setCreatedTime(new java.sql.Timestamp(System.currentTimeMillis()));
			if(theMember.getDeletedStatus()==0){
				newRegisterCall.setMemberId(theMember);				
			}
			
			newRegisterCall.setStatus(0);
			newRegisterCall.setDeleted(0);
			System.out.println("userservice sukses mas");
		}catch(Exception e){

			System.out.println("userservice gagal mas"+e.getMessage());
			e.printStackTrace();
		}

		
		try{
			RegisterCall createdRegisterCall = registerCallService.create(newRegisterCall,user);
			dto.setMessage("success");
		}catch(Exception e){
			dto.setMessage("failed");
			System.out.println("gagal mas "+e.getMessage());
			e.printStackTrace();
		}
		return dto;
	}


}
