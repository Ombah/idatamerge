package com.ametis.cms.webservice;

import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import javax.jws.WebService;

import org.jgroups.Message;

import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Member;
import com.ametis.cms.datamodel.PanicAction;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.dto.CaseCategoryDto;
import com.ametis.cms.dto.MemberProductDto;
import com.ametis.cms.dto.PanicCallDto;
import com.ametis.cms.service.ClaimService;
import com.ametis.cms.service.MemberProductService;
import com.ametis.cms.service.MemberService;
import com.ametis.cms.service.PanicActionService;
import com.ametis.cms.service.ProductService;
import com.ametis.cms.service.UserService;

@WebService(name = "PanicCallWebService", endpointInterface = "com.ametis.cms.webservice.IPanicCallWebService")

public class PanicCallWebServiceImpl implements IPanicCallWebService {
	private PanicActionService panicActionService;
	private UserService userService;
	private MemberService memberService;
	
	public PanicActionService getPanicActionService() {
		return panicActionService;
	}

	public void setPanicActionService(PanicActionService panicActionService) {
		this.panicActionService = panicActionService;
	}

	
	public MemberService getMemberService() {
		return memberService;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}


	
	public boolean synchronize(Integer memberProductId, ActionUser actionUser) {
		// TODO Auto-generated method stub
		return false;
	}

	
	public Collection<CaseCategoryDto> getGroupCaseCategories(Integer groupId) {
		// TODO Auto-generated method stub
		System.out.println("tes masuk");
		Collection<CaseCategoryDto> result = new Vector<CaseCategoryDto>();
		try {
			String[] eqParam = {"deletedStatus","isProductActive"};
			Object[] eqValue = {};
			
			
		}
		catch (Exception e){
			e.printStackTrace();
		}
		return result;
	}
	
	
	public PanicCallDto sentMessage(String telephoneNumber, String userId, String memberId){
		System.out.println(telephoneNumber+" "+userId+" "+memberId);
		PanicCallDto dto = new PanicCallDto();
		PanicAction newPanicCall = new PanicAction();
//		newPanicCall.setTelephoneNumber(telephoneNumber);
		
		Member member = new Member();
		ActionUser user = new ActionUser();
		
		try{
			User theUser = userService.get(Integer.parseInt(userId));
			user.setUser(theUser);

			Member theMember = memberService.get(Integer.parseInt(memberId));
			newPanicCall.setTelephoneNumber(theMember.getTelephone());
			newPanicCall.setStatus(0);
			newPanicCall.setCreatedTime(new java.sql.Timestamp(System.currentTimeMillis()));
			if(!telephoneNumber.equalsIgnoreCase("")){
				newPanicCall.setTelephoneNumber(telephoneNumber);
			}else if(!theMember.getTelephone().equalsIgnoreCase("")){
				newPanicCall.setTelephoneNumber(theMember.getTelephone());				
			}else if(!theUser.getMobilePhone().equalsIgnoreCase("")){
				newPanicCall.setTelephoneNumber(theUser.getMobilePhone());
			}else if(!theUser.getTelephone().equalsIgnoreCase("")){
				newPanicCall.setTelephoneNumber(theUser.getTelephone());
			}else if(!theUser.getTelephoneExt().equalsIgnoreCase("")){
				newPanicCall.setTelephoneNumber(theUser.getTelephoneExt());		
			}
			newPanicCall.setMemberId(theMember);
			System.out.println("userservice sukses mas");
		}catch(Exception e){

			System.out.println("userservice gagal mas"+e.getMessage());
			e.printStackTrace();
		}

		
		try{
			
			PanicAction createdPanicCall = panicActionService.create(newPanicCall,user);
			dto.setMessage("success");
		}catch(Exception e){
			dto.setMessage("failed");
			System.out.println("gagal mas "+e.getMessage());
			e.printStackTrace();
		}
		return dto;
	}


}
