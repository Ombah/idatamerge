package com.ametis.cms.webservice;

import java.util.Collection;

import javax.jws.WebService;

import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Diagnosis;
import com.ametis.cms.dto.MemberBenefitDto;
import com.ametis.cms.dto.RegisterCallDto;
@WebService (targetNamespace="http://ametis.co.id/services/RegisterCallWebService")

public interface IRegisterCallWebService {

	public boolean synchronize (Integer clientId, ActionUser actionUser);
//	public RegisterCallDto sentMessage(String telephoneNumber);
	public RegisterCallDto sentMessage(String telephoneNumber,String userId,String memberId, String rumahSakitId);

}
