package com.ametis.cms.webservice;

import java.util.Collection;

import javax.jws.WebService;

import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Diagnosis;
import com.ametis.cms.dto.MemberBenefitDto;
import com.ametis.cms.dto.ConsultationDetailDto;
@WebService (targetNamespace="http://ametis.co.id/services/MessageWebService")

public interface IConsultationDetailWebService {

	public Collection<ConsultationDetailDto> getAllMessage (String consultationId, String userId);
//	public boolean synchronize (Integer clientId, ActionUser actionUser);
	public ConsultationDetailDto sentMessage(String isiPesan, String consultationId, String userId );
//	public ConsultationDetailDto getDetail(String userId);
}
