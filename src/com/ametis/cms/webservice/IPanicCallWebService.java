package com.ametis.cms.webservice;

import java.util.Collection;

import javax.jws.WebService;

import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Diagnosis;
import com.ametis.cms.dto.MemberBenefitDto;
import com.ametis.cms.dto.PanicCallDto;
@WebService (targetNamespace="http://ametis.co.id/services/PanicCallWebService")

public interface IPanicCallWebService {

	public boolean synchronize (Integer clientId, ActionUser actionUser);
//	public PanicCallDto sentMessage(String telephoneNumber);
	public PanicCallDto sentMessage(String telephoneNumber,String userId,String memberId);

}
