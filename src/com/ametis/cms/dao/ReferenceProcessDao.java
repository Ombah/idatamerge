
package com.ametis.cms.dao;


import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;

import com.ametis.cms.datamodel.Benefit;
import com.ametis.cms.datamodel.ReferenceProcess;



// imports+ 

// imports- 

/**
 * BenefitDao adalah dao Interface untuk operasi CRUD tabel benefit .
*/
public interface ReferenceProcessDao

// extends+ 

// extends- 
{	
	public ReferenceProcess create (ReferenceProcess object) throws DataAccessException ;

	public ReferenceProcess update (ReferenceProcess object) throws DataAccessException;
	
	public ReferenceProcess delete (ReferenceProcess object) throws DataAccessException;
	
	public ReferenceProcess get (java.io.Serializable pkey) throws DataAccessException ;

	public Criteria getCriteria() throws Exception ;

	public DetachedCriteria getDetachedCriteria() throws Exception ;

// class+ 

// class- 
}
