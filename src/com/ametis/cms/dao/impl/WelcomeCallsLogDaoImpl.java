package com.ametis.cms.dao.impl;

import java.io.Serializable;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.ametis.cms.dao.WelcomeCallsDao;
import com.ametis.cms.dao.WelcomeCallsLogDao;
import com.ametis.cms.datamodel.WelcomeCallsLog;
import com.ametis.cms.util.dao.DaoSupportUtil;

public class WelcomeCallsLogDaoImpl  extends DaoSupportUtil implements WelcomeCallsLogDao{
	@Override
	public WelcomeCallsLog create(WelcomeCallsLog object) throws DataAccessException {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().save(object);
		return object;
	}

	@Override
	public WelcomeCallsLog update(WelcomeCallsLog object) throws DataAccessException {
		this.getHibernateTemplate().update(object);
	    return object;
	}

	@Override
	public WelcomeCallsLog delete(WelcomeCallsLog object) throws DataAccessException {
		this.getHibernateTemplate().delete(object);
		return object;
	}

	@Override
	public WelcomeCallsLog get(Serializable pkey) throws DataAccessException {
		return (WelcomeCallsLog) this.getHibernateTemplate().get (WelcomeCallsLog.class, pkey);
	}

	@Override
	public Criteria getCriteria() throws Exception {
		HibernateTemplate template = this.getHibernateTemplate();
		SessionFactory sessionFactory = template.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(WelcomeCallsLog.class);
		return criteria;
	}

	@Override
	public DetachedCriteria getDetachedCriteria() throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(WelcomeCallsLog.class);
		return dc;
	}
	public Session getWelcomeCallsLogSession() throws Exception {
		// TODO Auto-generated method stub
		HibernateTemplate template = this.getHibernateTemplate();
		SessionFactory sessionFactory = template.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		
		return session;
	}


}
