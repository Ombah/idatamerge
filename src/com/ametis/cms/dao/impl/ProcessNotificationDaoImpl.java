
package com.ametis.cms.dao.impl;


import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.ametis.cms.dao.BenefitDao;
import com.ametis.cms.dao.ProcessNotificationDao;
import com.ametis.cms.datamodel.Benefit;
import com.ametis.cms.datamodel.ProcessNotification;
import com.ametis.cms.util.dao.DaoSupportUtil;

// imports+ 

// imports- 

/**
 * BenefitDao adalah bean implementation untuk DAO tabel benefit.
*/
public class ProcessNotificationDaoImpl extends DaoSupportUtil implements ProcessNotificationDao

// extends+ 

// extends- 
{
	@Override
	public ProcessNotification create (ProcessNotification object) throws DataAccessException {
		this.getHibernateTemplate().save(object);
		return object;
	}
	@Override
	public ProcessNotification update (ProcessNotification object) throws DataAccessException{
 	    this.getHibernateTemplate().update(object);
	    return object;
	}
	@Override
	public ProcessNotification delete (ProcessNotification object) throws DataAccessException{
		this.getHibernateTemplate().delete(object);
		return object;
	}
	@Override
	public ProcessNotification get (java.io.Serializable pkey) throws DataAccessException {
		return (ProcessNotification) this.getHibernateTemplate().get (ProcessNotification.class, pkey);
	}
	@Override
	public Criteria getCriteria() throws Exception {
		HibernateTemplate template = this.getHibernateTemplate();
		SessionFactory sessionFactory = template.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(ProcessNotification.class);
		return criteria;
	}
	@Override
	public DetachedCriteria getDetachedCriteria() throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(ProcessNotification.class);
		return dc;
	}

// class+ 

// class- 

}
