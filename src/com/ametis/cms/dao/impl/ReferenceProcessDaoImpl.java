
package com.ametis.cms.dao.impl;


import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.ametis.cms.dao.BenefitDao;
import com.ametis.cms.dao.ReferenceProcessDao;
import com.ametis.cms.datamodel.Benefit;
import com.ametis.cms.datamodel.ReferenceProcess;
import com.ametis.cms.util.dao.DaoSupportUtil;

// imports+ 

// imports- 

public class ReferenceProcessDaoImpl extends DaoSupportUtil implements ReferenceProcessDao

// extends+ 

// extends- 
{
	public ReferenceProcess create (ReferenceProcess object) throws DataAccessException {
		this.getHibernateTemplate().save(object);
		return object;

	}
	public ReferenceProcess update (ReferenceProcess object) throws DataAccessException{
 	    this.getHibernateTemplate().update(object);
	    return object;
	}

	public ReferenceProcess delete (ReferenceProcess object) throws DataAccessException{
		this.getHibernateTemplate().delete(object);
		return object;
	}
	public ReferenceProcess get (java.io.Serializable pkey) throws DataAccessException {
		return (ReferenceProcess) this.getHibernateTemplate().get (ReferenceProcess.class, pkey);
	}


	public Criteria getCriteria() throws Exception {
		HibernateTemplate template = this.getHibernateTemplate();
		SessionFactory sessionFactory = template.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(ReferenceProcess.class);
		return criteria;
	}

	public DetachedCriteria getDetachedCriteria() throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(ReferenceProcess.class);
		return dc;
	}

// class+ 

// class- 

}
