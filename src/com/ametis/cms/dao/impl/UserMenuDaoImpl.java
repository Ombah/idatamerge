package com.ametis.cms.dao.impl;

import java.io.Serializable;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.ametis.cms.dao.UserMenuDao;
import com.ametis.cms.datamodel.UserMenu;
import com.ametis.cms.util.dao.DaoSupportUtil;

public class UserMenuDaoImpl extends DaoSupportUtil implements UserMenuDao {

	@Override
	public UserMenu create(UserMenu object) throws DataAccessException {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().save(object);
		return object;

	}

	@Override
	public UserMenu update(UserMenu object) throws DataAccessException {
		// TODO Auto-generated method stub
		   this.getHibernateTemplate().update(object);
		    return object;
	}

	@Override
	public UserMenu delete(UserMenu object) throws DataAccessException {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().delete(object);
		return object;
	}

	@Override
	public Criteria getCriteria() throws Exception {
		// TODO Auto-generated method stub
		HibernateTemplate template = this.getHibernateTemplate();
		SessionFactory sessionFactory = template.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(UserMenu.class);
		return criteria;
	}

	@Override
	public DetachedCriteria getDetachedCriteria() throws Exception {
		// TODO Auto-generated method stub
		DetachedCriteria dc = DetachedCriteria.forClass(UserMenu.class);
		return dc;
	}

	@Override
	public UserMenu get(Serializable pkey) throws DataAccessException {
		// TODO Auto-generated method stub
		return (UserMenu) this.getHibernateTemplate().get (UserMenu.class, pkey);
	}

}
