package com.ametis.cms.dao.impl;

import java.io.Serializable;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.ametis.cms.dao.TestingDao;
import com.ametis.cms.dao.TestingDao;
import com.ametis.cms.datamodel.Testing;
import com.ametis.cms.util.dao.DaoSupportUtil;

public class TestingDaoImpl extends DaoSupportUtil implements TestingDao{
	@Override
	public Testing create(Testing object) throws DataAccessException {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().save(object);
		return object;
	}

	@Override
	public Testing update(Testing object) throws DataAccessException {
		this.getHibernateTemplate().update(object);
	    return object;
	}

	@Override
	public Testing delete(Testing object) throws DataAccessException {
		this.getHibernateTemplate().delete(object);
		return object;
	}

	@Override
	public Testing get(Serializable pkey) throws DataAccessException {
		return (Testing) this.getHibernateTemplate().get (Testing.class, pkey);
	}

	@Override
	public Criteria getCriteria() throws Exception {
		HibernateTemplate template = this.getHibernateTemplate();
		SessionFactory sessionFactory = template.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Testing.class);
		return criteria;
	}

	@Override
	public DetachedCriteria getDetachedCriteria() throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(Testing.class);
		return dc;
	}
	public Session getTestingSession() throws Exception {
		// TODO Auto-generated method stub
		HibernateTemplate template = this.getHibernateTemplate();
		SessionFactory sessionFactory = template.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		
		return session;
	}

}
