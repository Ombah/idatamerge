package com.ametis.cms.dao.impl;

import java.io.Serializable;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.ametis.cms.dao.RegisterCallDao;
import com.ametis.cms.datamodel.RegisterCall;
import com.ametis.cms.util.dao.DaoSupportUtil;

public class RegisterCallDaoImpl extends DaoSupportUtil implements RegisterCallDao {

	@Override
	public Session getProductSession() throws Exception {
		// TODO Auto-generated method stub
		return getHibernateTemplate().getSessionFactory().getCurrentSession();
	}

	@Override
	public RegisterCall create(RegisterCall object) throws DataAccessException {
		// TODO Auto-generated method stub
		System.out.println("tes sebelum masuk hibernate regcall");
		this.getHibernateTemplate().save(object);
		System.out.println("tes setelah masuk hibernate regcall");
		return object;
	}

	@Override
	public RegisterCall update(RegisterCall object) throws DataAccessException {
		// TODO Auto-generated method stub
		  this.getHibernateTemplate().update(object);
		    return object;
	}

	@Override
	public RegisterCall delete(RegisterCall object) throws DataAccessException {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().delete(object);
		return object;
	}

	@Override
	public RegisterCall get(Serializable pkey) throws DataAccessException {
		// TODO Auto-generated method stub
		return (RegisterCall) this.getHibernateTemplate().get (RegisterCall.class, pkey);
	}

	@Override
	public Criteria getCriteria() throws Exception {
		// TODO Auto-generated method stub
		HibernateTemplate template = this.getHibernateTemplate();
		SessionFactory sessionFactory = template.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(RegisterCall.class);
		return criteria;
	}

	@Override
	public DetachedCriteria getDetachedCriteria() throws Exception {
		// TODO Auto-generated method stub
		DetachedCriteria dc = DetachedCriteria.forClass(RegisterCall.class);
		return dc;
	}
	@Override
	public Session getCaseSession() throws Exception {
		// TODO Auto-generated method stub

		HibernateTemplate template = this.getHibernateTemplate();
		SessionFactory sessionFactory = template.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		
		return session;
	}

}
