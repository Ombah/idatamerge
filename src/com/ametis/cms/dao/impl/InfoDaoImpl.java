package com.ametis.cms.dao.impl;

import java.io.Serializable;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.ametis.cms.dao.InfoDao;
import com.ametis.cms.datamodel.Info;
import com.ametis.cms.datamodel.ProcessNotification;
import com.ametis.cms.util.dao.DaoSupportUtil;

public class InfoDaoImpl extends DaoSupportUtil implements InfoDao {

	@Override
	public Info create(Info object) throws DataAccessException {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().save(object);
		return object;
	}

	@Override
	public Info update(Info object) throws DataAccessException {
		this.getHibernateTemplate().update(object);
	    return object;
	}

	@Override
	public Info delete(Info object) throws DataAccessException {
		this.getHibernateTemplate().delete(object);
		return object;
	}

	@Override
	public Info get(Serializable pkey) throws DataAccessException {
		return (Info) this.getHibernateTemplate().get (Info.class, pkey);
	}

	@Override
	public Criteria getCriteria() throws Exception {
		HibernateTemplate template = this.getHibernateTemplate();
		SessionFactory sessionFactory = template.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Info.class);
		return criteria;
	}

	@Override
	public DetachedCriteria getDetachedCriteria() throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(Info.class);
		return dc;
	}
	public Session getInfoSession() throws Exception {
		// TODO Auto-generated method stub
		HibernateTemplate template = this.getHibernateTemplate();
		SessionFactory sessionFactory = template.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		
		return session;
	}



//------------------------------------------------
// GAGAL TERUS -GAK SEMUA JALAN DENGAN BAIK - DINONAKTIFKAN
/*	public Collection searchInfo (Info object) throws Exception{

		HibernateTemplate template = getHibernateTemplate();
		SessionFactory sessionFactory = template.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(Info.class);
		criteria.add(Example.create(object));
		return criteria.list();
	}
*/
// class+ 

// class- 
}
