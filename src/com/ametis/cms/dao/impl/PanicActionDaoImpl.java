package com.ametis.cms.dao.impl;

import java.io.Serializable;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.ametis.cms.dao.InvestigationDao;
import com.ametis.cms.dao.PanicActionDao;
import com.ametis.cms.datamodel.PanicAction;
import com.ametis.cms.datamodel.PanicAction;
import com.ametis.cms.util.dao.DaoSupportUtil;

public class PanicActionDaoImpl  extends DaoSupportUtil implements PanicActionDao{

	@Override
	public PanicAction create(PanicAction object) throws DataAccessException {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().save(object);
		return object;
	}

	@Override
	public PanicAction update(PanicAction object) throws DataAccessException {
		this.getHibernateTemplate().update(object);
	    return object;
	}

	@Override
	public PanicAction delete(PanicAction object) throws DataAccessException {
		this.getHibernateTemplate().delete(object);
		return object;
	}

	@Override
	public PanicAction get(Serializable pkey) throws DataAccessException {
		return (PanicAction) this.getHibernateTemplate().get (PanicAction.class, pkey);
	}

	@Override
	public Criteria getCriteria() throws Exception {
		HibernateTemplate template = this.getHibernateTemplate();
		SessionFactory sessionFactory = template.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(PanicAction.class);
		return criteria;
	}

	@Override
	public DetachedCriteria getDetachedCriteria() throws Exception {
		DetachedCriteria dc = DetachedCriteria.forClass(PanicAction.class);
		return dc;
	}
	public Session getPanicActionSession() throws Exception {
		// TODO Auto-generated method stub
		HibernateTemplate template = this.getHibernateTemplate();
		SessionFactory sessionFactory = template.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		
		return session;
	}

}
