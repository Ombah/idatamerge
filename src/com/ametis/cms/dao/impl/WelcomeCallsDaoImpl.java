package com.ametis.cms.dao.impl;

import java.io.Serializable;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.ametis.cms.dao.WelcomeCallsDao;
import com.ametis.cms.datamodel.WelcomeCalls;
import com.ametis.cms.util.dao.DaoSupportUtil;

public class WelcomeCallsDaoImpl extends DaoSupportUtil implements WelcomeCallsDao {

	@Override
	public Session getProductSession() throws Exception {
		// TODO Auto-generated method stub
		return getHibernateTemplate().getSessionFactory().getCurrentSession();
	}

	@Override
	public WelcomeCalls create(WelcomeCalls object) throws DataAccessException {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().save(object);
		return object;
	}

	@Override
	public WelcomeCalls update(WelcomeCalls object) throws DataAccessException {
		// TODO Auto-generated method stub
		  this.getHibernateTemplate().update(object);
		    return object;
	}

	@Override
	public WelcomeCalls delete(WelcomeCalls object) throws DataAccessException {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().delete(object);
		return object;
	}

	@Override
	public WelcomeCalls get(Serializable pkey) throws DataAccessException {
		// TODO Auto-generated method stub
		return (WelcomeCalls) this.getHibernateTemplate().get (WelcomeCalls.class, pkey);
	}

	@Override
	public Criteria getCriteria() throws Exception {
		// TODO Auto-generated method stub
		HibernateTemplate template = this.getHibernateTemplate();
		SessionFactory sessionFactory = template.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(WelcomeCalls.class);
		return criteria;
	}

	@Override
	public DetachedCriteria getDetachedCriteria() throws Exception {
		// TODO Auto-generated method stub
		DetachedCriteria dc = DetachedCriteria.forClass(WelcomeCalls.class);
		return dc;
	}
	@Override
	public Session getCaseSession() throws Exception {
		// TODO Auto-generated method stub

		HibernateTemplate template = this.getHibernateTemplate();
		SessionFactory sessionFactory = template.getSessionFactory();
		Session session = sessionFactory.getCurrentSession();
		
		return session;
	}

}
