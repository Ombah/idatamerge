package com.ametis.cms.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;

import com.ametis.cms.datamodel.UserMenu;
import com.ametis.cms.datamodel.UserType;



public interface UserMenuDao {
	public UserMenu create (UserMenu object) throws DataAccessException;
	public UserMenu update (UserMenu object) throws DataAccessException;
	public UserMenu delete (UserMenu object) throws DataAccessException;
	public UserMenu get (java.io.Serializable pkey) throws DataAccessException ;
	public Criteria getCriteria() throws Exception ;
	public DetachedCriteria getDetachedCriteria() throws Exception ;
	
}
