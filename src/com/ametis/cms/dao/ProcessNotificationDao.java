
package com.ametis.cms.dao;


import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.dao.DataAccessException;

import com.ametis.cms.datamodel.Benefit;
import com.ametis.cms.datamodel.ProcessNotification;

// imports+ 

// imports- 

public interface ProcessNotificationDao 

// extends+ 

// extends- 
{	

	public ProcessNotification create (ProcessNotification object) throws DataAccessException ;
	public ProcessNotification update (ProcessNotification object) throws DataAccessException;
	public ProcessNotification  delete (ProcessNotification  object) throws DataAccessException;
	public ProcessNotification  get (java.io.Serializable pkey) throws DataAccessException ;
	public Criteria getCriteria() throws Exception ;
	public DetachedCriteria getDetachedCriteria() throws Exception ;

}
