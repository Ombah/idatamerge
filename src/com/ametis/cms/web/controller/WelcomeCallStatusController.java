package com.ametis.cms.web.controller;

import java.util.Collection;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.ametis.cms.service.WelcomeCallStatusService;
import com.ametis.cms.util.WebUtil;

public class WelcomeCallStatusController implements Controller{
	WelcomeCallStatusService welcomeCallStatusService;
	
	public WelcomeCallStatusService getWelcomeCallStatusService() {
		return welcomeCallStatusService;
	}

	public void setWelcomeCallStatusService(WelcomeCallStatusService welcomeCallStatusService) {
		this.welcomeCallStatusService = welcomeCallStatusService;
	}

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		String navigation = request.getParameter("navigation");
		if(navigation==null){
			navigation = "searchWelcomeCallStatus";
		}
		ModelAndView result = null;
		if(navigation.equalsIgnoreCase("sesuatu")){
			
		}else{
			result = searchWelcomeCallsStatus(request,response,"searchWelcomeCallStatus");
		}
		return result;
	}

	private ModelAndView searchWelcomeCallsStatus(HttpServletRequest request, HttpServletResponse response, String location) {
		// TODO Auto-generated method stub
		ModelAndView result = null;
		result = new ModelAndView(location);
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Integer index = WebUtil.getParameterInteger(request, "index");
		if (index == null){
			index = new Integer(1);			
		}
		request.setAttribute("index", new Integer(index));
		Collection collection = null;
		Vector vLikeP = new Vector();
		Vector vLikeQ = new Vector();
		Vector vEqP = new Vector();
		Vector vEqQ = new Vector();
		vEqP.add("deleted");
		vEqQ.add(0);
		String sLikeP[] = new String[vLikeP.size()];
		vLikeP.toArray(sLikeP);
		Object sLikeQ[] = new Object[vLikeP.size()];
		vLikeQ.toArray(sLikeQ);
		String sEqP[] = new String[vEqP.size()];
		vEqP.toArray(sEqP);
		Object sEqQ[] = new Object[vEqP.size()];
		vEqQ.toArray(sEqQ);
		
		try {
			collection = welcomeCallStatusService.getAll();			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println("tes size" + collection.size());
		result.addObject("WelcomeCallsStatus", collection);
		return result;
	}

}
