package com.ametis.cms.web.controller;

import java.util.Collection;
import java.util.Iterator;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.ametis.cms.datamodel.ActionResult;
import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Info;
import com.ametis.cms.datamodel.RegisterCall;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.service.InfoService;
import com.ametis.cms.service.SecurityService;
import com.ametis.cms.service.UserService;
import com.ametis.cms.util.WebUtil;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

public class InfoController implements Controller {
	
	InfoService infoService;
	SecurityService securityService;
	private Integer countSet;
	
	public Integer getCountSet() {
		return countSet;
	}

	public void setCountSet(Integer countSet) {
		this.countSet = countSet;
	}
	
	public SecurityService getSecurityService() {
		return securityService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	public InfoService getInfoService() {
		return infoService;
	}

	public void setInfoService(InfoService infoService) {
		this.infoService = infoService;
	}

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		String navigation = request.getParameter("navigation");
		System.out.println("tes "+navigation);
		navigation = WebUtil.getParameterString(request, "navigation", "");
		System.out.println("tes "+navigation);
		if(navigation==null){
			navigation = "searchInfo";
		}
		ModelAndView result = null;
		if(navigation.equalsIgnoreCase("hapus")){
			result = deletePerformed(request,response);
		}else if(navigation.equalsIgnoreCase("gosearch")){
			result = searchInfo(request, response, "searchInfo");
		}
		else{
			result = searchInfo(request,response,"searchInfo");
		}
		return result;
	}

	private ModelAndView deletePerformed(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView result = null;
		try{
			System.out.println("tes delete prfm");
			Integer infoId = WebUtil.getParameterInteger(request, "id");
			ActionUser user = securityService.getActionUser(request);
			Info info = infoService.get(infoId);
			Info res = infoService.delete(info, user);
			
			result = searchInfo(request, response, "searchInfo");
		}catch (Exception e) {
			// nanti kalo sudah OK codenya e.printStackTrace nya di hapus saja
			e.printStackTrace();
			result = new ModelAndView("error");
		}
		return result;
	}
	
	private ModelAndView searchInfo(HttpServletRequest request, HttpServletResponse response, String location) {
		// TODO Auto-generated method stub
		ModelAndView result = null;
		result = new ModelAndView(location);
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Integer index = WebUtil.getParameterInteger(request, "index");
		String arah = WebUtil.getParameterString(request, "arah", "");
		String searchtext = WebUtil.getParameterString(request, "searchtext", "");
		String searchby = WebUtil.getParameterString(request, "searchby", "");
		Integer searchStatus = WebUtil.getParameterInteger(request, "status");
		
		if (index == null){
			index = new Integer(1);			
		}
		Collection collection = null;
		Vector vLikeP = new Vector();
		Vector vLikeQ = new Vector();
		Vector vEqP = new Vector();
		Vector vEqQ = new Vector();
		vEqP.add("deletedStatus");
		vEqQ.add(0);
		
		if (searchby!=null && searchtext != null && !searchtext.equals("")) {
			if (searchby.equalsIgnoreCase("title")) {
				vLikeP.add("title");
				vLikeQ.add(searchtext);
			}
			if(searchby.equalsIgnoreCase("content")){
				vLikeP.add("content");
				vLikeQ.add(searchtext);
			}
		}
		
		String sLikeP[] = new String[vLikeP.size()];
		vLikeP.toArray(sLikeP);
		Object sLikeQ[] = new Object[vLikeP.size()];
		vLikeQ.toArray(sLikeQ);
		String sEqP[] = new String[vEqP.size()];
		vEqP.toArray(sEqP);
		Object sEqQ[] = new Object[vEqP.size()];
		vEqQ.toArray(sEqQ);
		
		Vector vSortP = new Vector();
		vSortP.add("createdTime");
		
		String sSortP[] = new String[vSortP.size()];
		vSortP.toArray(sSortP);
		
		
		int minIndex = 0;
		int maxIndex = 0;
		int totalIndex = 0;

		
		
		int rowsetint = 0;
		int count = 0;
		try{
			count = infoService.getTotal(sLikeP, sLikeQ, sEqP, sEqQ);	
		}catch(Exception e){
			
		}
		
		if (index == null)
			index = new Integer(1);
		if (arah.equals("kanan"))
			index = new Integer(index.intValue() + 1);
		else if (arah.equals("kiri"))
			index = new Integer(index.intValue() - 1);
		else if (arah.equals("kiribgt"))
			index = new Integer(1);
		else if (arah.equals("kananbgt"))
			index = new Integer(count / countSet.intValue() + 1);
		if (index.compareTo(new Integer(1)) == new Integer(-1).intValue())
			index = new Integer(1);
		else if (index.compareTo(new Integer(count / countSet.intValue() + 1)) == new Integer(1).intValue())
			index = new Integer(count / countSet.intValue() + 1);

		rowsetint = (new Integer((index.intValue() - 1) * countSet.intValue())).intValue();
		if (count % countSet.intValue() > 0) {
			result.addObject("halAkhir", new Integer(count / countSet.intValue() + 1));
		} else {
			result.addObject("halAkhir", new Integer(count / countSet.intValue()));
		}

		minIndex = (index - 1) * countSet;
		maxIndex = index * countSet;

		if (maxIndex > count) {
			maxIndex = count;
		}
		try {
			collection = infoService.search(sLikeP, sLikeQ, sEqP, sEqQ, null, null, null, false, sSortP,null, rowsetint, countSet.intValue());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println("tes size" + collection.size());
		request.setAttribute("index", new Integer(index));
		request.setAttribute("navigation", navigation);
		request.setAttribute("countSet", countSet);
		request.setAttribute("count", new Integer(count));
		request.setAttribute("alert", request.getParameter("alert"));
		request.setAttribute("minIndex", new Integer(minIndex));
		request.setAttribute("maxIndex", new Integer(maxIndex));
		result.addObject("Infos", collection);
		return result;
	}
	

}
