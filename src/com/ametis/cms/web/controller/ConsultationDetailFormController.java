package com.ametis.cms.web.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.JDBCException;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.ametis.cms.datamodel.ActionResult;
import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Bank;
import com.ametis.cms.datamodel.BankAccount;
import com.ametis.cms.datamodel.ClaimReceiving;
import com.ametis.cms.datamodel.Consultation;
import com.ametis.cms.datamodel.ConsultationDetail;
import com.ametis.cms.datamodel.Product;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.service.ConsultationDetailService;
import com.ametis.cms.service.ConsultationService;
import com.ametis.cms.service.SecurityService;
import com.ametis.cms.service.UserService;
import com.ametis.cms.util.WebUtil;
import com.ametis.cms.web.form.BankAccountForm;
import com.ametis.cms.web.form.ClaimReceivingForm;
import com.ametis.cms.web.form.ConsultationDetailForm;
import com.ametis.cms.web.form.ProductForm;
import com.ametis.cms.webservice.MobileApplicationWebServiceImpl;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

import atg.taglib.json.util.JSONObject;

public class ConsultationDetailFormController extends SimpleFormController {

	String GOOGLE_SERVER_KEY = MobileApplicationWebServiceImpl.GOOGLE_SERVER_KEY;
	static final String MESSAGE_KEY = "message";
	
	ConsultationDetailService consultationDetailService;
	ConsultationService consultationService;
	UserService userService;
	SecurityService securityService;
	String createdByOrigin = "";
	Timestamp createdTimeOrigin = null;
	Integer createdByIdOrigin;

	Integer consultationIdOri = null;
	
	public ConsultationService getConsultationService() {
		return consultationService;
	}

	public void setConsultationService(ConsultationService consultationService) {
		this.consultationService = consultationService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public SecurityService getSecurityService() {
		return securityService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	public ConsultationDetailService getConsultationDetailService() {
		return consultationDetailService;
	}

	public void setConsultationDetailService(ConsultationDetailService consultationDetailService) {
		this.consultationDetailService = consultationDetailService;
	}

	public ConsultationDetailFormController() {
		setSessionForm(true);
		setValidateOnBinding(true);
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		Object result = null;
		ConsultationDetailForm tmp = null;
		Integer id=null;
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		if(navigation.equalsIgnoreCase("tambah")){
			
		}else{
			id = WebUtil.getParameterInteger(request, "id");			
		}
		
	
		Integer index = WebUtil.getParameterInteger(request, "index");
		String title = WebUtil.getParameterString(request, "title", "");
		String createdBy = WebUtil.getParameterString(request, "createdBy", "");
	
		Integer consultationId = WebUtil.getParameterInteger(request, "consultationId");
		
		if(consultationId!=null){
			consultationIdOri = consultationId;
		}
		System.out.println("consultationId baris 110"+consultationId);
		editConsultationStatus(consultationId, request);
		String message = WebUtil.getParameterString(request, "message", "");
		String modifiedBy = WebUtil.getParameterString(request, "modifiedBy", "");
		
		/*
		 * ini ntar di uncomment aja .. trus yang tra --> ini diganti variable
		 * primary key nya
		 */
		if (id != null) {
//			System.out.println("id not null");
			java.io.Serializable pkey = id;
			ConsultationDetail object = consultationDetailService.get(pkey);
			
			if (object != null) { // edit object
//				System.out.println("objek not null");
				tmp = new ConsultationDetailForm();
				if(object.getId()!=null){
					tmp.setId(object.getId());
				}
				if(object.getMessage()!=null){
					tmp.setMessage(object.getMessage());
				}
				if(object.getCreatedBy()!=null){
//					System.out.println("createdby dari table tidak null");
					createdByOrigin = object.getCreatedBy();
					tmp.setCreatedBy(object.getCreatedBy());
				}
				if(object.getCreatedTime()!=null){
					createdTimeOrigin = object.getCreatedTime();
					tmp.setCreatedTime(object.getCreatedTime());
				}
				if(object.getModifiedBy()!=null){
					tmp.setModifiedBy(object.getModifiedBy());
				}
				if(object.getModifiedTime()!=null ){
					tmp.setModifiedTime(object.getModifiedTime());
				}

				// -- foreign affairs end
			} else {// object tidak ditemukan ?? anggap kita mau bikin baru !
//				System.out.println("objek null");
				tmp = new ConsultationDetailForm();
				// foreign affairs
				tmp.setMessage(message);
				tmp.setCreatedBy(createdBy);
				

				// -- foreign affairs end
			}
		} // mau edit end
		else { // bikin baru
//			System.out.println("bikin baru");
			tmp = new ConsultationDetailForm();
			// foreign affairs
			tmp.setMessage(message);
			tmp.setCreatedBy(createdBy);
		}
		request.setAttribute("navigation", navigation);
		request.setAttribute("consultationId", consultationId);
		request.setAttribute("message", message);
		request.setAttribute("createdBy",createdBy);
		request.setAttribute("title",title );
		request.setAttribute("index", index);
		
		result = tmp;
		return result;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {
		Integer id = WebUtil.getParameterInteger(request, "id");
//		System.out.println("id di bind = "+id);
		Integer index = WebUtil.getParameterInteger(request, "index");
		String title = WebUtil.getParameterString(request, "title", "");
		String createdBy = WebUtil.getParameterString(request, "createdBy", "");
//		System.out.println("created by di bind = "+createdBy);
		Integer consultationId = WebUtil.getParameterInteger(request, "consultationId");
		consultationId = consultationIdOri;
		
		String message = WebUtil.getParameterString(request, "message", "");
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		ConsultationDetailForm consultationDetailForm = (ConsultationDetailForm) command;
		ConsultationDetail consultationDetail = consultationDetailForm.getConsultationDetailBean();
		request.setAttribute("id", id);
		request.setAttribute("navigation", navigation);
		request.setAttribute("consultationId", consultationId);
		request.setAttribute("message", message);
		request.setAttribute("createdBy",createdBy );
		request.setAttribute("title",title );
		request.setAttribute("index", index);
	}

	private void editConsultationStatus(Integer consultationId, HttpServletRequest request) {
		try{
			Consultation consultation = consultationService.get(consultationId);
			ActionUser user = securityService.getActionUser(request);
			if(consultation.getStatus()==Consultation.MESSAGE_NOT_READ){
				consultation.setStatus(Consultation.MESSAGE_READ);				
			}
			consultationService.update(consultation, user);
		}catch(Exception ee){
			
		}
		
	}

	protected Map referenceData(HttpServletRequest request) throws Exception {
		Map model = new HashMap();
		Collection collection = null;
		Integer id = WebUtil.getParameterInteger(request, "id");
		Integer consultationId = WebUtil.getParameterInteger(request, "consultationId");
		consultationId = consultationIdOri;
		
		System.out.println("consultationId baris 207 "+consultationId);
		Consultation cc = null;		
//		Consultation cc = consultationService.get(152);
		if(id==null){
			cc = consultationService.get(consultationId);
		}else{
			cc = consultationService.get(id);
			
		}
		Vector vEqP = new Vector();
		Vector vEqQ = new Vector();
		vEqP.add("deletedStatus");
		vEqQ.add(Integer.valueOf(0));
		
		if(id!=null){
			vEqP.add("consultationId.consultationId");
//			vEqP.add("consultation.id");
//			vEqQ.add(consultationId);
//			vEqQ.add(cc.getConsultationId());
			vEqQ.add(id);
		}
		String sEqP[] = new String[vEqP.size()];
		vEqP.toArray(sEqP);
		Object sEqQ[] = new Object[vEqP.size()];
		vEqQ.toArray(sEqQ);
		
		int total =  consultationDetailService.getTotal(null, null, sEqP, sEqQ);
		collection = consultationDetailService.search(null,null,sEqP, sEqQ,false,"modifiedTime" ,0,total);

//		collection = consultationDetailService.getAll();
//		collection = consultationDetailService.getAll();
//		model.addObject("ConsultationDetails", collection);
		model.put("ConsultationDetails", collection);
		request.setAttribute("consultationId", consultationId);
		System.out.println("consultatoin Id where are you"+consultationId);
		return model;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		
		Integer id = WebUtil.getParameterInteger(request, "id");
	
		Integer consultationId = WebUtil.getParameterInteger(request, "consultationId");
		consultationId = consultationIdOri;
//		consultationId = id;
//		System.out.println("consultationId baris 246 "+consultationId);
//		if(consultationId==null){
//			return new ModelAndView(new RedirectView("consultation"));
//		}
//		System.out.println("consultationId baris 246 "+id);
//		System.out.println("consultationId baris 246 "+consultationIdOri);
		
		Integer index = WebUtil.getParameterInteger(request, "index");
		String title = WebUtil.getParameterString(request, "title", "");
		String createdBy = WebUtil.getParameterString(request, "createdBy", "");
		
		String question = WebUtil.getParameterString(request, "question", "");
		String message = WebUtil.getParameterString(request, "message", "");
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		 
		ConsultationDetailForm consultationDetailForm = (ConsultationDetailForm) command;
		if(id!=null){
			consultationDetailForm.setIsNewConsultationDetail(false);
		}
		ConsultationDetail res = null;
		String alertMsg = "";
		
		try {
			ActionUser user = securityService.getActionUser(request);
			consultationDetailForm.getConsultationDetailBean().setId(id);
			consultationDetailForm.getConsultationDetailBean().setMessage(message);
			consultationDetailForm.getConsultationDetailBean().setCreatedBy(createdByOrigin);
			consultationDetailForm.getConsultationDetailBean().setCreatedTime(createdTimeOrigin);
			consultationDetailForm.getConsultationDetailBean().setDeletedStatus(0);
			consultationDetailForm.getConsultationDetailBean().setModifiedBy(user.getUser().getUsername());
			if (consultationDetailForm.isNewConsultationDetail()) {
//				System.out.println("new cons");
				boolean isUserAuthorized = securityService.isUserAuthorized(user, "CREATECONSULTATIONDETAIL");
				if (!isUserAuthorized) {
					ModelAndView errorResult = new ModelAndView(new RedirectView("errorpage"));
					errorResult.addObject("errorType", "accessDenied");
					errorResult.addObject("errorMessage", "You Are Not Authorized for CREATECONSULTATIONDETAIL access");
					return errorResult;
				}
				ConsultationDetail consultationDetail = consultationDetailForm.getConsultationDetailBean();
				if (consultationDetail != null) {
//					System.out.println("cons bean not null");
//					System.out.println("tes coba masuk ga nih sebelum cteate");
					Consultation cc = new Consultation();
//					Consultation cc = consultationService.get(consultationId);
					cc.setConsultationId(consultationId);
//					System.out.println("cc id" + id);
					
					consultationDetail.setConsultationId(cc);
					System.out.println("tes isi message "+ consultationDetail.getMessage());
					consultationDetail.setIsRead(ConsultationDetail.STATUS_UNREAD);
					res = consultationDetailService.create(consultationDetail, user);
					Consultation consultation = consultationService.searchUnique("consultationId", consultationId);
					//System.out.println("consultation id dapat user id"+ccc.getCreatedById());
//					consultation.setModifiedBy(user.getUser().getUsername());
//					consultation.setModifiedTime(new Timestamp(new Date().getDate()));
					StringTokenizer st = new StringTokenizer(message, System.getProperty("line.separator"));
					String preview = st.nextToken();
					if(preview.length()>90){
						consultation.setPreviewContent(preview.substring(0,85)+"...");
					}else{
						consultation.setPreviewContent(preview);
					}
					consultation.setStatus(Consultation.MESSAGE_REPLIED);
					consultationService.update(consultation, user);
//					doMobileAnnouncementByUserId(""+consultation.getCreatedById(), "you have new consultation message", consultationId);
					doMobileAnnouncementByUserId(consultation);
//					consultationDetailService.create(consultationDetail, user);
					System.out.println("tes coba masuk ga nih setelah create");
				}
			} else {
//				System.out.println("not new cons");
				Date date = new Date();
				consultationDetailForm.getConsultationDetailBean().setModifiedTime(new Timestamp(date.getTime()));
				res = consultationDetailService.update(consultationDetailForm.getConsultationDetailBean(), user);
			}
		}catch (JDBCException ex){
			System.out.println("jdbc exc");
			ex.getSQLException().getNextException().printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
			return showForm(request, response, errors);
		}
		
		request.setAttribute("id", id);
		request.setAttribute("navigation", navigation);
		request.setAttribute("consultationId", consultationId);
		request.setAttribute("message", message);
		request.setAttribute("createdBy",createdBy );
		request.setAttribute("index", index);
		return new ModelAndView(new RedirectView("consultation"));
		// return super.onSubmit(request, response, command, errors);
	}
	
	public Collection<ActionResult> doMobileAnnouncementByUserId(
			Consultation consultation) {

		User theUser;
		Integer userId = consultation.getUserId().getUserId();
//		try {
//			theUser = userService.search(null, null, "userId", userId, 0, 1);
//		}catch(Exception e){
//			
//		}
		Collection<ActionResult> result = null;
		result = new Vector();
		System.out.println("masuk domobileannouncementbydeviceid");
		
		Result sentAnnouncementResult = null;
				
		try {
//			theUser = userService.search(null, null, "mobileDeviceId", mobileDeviceId, 0, 10);
//			theUser = userService.search(null, null, "userId.userId", userId, 0, 1);
			theUser = userService.get(userId);
				
//			User user = userIterator.next();

			 JSONObject obj = new JSONObject();

		      obj.put("type","consultation");
		      obj.put("consultationId",""+consultation.getConsultationId());
		      obj.put("title", consultation.getModifiedBy()+" - "+consultation.getTitle());
		      obj.put("myMessage", consultation.getPreviewContent());
		      System.out.print(obj);

			boolean resultSend = false;
			if(theUser.getOperatingSystem()!=null && theUser.getOperatingSystem().equalsIgnoreCase("android")){
				resultSend = sendMessageAndroid(theUser, obj);							
			}else{
				resultSend = sendMessageIOS(theUser);							
			}
//			resultSend = sendMessage(theUser);
//			ActionResult actionResult = new ActionResult();
//			actionResult.setResult(resultSend);
//			actionResult.setReason("send");
//			result.add(actionResult);
			
			
			ActionResult actionResult = new ActionResult();
			actionResult.setResult(true);
			actionResult.setReason("Sent announcement to " + theUser.getUsername() + " with deviceId : " + theUser.getMobileDeviceId());
			System.out.println("Sent announcement to " + theUser.getUsername() + " with deviceId : " + theUser.getMobileDeviceId());
			
			result.add(actionResult);
//			if(theUser != null){
//				Iterator<User> userIterator = theUser.iterator();
//				while(userIterator.hasNext()){
//					User user = userIterator.next();
//					System.out.println("Found " + theUser.size() + " user with mobileDeviceId : "+user.getMobileDeviceId());
//					
//					Sender sender = new Sender(GOOGLE_SERVER_KEY);
//					com.google.android.gcm.server.Message message = new com.google.android.gcm.server.Message.Builder().timeToLive(30)
//							.delayWhileIdle(true).addData(MESSAGE_KEY, messageAnnouncement).build();
//					
//					sentAnnouncementResult = sender.send(message, user.getMobileDeviceId(), 1);
//					System.out.println("Mobile Announcement Result : " + sentAnnouncementResult.toString());
//					
//					ActionResult actionResult = new ActionResult();
//					actionResult.setResult(true);
//					actionResult.setReason("Sent announcement to " + user.getUsername() + " with deviceId : " + user.getMobileDeviceId());
//					System.out.println("Sent announcement to " + user.getUsername() + " with deviceId : " + user.getMobileDeviceId());
//					
//					result.add(actionResult);
//				}
//			}
//			else{
//				ActionResult res = new ActionResult();
//				res.setResult(false);
////				res.setReason("Can\'t find device with ID : " + mobileDeviceId);
//				
//				result.add(res);
//			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	public boolean sendMessageIOS(User theUser) {

	    //config
	    String apiKey = "AIzaSyCFr2HkZILX1BGk_y8vazimRaVQzdr_tPU"; // Put here your API key
	    String GCM_Token = theUser.getMobileDeviceId(); // put the GCM Token you want to send to here
	    String notification = "{\"sound\":\"default\",\"title\":\"Consultation Received\",\"body\":\"Your Consultation has been Answered\"}"; // put the message you want to send here
	    String messageToSend = "{\"to\":\"" + GCM_Token + "\",\"notification\":" + notification + "}"; // Construct the message.
	    
	        try {

	            // URL
	            URL url = new URL("https://android.googleapis.com/gcm/send");

	            System.out.println(messageToSend);
	            // Open connection
	            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

	            // Specify POST method
	            conn.setRequestMethod("POST");

	            //Set the headers
	            conn.setRequestProperty("Content-Type", "application/json");
	            conn.setRequestProperty("Authorization", "key=" + apiKey);
	            conn.setDoOutput(true);

	            //Get connection output stream
	            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());

	            byte[] data = messageToSend.getBytes("UTF-8");
	            wr.write(data);

	            //Send the request and close
	            wr.flush();
	            wr.close();

	            //Get the response
	            int responseCode = conn.getResponseCode();
	            System.out.println("\nSending 'POST' request to URL : " + url);
	            System.out.println("Response Code : " + responseCode);

	            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	            String inputLine;
	            StringBuffer response = new StringBuffer();

	            while ((inputLine = in.readLine()) != null) {
	                response.append(inputLine);
	            }
	            in.close();
	            
	            //Print result
	            System.out.println(response.toString()); //this is a good place to check for errors using the codes in http://androidcommunitydocs.com/reference/com/google/android/gcm/server/Constants.html
	            return true;
	            
	        } catch (MalformedURLException e) {
	            e.printStackTrace();
	            return false;
	        } catch (IOException e) {
	            e.printStackTrace();
	            return false;
	        }
	}
	public boolean sendMessageAndroid(User theUser, JSONObject obj) {
		try {
			Sender sender = new Sender(GOOGLE_SERVER_KEY);
			//String myMessage = "'to' : '"
				//	+theUser.getMobileDeviceId()+"', 'content-available' : 1, 'priority': 10, 'notification' : {'body' : 'asdfasdf(isinya)', 'title' : 'consultation (judulnya)', 'sound': 'default'}, 'data' : {'type' : 'consultation', 'consultationId' : '462''myMessage' : 'asdfasdf'}";
			com.google.android.gcm.server.Message message = new com.google.android.gcm.server.Message.Builder()
					.timeToLive(30).delayWhileIdle(true).addData(MESSAGE_KEY, obj.toString()).build();
//			com.google.android.gcm.server.Message message = new com.google.android.gcm.server.Message.Builder()
//				.timeToLive(30).delayWhileIdle(true).addData("aps", "{'alert':'haloo','badge':'9','sound':'bingbong.aiff'}").build();
//			com.google.android.gcm.server.Message message = new com.google.android.gcm.server.Message.Builder()
//					.timeToLive(30).delayWhileIdle(true).addData(MESSAGE_KEY, myMessage).build();

			Result sentAnnouncementResult = sender.send(message, theUser.getMobileDeviceId(), 1);
			System.out.println("Mobile Announcement Result : " + sentAnnouncementResult.toString());

			ActionResult actionResult = new ActionResult();
			actionResult.setResult(true);
			actionResult.setReason("Sent announcement to " + theUser.getUsername() + " with deviceId : "
					+ theUser.getMobileDeviceId());
			System.out.println("Sent announcement to " + theUser.getUsername() + " with deviceId : "
					+ theUser.getMobileDeviceId());
			return true;
		} catch (Exception e) {
			return false;
		}

	}

}
