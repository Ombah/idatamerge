package com.ametis.cms.web.controller;

import java.util.Collection;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Bank;
import com.ametis.cms.datamodel.Consultation;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.service.ConsultationService;
import com.ametis.cms.service.SecurityService;
import com.ametis.cms.util.LogUtil;
import com.ametis.cms.util.WebUtil;

public class ConsultationController implements Controller {
	
	ConsultationService consultationService;
	SecurityService securityService;
	private Integer countSet;
	
	public Integer getCountSet() {
		return countSet;
	}

	public void setCountSet(Integer countSet) {
		this.countSet = countSet;
	}

	public SecurityService getSecurityService() {
		return securityService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	public ConsultationService getConsultationService() {
		return consultationService;
	}

	public void setConsultationService(ConsultationService consultationService) {
		this.consultationService = consultationService;
	}

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		String navigation = request.getParameter("navigation");
		ModelAndView result = null;
		if(navigation==null){

//			result = searchConsultation(request,response,"searchConsultation");			
//			navigation = "searchConsultation";
			result = searchConsultation(request,response,"searchConsultation");		
		}else if(navigation.equalsIgnoreCase("jsontotalnewconsultation")){
			result = jsonTotalNewConsultation(request,response);
		}else if(navigation.equalsIgnoreCase("gosearch")){
			result = searchConsultation(request, response, "searchConsultation");
		}
		else if(navigation.equalsIgnoreCase("detail")){
			result = detailPerformed(request, response, "");			
		}
		else{
			result = searchConsultation(request,response,"searchConsultation");			
		}
		return result;
	}
	
	public ModelAndView jsonTotalNewConsultation(HttpServletRequest request, HttpServletResponse response){
		ModelAndView result = null;

		try {
			result = new ModelAndView("jsonTotalNewConsultation");
			User currentUser = securityService.getCurrentUser(request);

			int total = 0;
			if (currentUser != null) {

				String[] eqParam = { "deletedStatus", "status" };
				Object[] eqValue = { Integer.valueOf(0), Integer.valueOf(0)};
				total = consultationService.getTotal(null, null, eqParam, eqValue);
				System.out.println("tes "+total);
			}
			result.addObject("result", total);

		} catch (Exception e) {
			e.printStackTrace();
			result = new ModelAndView("error");
		}

		return result;
	
	}

	private ModelAndView searchConsultation(HttpServletRequest request, HttpServletResponse response, String location) {
		// TODO Auto-generated method stub
		ModelAndView result = null;
		result = new ModelAndView(location);
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Integer index = WebUtil.getParameterInteger(request, "index");

		String searchtext = WebUtil.getParameterString(request, "searchtext", "");
		String searchby = WebUtil.getParameterString(request, "searchby", "");
		Integer searchStatus = WebUtil.getParameterInteger(request, "status");
		System.out.println("tes " + searchby + " " + searchtext + " " + searchStatus);
		if (index == null){
			index = new Integer(1);			
		}
		
		request.setAttribute("index", new Integer(index));
		String arah = WebUtil.getParameterString(request, "arah", "");

		int minIndex = 0;
		int maxIndex = 0;
		int totalIndex = 0;

		
		Collection collection = null;
	
		Vector vLikeP = new Vector();
		Vector vLikeQ = new Vector();
		Vector vEqP = new Vector();
		Vector vEqQ = new Vector();
		vEqP.add("deletedStatus");
		vEqQ.add(0);
		
		if(searchStatus!=null && searchStatus>=0){
			vEqP.add("status");
			vEqQ.add(searchStatus);
		}
		
		if (searchby != null && searchtext != null && !searchtext.equals("")) {
			if (searchby.equalsIgnoreCase("title")) {
				vLikeP.add("title");
				vLikeQ.add(searchtext);
			}
			if(searchby.equalsIgnoreCase("customerFullName")){
				vLikeP.add("memberId.fullName");
				vLikeQ.add(searchtext);
			}
		}
		
		String sLikeP[] = new String[vLikeP.size()];
		vLikeP.toArray(sLikeP);
		Object sLikeQ[] = new Object[vLikeP.size()];
		vLikeQ.toArray(sLikeQ);
		String sEqP[] = new String[vEqP.size()];
		vEqP.toArray(sEqP);
		Object sEqQ[] = new Object[vEqP.size()];
		vEqQ.toArray(sEqQ);
		Vector vSortP = new Vector();
		vSortP.add("modifiedTime");
		vSortP.add("createdTime");
		
		String sSortP[] = new String[vSortP.size()];
		vSortP.toArray(sSortP);
		
		int rowsetint = 0;
		int count = 0;
		try{
			count =  consultationService.getTotal(sLikeP, sLikeQ, sEqP, sEqQ);	
		}catch(Exception e){
			
		}
		
		if (index == null)
			index = new Integer(1);
		if (arah.equals("kanan"))
			index = new Integer(index.intValue() + 1);
		else if (arah.equals("kiri"))
			index = new Integer(index.intValue() - 1);
		else if (arah.equals("kiribgt"))
			index = new Integer(1);
		else if (arah.equals("kananbgt"))
			index = new Integer(count / countSet.intValue() + 1);
		if (index.compareTo(new Integer(1)) == new Integer(-1).intValue())
			index = new Integer(1);
		else if (index.compareTo(new Integer(count / countSet.intValue() + 1)) == new Integer(1).intValue())
			index = new Integer(count / countSet.intValue() + 1);

		rowsetint = (new Integer((index.intValue() - 1) * countSet.intValue())).intValue();
		if (count % countSet.intValue() > 0) {
			result.addObject("halAkhir", new Integer(count / countSet.intValue() + 1));
		} else {
			result.addObject("halAkhir", new Integer(count / countSet.intValue()));
		}

		minIndex = (index - 1) * countSet;
		maxIndex = index * countSet;

		if (maxIndex > count) {
			maxIndex = count;
		}

		try {
			collection = consultationService.search(sLikeP, sLikeQ, sEqP, sEqQ, false, sSortP, rowsetint, countSet.intValue());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("index", new Integer(index));
		request.setAttribute("navigation", navigation);
		request.setAttribute("countSet", countSet);
		request.setAttribute("count", new Integer(count));
		request.setAttribute("alert", request.getParameter("alert"));
		request.setAttribute("minIndex", new Integer(minIndex));
		request.setAttribute("maxIndex", new Integer(maxIndex));

//		System.out.println("tes size" + collection.size());
		result.addObject("Consultations", collection);
		return result;
	}
	
	public ModelAndView detailPerformed(HttpServletRequest request, HttpServletResponse response, String location) throws Exception {
		Integer consultationId = WebUtil.getParameterInteger(request, "consultationId");
		Consultation consultation = consultationService.get(consultationId);
		ActionUser user = securityService.getActionUser(request);
		
		consultation.setStatus(Consultation.MESSAGE_READ);
		consultationService.update(consultation, user);
		String id = WebUtil.getParameterString(request, "id", "");
		System.out.println("consultation id ="+consultationId+" id = "+id);
		return new ModelAndView(new RedirectView("consultationdetail-form?consultationId="+consultationId));			
    }

}
