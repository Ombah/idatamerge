package com.ametis.cms.web.controller;

import java.util.Collection;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.ametis.cms.service.WelcomeCallCategoryService;
import com.ametis.cms.util.WebUtil;

public class WelcomeCallCategoryController implements Controller{
WelcomeCallCategoryService welcomeCallCategoryService;
	
	public WelcomeCallCategoryService getWelcomeCallCategoryService() {
		return welcomeCallCategoryService;
	}

	public void setWelcomeCallCategoryService(WelcomeCallCategoryService welcomeCallCategoryService) {
		this.welcomeCallCategoryService = welcomeCallCategoryService;
	}

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		String navigation = request.getParameter("navigation");
		if(navigation==null){
			navigation = "searchWelcomeCallCategory";
		}
		ModelAndView result = null;
		if(navigation.equalsIgnoreCase("sesuatu")){
			
		}else{
			result = searchWelcomeCallCategory(request,response,"searchWelcomeCallCategory");
		}
		return result;
	}

	private ModelAndView searchWelcomeCallCategory(HttpServletRequest request, HttpServletResponse response, String location) {
		// TODO Auto-generated method stub
		ModelAndView result = null;
		result = new ModelAndView(location);
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Integer index = WebUtil.getParameterInteger(request, "index");
		if (index == null){
			index = new Integer(1);			
		}
		request.setAttribute("index", new Integer(index));
		Collection collection = null;
		Vector vLikeP = new Vector();
		Vector vLikeQ = new Vector();
		Vector vEqP = new Vector();
		Vector vEqQ = new Vector();
		vEqP.add("deleted");
		vEqQ.add(0);
		String sLikeP[] = new String[vLikeP.size()];
		vLikeP.toArray(sLikeP);
		Object sLikeQ[] = new Object[vLikeP.size()];
		vLikeQ.toArray(sLikeQ);
		String sEqP[] = new String[vEqP.size()];
		vEqP.toArray(sEqP);
		Object sEqQ[] = new Object[vEqP.size()];
		vEqQ.toArray(sEqQ);
		
		try {
			collection = welcomeCallCategoryService.getAll();			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println("tes size" + collection.size());
		result.addObject("WelcomeCallCategorys", collection);
		return result;
	}
}
