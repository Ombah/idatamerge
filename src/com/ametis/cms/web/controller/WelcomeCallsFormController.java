package com.ametis.cms.web.controller;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Member;
import com.ametis.cms.datamodel.WelcomeCalls;
import com.ametis.cms.service.WelcomeCallsService;
import com.ametis.cms.service.MemberService;
import com.ametis.cms.service.SecurityService;
import com.ametis.cms.util.WebUtil;
import com.ametis.cms.web.form.WelcomeCallsForm;

public class WelcomeCallsFormController extends SimpleFormController {
	WelcomeCallsService welcomeCallsService;
	SecurityService securityService;
	
	String createdByOrigin = "";
	Timestamp createdTimeOrigin = null;
	Member memberIdOrigin = null;
	String memberNameOrigin = null;
	Integer logTypeOrigin = null;
	

	public SecurityService getSecurityService() {
		return securityService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	public WelcomeCallsService getWelcomeCallsService() {
		return welcomeCallsService;
	}

	public void setWelcomeCallsService(WelcomeCallsService welcomeCallsService) {
		this.welcomeCallsService = welcomeCallsService;
	}

	public WelcomeCallsFormController() {
		setSessionForm(true);
		setValidateOnBinding(true);
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		Object result = null;
		WelcomeCallsForm tmp = null;
		Integer id=null;
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		if(navigation.equalsIgnoreCase("tambah")){
		}else{
			id = WebUtil.getParameterInteger(request, "welcomeCallsId");
		}
		
		System.out.println("id di fob = "+id);
		Integer index = WebUtil.getParameterInteger(request, "index");
		String createdBy = WebUtil.getParameterString(request, "createdBy", "");
		String modifiedBy = WebUtil.getParameterString(request, "modifiedBy", "");
		
		
		/*
		 * ini ntar di uncomment aja .. trus yang tra --> ini diganti variable
		 * primary key nya
		 */
		Date date = new Date();
		if (id != null) {
			System.out.println("id not null");
			java.io.Serializable pkey = id;
			WelcomeCalls object = welcomeCallsService.get(pkey);
			
			if (object != null) { // edit object
				System.out.println("objek not null");
				tmp = new WelcomeCallsForm();
				if(object.getId()!=null){
					tmp.setId(object.getId());
				}
				if(object.getCreatedBy()!=null){
					createdByOrigin = object.getCreatedBy();
					tmp.setCreatedBy(object.getCreatedBy());
				}
				if(object.getCreatedTime()!=null){
					createdTimeOrigin = object.getCreatedTime();
					tmp.setCreatedTime(object.getCreatedTime());
				}
				if(object.getModifiedBy()!=null){
					tmp.setModifiedBy(object.getModifiedBy());
				}
				if(object.getModifiedTime()!=null ){
					tmp.setModifiedTime(object.getModifiedTime());
				}
				if(object.getStatus()!=null){
					tmp.setStatus(object.getStatus());
				}
				if(object.getTelephoneNumber()!=null){
					tmp.setTelephoneNumber(object.getTelephoneNumber());
				}
				if(object.getMemberId().getMemberId()!=null){
					memberIdOrigin = object.getMemberId();
					tmp.setMemberId(object.getMemberId().getMemberId());
				}
				if(object.getLogType()!=null){
					logTypeOrigin = object.getLogType();
					tmp.setLogType(object.getLogType());
				}
				if(object.getMemberName()!=null){
					memberNameOrigin = object.getMemberName();
					tmp.setMemberName(object.getMemberName());
				}

				// -- foreign affairs end
			} else {// object tidak ditemukan ?? anggap kita mau bikin baru !
				System.out.println("objek null");
				tmp = new WelcomeCallsForm();
				// foreign affairs
				tmp.setCreatedBy(createdBy);
				tmp.setCreatedTime(new Timestamp(date.getTime()));
				
				

				// -- foreign affairs end
			}
		} // mau edit end
		else { // bikin baru
			System.out.println("bikin baru");
			tmp = new WelcomeCallsForm();
			// foreign affairs
			tmp.setCreatedBy(createdBy);
			tmp.setCreatedTime(new Timestamp(date.getTime()));
		}
		request.setAttribute("navigation", navigation);
		request.setAttribute("createdBy",createdBy );
		request.setAttribute("index", index);
		request.setAttribute("welcomeCallsId", id);
		result = tmp;
		return result;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {
		Integer id = WebUtil.getParameterInteger(request, "welcomeCallsId");
		System.out.println("id di bind = "+id);
		Integer index = WebUtil.getParameterInteger(request, "index");
		String createdBy = WebUtil.getParameterString(request, "createdBy", "");
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Integer status = WebUtil.getParameterInteger(request, "status");
		String telephoneNumber = WebUtil.getParameterString(request, "telephoneNumber","");
		WelcomeCallsForm welcomeCallsForm = (WelcomeCallsForm) command;
		WelcomeCalls welcomeCalls = welcomeCallsForm.getWelcomeCallsBean();
		request.setAttribute("welcomeCallsId", id);
		request.setAttribute("navigation", navigation);
		request.setAttribute("telephoneNumber", telephoneNumber);
		request.setAttribute("status", status);
		request.setAttribute("createdBy",createdBy );
		request.setAttribute("index", index);
	}

	protected Map referenceData(HttpServletRequest request) throws Exception {
		Map model = new HashMap();
		Integer id = WebUtil.getParameterInteger(request, "welcomeCallsId");
		System.out.println("id di refdat = "+id);
		model.put("welcomeCallsId", id);
		return model;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		
		Integer id = WebUtil.getParameterInteger(request, "welcomeCallsId");
		System.out.println("id di submit = "+id);
		Integer index = WebUtil.getParameterInteger(request, "index");
		String createdBy = WebUtil.getParameterString(request, "createdBy", "");
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Integer status = WebUtil.getParameterInteger(request, "status");
		String telephoneNumber = WebUtil.getParameterString(request, "telephoneNumber", "");
		Date date = new Date();
		WelcomeCallsForm welcomeCallsForm = (WelcomeCallsForm) command;
		if(id!=null){
			welcomeCallsForm.setNewWelcomeCalls(false);
		}
		WelcomeCalls res = null;
		String alertMsg = "";
		
		try {
			ActionUser user = securityService.getActionUser(request);
			welcomeCallsForm.getWelcomeCallsBean().setId(id);
			welcomeCallsForm.getWelcomeCallsBean().setCreatedBy(createdBy);
			welcomeCallsForm.getWelcomeCallsBean().setDeleted(0);
			welcomeCallsForm.getWelcomeCallsBean().setStatus(status);
			welcomeCallsForm.getWelcomeCallsBean().setMemberId(memberIdOrigin);
			welcomeCallsForm.getWelcomeCallsBean().setLogType(logTypeOrigin);
			welcomeCallsForm.getWelcomeCallsBean().setMemberName(memberNameOrigin);
			welcomeCallsForm.getWelcomeCallsBean().setTelephoneNumber(telephoneNumber);
			welcomeCallsForm.getWelcomeCallsBean().setModifiedBy(user.getUser().getUsername());
			if (welcomeCallsForm.isNewWelcomeCalls()) {
				System.out.println("new welcomeCalls");
				boolean isUserAuthorized = securityService.isUserAuthorized(user, "CREATEWELCOMECALLS");
				if (!isUserAuthorized) {
					ModelAndView errorResult = new ModelAndView(new RedirectView("errorpage"));
					errorResult.addObject("errorType", "accessDenied");
					errorResult.addObject("errorMessage", "You Are Not Authorized for CREATEWELCOMECALLS access");
					return errorResult;
				}
				WelcomeCalls welcomeCalls = welcomeCallsForm.getWelcomeCallsBean();
				if (welcomeCalls != null) {
					System.out.println("welcomeCalls bean not null");
					welcomeCalls.setCreatedTime(new Timestamp(date.getTime()));
					res = welcomeCallsService.create(welcomeCalls, user);
				}
			} else {
				System.out.println("not new cons");
				welcomeCallsForm.getWelcomeCallsBean().setModifiedTime(new Timestamp(date.getTime()));
				welcomeCallsForm.getWelcomeCallsBean().setCreatedBy(createdByOrigin);
				welcomeCallsForm.getWelcomeCallsBean().setCreatedTime(createdTimeOrigin);
				res = welcomeCallsService.update(welcomeCallsForm.getWelcomeCallsBean(), user);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return showForm(request, response, errors);
		}
		
		request.setAttribute("welcomeCallsId", id);
		request.setAttribute("navigation", navigation);
		request.setAttribute("createdBy",createdBy );
		request.setAttribute("index", index);
		return new ModelAndView(new RedirectView("welcomecalls"));
		// return super.onSubmit(request, response, command, errors);
	}
}
