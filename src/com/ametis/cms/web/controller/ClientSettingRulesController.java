package com.ametis.cms.web.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Document;
import com.ametis.cms.service.CaseService;
import com.ametis.cms.service.DocumentService;
import com.ametis.cms.service.SecurityService;
import com.ametis.cms.util.WebUtil;
import com.ametis.cms.web.form.DocumentForm;

public class ClientSettingRulesController extends SimpleFormController{
	
	public Integer[] getClientIds() {
		return clientIds;
	}

	public void setClientIds(Integer[] clientIds) {
		this.clientIds = clientIds;
	}

	public boolean[] getIs_client_email() {
		return is_client_email;
	}

	public void setIs_client_email(boolean[] is_client_email) {
		this.is_client_email = is_client_email;
	}

	public boolean[] getIs_patient_email() {
		return is_patient_email;
	}

	public void setIs_patient_email(boolean[] is_patient_email) {
		this.is_patient_email = is_patient_email;
	}

	public boolean[] getIs_patient_sms() {
		return is_patient_sms;
	}

	public void setIs_patient_sms(boolean[] is_patient_sms) {
		this.is_patient_sms = is_patient_sms;
	}

	public Integer getCountSet() {
		return countSet;
	}

	public void setCountSet(Integer countSet) {
		this.countSet = countSet;
	}

	public Integer getMaxPercountSet() {
		return maxPercountSet;
	}

	public void setMaxPercountSet(Integer maxPercountSet) {
		this.maxPercountSet = maxPercountSet;
	}

	public DocumentService getDocumentService() {
		return documentService;
	}

	public void setDocumentService(DocumentService documentService) {
		this.documentService = documentService;
	}

	public ResourceBundleMessageSource getAlertProperties() {
		return alertProperties;
	}

	public void setAlertProperties(ResourceBundleMessageSource alertProperties) {
		this.alertProperties = alertProperties;
	}

	public SecurityService getSecurityService() {
		return securityService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	public CaseService getCaseService() {
		return caseService;
	}

	public void setCaseService(CaseService caseService) {
		this.caseService = caseService;
	}

	public String getNavs() {
		return navs;
	}

	public void setNavs(String navs) {
		this.navs = navs;
	}

	public String getSearchText() {
		return searchText;
	}

	public void setSearchText(String searchText) {
		this.searchText = searchText;
	}

	public Date getSearchDateAwal() {
		return searchDateAwal;
	}

	public void setSearchDateAwal(Date searchDateAwal) {
		this.searchDateAwal = searchDateAwal;
	}

	public Date getSearchDateAkhir() {
		return searchDateAkhir;
	}

	public void setSearchDateAkhir(Date searchDateAkhir) {
		this.searchDateAkhir = searchDateAkhir;
	}

	public Integer getSearchStatus() {
		return searchStatus;
	}

	public void setSearchStatus(Integer searchStatus) {
		this.searchStatus = searchStatus;
	}

	public Integer getIndex() {
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public String getArah() {
		return arah;
	}

	public void setArah(String arah) {
		this.arah = arah;
	}

	private Integer[] clientIds;
	private boolean[] is_client_email;
	private boolean[] is_patient_email;
	private boolean[] is_patient_sms;

	private Integer countSet;
	private Integer maxPercountSet;
	DocumentService documentService;
	ResourceBundleMessageSource alertProperties;
	private SecurityService securityService;
	private CaseService caseService;
	private String navs = "";
	private String searchText = "";
	private Date searchDateAwal = null;
	private Date searchDateAkhir = null;
	private Integer searchStatus = null;
	private Integer index = null;
	private String arah = "";

	

	
	public ClientSettingRulesController() {
		setSessionForm(true);
		setValidateOnBinding(true);
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		Object result = null;
		DocumentForm tmp = new DocumentForm();
		result = tmp;
		return result;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {

		Integer batchClaimId = WebUtil.getParameterInteger(request, "batchClaimId");
		Integer claimId = WebUtil.getParameterInteger(request, "claimId");
		Integer index = WebUtil.getParameterInteger(request, "index");
		Integer caseId = WebUtil.getParameterInteger(request, "caseId");
		String searchby = WebUtil.getParameterString(request, "searchby", "");
		String navigation = WebUtil.getParameterString(request, "navigation", "");

		index = 0;

		String breadcrumb = "";
		breadcrumb = "<a href=\"receivedocumentclaim"
				+ "\" class=\"linkbreadcrumb\">Receive Upload Document Document</a>";

		request.setAttribute("breadcrumb", breadcrumb);
		request.setAttribute("batchClaimId", batchClaimId);
		request.setAttribute("claimId", claimId);
		request.setAttribute("index", index);
		request.setAttribute("searchby", searchby);
		request.setAttribute("caseId", caseId);
		request.setAttribute("navigation", navigation);
	}

	protected Map referenceData(HttpServletRequest request) throws Exception {
		Map result = new HashMap();
		System.out.println("kepanggil references data client setting rules");
//		try {
//			System.out.println("Call referenceData");
//
//			String rowset = WebUtil.getParameterString(request, "rowset", "0");
//
////			Integer index = WebUtil.getParameterInteger(request, "index");
//
//			String navigation = WebUtil.getParameterString(request, "navigation", "");
//
//			String searchby = WebUtil.getParameterString(request, "searchby", "");
//
//			System.out.println("searchtext = "+searchText);
//			
//			System.out.println(" doc status = "+searchStatus);
//			// String sortby = WebUtil.getParameterString(request, "sortby",
//			// "");
//
//			int minIndex = 0;
//			int maxIndex = 0;
//			int totalIndex = 0;
//
//			Collection collection = null;
//
//			int rowsetint = 0;
//			int count = 0;
//
//			if (StringUtils.isNumeric(rowset)) {
//				rowsetint = Integer.parseInt(rowset);
//			}
//			Vector vLikeP = new Vector();
//			Vector vLikeQ = new Vector();
//			Vector vEqP = new Vector();
//			Vector vEqQ = new Vector();
//
//			if (navigation.trim().equalsIgnoreCase("gosearch")) {
//				if(!searchText.trim().equals("")){
//					vLikeP.add("originalDocName");
//					vLikeQ.add(this.searchText);
//				}if(searchStatus!=-1 && searchStatus != null){
//					System.out.println("searchStatus = "+searchStatus);
//					vEqP.add("status");
//					vEqQ.add(searchStatus);
//				}
//				
//				
//			} else {
//				searchText = "";
//			}
//
//			// if (!navs.trim().equals("") && searchDate != null) {
//			// vLikeP.add("createdBy");
//			// // SimpleDateFormat formater = new
//			// // SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//			// // String tgl = formater.format(searchDate);
//			// vLikeQ.add(searchDate);
//			//
//			// }
//
//			ActionUser user = securityService.getActionUser(request);
//			// vLikeP.add("documentCategoryId.documentCategoryId");
//			// vLikeQ.add(6);
//
//			/* remove filter createdby */
//			// vLikeP.add("createdBy");
//			// vLikeQ.add(user.getUser().getUsername());
//
//			vEqP.add("deletedStatus");
//			vEqQ.add(new Integer(0));
//			
//			vEqP.add("documentType");
//			vEqQ.add(new Integer(1));
//
//			String sLikeP[] = new String[vLikeP.size()];
//			vLikeP.toArray(sLikeP);
//			Object sLikeQ[] = new Object[vLikeP.size()];
//			vLikeQ.toArray(sLikeQ);
//
//			String sEqP[] = new String[vEqP.size()];
//			vEqP.toArray(sEqP);
//			Object sEqQ[] = new Object[vEqP.size()];
//			vEqQ.toArray(sEqQ);
//
//			if (searchDateAwal != null && !searchDateAwal.toString().equalsIgnoreCase("1970-01-01")
//					&& searchDateAkhir != null && !searchDateAkhir.toString().equalsIgnoreCase("1970-01-01")) {
//
//				Object[] minColumn = { searchDateAwal };
//				Object[] maxColumn = { searchDateAkhir };
//				String[] betweenCount = { "createdTime" };
//
//				count = documentService.getTotal(sLikeP, sLikeQ, sEqP, sEqQ, betweenCount, minColumn, maxColumn);
//			} else {
//
//				count = documentService.getTotal(sLikeP, sLikeQ, sEqP, sEqQ);
//			}
//
////			String arah = WebUtil.getParameterString(request, "arah", "");
//
//			if (index == null)
//				index = new Integer(1);
//
//			if (arah.equals("kanan"))
//				index = new Integer(index.intValue() + 1);
//			else if (arah.equals("kiri"))
//				index = new Integer(index.intValue() - 1);
//			else if (arah.equals("kiribgt"))
//				index = new Integer(1);
//			else if (arah.equals("kananbgt"))
//				index = new Integer(count / countSet.intValue() + 1);
//
//			if (index.compareTo(new Integer(1)) == new Integer(-1).intValue())
//				index = new Integer(1);
//			else if (index.compareTo(new Integer(count / countSet.intValue() + 1)) == new Integer(1).intValue())
//				index = new Integer(count / countSet.intValue() + 1);
//
//			rowsetint = (new Integer((index.intValue() - 1) * countSet.intValue())).intValue();
//			if (count % countSet.intValue() > 0) {
//				result.put("halAkhir", new Integer(count / countSet.intValue() + 1));
//			} else {
//				result.put("halAkhir", new Integer(count / countSet.intValue()));
//			}
//
//			minIndex = (index - 1) * countSet;
//			maxIndex = index * countSet;
//
//			if (maxIndex > count) {
//				maxIndex = count;
//			}
//
//			String required[] = new String[] { "Document.ClaimId", "Document.DocumentCategoryId",
//					"Document.MemberId", };
//
//			if (searchDateAwal != null && !searchDateAwal.toString().equalsIgnoreCase("1970-01-01")
//					&& searchDateAkhir != null && !searchDateAkhir.toString().equalsIgnoreCase("1970-01-01")) {
//
//				Object[] minColumn = { searchDateAwal };
//				Object[] maxColumn = { searchDateAkhir };
//				String[] betweenColumn = { "createdTime" };
//
//				collection = documentService.search(sLikeP, sLikeQ, sEqP, sEqQ, betweenColumn, minColumn, maxColumn,
//						false, "createdTime", required, rowsetint, countSet.intValue());
//			} else {
//				collection = documentService.search(sLikeP, sLikeQ, sEqP, sEqQ, false, "createdTime", required,
//						rowsetint, countSet.intValue());
//			}
//
//			if (collection.size() <= 0) {
//				index = new Integer(index.intValue() - 1);
//				if (index.compareTo(new Integer(1)) == new Integer(-1).intValue())
//					index = new Integer(1);
//				else if (index.compareTo(new Integer(count / countSet.intValue() + 1)) == new Integer(1).intValue())
//					index = new Integer(count / countSet.intValue() + 1);
//
//				rowsetint = (new Integer((index.intValue() - 1) * countSet.intValue())).intValue();
//				if (count % countSet.intValue() > 0) {
//					result.put("halAkhir", new Integer(count / countSet.intValue() + 1));
//				} else {
//					result.put("halAkhir", new Integer(count / countSet.intValue()));
//				}
//				collection = documentService.search(sLikeP, sLikeQ, sEqP, sEqQ, required, rowsetint,
//						countSet.intValue());
//			}
//
//			request.setAttribute("searchtext", searchText);
//			request.setAttribute("searchby", searchby);
//			request.setAttribute("navigation", navigation);
//			request.setAttribute("countSet", countSet);
//			request.setAttribute("index", new Integer(index));
//			request.setAttribute("count", new Integer(count));
//			request.setAttribute("alert", request.getParameter("alert"));
//			request.setAttribute("minIndex", new Integer(minIndex));
//			request.setAttribute("maxIndex", new Integer(maxIndex));
//			request.setAttribute("Documents", collection);
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return result;
		return null;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		System.out.println("kepanggil onsubmit");
		String[] smsPatient = WebUtil.getParameterStringArray(request, "arrOfSmsPatient");
		System.out.print("sms Patient = "+Arrays.toString(smsPatient));
//		String navigation = WebUtil.getParameterString(request, "navigation", "");
//		String searchtext = WebUtil.getParameterString(request, "searchtext", "");
//
//		String temparah = WebUtil.getParameterString(request, "arah", "");
		Integer tempindex = WebUtil.getParameterInteger(request, "index");
		System.out.println("tempindex"+tempindex);
//		Date searchdate = WebUtil.getParameterDate(request, "upload_date");
//		
//		Integer searchstatus = WebUtil.getParameterInteger(request, "searchDocumentStatus");
//		System.out.println(" doc status = "+searchstatus);
//		
//		if (tempindex!=null) {
//			index = new Integer(tempindex);
//		} else {
//			arah = null;
//		}
//		
//		if (!temparah.trim().equals("")) {
//			arah = temparah;
//		} else {
//			arah = "";
//		}
//		
//		if (navigation.equalsIgnoreCase("gosearch")) {
//			if (searchdate != null) {
//				SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
//				SimpleDateFormat formater2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//
//				String tglAwal = formater.format(searchdate) + " 00:00:00";
//				String tglAkhir = formater.format(searchdate) + " 23:59:59";
//
//				Date tglawal = formater2.parse(tglAwal);
//				Date tglakhir = formater2.parse(tglAkhir);
//				searchDateAwal = tglawal;
//				searchDateAkhir = tglakhir;
//			}
//
//			if (!searchtext.trim().equals("")) {
//				searchText = searchtext;
//			} else {
//				searchText = "";
//			}
//			
//			if(searchstatus!=null){
//				System.out.println("search status !=null");
//				searchStatus = searchstatus;
//			}else{
//				System.out.println("search status ==null");
//				searchStatus = -1;
//			}
//
//			// navs = "gosearch";
//			System.out.println("#x: " + navigation + ", searchText: " + searchText);
//		}else if(navigation.equalsIgnoreCase("updateStatus")){
//			int docId = WebUtil.getParameterInteger(request, "documentId");
//			int newStatus = WebUtil.getParameterInteger(request, "newDocumentStatus");
//			Document old = documentService.get(docId);
//			old.setStatus(newStatus);
//			ActionUser user = securityService.getActionUser(request);
//			documentService.update(old, user);
//			System.out.println("doc id="+docId+"  new status = "+newStatus);
//		}else if(navigation.equalsIgnoreCase("delete")){
//			int docId = WebUtil.getParameterInteger(request, "documentId");
//			ActionUser user = securityService.getActionUser(request);
//			documentService.delete(docId, user);
//			System.out.println("doc id="+docId+"  deleted");
//		}else {
//			System.out.println("#y: " + navigation + ", searchText: " + searchText);
//			// navs = "";
//			searchText = "";
//			searchDateAwal = null;
//			searchDateAkhir = null;	
//		}
//		
//
//		String alertMsg = "";
//
//		String breadcrumb = "";
//		breadcrumb = "<a href=\"receivedocumentclaim\">Receive Upload Document Claim</a>";
//
//		request.setAttribute("breadcrumb", breadcrumb);
//
//		return new ModelAndView(new RedirectView("receivedocumentclaim?navigation=gosearch" + alertMsg));
		return null;
	}

	protected void initBinder(HttpServletRequest req, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(req, binder);
//		CustomDateEditor cde = new CustomDateEditor(new SimpleDateFormat("yyyy/MM/dd"), true);
//		binder.registerCustomEditor(Date.class, cde);
//		CustomNumberEditor num = new CustomNumberEditor(Number.class, true);
//		binder.registerCustomEditor(Number.class, num);
	}
}
