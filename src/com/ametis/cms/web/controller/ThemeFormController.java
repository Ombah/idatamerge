package com.ametis.cms.web.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.ametis.cms.datamodel.ActionResult;
import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Document;
import com.ametis.cms.datamodel.DocumentCategory;
import com.ametis.cms.datamodel.Theme;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.service.ThemeService;
import com.ametis.cms.service.ClientService;
import com.ametis.cms.service.DocumentService;
import com.ametis.cms.service.MemberGroupService;
import com.ametis.cms.service.SecurityService;
import com.ametis.cms.service.UserService;
import com.ametis.cms.util.StringUtil;
import com.ametis.cms.util.WebUtil;
import com.ametis.cms.web.form.ThemeForm;
import com.ametis.cms.webservice.MobileApplicationWebServiceImpl;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

import atg.taglib.json.util.JSONObject;

public class ThemeFormController extends SimpleFormController{
	ThemeService themeService;
	SecurityService securityService;
	String createdByOrigin = "";
	DocumentService documentService;
	Timestamp createdTimeOrigin = null;
	MemberGroupService memberGroupService;
	
	public DocumentService getDocumentService() {
		return documentService;
	}

	public void setDocumentService(DocumentService documentService) {
		this.documentService = documentService;
	}

	public MemberGroupService getMemberGroupService() {
		return memberGroupService;
	}

	public void setMemberGroupService(MemberGroupService memberGroupService) {
		this.memberGroupService = memberGroupService;
	}

	ClientService clientService;
	

	public ClientService getClientService() {
		return clientService;
	}

	public void setClientService(ClientService clientService) {
		this.clientService = clientService;
	}

	public SecurityService getSecurityService() {
		return securityService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	public ThemeService getThemeService() {
		return themeService;
	}

	public void setThemeService(ThemeService themeService) {
		this.themeService = themeService;
	}

	public ThemeFormController() {
		setSessionForm(true);
		setValidateOnBinding(true);
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		Object result = null;
		ThemeForm tmp = null;
		Integer id = null;
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		if (navigation.equalsIgnoreCase("tambah")) {
			
		} else {
			id = WebUtil.getParameterInteger(request, "id");
		}

		System.out.println("id di fob = " + id);
		Integer index = WebUtil.getParameterInteger(request, "index");
		String createdBy = WebUtil.getParameterString(request, "createdBy", "");
		String modifiedBy = WebUtil.getParameterString(request, "modifiedBy", "");
		/*
		 * ini ntar di uncomment aja .. trus yang tra --> ini diganti variable
		 * primary key nya
		 */
		Date date = new Date();
		if (id != null) {
			System.out.println("id not null");
			java.io.Serializable pkey = id;
			Theme object = themeService.get(pkey);

			if (object != null) { // edit object
				System.out.println("objek not null");
				tmp = new ThemeForm();
				if (object.getThemeId() != null) {
					tmp.setThemeId(object.getThemeId());
				}
				if (object.getCreatedBy() != null) {
					createdByOrigin = object.getCreatedBy();
					tmp.setCreatedBy(object.getCreatedBy());
				}
				if (object.getCreatedTime() != null) {
					createdTimeOrigin = object.getCreatedTime();
					tmp.setCreatedTime(object.getCreatedTime());
				}
				if (object.getModifiedBy() != null) {
					tmp.setModifiedBy(object.getModifiedBy());
				}
				if (object.getModifiedTime() != null) {
					tmp.setModifiedTime(object.getModifiedTime());
				}if(object.getMembergroupId()!=null){
					tmp.setMembergroupId(object.getMembergroupId().getMemberGroupId());
				}if(object.getLogoId()!=null){
					
					tmp.setOriginalLogoName(object.getLogoId().getOriginalDocName());
				}if(object.getBgId()!=null){
					
					tmp.setOriginalBgName(object.getBgId().getOriginalDocName());
				}

				// -- foreign affairs end
			} else {// object tidak ditemukan ?? anggap kita mau bikin baru !
				System.out.println("objek null");
				tmp = new ThemeForm();
				// foreign affairs
				
				tmp.setCreatedBy(createdBy);
				tmp.setCreatedTime(new Timestamp(date.getTime()));

				// -- foreign affairs end
			}
		} // mau edit end
		else { // bikin baru
			System.out.println("bikin baru");
			tmp = new ThemeForm();
			// foreign affairs
			tmp.setCreatedBy(createdBy);
			tmp.setCreatedTime(new Timestamp(date.getTime()));
		}
		request.setAttribute("navigation", navigation);
		request.setAttribute("createdBy", createdBy);
		request.setAttribute("themeId", id);
		request.setAttribute("index", index);
		
		result = tmp;
		return result;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {
		ThemeForm themeForm = (ThemeForm) command;
		Theme theme = themeForm.getThemeBean();
//		System.out.println("tes "+themeForm.getFile1().getOriginalFilename());
		
		Integer id = WebUtil.getParameterInteger(request, "themeId");
		System.out.println("id di bind = " + id);
		Integer index = WebUtil.getParameterInteger(request, "index");
		String createdBy = WebUtil.getParameterString(request, "createdBy", "");
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Integer membergroupId = WebUtil.getParameterInteger(request, "membergroupId");
		
		request.setAttribute("themeId", id);
		request.setAttribute("navigation", navigation);
		request.setAttribute("createdBy", createdBy);
		request.setAttribute("index", index);
		request.setAttribute("membergroupId", membergroupId);
		errors.printStackTrace();
	}
	protected Map referenceData(HttpServletRequest request) throws Exception {
		Map model = new HashMap();
		return model;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		ThemeForm themeForm = (ThemeForm) command;
		
		Integer id = WebUtil.getParameterInteger(request, "themeId");
		System.out.println("id di submit = " + id);
		Integer membergroupId = WebUtil.getParameterInteger(request, "memberGroupId"); 
		Integer index = WebUtil.getParameterInteger(request, "index");
		String createdBy = WebUtil.getParameterString(request, "createdBy", "");
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Collection<Document> documentLogo = new Vector<Document>();
		Collection<byte[]> contentLogo = new Vector<byte[]>();
		Collection<Document> documentBg = new Vector<Document>();
		Collection<byte[]> contentBg = new Vector<byte[]>();
		Date date = new Date();

		if (id != null) {
			themeForm.setNewTheme(false);
		}
		Theme res = null;
		String alertMsg = "";

		try {
			ActionUser user = securityService.getActionUser(request);
			themeForm.getThemeBean().setThemeId(id);
			themeForm.getThemeBean().setCreatedBy(createdBy);
			themeForm.getThemeBean().setDeleted(0);
			themeForm.getThemeBean().setMembergroupId(memberGroupService.get(membergroupId));
			themeForm.getThemeBean().setModifiedBy(user.getUser().getUsername());
			
			if (themeForm.isNewTheme()) {
				System.out.println("new theme");
				
				boolean isUserAuthorized = securityService.isUserAuthorized(user, "CREATEINFO");
				if (!isUserAuthorized) {
					ModelAndView errorResult = new ModelAndView(new RedirectView("errorpage"));
					errorResult.addObject("errorType", "accessDenied");
					errorResult.addObject("errorMessage", "You Are Not Authorized for CREATEINFO access");
					return errorResult;
				}
				Theme theme = themeForm.getThemeBean();
				if (theme != null) {
					System.out.println("theme bean not null");
					String extLogo = "";
					StringTokenizer tokenizerLogo = new StringTokenizer(themeForm.getLogo().getOriginalFilename(),".");
					while(tokenizerLogo.hasMoreTokens()){
						extLogo = tokenizerLogo.nextToken();
					}
					
					theme.setCreatedTime(new Timestamp(date.getTime()));
					String urlLogo = StringUtil.hash(System.currentTimeMillis()+"") + "." + extLogo;
					DocumentCategory categoryLogo = new DocumentCategory();
					categoryLogo.setDocumentCategoryId(8);
					Document docLogo = new Document();
					docLogo.setDocumentUrl(urlLogo);
					docLogo.setCreatedBy(user.getUser().getUsername());
					docLogo.setDeletedStatus(0);
					docLogo.setStatus(0);
					docLogo.setOriginalDocName(themeForm.getLogo().getOriginalFilename());
					System.out.println("tes logo name = "+themeForm.getLogo().getOriginalFilename());
					contentLogo.add(themeForm.getLogo().getBytes());
					docLogo.setDocumentCategoryId(categoryLogo);
					documentLogo.add(docLogo);
					
					String extBg = "";
					StringTokenizer tokenizerBg = new StringTokenizer(themeForm.getLogo().getOriginalFilename(),".");
					while(tokenizerBg.hasMoreTokens()){
						extLogo = tokenizerBg.nextToken();
					}
					
					String urlBg = StringUtil.hash(System.currentTimeMillis()+"") + "." + extBg;
					DocumentCategory categoryBg = new DocumentCategory();
					categoryBg.setDocumentCategoryId(8);
					
					Document docBg = new Document();
					docBg.setDocumentUrl(urlBg);
					docBg.setCreatedBy(user.getUser().getUsername());
					docBg.setDeletedStatus(0);
					docBg.setStatus(0);
					docBg.setOriginalDocName(themeForm.getBg().getOriginalFilename());
					System.out.println("tes name bg = "+themeForm.getBg().getOriginalFilename() );
					docBg.setDocumentCategoryId(categoryBg);
					documentBg.add(docBg);
					contentBg.add(themeForm.getBg().getBytes());
					theme.setCreatedTime(new Timestamp(date.getTime()));
					ActionResult ac = documentService.create(documentLogo, contentLogo, user);
					ActionResult ac2 = documentService.create(documentBg, contentBg, user);
					Document resLogo = (Document)ac.getResultObject();
					Document resBg = (Document)ac2.getResultObject();
					theme.setLogoId(resLogo);
					theme.setBgId(resBg);
					res = themeService.create(theme, user);
				}
			} else {
				System.out.println("not new cons");
				themeForm.getThemeBean().setModifiedTime(new Timestamp(date.getTime()));
				themeForm.getThemeBean().setCreatedBy(createdByOrigin);
				themeForm.getThemeBean().setCreatedTime(createdTimeOrigin);
				res = themeService.update(themeForm.getThemeBean(), user);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			return showForm(request, response, errors);
		}

		request.setAttribute("id", id);
		request.setAttribute("navigation", navigation);
		request.setAttribute("createdBy", createdBy);
		request.setAttribute("index", index);
		errors.printStackTrace();
		return new ModelAndView(new RedirectView("theme"));
		// return super.onSubmit(request, response, command, errors);
	}
	protected void initBinder(HttpServletRequest req, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(req, binder);
		CustomDateEditor cde = new CustomDateEditor(new SimpleDateFormat("dd-MM-yyyy"), true);
		binder.registerCustomEditor(Date.class, cde);
		CustomNumberEditor num = new CustomNumberEditor(Number.class, true);
		binder.registerCustomEditor(Number.class, num);
		CustomNumberEditor numInt = new CustomNumberEditor(Integer.class, true);
		binder.registerCustomEditor(Integer.class, numInt);
	}
}
