package com.ametis.cms.web.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Collection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.ametis.cms.datamodel.ActionResult;
import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Consultation;
import com.ametis.cms.datamodel.Document;
import com.ametis.cms.datamodel.DocumentCategory;
import com.ametis.cms.datamodel.Info;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.service.DocumentService;
import com.ametis.cms.service.InfoService;
import com.ametis.cms.service.SecurityService;
import com.ametis.cms.service.UserService;
import com.ametis.cms.util.StringUtil;
import com.ametis.cms.util.WebUtil;

import com.ametis.cms.web.form.InfoForm;
import com.ametis.cms.webservice.MobileApplicationWebServiceImpl;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

import atg.taglib.json.util.JSONObject;

public class InfoFormController extends SimpleFormController {

	InfoService infoService;
	SecurityService securityService;
	UserService userService;
	DocumentService documentService;
	String createdByOrigin = "";
	Timestamp createdTimeOrigin = null;
	String GOOGLE_SERVER_KEY = MobileApplicationWebServiceImpl.GOOGLE_SERVER_KEY;
	static final String MESSAGE_KEY = "message";
	
	public DocumentService getDocumentService() {
		return documentService;
	}

	public void setDocumentService(DocumentService documentService) {
		this.documentService = documentService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public SecurityService getSecurityService() {
		return securityService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	public InfoService getInfoService() {
		return infoService;
	}

	public void setInfoService(InfoService infoService) {
		this.infoService = infoService;
	}

	public InfoFormController() {
		setSessionForm(true);
		setValidateOnBinding(true);
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		Object result = null;
		InfoForm tmp = null;
		Integer id = null;
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		if (navigation.equalsIgnoreCase("tambah")) {
		} else {
			id = WebUtil.getParameterInteger(request, "id");
		}

		System.out.println("id di fob = " + id);
		Integer index = WebUtil.getParameterInteger(request, "index");
		String title = WebUtil.getParameterString(request, "title", "");
		String content = WebUtil.getParameterString(request, "content", "");
		String createdBy = WebUtil.getParameterString(request, "createdBy", "");
		String modifiedBy = WebUtil.getParameterString(request, "modifiedBy", "");
		Integer fileId = WebUtil.getParameterInteger(request, "fileId");
		/*
		 * ini ntar di uncomment aja .. trus yang tra --> ini diganti variable
		 * primary key nya
		 */
		Date date = new Date();
		if (id != null) {
			System.out.println("id not null");
			java.io.Serializable pkey = id;
			Info object = infoService.get(pkey);

			if (object != null) { // edit object
				System.out.println("objek not null");
				tmp = new InfoForm();
				if (object.getId() != null) {
					tmp.setId(object.getId());
				}
				if (object.getCreatedBy() != null) {
					createdByOrigin = object.getCreatedBy();
					tmp.setCreatedBy(object.getCreatedBy());
				}
				if (object.getTitle() != null) {
					tmp.setTitle(object.getTitle());
				}
				if (object.getCreatedTime() != null) {
					createdTimeOrigin = object.getCreatedTime();
					tmp.setCreatedTime(object.getCreatedTime());
				}
				if (object.getModifiedBy() != null) {
					tmp.setModifiedBy(object.getModifiedBy());
				}
				if (object.getModifiedTime() != null) {
					tmp.setModifiedTime(object.getModifiedTime());
				}
				if (object.getContent() != null) {
					tmp.setContent(object.getContent());
				}
				if(object.getEndDate()!=null){
					Date timestampDate = new Date(object.getEndDate().getTime());
					tmp.setEndDate(timestampDate);
				}
				if(object.getDocumentId()!=null){
					fileId = object.getDocumentId().getDocumentId();
					tmp.setOriginalFilename(object.getDocumentId().getOriginalDocName());
				}

				// -- foreign affairs end
			} else {// object tidak ditemukan ?? anggap kita mau bikin baru !
				System.out.println("objek null");
				tmp = new InfoForm();
				// foreign affairs
				tmp.setTitle(title);
				tmp.setContent(content);
				tmp.setCreatedBy(createdBy);
				tmp.setCreatedTime(new Timestamp(date.getTime()));

				// -- foreign affairs end
			}
		} // mau edit end
		else { // bikin baru
			System.out.println("bikin baru");
			tmp = new InfoForm();
			// foreign affairs
			tmp.setTitle(title);
			tmp.setContent(content);
			tmp.setCreatedBy(createdBy);
			tmp.setCreatedTime(new Timestamp(date.getTime()));
		}
		request.setAttribute("navigation", navigation);
		request.setAttribute("createdBy", createdBy);
		request.setAttribute("title", title);
		request.setAttribute("index", index);
		request.setAttribute("content", content);
		request.setAttribute("fileId",fileId );
		result = tmp;
		return result;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {
		InfoForm infoForm = (InfoForm) command;
		Info info = infoForm.getInfoBean();
		// System.out.println("tes "+infoForm.getFile1().getOriginalFilename());

		Integer id = WebUtil.getParameterInteger(request, "id");
		System.out.println("id di bind = " + id);
		Integer index = WebUtil.getParameterInteger(request, "index");
		String title = WebUtil.getParameterString(request, "title", "");
		String createdBy = WebUtil.getParameterString(request, "createdBy", "");
		String content = WebUtil.getParameterString(request, "content", "");
		String navigation = WebUtil.getParameterString(request, "navigation", "");

		request.setAttribute("id", id);
		request.setAttribute("navigation", navigation);
		request.setAttribute("createdBy", createdBy);
		request.setAttribute("title", title);
		request.setAttribute("content", content);
		request.setAttribute("index", index);
		if(errors.getErrorCount()>0){
			errors.printStackTrace();			
		}
	}

	protected Map referenceData(HttpServletRequest request) throws Exception {
		Map model = new HashMap();
		return model;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		InfoForm infoForm = (InfoForm) command;
		MultipartFile file = infoForm.getFile();
		
		System.out.println(file.getOriginalFilename());
//		MultipartFile file = infoForm.getFile1();
		Timestamp startDate = null;
		Timestamp endDate = null;
//		System.out.println(file.getOriginalFilename());
		Integer id = WebUtil.getParameterInteger(request, "id");
		System.out.println("id di submit = " + id);
		Integer index = WebUtil.getParameterInteger(request, "index");
		String title = WebUtil.getParameterString(request, "title", "");
		String createdBy = WebUtil.getParameterString(request, "createdBy", "");
		String content = WebUtil.getParameterString(request, "content", "");
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		String startDateString = WebUtil.getParameterString(request, "startDate", "");
		String endDateString = WebUtil.getParameterString(request, "endDate", "");
		
		Collection<Document> documentList = new Vector<Document>();
		Collection<byte[]> contentList = new Vector<byte[]>();
		
		System.out.println("tes title = " + title);
		Date date = new Date();

		if (id != null) {
			infoForm.setNewInfo(false);
		}
		Info res = null;
		String alertMsg = "";

		try {
//			if(startDateString!=null && startDateString.equalsIgnoreCase("")){
//				SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
//				Date parsedDate  = format.parse(startDateString);
//				startDate = new Timestamp(parsedDate.getTime());	
//			}
			if(endDateString!=null){
				SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
				Date parsedDate  = format.parse(endDateString);
				endDate = new Timestamp(parsedDate.getTime());	
			}
			ActionUser user = securityService.getActionUser(request);
			infoForm.getInfoBean().setId(id);
			infoForm.getInfoBean().setCreatedBy(createdBy);
			infoForm.getInfoBean().setContent(content);
			infoForm.getInfoBean().setDeletedStatus(0);
			infoForm.getInfoBean().setModifiedBy(user.getUser().getUsername());
			infoForm.getInfoBean().setTitle(title);
			infoForm.getInfoBean().setStartDate(startDate);
			infoForm.getInfoBean().setEndDate(endDate);
			if (infoForm.isNewInfo()) {
				System.out.println("new info");
				boolean isUserAuthorized = securityService.isUserAuthorized(user, "CREATEINFO");
				if (!isUserAuthorized) {
					ModelAndView errorResult = new ModelAndView(new RedirectView("errorpage"));
					errorResult.addObject("errorType", "accessDenied");
					errorResult.addObject("errorMessage", "You Are Not Authorized for CREATEINFO access");
					return errorResult;
				}
				Info info = infoForm.getInfoBean();
				if (info != null) {
					System.out.println("info bean not null");
					String ext = "";
					StringTokenizer tokenizer = new StringTokenizer(infoForm.getFile().getOriginalFilename(),".");
					while(tokenizer.hasMoreTokens()){
						ext = tokenizer.nextToken();
					}
					
					String url = StringUtil.hash(System.currentTimeMillis()+"") + "." + ext;
					DocumentCategory category = new DocumentCategory();
					category.setDocumentCategoryId(7);
					info.setCreatedTime(new Timestamp(date.getTime()));
					Document doc = new Document();
					doc.setDocumentUrl(url);
					doc.setCreatedBy(user.getUser().getUsername());
					doc.setDeletedStatus(0);
					doc.setStatus(0);
					doc.setOriginalDocName(infoForm.getFile().getOriginalFilename());
					doc.setDocumentCategoryId(category);
					documentList.add(doc);
					
					contentList.add(infoForm.getFile().getBytes());
					ActionResult ac = documentService.create(documentList, contentList, user);
					Document docRes = (Document) ac.getResultObject();
					System.out.println("tes " + docRes.getDocumentId());
					info.setDocumentId(docRes);
					res = infoService.create(info, user);
					doMobileAnnouncementToAll(res);
				}
			} else {
				System.out.println("not new info");
				infoForm.getInfoBean().setModifiedTime(new Timestamp(date.getTime()));
				infoForm.getInfoBean().setCreatedBy(createdByOrigin);
				infoForm.getInfoBean().setCreatedTime(createdTimeOrigin);
				res = infoService.update(infoForm.getInfoBean(), user);
			}

			if (infoForm.getFile() != null && infoForm.getFile().getSize() > 0) {
				System.out.println("nama file: " + infoForm.getFile().getOriginalFilename());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return showForm(request, response, errors);
		}

		request.setAttribute("id", id);
		request.setAttribute("navigation", navigation);
		request.setAttribute("createdBy", createdBy);
		request.setAttribute("content", content);
		request.setAttribute("title", title);
		request.setAttribute("index", index);
		if(errors.getErrorCount()>0){
			errors.printStackTrace();			
		}
		return new ModelAndView(new RedirectView("info"));
		// return super.onSubmit(request, response, command, errors);
	}

	public Collection<ActionResult> doMobileAnnouncementToAll(Info info) {
		Collection<ActionResult> result = null;
		result = new Vector();
		Collection<User> theUser;

		Result sentAnnouncementResult = null;
		try {
			String[] eqColumns = { "deletedStatus", "roleId.roleId", "userType" };
			Object[] eqParams = { 0, 17, 7 };
			String[] requiredParam = {
					// foreign affairs
					"MobileDeviceId", };

			int count = userService.getTotal(null, null, eqColumns, eqParams);
			theUser = userService.search(null, null, eqColumns, eqParams, requiredParam, 0, count);
			// theUser = userService.getAll(requiredParam);
			if (theUser != null) {
				System.out.println("masuk domobileannouncementToAll user not null with size " + theUser.size());
				Iterator<User> iterator = theUser.iterator();
				User user = null;
				while (iterator.hasNext()) {
					user = iterator.next();
					if (user.getMobileDeviceId() != null) {
						// doMobileAnnouncementByDeviceId(user.getMobileDeviceId(),
						// messageAnnouncement);
						JSONObject obj = new JSONObject();

						obj.put("type", "info");
						obj.put("id", info.getId());// ga jalan jadi bikin
													// activity
						obj.put("title", "New Info - " + info.getTitle());
						obj.put("myMessage", info.getContent());
						boolean resultSend = false;
						if(user.getOperatingSystem()!=null && user.getOperatingSystem().equalsIgnoreCase("android")){
							resultSend = sendMessageAndroid(user, obj);							
						}else{
							resultSend = sendMessageIOS(user);							
						}
						ActionResult actionResult = new ActionResult();
						actionResult.setResult(resultSend);
						actionResult.setReason("send");
						result.add(actionResult);
					}
				}
			} else {
				ActionResult res = new ActionResult();
				res.setResult(false);
				res.setReason("Can\'t find device with ID");

				result.add(res);
			}

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return result;
	}

	
	public boolean sendMessageIOS(User theUser) {

	    //config
	    String apiKey = "AIzaSyCFr2HkZILX1BGk_y8vazimRaVQzdr_tPU"; // Put here your API key
	    String GCM_Token = theUser.getMobileDeviceId(); // put the GCM Token you want to send to here
	    String notification = "{\"sound\":\"default\",\"title\":\"Information Updated\",\"body\":\"Check Out Our New Info at Owlexa app\"}"; // put the message you want to send here
	    String messageToSend = "{\"to\":\"" + GCM_Token + "\",\"notification\":" + notification + "}"; // Construct the message.
	    
	        try {

	            // URL
	            URL url = new URL("https://android.googleapis.com/gcm/send");

	            System.out.println(messageToSend);
	            // Open connection
	            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

	            // Specify POST method
	            conn.setRequestMethod("POST");

	            //Set the headers
	            conn.setRequestProperty("Content-Type", "application/json");
	            conn.setRequestProperty("Authorization", "key=" + apiKey);
	            conn.setDoOutput(true);

	            //Get connection output stream
	            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());

	            byte[] data = messageToSend.getBytes("UTF-8");
	            wr.write(data);

	            //Send the request and close
	            wr.flush();
	            wr.close();

	            //Get the response
	            int responseCode = conn.getResponseCode();
	            System.out.println("\nSending 'POST' request to URL : " + url);
	            System.out.println("Response Code : " + responseCode);

	            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	            String inputLine;
	            StringBuffer response = new StringBuffer();

	            while ((inputLine = in.readLine()) != null) {
	                response.append(inputLine);
	            }
	            in.close();
	            
	            //Print result
	            System.out.println(response.toString()); //this is a good place to check for errors using the codes in http://androidcommunitydocs.com/reference/com/google/android/gcm/server/Constants.html
	            return true;
	            
	        } catch (MalformedURLException e) {
	            e.printStackTrace();
	            return false;
	        } catch (IOException e) {
	            e.printStackTrace();
	            return false;
	        }
	}
	
	
	
	
	
	
	public boolean sendMessageAndroid(User theUser, JSONObject obj) {
		try {
			Sender sender = new Sender(GOOGLE_SERVER_KEY);
			//String myMessage = "'to' : '"
				//	+theUser.getMobileDeviceId()+"', 'content-available' : 1, 'priority': 10, 'notification' : {'body' : 'asdfasdf(isinya)', 'title' : 'consultation (judulnya)', 'sound': 'default'}, 'data' : {'type' : 'consultation', 'consultationId' : '462''myMessage' : 'asdfasdf'}";
			com.google.android.gcm.server.Message message = new com.google.android.gcm.server.Message.Builder()
					.timeToLive(30).delayWhileIdle(true).addData(MESSAGE_KEY, obj.toString()).build();
//			com.google.android.gcm.server.Message message = new com.google.android.gcm.server.Message.Builder()
//				.timeToLive(30).delayWhileIdle(true).addData("aps", "{'alert':'haloo','badge':'9','sound':'bingbong.aiff'}").build();
//			com.google.android.gcm.server.Message message = new com.google.android.gcm.server.Message.Builder()
//					.timeToLive(30).delayWhileIdle(true).addData(MESSAGE_KEY, myMessage).build();

			Result sentAnnouncementResult = sender.send(message, theUser.getMobileDeviceId(), 1);
			System.out.println("Mobile Announcement Result : " + sentAnnouncementResult.toString());

			ActionResult actionResult = new ActionResult();
			actionResult.setResult(true);
			actionResult.setReason("Sent announcement to " + theUser.getUsername() + " with deviceId : "
					+ theUser.getMobileDeviceId());
			System.out.println("Sent announcement to " + theUser.getUsername() + " with deviceId : "
					+ theUser.getMobileDeviceId());
			return true;
		} catch (Exception e) {
			return false;
		}

	}


	protected void initBinder(HttpServletRequest req, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(req, binder);
		CustomDateEditor cde = new CustomDateEditor(new SimpleDateFormat("dd-MM-yyyy"), true);
		binder.registerCustomEditor(Date.class, cde);
		CustomNumberEditor num = new CustomNumberEditor(Number.class, true);
		binder.registerCustomEditor(Number.class, num);
		CustomNumberEditor numInt = new CustomNumberEditor(Integer.class, true);
		binder.registerCustomEditor(Integer.class, numInt);
	}
}
