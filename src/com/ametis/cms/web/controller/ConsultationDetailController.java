package com.ametis.cms.web.controller;

import java.util.Collection;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.ametis.cms.datamodel.ConsultationDetail;
import com.ametis.cms.service.ConsultationDetailService;
import com.ametis.cms.util.WebUtil;

public class ConsultationDetailController implements Controller {
	
	ConsultationDetailService consultationDetailService;

	public ConsultationDetailService getConsultationDetailService() {
		return consultationDetailService;
	}

	public void setConsultationDetailService(ConsultationDetailService consultationDetailService) {
		this.consultationDetailService = consultationDetailService;
	}

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		String navigation = request.getParameter("navigation");
		if(navigation==null||navigation.equalsIgnoreCase("detail")){
			navigation = "searchConsultationDetail";
		}
		ModelAndView result = null;
		if(navigation.equalsIgnoreCase("sesuatu")){
			
		}else{
			result = searchConsultationDetail(request,response,"searchConsultationDetail");
		}
		return result;
	}

	private ModelAndView searchConsultationDetail(HttpServletRequest request, HttpServletResponse response, String location) {
		// TODO Auto-generated method stub
		ModelAndView result = null;
		result = new ModelAndView(location);
		String consultationId = WebUtil.getParameterString(request, "id", "");
		System.out.println("consuiltation ID = "+consultationId);
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Integer index = WebUtil.getParameterInteger(request, "index");
		if (index == null){
			index = new Integer(1);			
		}
		request.setAttribute("index", new Integer(index));
		Collection collection = null;
		Vector vLikeP = new Vector();
		Vector vLikeQ = new Vector();
		Vector vEqP = new Vector();
		Vector vEqQ = new Vector();
		String sLikeP[] = new String[vLikeP.size()];
		vLikeP.toArray(sLikeP);
		Object sLikeQ[] = new Object[vLikeP.size()];
		vLikeQ.toArray(sLikeQ);
		
		try {
			vEqP.add("deletedStatus");
			vEqQ.add(Integer.valueOf(0));
			
		
			if(consultationId!=null){
				vEqP.add("consultationId");
				vEqQ.add(Integer.parseInt(consultationId));
			}
			String sEqP[] = new String[vEqP.size()];
			vEqP.toArray(sEqP);
			Object sEqQ[] = new Object[vEqP.size()];
			vEqQ.toArray(sEqQ);
			

			int total =  consultationDetailService.getTotal(null, null, sEqP, sEqQ);
			collection = consultationDetailService.search(null,null,sEqP, sEqQ,0,total);
			
			System.out.println("collection.size = "+collection.size());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		result.addObject("ConsultationDetails", collection);
		return result;
	}

}
