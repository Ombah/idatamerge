package com.ametis.cms.web.controller;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.Hibernate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.MemberGroup;
import com.ametis.cms.datamodel.Theme;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.service.ThemeService;
import com.ametis.cms.service.SecurityService;
import com.ametis.cms.service.MemberGroupService;
import com.ametis.cms.util.WebUtil;

public class ThemeController implements Controller{
	ThemeService themeService;
	SecurityService securityService;
	MemberGroupService memberGroupService;
	private Integer countSet;
	
	public Integer getCountSet() {
		return countSet;
	}

	public void setCountSet(Integer countSet) {
		this.countSet = countSet;
	}
	public SecurityService getSecurityService() {
		return securityService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	public ThemeService getThemeService() {
		return themeService;
	}

	public void setThemeService(ThemeService themeService) {
		this.themeService = themeService;
	}
	
	public MemberGroupService getMemberGroupService() {
		return memberGroupService;
	}

	public void setMemberGroupService(MemberGroupService memberGroupService) {
		this.memberGroupService = memberGroupService;
	}

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		
		String navigation = request.getParameter("navigation");
		System.out.println("tes nav = "+navigation);
		if(navigation==null){
			navigation = "searchTheme";
		}
		ModelAndView result = null;
		if(navigation.equalsIgnoreCase("sesuatu")){
			result = searchTheme(request,response,"searchTheme");
		}else if(navigation.equalsIgnoreCase("delete")){
			result = deletePerformed(request,response);
		}else{
			result = searchTheme(request,response,"searchTheme");
		}
		return result;
	}
	

	public ModelAndView deletePerformed(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView result = null;
		try{
			Integer themeId = WebUtil.getParameterInteger(request, "id");
			ActionUser user = securityService.getActionUser(request);
			Theme res = themeService.delete(themeId, user);
			result = searchTheme(request, response, "searchTheme");
		}catch (Exception e) {
			// nanti kalo sudah OK codenya e.printStackTrace nya di hapus saja
			e.printStackTrace();
			result = new ModelAndView("error");
		}
		return result;
	}
		
	private ModelAndView searchTheme(HttpServletRequest request, HttpServletResponse response, String location) {
		// TODO Auto-generated method stub
		
		ModelAndView result = null;
		result = new ModelAndView(location);
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		String arah= WebUtil.getParameterString(request, "arah", "");
		Integer index = WebUtil.getParameterInteger(request, "index");
		String searchby = "";
		Collection collection = null;
		Vector vLikeP = new Vector();
		Vector vLikeQ = new Vector();
		Vector vEqP = new Vector();
		Vector vEqQ = new Vector();
		if(navigation != null && navigation.equalsIgnoreCase("gosearch")){
			System.out.println("Masuk");
			searchby = WebUtil.getParameterString(request, "searchby", "");
			String searchtext = WebUtil.getParameterString(request, "searchtext", "");
			if (searchby != null && searchtext!=null && !searchtext.equals("")) {
				if (searchby.equalsIgnoreCase("memberGroupName")) {
					vLikeP.add("membergroupId.groupName");
				}else if(searchby.equalsIgnoreCase("createdBy")){
					vLikeP.add("createdBy");
				}else if(searchby.equalsIgnoreCase("modifiedBy")){
					vLikeP.add("modifiedBy");
				}
				vLikeQ.add(searchtext);
			}
		}else{
			System.out.println("Tidak Masuk");
		}
		if (index == null){
			index = new Integer(1);			
		}
		request.setAttribute("index", new Integer(index));
		
		
		vEqP.add("deleted");
		vEqQ.add(0);
		
		String sLikeP[] = new String[vLikeP.size()];
		vLikeP.toArray(sLikeP);
		Object sLikeQ[] = new Object[vLikeP.size()];
		vLikeQ.toArray(sLikeQ);
		String sEqP[] = new String[vEqP.size()];
		vEqP.toArray(sEqP);
		Object sEqQ[] = new Object[vEqP.size()];
		vEqQ.toArray(sEqQ);
		
		
		Vector vSortP = new Vector();
		vSortP.add("modifiedTime");
		vSortP.add("createdTime");
		
		String sSortP[] = new String[vSortP.size()];
		vSortP.toArray(sSortP);
		int minIndex = 0;
		int maxIndex = 0;
		int totalIndex = 0;

		
		
		int rowsetint = 0;
		int count = 0;
		try{
			count = themeService.getTotal(sLikeP, sLikeQ, sEqP, sEqQ);
			System.out.println(count);
		}catch(Exception e){
			
		}
		
		if (index == null)
			index = new Integer(1);
		if (arah.equals("kanan"))
			index = new Integer(index.intValue() + 1);
		else if (arah.equals("kiri"))
			index = new Integer(index.intValue() - 1);
		else if (arah.equals("kiribgt"))
			index = new Integer(1);
		else if (arah.equals("kananbgt"))
			index = new Integer(count / countSet.intValue() + 1);
		if (index.compareTo(new Integer(1)) == new Integer(-1).intValue())
			index = new Integer(1);
		else if (index.compareTo(new Integer(count / countSet.intValue() + 1)) == new Integer(1).intValue())
			index = new Integer(count / countSet.intValue() + 1);

		rowsetint = (new Integer((index.intValue() - 1) * countSet.intValue())).intValue();
		if (count % countSet.intValue() > 0) {
			result.addObject("halAkhir", new Integer(count / countSet.intValue() + 1));
		} else {
			result.addObject("halAkhir", new Integer(count / countSet.intValue()));
		}

		minIndex = (index - 1) * countSet;
		maxIndex = index * countSet;

		if (maxIndex > count) {
			maxIndex = count;
		}
		try {
//			collection = themeService.search(sLikeP, sLikeQ, sEqP, sEqQ, false, sSortP,null, rowsetint, countSet.intValue());
			collection = themeService.search(sLikeP,sLikeQ, sEqP, sEqQ, false, sSortP,null, rowsetint, countSet.intValue());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


//		System.out.println("tes size" + collection.size());s countSet
		request.setAttribute("index", new Integer(index));
		request.setAttribute("navigation", navigation);
		request.setAttribute("searchby", searchby);
		request.setAttribute("countSet", countSet);
		request.setAttribute("count", new Integer(count));
		request.setAttribute("alert", request.getParameter("alert"));
		request.setAttribute("minIndex", new Integer(minIndex));
		request.setAttribute("maxIndex", new Integer(maxIndex));
		result.addObject("Themes", collection);
		return result;
	}
}
