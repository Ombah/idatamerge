package com.ametis.cms.web.controller;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Policy;
import com.ametis.cms.datamodel.ProcessNotification;
import com.ametis.cms.datamodel.Product;
import com.ametis.cms.service.ClientService;
import com.ametis.cms.service.PolicyService;
import com.ametis.cms.service.ProcessNotificationService;
import com.ametis.cms.service.ProductService;
import com.ametis.cms.service.SecurityService;
import com.ametis.cms.service.UserMenuService;
import com.ametis.cms.util.WebUtil;
import com.ametis.cms.web.form.DocumentForm;

public class SettingRulesPointController extends SimpleFormController {

	ProcessNotificationService processNotificationService;
	ProductService productService;
	String sessionClientName;
	String sessionPolicyNumber;
	SecurityService securityService;

	public SecurityService getSecurityService() {
		return securityService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	private Integer countSet;
	private Integer maxPercountSet;

	public Integer getCountSet() {
		return countSet;
	}

	public void setCountSet(Integer countSet) {
		this.countSet = countSet;
	}

	public Integer getMaxPercountSet() {
		return maxPercountSet;
	}

	public void setMaxPercountSet(Integer maxPercountSet) {
		this.maxPercountSet = maxPercountSet;
	}

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public ProcessNotificationService getProcessNotificationService() {
		return processNotificationService;
	}

	public void setProcessNotificationService(ProcessNotificationService processNotificationService) {
		this.processNotificationService = processNotificationService;
	}

	public SettingRulesPointController() {
		System.out.println("tes kepanggil constructor di  setting rules point controller");
		setSessionForm(true);
		setValidateOnBinding(true);
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		System.out.println("tes kepanggil form backing object di setting rules point  controller");
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Integer productId = WebUtil.getParameterInteger(request, "productId");
		String clientName = WebUtil.getParameterString(request, "clientName", sessionClientName);
		String policyNumber = WebUtil.getParameterString(request, "policyNumber", sessionPolicyNumber);
		if (!policyNumber.equalsIgnoreCase("")) {
			sessionPolicyNumber = policyNumber;
		}
		if (!clientName.equalsIgnoreCase("")) {
			sessionClientName = clientName;
		}

		System.out.println("nav = " + navigation + "product id = " + productId + "client Name = " + clientName
				+ "policy number = " + policyNumber);
		Object result = null;
		DocumentForm tmp = new DocumentForm();
		result = tmp;
		return result;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {
		System.out.println("tes kepanggil on bind and validate di  setting rules point controller");
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Integer productId = WebUtil.getParameterInteger(request, "productId");
		String policyNumber = WebUtil.getParameterString(request, "policyNumber", sessionPolicyNumber);
		String clientName = WebUtil.getParameterString(request, "clientName", sessionClientName);
		String rawEmailMember = WebUtil.getParameterString(request, "arrEmailMember", "");
		String rawEmailClient = WebUtil.getParameterString(request, "arrEmailClient", "");
		String rawEmailProvider = WebUtil.getParameterString(request, "arrEmailProvider", "");
		String rawSmsMember = WebUtil.getParameterString(request, "arrSmsMember", "");
		if (!rawEmailMember.equalsIgnoreCase("")) {
			rawEmailMember = rawEmailMember.substring(0, rawEmailMember.length() - 2);
			rawEmailClient = rawEmailClient.substring(0, rawEmailClient.length() - 2);
			rawEmailProvider = rawEmailProvider.substring(0, rawEmailProvider.length() - 2);
			rawSmsMember = rawSmsMember.substring(0, rawSmsMember.length() - 2);
			
		}
		if (!rawEmailMember.equals("")) {
			String[] arrEmailMember = rawEmailMember.split("--");
			String[] arrEmailClient = rawEmailClient.split("--");
			String[] arrEmailProvider = rawEmailProvider.split("--");
			String[] arrSmsMember = rawSmsMember.split("--");
			request.setAttribute("arrEmailMember", arrEmailMember);
			request.setAttribute("arrEmailClient", arrEmailClient);
			request.setAttribute("arrEmailProvider", arrEmailProvider);
			request.setAttribute("arrSmsMember", arrSmsMember);
		} else {
			System.out.println("arr null");
		}

		request.setAttribute("navigation", navigation);
		request.setAttribute("productId", productId);
		request.setAttribute("clientName", clientName);
		request.setAttribute("policyNumber", policyNumber);

	}

	protected Map referenceData(HttpServletRequest request) throws Exception {
		Map result = new HashMap();
		try {
			System.out.println("Call referenceData on  setting rules point  controller");
			String navigation = WebUtil.getParameterString(request, "navigation", "");
			String policyNumber = WebUtil.getParameterString(request, "policyNumber", sessionPolicyNumber);
			Integer productId = WebUtil.getParameterInteger(request, "productId");
			String clientName = WebUtil.getParameterString(request, "clientName", sessionClientName);
			System.out.println("nav = " + navigation + "product id = " + productId + "client Name = " + clientName
					+ "policy number = " + policyNumber);
			if (navigation.equalsIgnoreCase("gosetting")) {

				Product product = productService.get(productId);
				Collection collection = null;

				String[] eqColumn = { "productId" };
				Integer[] eqValue = { productId };
				collection = processNotificationService.search(eqColumn, eqValue);
				System.out.println("tes collection size = " + collection.size());

				request.setAttribute("ProductId", productId);
				request.setAttribute("ClientName", clientName);
				request.setAttribute("ProductName", product.getProductName());
				request.setAttribute("Points", collection);
				request.setAttribute("PolicyNumber", policyNumber);
				System.out.println("tes collection size = " + collection.size());
			} else {
				Collection collection = null;
				collection = processNotificationService.getAll();
				request.setAttribute("Points", collection);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		System.out.println("kepanggil onsubmit di setting rules point  controller");
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		String clientName = WebUtil.getParameterString(request, "clientName", sessionClientName);
		String policyNumber = WebUtil.getParameterString(request, "policyNumber", sessionPolicyNumber);

		if (navigation.equalsIgnoreCase("gosetting")) {
			System.out.println("called nav = gosetting");
			Integer productId = WebUtil.getParameterInteger(request, "productId");
			return new ModelAndView(new RedirectView("settingrulespoint?navigation=gosetting&productId=" + productId));
		} else if (navigation.equalsIgnoreCase("gosave")) {
			String rawEmailMember = WebUtil.getParameterString(request, "arrEmailMember", "");
			String rawEmailClient = WebUtil.getParameterString(request, "arrEmailClient", "");
			String rawEmailProvider = WebUtil.getParameterString(request, "arrEmailProvider", "");
			String rawSmsMember = WebUtil.getParameterString(request, "arrSmsMember", "");
			
			String[] arrEmailMember = null;
			String[] arrEmailClient = null;
			String[] arrEmailProvider = null;
			String[] arrSmsMember = null;
			if (!rawEmailMember.equalsIgnoreCase("")) {
				rawEmailMember = rawEmailMember.substring(0, rawEmailMember.length() - 2);
				rawEmailClient = rawEmailClient.substring(0, rawEmailClient.length() - 2);
				rawEmailProvider = rawEmailProvider.substring(0, rawEmailProvider.length() - 2);
				rawSmsMember = rawSmsMember.substring(0, rawSmsMember.length() - 2);
				
			}
			if (!rawEmailMember.equals("")) {
				arrEmailMember = rawEmailMember.split("--");
				arrEmailClient = rawEmailClient.split("--");
				arrEmailProvider = rawEmailProvider.split("--");
				arrSmsMember = rawSmsMember.split("--");
				request.setAttribute("arrEmailMember", arrEmailMember);
				request.setAttribute("arrEmailClient", arrEmailClient);
				request.setAttribute("arrEmailProvider", arrEmailProvider);
				request.setAttribute("arrSmsMember", arrSmsMember);
			} else {
				System.out.println("arr null");
			}

			System.out.println("called nav = gosave");
			Integer productId = WebUtil.getParameterInteger(request, "productId");
			if (arrEmailMember != null) {
				for (int i = 0; i < arrEmailMember.length; i++) {
					Integer processId = Integer.parseInt(arrEmailMember[i].split("_")[0]);
					String emailMemberStatus = arrEmailMember[i].split("_")[1];
					String emailClientStatus = arrEmailClient[i].split("_")[1];
					String emailProviderStatus = arrEmailProvider[i].split("_")[1];
					String smsMemberStatus = arrSmsMember[i].split("_")[1];
					
					ActionUser actionUser = securityService.getActionUser(request);
					String[] eqColumn = { "processId", "productId" };
					Integer[] eqValue = { processId, productId };
					Collection rawProcess = processNotificationService.search(eqColumn, eqValue);
					Iterator it = rawProcess.iterator();
					ProcessNotification processNotification = (ProcessNotification) it.next();

					if (emailMemberStatus.equalsIgnoreCase("true")) {
						processNotification.setIsMemberEmail(1);
					} else if (emailMemberStatus.equalsIgnoreCase("false")) {
						processNotification.setIsMemberEmail(0);
					}
					if (emailClientStatus.equalsIgnoreCase("true")) {
						processNotification.setIsClientEmail(1);
					} else if (emailClientStatus.equalsIgnoreCase("false")) {
						processNotification.setIsClientEmail(0);
					}
					if (emailProviderStatus.equalsIgnoreCase("true")) {
						processNotification.setIsProviderEmail(1);
					} else if (emailProviderStatus.equalsIgnoreCase("false")) {
						processNotification.setIsProviderEmail(0);
					}
					if (smsMemberStatus.equalsIgnoreCase("true")) {
						processNotification.setIsMemberSms(1);
					} else if (smsMemberStatus.equalsIgnoreCase("false")) {
						processNotification.setIsMemberSms(0);
					}
					processNotificationService.update(processNotification, actionUser);
				}
			}
			return new ModelAndView(new RedirectView("settingrulespoint?navigation=gosetting&productId=" + productId));
		} else {
			String alertMsg = "";
			String breadcrumb = "";
			breadcrumb = "<a href=\"receivedocumentclaim\">Receive Upload Document Claim</a>";
			request.setAttribute("breadcrumb", breadcrumb);
			return new ModelAndView(new RedirectView("settingrulespoint" + alertMsg));
		}
	}

	protected void initBinder(HttpServletRequest req, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(req, binder);
		CustomDateEditor cde = new CustomDateEditor(new SimpleDateFormat("yyyy/MM/dd"), true);
		binder.registerCustomEditor(Date.class, cde);
		CustomNumberEditor num = new CustomNumberEditor(Number.class, true);
		binder.registerCustomEditor(Number.class, num);
	}
}
