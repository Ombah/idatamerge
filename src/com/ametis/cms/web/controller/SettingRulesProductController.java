package com.ametis.cms.web.controller;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.ametis.cms.datamodel.Policy;
import com.ametis.cms.datamodel.ProcessNotification;
import com.ametis.cms.datamodel.Product;
import com.ametis.cms.service.ClientService;
import com.ametis.cms.service.PolicyService;
import com.ametis.cms.service.ProcessNotificationService;
import com.ametis.cms.service.ProductService;
import com.ametis.cms.service.UserMenuService;
import com.ametis.cms.util.WebUtil;
import com.ametis.cms.web.form.DocumentForm;

public class SettingRulesProductController extends SimpleFormController {

	ProcessNotificationService processNotificationService;
	ProductService productService;
	ClientService clientService;
	PolicyService policyService;
	private Integer countSet;
	private Integer maxPercountSet;
	String sessionClientName;

	public ClientService getClientService() {
		return clientService;
	}

	public void setClientService(ClientService clientService) {
		this.clientService = clientService;
	}

	public PolicyService getPolicyService() {
		return policyService;
	}

	public void setPolicyService(PolicyService policyService) {
		this.policyService = policyService;
	}

	public Integer getCountSet() {
		return countSet;
	}

	public void setCountSet(Integer countSet) {
		this.countSet = countSet;
	}

	public Integer getMaxPercountSet() {
		return maxPercountSet;
	}

	public void setMaxPercountSet(Integer maxPercountSet) {
		this.maxPercountSet = maxPercountSet;
	}

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public ProcessNotificationService getProcessNotificationService() {
		return processNotificationService;
	}

	public void setProcessNotificationService(ProcessNotificationService processNotificationService) {
		this.processNotificationService = processNotificationService;
	}
	
	public SettingRulesProductController() {
//		System.out.println("tes kepanggil constructor di  setting rules product controller");
		setSessionForm(true);
		setValidateOnBinding(true);
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
//		System.out.println("tes kepanggil form backing object di setting rules product  controller");
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Integer policyId = WebUtil.getParameterInteger(request, "policyId");
		String ClientName = WebUtil.getParameterString(request, "clientName", sessionClientName);
		if(!ClientName.equalsIgnoreCase("")){
			sessionClientName = ClientName;
		}
//		System.out.println("nav = "+navigation+"policy id = "+policyId + "client name = " +ClientName);
		Object result = null;
		DocumentForm tmp = new DocumentForm();
		result = tmp;
		return result;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {
//		System.out.println("tes kepanggil on bind and validate di  setting rules product controller");
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Integer policyId = WebUtil.getParameterInteger(request, "policyId");
		String ClientName = WebUtil.getParameterString(request, "clientName", sessionClientName);
//		System.out.println("nav = "+navigation+"policy id = "+policyId + "client name = " +ClientName);
		request.setAttribute("navigation", navigation);
		request.setAttribute("clientName", ClientName);
		request.setAttribute("policyId", policyId);
	}

	protected Map referenceData(HttpServletRequest request) throws Exception {
		Map result = new HashMap();
		try {
//			System.out.println("Call referenceData on  setting rules product  controller");
			String navigation = WebUtil.getParameterString(request, "navigation", "");
			String ClientName = WebUtil.getParameterString(request, "clientName", sessionClientName);
			Integer policyId = WebUtil.getParameterInteger(request, "policyId");
//			System.out.println("nav = "+navigation+"policy id = "+policyId + "client name = " +ClientName);
			if(navigation.equalsIgnoreCase("gosetting")){
				Collection collection = null;
				Policy policy = policyService.get(policyId);
				
				String[] eqColumn = {"policyId"};
				Policy[] eqValue = {policy};
				
				collection = productService.search(eqColumn,eqValue);
//				System.out.println("tes collection size = "+collection.size());
//				System.out.println("tes client name = "+ClientName);
				request.setAttribute("ClientName", ClientName);
				request.setAttribute("PolicyNumber", policy.getPolicyNumber());
				request.setAttribute("PolicyId", policyId);
				request.setAttribute("Products", collection);
				
//				System.out.println("tes collection size = "+collection.size());
			}else{
				Collection collection = null;
				collection = policyService.getAll();
				request.setAttribute("Policies", collection);	
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
//		System.out.println("kepanggil onsubmit di setting rules product  controller");
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		
		if (navigation.equalsIgnoreCase("gosetting")) {
//			System.out.println("called nav = gosetting");
			Integer policyId = WebUtil.getParameterInteger(request, "policyId");
			String ClientName = WebUtil.getParameterString(request, "clientName", sessionClientName);
//			System.out.println("nav = "+navigation+"policy id = "+policyId + "client name = " +ClientName);
			return new ModelAndView(new RedirectView("settingrulesproduct?navigation=gosetting&policyId="+policyId));
		} else {
			String alertMsg = "";
			String breadcrumb = "";
			breadcrumb = "<a href=\"receivedocumentclaim\">Receive Upload Document Claim</a>";
			request.setAttribute("breadcrumb", breadcrumb);
			return new ModelAndView(new RedirectView("settingrulesproduct" + alertMsg));
		}
	}

	protected void initBinder(HttpServletRequest req, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(req, binder);
		CustomDateEditor cde = new CustomDateEditor(new SimpleDateFormat("yyyy/MM/dd"), true);
		binder.registerCustomEditor(Date.class, cde);
		CustomNumberEditor num = new CustomNumberEditor(Number.class, true);
		binder.registerCustomEditor(Number.class, num);
	}
}
