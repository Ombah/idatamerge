package com.ametis.cms.web.controller;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.WelcomeCallCategory;
import com.ametis.cms.datamodel.WelcomeCalls;
import com.ametis.cms.datamodel.WelcomeCallsLog;
import com.ametis.cms.datamodel.WelcomeCallsStatus;
import com.ametis.cms.service.WelcomeCallsLogService;
import com.ametis.cms.service.WelcomeCallsService;
import com.ametis.cms.service.SecurityService;
import com.ametis.cms.service.WelcomeCallCategoryService;
import com.ametis.cms.service.WelcomeCallStatusService;
import com.ametis.cms.util.WebUtil;
import com.ametis.cms.web.form.WelcomeCallsLogForm;

public class WelcomeCallsLogFormController extends SimpleFormController {
	WelcomeCallsLogService welcomeCallsLogService;
	WelcomeCallsService welcomeCallsService;
	SecurityService securityService;
	String createdByOrigin = "";
	Timestamp createdTimeOrigin = null;
	WelcomeCallStatusService welcomeCallStatusService;
	WelcomeCallCategoryService welcomeCallCategoryService;
	
	
	
	public WelcomeCallsService getWelcomeCallsService() {
		return welcomeCallsService;
	}

	public void setWelcomeCallsService(WelcomeCallsService welcomeCallsService) {
		this.welcomeCallsService = welcomeCallsService;
	}

	public WelcomeCallCategoryService getWelcomeCallCategoryService() {
		return welcomeCallCategoryService;
	}

	public void setWelcomeCallCategoryService(WelcomeCallCategoryService welcomeCallCategoryService) {
		this.welcomeCallCategoryService = welcomeCallCategoryService;
	}

	public WelcomeCallStatusService getWelcomeCallStatusService() {
		return welcomeCallStatusService;
	}

	public void setWelcomeCallStatusService(WelcomeCallStatusService welcomeCallStatusService) {
		this.welcomeCallStatusService = welcomeCallStatusService;
	}

	public SecurityService getSecurityService() {
		return securityService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	public WelcomeCallsLogService getWelcomeCallsLogService() {
		return welcomeCallsLogService;
	}

	public void setWelcomeCallsLogService(WelcomeCallsLogService welcomeCallsLogService) {
		this.welcomeCallsLogService = welcomeCallsLogService;
	}

	public WelcomeCallsLogFormController() {
		setSessionForm(true);
		setValidateOnBinding(true);
	}
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		Object result = null;
		WelcomeCallsLogForm tmp = null;
		Integer id=null;
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		if(navigation.equalsIgnoreCase("tambah")){
			
		}else{
			id = WebUtil.getParameterInteger(request, "id");
		}
		
		System.out.println("id di fob = "+id);
		Integer index = WebUtil.getParameterInteger(request, "index");
		String createdBy = WebUtil.getParameterString(request, "createdBy", "");
		String modifiedBy = WebUtil.getParameterString(request, "modifiedBy", "");
		String callDescription = WebUtil.getParameterString(request, "callDesription", "");
		Integer callCategory = WebUtil.getParameterInteger(request, "callCategory");
		Integer callStatus = WebUtil.getParameterInteger(request, "callStatus");
		Integer followupNeeded = WebUtil.getParameterInteger(request, "followupNeeded");
		
		System.out.println("cades " + callDescription + " callcate "+callCategory+" callstra "+ callStatus+"folow = "+followupNeeded);
		/*
		 * ini ntar di uncomment aja .. trus yang tra --> ini diganti variable
		 * primary key nya
		 */
		Date date = new Date();
		if (id != null) {
			System.out.println("id not null");
			java.io.Serializable pkey = id;
			WelcomeCallsLog object = welcomeCallsLogService.get(pkey);
			
			if (object != null) { // edit object
				System.out.println("objek not null");
				
				tmp = new WelcomeCallsLogForm();
				if(object.getId()!=null){
					tmp.setId(object.getId());
				}
				if(object.getCreatedBy()!=null){
					createdByOrigin = object.getCreatedBy();
					tmp.setCreatedBy(object.getCreatedBy());
				}
				if(object.getCreatedTime()!=null){
					createdTimeOrigin = object.getCreatedTime();
					tmp.setCreatedTime(object.getCreatedTime());
				}
				if(object.getModifiedBy()!=null){
					tmp.setModifiedBy(object.getModifiedBy());
				}
				if(object.getModifiedTime()!=null ){
					tmp.setModifiedTime(object.getModifiedTime());
				}
				if(object.getCallCategoryId().getId()!=null){
					tmp.setCallCategory(object.getCallCategoryId().getId());
				}
				if(object.getCallDescription()!=null){
					tmp.setCallDescription(object.getCallDescription());
				}
				if(object.getFollowupNeeded()!=null){
					tmp.setFollowupNeeded(object.getFollowupNeeded());
				}
				if(object.getStatusCallId().getId()!=null){
					tmp.setStatusCall(object.getStatusCallId().getId());
				}
				if(object.getCallTime()!=null){
					Date timestampDate = new Date(object.getCallTime().getTime());
					tmp.setDate(timestampDate);
					SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
					Date dt = new Date(timestampDate.getTime());
					String hourNmin = sdf.format(dt);
					String hour = hourNmin.split(":")[0];
					String minutes = hourNmin.split(":")[1];
					System.out.println("tes hour min" +hour +" "+minutes);
					tmp.setHour(hour);
					tmp.setMinute(minutes);
				}
				
				// -- foreign affairs end
			} else {// object tidak ditemukan ?? anggap kita mau bikin baru !
				System.out.println("objek null");
				tmp = new WelcomeCallsLogForm();
				// foreign affairs
				tmp.setCreatedBy(createdBy);
				tmp.setCreatedTime(new Timestamp(date.getTime()));
				
				

				// -- foreign affairs end
			}
		} // mau edit end
		else { // bikin baru
			System.out.println("bikin baru");
			tmp = new WelcomeCallsLogForm();
			// foreign affairs
			tmp.setCreatedBy(createdBy);
			tmp.setCreatedTime(new Timestamp(date.getTime()));
		}
		request.setAttribute("navigation", navigation);
		request.setAttribute("createdBy",createdBy );
		request.setAttribute("index", index);
		result = tmp;
		return result;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {
		Integer id = WebUtil.getParameterInteger(request, "id");
//		String dateForm = WebUtil.getParameterString(request, "date", "");
//		System.out.println("tes"+dateForm);
		Integer index = WebUtil.getParameterInteger(request, "index");
		String callDescription = WebUtil.getParameterString(request, "callDescription", "");
		Integer callCategory = WebUtil.getParameterInteger(request, "callCategory");
		Integer callStatus = WebUtil.getParameterInteger(request, "statusCall");
		Integer followupNeeded = WebUtil.getParameterInteger(request, "followupNeeded");
		if(followupNeeded==null){
			followupNeeded = 0;
		}
		
		System.out.println("cades " + callDescription + " callcate "+callCategory+" callstra "+ callStatus+"folow = "+followupNeeded);
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		WelcomeCallsLogForm welcomeCallsLogForm = (WelcomeCallsLogForm) command;
		WelcomeCallsLog welcomeCallsLog = welcomeCallsLogForm.getWelcomeCallsLogBean();
//		request.setAttribute("welcomeCallsId", welcomeCallsId);
		request.setAttribute("callDescription", callDescription);
		request.setAttribute("callCategory", callCategory);
		request.setAttribute("statusCall", callStatus);
		request.setAttribute("followupNeeded", followupNeeded);
		
		request.setAttribute("id", id);
		request.setAttribute("navigation", navigation);
		request.setAttribute("index", index);
		System.out.println("bind done");
//		errors.printStackTrace();
		if(errors.getErrorCount()>0){
			errors.printStackTrace();			
		}
	}

	protected Map referenceData(HttpServletRequest request) throws Exception {
		System.out.println("ref data");
		Map model = new HashMap();
		String telephoneNumber ="";
		WelcomeCalls welcomeCalls = null;
		Integer welcomeCallsLogId = WebUtil.getParameterInteger(request, "id");
		if(welcomeCallsLogId!=null){
			WelcomeCallsLog welcomeCallsLog = welcomeCallsLogService.get(welcomeCallsLogId);
			if(welcomeCallsLog!=null){
				telephoneNumber = welcomeCallsLog.getWelcomeCallsId().getTelephoneNumber();
				model.put("telephoneNumber", telephoneNumber);
			}
		}
		
		Integer welcomeCallsId = WebUtil.getParameterInteger(request, "welcomeCallsId");
		if(welcomeCallsId!=null){
			welcomeCalls = welcomeCallsService.get(welcomeCallsId);	
			model.put("welcomeCallsId", welcomeCallsId);
		}
		if(welcomeCalls!=null){
			telephoneNumber = welcomeCalls.getTelephoneNumber();
			model.put("telephoneNumber", telephoneNumber);
		}
		 
		Collection statuses = welcomeCallStatusService.getAll();
		model.put("Statuses", statuses);
//		System.out.println("tes stat " +statuses.size());
		Collection categories = welcomeCallCategoryService.getAll();
		model.put("Categories", categories);
//		System.out.println("tes cat "+categories.size());
		
		Collection hours = new Vector();
		String tmp = "";
		for (int i = 0; i < 24; i++) {
			tmp = "";
			if (i < 10) {
				tmp = "0" + i;
			} else {
				tmp = "" + i;
			}
			hours.add(tmp);
		}
		model.put("hours", hours);

		Collection minutes = new Vector();

		for (int i = 0; i < 60; i++) {
			tmp = "";
			if (i < 10) {
				tmp = "0" + i;
			} else {
				tmp = "" + i;
			}
			minutes.add(tmp);
		}
		model.put("minutes", minutes);
		return model;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
//		System.out.println("called onsubmit");
		Timestamp callTime = null;
		if(errors.getErrorCount()>0){
			errors.printStackTrace();			
		}
		Integer id = WebUtil.getParameterInteger(request, "id");
		System.out.println("id di submit = "+id);
		String dateString = WebUtil.getParameterString(request, "date", "");
		String hourString = WebUtil.getParameterString(request, "hour", "");
		String minuteString = WebUtil.getParameterString(request, "minute", "");
		System.out.println("hour = " +hourString + " minu "+minuteString);
		if(dateString!=null){
			dateString +=" "+ hourString+":"+minuteString;			
			SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm");
			Date parsedDate  = format.parse(dateString);
			callTime = new Timestamp(parsedDate.getTime());			
			System.out.println("tes c" +callTime.toString());
		}
		Integer welcomeCallsId = WebUtil.getParameterInteger(request, "refId");
		if(welcomeCallsId==null&&id!=null){
			welcomeCallsId = welcomeCallsLogService.get(id).getWelcomeCallsId().getId();
		}
		Integer index = WebUtil.getParameterInteger(request, "index");
		String createdBy = WebUtil.getParameterString(request, "createdBy", "");
		String callDescription = WebUtil.getParameterString(request, "callDescription", "");
		Integer callCategory = WebUtil.getParameterInteger(request, "callCategory");
		Integer callStatus = WebUtil.getParameterInteger(request, "statusCall");
		Integer followupNeeded = WebUtil.getParameterInteger(request, "followupNeeded");
		if(followupNeeded==null){
			followupNeeded=0;
		}
		
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Date date = new Date();
		WelcomeCallsLogForm welcomeCallsLogForm = (WelcomeCallsLogForm) command;
		if(id!=null){
			welcomeCallsLogForm.setNewWelcomeCallsLog(false);
		}
		WelcomeCallsLog res = null;
		String alertMsg = "";
		
		try {
			ActionUser user = securityService.getActionUser(request);
			WelcomeCalls welcomeCalls = null;
			WelcomeCallsStatus welcomeCallStatus=null;
			WelcomeCallCategory welcomeCallCategory=null;
			if(welcomeCallsId!=null){
				welcomeCalls = welcomeCallsService.get(welcomeCallsId);
			}
			if(callStatus!=null){
				welcomeCallStatus = welcomeCallStatusService.get(callStatus);
			}
			if(callCategory!=null){
				welcomeCallCategory = welcomeCallCategoryService.get(callCategory);
			}
			if(callTime!=null){
				welcomeCallsLogForm.getWelcomeCallsLogBean().setCallTime(callTime);				
			}
			welcomeCallsLogForm.getWelcomeCallsLogBean().setId(id);
			welcomeCallsLogForm.getWelcomeCallsLogBean().setCallDescription(callDescription);
			welcomeCallsLogForm.getWelcomeCallsLogBean().setDeleted(0);
			welcomeCallsLogForm.getWelcomeCallsLogBean().setStatusCallId(welcomeCallStatus);
			welcomeCallsLogForm.getWelcomeCallsLogBean().setWelcomeCallsId(welcomeCalls);
			welcomeCallsLogForm.getWelcomeCallsLogBean().setCreatedBy(createdBy);
			welcomeCallsLogForm.getWelcomeCallsLogBean().setFollowupNeeded(followupNeeded);
			welcomeCallsLogForm.getWelcomeCallsLogBean().setCallCategoryId(welcomeCallCategory);
			if (welcomeCallsLogForm.isNewWelcomeCallsLog()) {
				System.out.println("new welcomeCallsLog");
				boolean isUserAuthorized = securityService.isUserAuthorized(user, "CREATEWELCOMECALLSLOG");
				if (!isUserAuthorized) {
					ModelAndView errorResult = new ModelAndView(new RedirectView("errorpage"));
					errorResult.addObject("errorType", "accessDenied");
					errorResult.addObject("errorMessage", "You Are Not Authorized for CREATEINFO access");
					return errorResult;
				}
				WelcomeCallsLog welcomeCallsLog = welcomeCallsLogForm.getWelcomeCallsLogBean();
				if (welcomeCallsLog != null) {
					System.out.println("welcomeCallsLog bean not null");
					welcomeCallsLog.setCreatedTime(new Timestamp(date.getTime()));
					res = welcomeCallsLogService.create(welcomeCallsLog, user);
				}
			} else {
				System.out.println("not new cons");
				welcomeCallsLogForm.getWelcomeCallsLogBean().setModifiedBy(user.getUser().getUsername());
				welcomeCallsLogForm.getWelcomeCallsLogBean().setModifiedTime(new Timestamp(date.getTime()));
				welcomeCallsLogForm.getWelcomeCallsLogBean().setCreatedBy(createdByOrigin);
				welcomeCallsLogForm.getWelcomeCallsLogBean().setCreatedTime(createdTimeOrigin);
				res = welcomeCallsLogService.update(welcomeCallsLogForm.getWelcomeCallsLogBean(), user);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return showForm(request, response, errors);
		}
		
		request.setAttribute("id", id);
		request.setAttribute("navigation", navigation);
		request.setAttribute("createdBy",createdBy );
		request.setAttribute("index", index);
		request.setAttribute("welcomeCallsId", welcomeCallsId);
		request.setAttribute("callDescription", callDescription);
		request.setAttribute("callCategory", callCategory);
		request.setAttribute("statusCall", callStatus);
		request.setAttribute("followupNeeded", followupNeeded);
		return new ModelAndView(new RedirectView("welcomecallslog?navigation=searchWelcomeCallsLog&welcomeCallsId="+welcomeCallsId));
		// return super.onSubmit(request, response, command, errors);
	}
	
	protected void initBinder(HttpServletRequest req, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(req, binder);
		CustomDateEditor cde = new CustomDateEditor(new SimpleDateFormat("dd-MM-yyyy"), true);
		binder.registerCustomEditor(Date.class, cde);
		CustomNumberEditor num = new CustomNumberEditor(Number.class, true);
		binder.registerCustomEditor(Number.class, num);
		CustomNumberEditor numInt = new CustomNumberEditor(Integer.class, true);
		binder.registerCustomEditor(Integer.class, numInt);
	}
}
