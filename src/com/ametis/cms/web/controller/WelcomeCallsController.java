package com.ametis.cms.web.controller;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collection;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.postgresql.util.MD5Digest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.servlet.view.RedirectView;

import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Member;
import com.ametis.cms.datamodel.MemberElectronicCard;
import com.ametis.cms.datamodel.MemberProduct;
import com.ametis.cms.datamodel.SubscriptionStatus;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.datamodel.WelcomeCalls;
import com.ametis.cms.service.WelcomeCallsService;
import com.ametis.cms.service.MemberService;
import com.ametis.cms.service.SecurityService;
import com.ametis.cms.util.WebUtil;

public class WelcomeCallsController implements Controller{
	WelcomeCallsService welcomeCallService;
	SecurityService securityService;
	MemberService memberService;

	public MemberService getMemberService() {
		return memberService;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	public SecurityService getSecurityService() {
		return securityService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	public WelcomeCallsService getWelcomeCallsService() {
		return welcomeCallService;
	}

	public void setWelcomeCallsService(WelcomeCallsService welcomeCallService) {
		this.welcomeCallService = welcomeCallService;
	}

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		
		String navigation = request.getParameter("navigation");
		if(navigation==null){
			navigation = "searchWelcomeCalls";
		}
		System.out.println("tes nav = "+navigation);
		ModelAndView result = null;
		if(navigation.equalsIgnoreCase("add")){
			result = addPerformed(request,response,"searchMemberForWelcomeCalls");
		}else if(navigation.equalsIgnoreCase("delete")){
			result = deletePerformed(request,response);
		}else if(navigation.equalsIgnoreCase("gosave")){
			result = savePerformed(request,response);
		}else if(navigation.equalsIgnoreCase("closed")){
			result = searchWelcomeCalls(request, response, "searchWelcomeCalls");
		}else if(navigation.equalsIgnoreCase("welcomeCallDetail")){
			result = detailPerformed(request, response, "detailWelcomeCalls");
		}else if(navigation.equalsIgnoreCase("tutup")){
			result = closePerformed(request,response);
		}else if(navigation.equalsIgnoreCase("jsontotalopenwelcomecall")){
			result = jsonTotalOpenWelcomeCall(request,response);
		}else if(navigation.equalsIgnoreCase("jsontotalclosedcalls")){
			result = jsonTotalClosedWelcomeCall(request, response);
		}
		else{
			result = searchWelcomeCalls(request,response,"searchWelcomeCalls");
		}
		return result;
	}
	
	public ModelAndView jsonTotalOpenWelcomeCall(HttpServletRequest request, HttpServletResponse response){
		ModelAndView result = null;
		try{
			result = new ModelAndView("jsonTotalOpenWelcomeCalls");
			User currentUser = securityService.getCurrentUser(request);

			int total = 0;
			if (currentUser != null) {

				String[] eqParam = { "deleted", "status" };
				Object[] eqValue = { Integer.valueOf(0), Integer.valueOf(0)};
				total = welcomeCallService.getTotal(null, null, eqParam, eqValue);

			}
			result.addObject("result", total);
			System.out.println("total open = " + total);
		} catch (Exception e) {
			e.printStackTrace();

			result = new ModelAndView("error");
		}

		return result;
		
	}
	
	public ModelAndView jsonTotalClosedWelcomeCall(HttpServletRequest request, HttpServletResponse response){
		ModelAndView result = null;
		try{
			result = new ModelAndView("jsonTotalClosedWelcomeCalls");
			User currentUser = securityService.getCurrentUser(request);

			int total = 0;
			if (currentUser != null) {

				String[] eqParam = { "deleted", "status" };
				Object[] eqValue = { Integer.valueOf(0), Integer.valueOf(1)};
				total = welcomeCallService.getTotal(null, null, eqParam, eqValue);

			}
			result.addObject("result", total);
			System.out.println("total closed = " + total);
		} catch (Exception e) {
			e.printStackTrace();

			result = new ModelAndView("error");
		}

		return result;
		
		
	}
	
	public ModelAndView detailPerformed(HttpServletRequest request, HttpServletResponse response, String location)
			throws Exception {
		ModelAndView result = null;

		try {
			String navigation = WebUtil.getParameterString(request, "navigation", "");

			Integer welcomeCallId = null;
			
				welcomeCallId = WebUtil.getParameterInteger(request, "welcomeCallId");
				WelcomeCalls welcomeCalls = welcomeCallService.get(welcomeCallId);
				
			// add by aju on 20150928, sorry, no url param changes for member
			// access fufufu :D
		
			result = new ModelAndView(location);
			result.addObject("welcomeCallsId",	welcomeCallId);
			result.addObject("WelcomeCall", welcomeCalls);
			
			// hitung umur
			
		} catch (Exception e) {
			e.printStackTrace();
			result = new ModelAndView("error");
		}
		return result;
	}
	
	public ModelAndView closePerformed(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView result = null;
		try{
			System.out.println("tes ttup");
			Integer welcomeCallId = WebUtil.getParameterInteger(request, "welcomeCallsId");
			System.out.println("tes id = " +welcomeCallId);
			ActionUser user = securityService.getActionUser(request);
			WelcomeCalls res = welcomeCallService.get(welcomeCallId);
			res.setStatus(WelcomeCalls.STATUS_CLOSED);
			welcomeCallService.update(res, user);
			result = searchWelcomeCalls(request, response, "searchWelcomeCalls");
		}catch (Exception e) {
			// nanti kalo sudah OK codenya e.printStackTrace nya di hapus saja
			e.printStackTrace();
			result = new ModelAndView("error");
		}
		return result;
	}
	
	public ModelAndView savePerformed(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView result = null;
		try{
			String raw = WebUtil.getParameterString(request, "arrAddedMember", "");
			String[] member = raw.split("--");
			for(int i =0 ;i<member.length;i++){
				Integer id = Integer.parseInt(member[i].split("_")[0]);
				Integer added = null;
				if(member[i].split("_")[1].equalsIgnoreCase("true")){
					added = 1;
				}else if (member[i].split("_")[1].equalsIgnoreCase("false")){
					added = 0;
				}
				Member member2 = memberService.get(id);
				member2.setWelcomeCall(added);
				ActionUser user = securityService.getActionUser(request);
				memberService.update(member2, user);
				
				if(added == 1){
					WelcomeCalls welcomeCalls = new WelcomeCalls();
					welcomeCalls.setCreatedBy(user.getUser().getUsername());
					java.util.Date date= new java.util.Date();
					welcomeCalls.setCreatedTime(new Timestamp(date.getTime()));
					welcomeCalls.setDeleted(0);
					welcomeCalls.setLogType(WelcomeCalls.LOG_TYPE_WELCOME_CALL);
					welcomeCalls.setMemberId(member2);
					welcomeCalls.setMemberName(member2.getFullName());
					welcomeCalls.setStatus(WelcomeCalls.STATUS_OPEN);
					welcomeCalls.setTelephoneNumber(member2.getTelephone());
					welcomeCallService.create(welcomeCalls, user);					
				}
				
			}
			request.setAttribute("navigation", "");
			result = searchWelcomeCalls(request, response, "searchWelcomeCalls");
		}catch (Exception e) {
			// nanti kalo sudah OK codenya e.printStackTrace nya di hapus saja
			e.printStackTrace();
			result = new ModelAndView("error");
		}
		return result;
	}

	public ModelAndView deletePerformed(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView result = null;
		try{
			Integer welcomeCallId = WebUtil.getParameterInteger(request, "id");
			ActionUser user = securityService.getActionUser(request);
			WelcomeCalls res = welcomeCallService.delete(welcomeCallId, user);
			result = searchWelcomeCalls(request, response, "searchWelcomeCalls");
		}catch (Exception e) {
			// nanti kalo sudah OK codenya e.printStackTrace nya di hapus saja
			e.printStackTrace();
			result = new ModelAndView("error");
		}
		return result;
	}
		
	private ModelAndView searchWelcomeCalls(HttpServletRequest request, HttpServletResponse response, String location) {
		// TODO Auto-generated method stub
		ModelAndView result = null;
		result = new ModelAndView(location);
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		System.out.println("tes nav = "+navigation);
		Integer index = WebUtil.getParameterInteger(request, "index");
		if (index == null){
			index = new Integer(1);			
		}
		request.setAttribute("index", new Integer(index));
		Collection collection = null;
		Vector vLikeP = new Vector();
		Vector vLikeQ = new Vector();
		Vector vEqP = new Vector();
		Vector vEqQ = new Vector();
		
		if(navigation.equalsIgnoreCase("closed")){
			vEqP.add("status");
			vEqQ.add(WelcomeCalls.STATUS_CLOSED);
			
		}else{
			vEqP.add("status");
			vEqQ.add(WelcomeCalls.STATUS_OPEN);
			
		}
		
		vEqP.add("deleted");
		vEqQ.add(0);
		
		
		String sLikeP[] = new String[vLikeP.size()];
		vLikeP.toArray(sLikeP);
		Object sLikeQ[] = new Object[vLikeP.size()];
		vLikeQ.toArray(sLikeQ);
		String sEqP[] = new String[vEqP.size()];
		vEqP.toArray(sEqP);
		Object sEqQ[] = new Object[vEqP.size()];
		vEqQ.toArray(sEqQ);
		
		try {
			collection = welcomeCallService.search(sEqP, sEqQ);			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println("tes size" + collection.size());s
		request.setAttribute("navigation", navigation);
		result.addObject("WelcomeCalls", collection);
		return result;
	}
	
	private ModelAndView addPerformed(HttpServletRequest request, HttpServletResponse response, String location) {
		// TODO Auto-generated method stub
		ModelAndView result = null;
		result = new ModelAndView(location);
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Integer index = WebUtil.getParameterInteger(request, "index");
		if (index == null){
			index = new Integer(1);			
		}
		request.setAttribute("index", new Integer(index));
		Collection collection = null;
		Vector vLikeP = new Vector();
		Vector vLikeQ = new Vector();
		Vector vEqP = new Vector();
		Vector vEqQ = new Vector();
		
		vEqP.add("status");
		vEqQ.add(5);
		
		vEqP.add("deletedStatus");
		vEqQ.add(0);
		
		String sLikeP[] = new String[vLikeP.size()];
		vLikeP.toArray(sLikeP);
		Object sLikeQ[] = new Object[vLikeP.size()];
		vLikeQ.toArray(sLikeQ);
		String sEqP[] = new String[vEqP.size()];
		vEqP.toArray(sEqP);
		Object sEqQ[] = new Object[vEqP.size()];
		vEqQ.toArray(sEqQ);
		
		try {
			collection = memberService.search(sEqP, sEqQ);			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println("tes size" + collection.size());s
		request.setAttribute("navigation", navigation);
		result.addObject("Members", collection);
		return result;
	}

}
