package com.ametis.cms.web.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.ametis.cms.datamodel.ActionResult;
import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Consultation;
import com.ametis.cms.datamodel.ConsultationDetail;
import com.ametis.cms.datamodel.Document;
import com.ametis.cms.datamodel.DocumentCategory;
import com.ametis.cms.datamodel.Member;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.dto.ConsultationDetailDto;
import com.ametis.cms.service.ConsultationDetailService;
import com.ametis.cms.service.ConsultationService;
import com.ametis.cms.service.DocumentService;
import com.ametis.cms.service.MemberService;
import com.ametis.cms.service.SecurityService;
import com.ametis.cms.service.UserService;
import com.ametis.cms.util.StringUtil;
import com.ametis.cms.util.WebUtil;
import com.ametis.cms.web.form.ConsultationForm;
import com.ametis.cms.web.form.InfoForm;
import com.ametis.cms.webservice.MobileApplicationWebServiceImpl;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

import atg.taglib.json.util.JSONObject;

public class ConsultationFormController extends SimpleFormController {
	
	String GOOGLE_SERVER_KEY = MobileApplicationWebServiceImpl.GOOGLE_SERVER_KEY;
	static final String MESSAGE_KEY = "message";
	

	ConsultationService consultationService;
	SecurityService securityService;
	String createdByOrigin = "";
	Timestamp createdTimeOrigin = null;
	Integer createdByIdOrigin;
	MemberService memberService;
	UserService userService;
	ConsultationDetailService consultationDetailService;
	DocumentService documentService;
	
	public DocumentService getDocumentService() {
		return documentService;
	}

	public void setDocumentService(DocumentService documentService) {
		this.documentService = documentService;
	}

	public ConsultationDetailService getConsultationDetailService() {
		return consultationDetailService;
	}

	public void setConsultationDetailService(ConsultationDetailService consultationDetailService) {
		this.consultationDetailService = consultationDetailService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public MemberService getMemberService() {
		return memberService;
	}

	public void setMemberService(MemberService memberService) {
		this.memberService = memberService;
	}

	public SecurityService getSecurityService() {
		return securityService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	public ConsultationService getConsultationService() {
		return consultationService;
	}

	public void setConsultationService(ConsultationService consultationService) {
		this.consultationService = consultationService;
	}

	public ConsultationFormController() {
		setSessionForm(true);
		setValidateOnBinding(true);
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		Object result = null;
		ConsultationForm tmp = null;
		Integer id=null;
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		if(navigation.equalsIgnoreCase("tambah")){
			
		}
		else{
			id = WebUtil.getParameterInteger(request, "id");			
		}
		
	
		Integer index = WebUtil.getParameterInteger(request, "index");
		String title = WebUtil.getParameterString(request, "title", "");
		String createdBy = WebUtil.getParameterString(request, "createdBy", "");
	
		String question = WebUtil.getParameterString(request, "question", "");
		String answer = WebUtil.getParameterString(request, "answer", "");
		String modifiedBy = WebUtil.getParameterString(request, "modifiedBy", "");
		String createdById = WebUtil.getParameterString(request, "createdById", "");
		Integer fileId = WebUtil.getParameterInteger(request, "fileId");
		
		/*
		 * ini ntar di uncomment aja .. trus yang tra --> ini diganti variable
		 * primary key nya
		 */
		if (id != null) {
			System.out.println("id not null");
			java.io.Serializable pkey = id;
			Consultation object = consultationService.get(pkey);
			
			if (object != null) { // edit object
				System.out.println("objek not null");
				tmp = new ConsultationForm();
				if(object.getConsultationId()!=null){
					tmp.setId(object.getConsultationId());
				}
				
				if(object.getCreatedBy()!=null){
					System.out.println("createdby dari table tidak null");
					createdByOrigin = object.getCreatedBy();
					tmp.setCreatedBy(object.getCreatedBy());
				}
				
				if(object.getTitle()!=null){
					tmp.setTitle(object.getTitle());
				}
				if(object.getCreatedTime()!=null){
					createdTimeOrigin = object.getCreatedTime();
					tmp.setCreatedTime(object.getCreatedTime());
				}
				if(object.getModifiedBy()!=null){
					tmp.setModifiedBy(object.getModifiedBy());
				}
				if(object.getModifiedTime()!=null ){
					tmp.setModifiedTime(object.getModifiedTime());
				}
//				if(object.getCreatedById()!=null){
//					createdByIdOrigin = object.getCreatedById();
//					tmp.setCreatedById(object.getCreatedById());
//				}
				if(object.getDocumentId()!=null){
					fileId = object.getDocumentId().getDocumentId();
					tmp.setOriginalFilename(object.getDocumentId().getOriginalDocName());
				}


				// -- foreign affairs end
			} else {// object tidak ditemukan ?? anggap kita mau bikin baru !
				System.out.println("objek null");
				tmp = new ConsultationForm();
				// foreign affairs
				tmp.setTitle(title);
				tmp.setCreatedBy(createdBy);
				tmp.setMessage(question);
				

				// -- foreign affairs end
			}
		} // mau edit end
		else { // bikin baru
//			System.out.println("bikin baru");
			tmp = new ConsultationForm();
			// foreign affairs
			tmp.setTitle(title);
//			tmp.setAnswer(answer);
			tmp.setCreatedBy(createdBy);
			tmp.setMessage(question);
		}
		request.setAttribute("navigation", navigation);
		request.setAttribute("question", question);
		request.setAttribute("answer", answer);
		request.setAttribute("createdBy",createdBy);
		request.setAttribute("title",title );
		request.setAttribute("index", index);
		result = tmp;
		return result;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {
		Integer id = WebUtil.getParameterInteger(request, "id");
		System.out.println("id di bind = "+id);
		Integer index = WebUtil.getParameterInteger(request, "index");
		String title = WebUtil.getParameterString(request, "title", "");
		String createdBy = WebUtil.getParameterString(request, "createdBy", "");

		createdBy = WebUtil.getParameterString(request, "userId", "");
		System.out.println("created by di bind = "+createdBy);
		String question = WebUtil.getParameterString(request, "question", "");
		String answer = WebUtil.getParameterString(request, "answer", "");
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		ConsultationForm consultationForm = (ConsultationForm) command;
		Consultation consultation = consultationForm.getConsultationBean();
		System.out.println("error => "+errors.toString());
		request.setAttribute("id", id);
		request.setAttribute("navigation", navigation);
		request.setAttribute("question", question);
		request.setAttribute("answer", answer);
		request.setAttribute("createdBy",createdBy );
		request.setAttribute("title",title );
		request.setAttribute("index", index);
	}

	protected Map referenceData(HttpServletRequest request) throws Exception {
		Map model = new HashMap();
		return model;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		Integer id = WebUtil.getParameterInteger(request, "id");
		
		Integer index = WebUtil.getParameterInteger(request, "index");
		String title = WebUtil.getParameterString(request, "title", "");
		String createdBy = WebUtil.getParameterString(request, "createdBy", "");
		
		String message = WebUtil.getParameterString(request, "message", "");
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		String memberId= WebUtil.getParameterString(request, "memberId", "");

		String createdById= WebUtil.getParameterString(request, "userId", "");
		System.out.println("message"+message);
		System.out.println("create"+createdBy);
		Collection<Document> documentList = new Vector<Document>();
		Collection<byte[]> contentList = new Vector<byte[]>();
		
		
		ConsultationForm consultationForm = (ConsultationForm) command;
		MultipartFile file = consultationForm.getFile();
		if(id!=null){
			consultationForm.setIsNewConsultation(false);
		}else{
			consultationForm.setIsNewConsultation(true);
				
		}
		Consultation res = null;
		String alertMsg = "";
		
		try {
			ActionUser user = securityService.getActionUser(request);
			consultationForm.getConsultationBean().setConsultationId(id);
//			consultationForm.getConsultationBean().setAnswer(answer);
			consultationForm.getConsultationBean().setCreatedBy(createdByOrigin);
			consultationForm.getConsultationBean().setCreatedTime(createdTimeOrigin);
			consultationForm.getConsultationBean().setDeletedStatus(0);
			consultationForm.getConsultationBean().setModifiedBy(user.getUser().getUsername());
			consultationForm.getConsultationBean().setContentPreview(message);
			consultationForm.getConsultationBean().setTitle(title);
//			consultationForm.getConsultationBean().setCreatedById(createdByIdOrigin);
//			consultationForm.getConsultationBean().setCreatedById(Integer.parseInt(createdById));
			
			
			if (consultationForm.isNewConsultation()) {
				System.out.println("new cons");
				boolean isUserAuthorized = securityService.isUserAuthorized(user, "CREATECONSULTATION");
				if (!isUserAuthorized) {
					ModelAndView errorResult = new ModelAndView(new RedirectView("errorpage"));
					errorResult.addObject("errorType", "accessDenied");
					errorResult.addObject("errorMessage", "You Are Not Authorized for CREATECONSULTATION access");
					return errorResult;
				}
				Consultation consultation = consultationForm.getConsultationBean();
				if (consultation != null) {
//					Member member = memberService.get(Integer.parseInt(memberId));
					User tempUser = userService.get(Integer.parseInt(createdById));
					Member member = memberService.searchUnique("memberId", tempUser.getMemberId().getMemberId());
					consultation.setStatus(Consultation.MESSAGE_REPLIED);
					consultation.setMemberId(member);
					consultation.setUserId(tempUser);
					String ext = "";
					StringTokenizer tokenizer = new StringTokenizer(consultationForm.getFile().getOriginalFilename(),".");
					while(tokenizer.hasMoreTokens()){
						ext = tokenizer.nextToken();
					}
					
					String url = StringUtil.hash(System.currentTimeMillis()+"") + "." + ext;
					DocumentCategory category = new DocumentCategory();
					category.setDocumentCategoryId(7);
					Document doc = new Document();
					doc.setDocumentUrl(url);
					doc.setCreatedBy(user.getUser().getUsername());
					doc.setDeletedStatus(0);
					doc.setStatus(0);
					doc.setOriginalDocName(consultationForm.getFile().getOriginalFilename());
					doc.setDocumentCategoryId(category);
					documentList.add(doc);
					
					contentList.add(consultationForm.getFile().getBytes());
					ActionResult ac = documentService.create(documentList, contentList, user);
					Document docRes = (Document) ac.getResultObject();
					System.out.println("tes " + docRes.getDocumentId());
					consultation.setDocumentId(docRes);
					consultation.setOriginalDocName(docRes.getOriginalDocName());
					consultation.setPreviewContent(parsePreview(message));
				
//					System.out.println("cons bean not null");
					res = consultationService.create(consultation, user);
					res = consultationService.update(res, user);
					doMobileAnnouncementByUserId(res);
					createFirstDetail(message, res, res.getUserId().getUserId());
				}
			} else {
				System.out.println("not new cons");
				Date date = new Date();
				consultationForm.getConsultationBean().setModifiedTime(new Timestamp(date.getTime()));
				res = consultationService.update(consultationForm.getConsultationBean(), user);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return showForm(request, response, errors);
		}
		
		request.setAttribute("id", id);
		request.setAttribute("navigation", navigation);
		request.setAttribute("message", message);
		
//		request.setAttribute("question", question);
//		request.setAttribute("answer", answer);
		request.setAttribute("createdBy",createdBy );
		request.setAttribute("title",title );
		request.setAttribute("index", index);
//		
		return new ModelAndView(new RedirectView("consultation"));
		// return super.onSubmit(request, response, command, errors);
	}
	protected void initBinder(HttpServletRequest req, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(req, binder);
		CustomDateEditor cde = new CustomDateEditor(new SimpleDateFormat("dd-MM-yyyy"), true);
		binder.registerCustomEditor(Date.class, cde);
		CustomNumberEditor num = new CustomNumberEditor(Number.class, true);
		binder.registerCustomEditor(Number.class, num);
		CustomNumberEditor numInt = new CustomNumberEditor(Integer.class, true);
		binder.registerCustomEditor(Integer.class, numInt);
	}
	
	public boolean createFirstDetail(String message, Consultation consultationId, Integer userId){
		ConsultationDetailDto messageDto = new ConsultationDetailDto();
		ConsultationDetail newConsultationDetail = new ConsultationDetail();
		newConsultationDetail.setDeletedStatus(0);
		newConsultationDetail.setMessage(message);
		newConsultationDetail.setDocumentId(consultationId.getDocumentId());
		newConsultationDetail.setIsRead(ConsultationDetail.STATUS_READED);
		Consultation cc = null;
//		try{
//			cc = consultationService.get(consultationId);
//		}catch(Exception ee){
//			
//		}
//		newConsultationDetail.setConsultationId(cc);
		newConsultationDetail.setConsultationId(consultationId);
		ActionUser user = new ActionUser();
		try{
			User theUser = userService.get(userId);
			user.setUser(theUser);

//			System.out.println("userservice sukses mas");
		}catch(Exception e){

//			System.out.println("userservice gagal mas"+e.getMessage());
			e.printStackTrace();
		}
		
		try{
			ConsultationDetail createdConsultationDetail = consultationDetailService.create(newConsultationDetail,user);
			messageDto.setMessage("masuk");
			System.out.println("sukses mas");
		}catch(Exception e){
			messageDto.setMessage("ga masuk");
			System.out.println("gagal mas "+e.getMessage());
			e.printStackTrace();
		}
		return true;
	}
	
	public Collection<ActionResult> doMobileAnnouncementByUserId(
			Consultation consultation) {

		User theUser;
		Integer userId = consultation.getUserId().getUserId();
//		try {
//			theUser = userService.search(null, null, "userId", userId, 0, 1);
//		}catch(Exception e){
//			
//		}
		Collection<ActionResult> result = null;
		result = new Vector();
		System.out.println("masuk domobileannouncementbydeviceid");
		
		Result sentAnnouncementResult = null;
				
		try {
//			theUser = userService.search(null, null, "mobileDeviceId", mobileDeviceId, 0, 10);
//			theUser = userService.search(null, null, "userId.userId", userId, 0, 1);
			theUser = userService.get(userId);
				
//			User user = userIterator.next();

			 JSONObject obj = new JSONObject();

		      obj.put("type","consultation");
		      obj.put("consultationId",""+consultation.getConsultationId());
		      obj.put("title", consultation.getModifiedBy()+" - "+consultation.getTitle());
		      obj.put("myMessage", consultation.getPreviewContent());
		      System.out.print(obj);

			
			System.out.println("Found user with mobileDeviceId : "+theUser.getMobileDeviceId());
			boolean resultSend = false;
			if(theUser.getOperatingSystem()!=null && theUser.getOperatingSystem().equalsIgnoreCase("android")){
				resultSend = sendMessageAndroid(theUser, obj);							
			}else{
				resultSend = sendMessageIOS(theUser);							
			}
//			ActionResult actionResult = new ActionResult();
//			actionResult.setResult(resultSend);
//			actionResult.setReason("send");
//			result.add(actionResult);
			
			
			ActionResult actionResult = new ActionResult();
			actionResult.setResult(true);
			actionResult.setReason("Sent announcement to " + theUser.getUsername() + " with deviceId : " + theUser.getMobileDeviceId());
			System.out.println("Sent announcement to " + theUser.getUsername() + " with deviceId : " + theUser.getMobileDeviceId());
			
			result.add(actionResult);
//			if(theUser != null){
//				Iterator<User> userIterator = theUser.iterator();
//				while(userIterator.hasNext()){
//					User user = userIterator.next();
//					System.out.println("Found " + theUser.size() + " user with mobileDeviceId : "+user.getMobileDeviceId());
//					
//					Sender sender = new Sender(GOOGLE_SERVER_KEY);
//					com.google.android.gcm.server.Message message = new com.google.android.gcm.server.Message.Builder().timeToLive(30)
//							.delayWhileIdle(true).addData(MESSAGE_KEY, messageAnnouncement).build();
//					
//					sentAnnouncementResult = sender.send(message, user.getMobileDeviceId(), 1);
//					System.out.println("Mobile Announcement Result : " + sentAnnouncementResult.toString());
//					
//					ActionResult actionResult = new ActionResult();
//					actionResult.setResult(true);
//					actionResult.setReason("Sent announcement to " + user.getUsername() + " with deviceId : " + user.getMobileDeviceId());
//					System.out.println("Sent announcement to " + user.getUsername() + " with deviceId : " + user.getMobileDeviceId());
//					
//					result.add(actionResult);
//				}
//			}
//			else{
//				ActionResult res = new ActionResult();
//				res.setResult(false);
////				res.setReason("Can\'t find device with ID : " + mobileDeviceId);
//				
//				result.add(res);
//			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	public boolean sendMessageIOS(User theUser) {

	    //config
	    String apiKey = "AIzaSyCFr2HkZILX1BGk_y8vazimRaVQzdr_tPU"; // Put here your API key
	    String GCM_Token = theUser.getMobileDeviceId(); // put the GCM Token you want to send to here
	    String notification = "{\"sound\":\"default\",\"title\":\"Consultation Received\",\"body\":\"You've got new message\"}"; // put the message you want to send here
	    String messageToSend = "{\"to\":\"" + GCM_Token + "\",\"notification\":" + notification + "}"; // Construct the message.
	    
	        try {

	            // URL
	            URL url = new URL("https://android.googleapis.com/gcm/send");

	            System.out.println(messageToSend);
	            // Open connection
	            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

	            // Specify POST method
	            conn.setRequestMethod("POST");

	            //Set the headers
	            conn.setRequestProperty("Content-Type", "application/json");
	            conn.setRequestProperty("Authorization", "key=" + apiKey);
	            conn.setDoOutput(true);

	            //Get connection output stream
	            DataOutputStream wr = new DataOutputStream(conn.getOutputStream());

	            byte[] data = messageToSend.getBytes("UTF-8");
	            wr.write(data);

	            //Send the request and close
	            wr.flush();
	            wr.close();

	            //Get the response
	            int responseCode = conn.getResponseCode();
	            System.out.println("\nSending 'POST' request to URL : " + url);
	            System.out.println("Response Code : " + responseCode);

	            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	            String inputLine;
	            StringBuffer response = new StringBuffer();

	            while ((inputLine = in.readLine()) != null) {
	                response.append(inputLine);
	            }
	            in.close();
	            
	            //Print result
	            System.out.println(response.toString()); //this is a good place to check for errors using the codes in http://androidcommunitydocs.com/reference/com/google/android/gcm/server/Constants.html
	            return true;
	            
	        } catch (MalformedURLException e) {
	            e.printStackTrace();
	            return false;
	        } catch (IOException e) {
	            e.printStackTrace();
	            return false;
	        }
	}
	public boolean sendMessageAndroid(User theUser, JSONObject obj) {
		try {
			Sender sender = new Sender(GOOGLE_SERVER_KEY);
			//String myMessage = "'to' : '"
				//	+theUser.getMobileDeviceId()+"', 'content-available' : 1, 'priority': 10, 'notification' : {'body' : 'asdfasdf(isinya)', 'title' : 'consultation (judulnya)', 'sound': 'default'}, 'data' : {'type' : 'consultation', 'consultationId' : '462''myMessage' : 'asdfasdf'}";
			com.google.android.gcm.server.Message message = new com.google.android.gcm.server.Message.Builder()
					.timeToLive(30).delayWhileIdle(true).addData(MESSAGE_KEY, obj.toString()).build();
//			com.google.android.gcm.server.Message message = new com.google.android.gcm.server.Message.Builder()
//				.timeToLive(30).delayWhileIdle(true).addData("aps", "{'alert':'haloo','badge':'9','sound':'bingbong.aiff'}").build();
//			com.google.android.gcm.server.Message message = new com.google.android.gcm.server.Message.Builder()
//					.timeToLive(30).delayWhileIdle(true).addData(MESSAGE_KEY, myMessage).build();

			Result sentAnnouncementResult = sender.send(message, theUser.getMobileDeviceId(), 1);
			System.out.println("Mobile Announcement Result : " + sentAnnouncementResult.toString());

			ActionResult actionResult = new ActionResult();
			actionResult.setResult(true);
			actionResult.setReason("Sent announcement to " + theUser.getUsername() + " with deviceId : "
					+ theUser.getMobileDeviceId());
			System.out.println("Sent announcement to " + theUser.getUsername() + " with deviceId : "
					+ theUser.getMobileDeviceId());
			return true;
		} catch (Exception e) {
			return false;
		}

	}
	
	public String parsePreview(String message){
		StringTokenizer st = new StringTokenizer(message, System.getProperty("line.separator"));
		String preview = st.nextToken();
		if(preview.length()>90){
			return preview.substring(0,85)+"...";
		}else{
			return preview;
		}
	}
}
