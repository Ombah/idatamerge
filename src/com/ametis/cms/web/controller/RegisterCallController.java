package com.ametis.cms.web.controller;

import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.ametis.cms.datamodel.ActionResult;
import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Case;
import com.ametis.cms.datamodel.PanicAction;
import com.ametis.cms.datamodel.RegisterCall;
import com.ametis.cms.datamodel.RegisterCall;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.service.PanicActionService;
import com.ametis.cms.service.RegisterCallService;
import com.ametis.cms.service.SecurityService;
import com.ametis.cms.service.UserService;
import com.ametis.cms.util.WebUtil;
import com.ametis.cms.webservice.MobileApplicationWebServiceImpl;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

import atg.taglib.json.util.JSONObject;

public class RegisterCallController implements Controller{
	
	String GOOGLE_SERVER_KEY = MobileApplicationWebServiceImpl.GOOGLE_SERVER_KEY;
	static final String MESSAGE_KEY = "message";
	static final String NOTIF_TITLE ="Owlexa Can't call you";
	static final String NOTIF_MESSAGE ="Please call us on 021-29830400";

	UserService userService;
	RegisterCallService registerCallService;
	SecurityService securityService;
	private Integer countSet;
	
	
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public Integer getCountSet() {
		return countSet;
	}

	public void setCountSet(Integer countSet) {
		this.countSet = countSet;
	}

	public SecurityService getSecurityService() {
		return securityService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	public RegisterCallService getRegisterCallService() {
		return registerCallService;
	}

	public void setRegisterCallService(RegisterCallService registerCallService) {
		this.registerCallService = registerCallService;
	}

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		
		String navigation = request.getParameter("navigation");
		System.out.println("tes nav = "+navigation);
		if(navigation==null){
			navigation = "searchRegisterCall";
		}
		ModelAndView result = null;
		if(navigation.equalsIgnoreCase("sesuatu")){
			result = searchRegisterCall(request,response,"searchRegisterCall");
		}else if(navigation.equalsIgnoreCase("delete")){
			result = deletePerformed(request,response);
		}else if(navigation.equalsIgnoreCase("jsontotalnewregistercall")){
			result = jsonTotalNewRegisterCall(request,response);
		}else if(navigation.equalsIgnoreCase("alertgcm")){
			result = alertGCMPerformed(request,response);
		}else if(navigation.equalsIgnoreCase("changestatus")){
			result = setStatusPerformed(request, response, "searchRegisterCall");
		}else{
			result = searchRegisterCall(request,response,"searchRegisterCall");
		}
		return result;
	}
	private ModelAndView setStatusPerformed(HttpServletRequest request, HttpServletResponse response, String location){
		System.out.println("tes set statsu perfm");
		ModelAndView result = null;
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Integer id = WebUtil.getParameterInteger(request, "id");
		System.out.println("tes id = "+id);
		Integer newStatus = WebUtil.getParameterInteger(request, "newStatus");
		// TODO FARHAN MESSAGE SILAHKAN DIGUNAKAN
		String solveMessage = WebUtil.getParameterString(request, "message", "");
		System.out.println("tes "+ solveMessage);
//		Integer index = WebUtil.getParameterInteger(request, "index");
		ActionUser actionUser = securityService.getActionUser(request);
		if(id!=null){
			RegisterCall registerCall = null;
			try {
				registerCall = registerCallService.get(id);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				result = new ModelAndView("error");
			}
			if(registerCall!=null){
				registerCall.setMessage(solveMessage);
				registerCall.setStatus(newStatus);
				registerCall.setModifiedTime(new java.sql.Timestamp(System.currentTimeMillis()));
				
				try {
					registerCallService.update(registerCall, actionUser);
					result = searchRegisterCall(request, response, location);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					result = new ModelAndView("error");
				}
			}
		}
		return result;
	}

	public ModelAndView jsonTotalNewRegisterCall(HttpServletRequest request, HttpServletResponse response){
		ModelAndView result = null;

		try {
			result = new ModelAndView("jsonTotalNewRegisterCall");

			User currentUser = securityService.getCurrentUser(request);

			int total = 0;
			if (currentUser != null) {

				String[] eqParam = { "deleted", "status" };
				Object[] eqValue = { Integer.valueOf(0), Integer.valueOf(0)};
				total = registerCallService.getTotal(null, null, eqParam, eqValue);

			}
			result.addObject("result", total);

		} catch (Exception e) {
			e.printStackTrace();

			result = new ModelAndView("error");
		}

		return result;
	
	}

	public ModelAndView deletePerformed(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView result = null;
		try{
			Integer registerCallId = WebUtil.getParameterInteger(request, "id");
			ActionUser user = securityService.getActionUser(request);
			RegisterCall res = registerCallService.delete(registerCallId, user);
			result = searchRegisterCall(request, response, "searchRegisterCall");
		}catch (Exception e) {
			// nanti kalo sudah OK codenya e.printStackTrace nya di hapus saja
			e.printStackTrace();
			result = new ModelAndView("error");
		}
		return result;
	}
		
	private ModelAndView searchRegisterCall(HttpServletRequest request, HttpServletResponse response, String location) {
		// TODO Auto-generated method stub
		System.out.println("masuk search regis");
		ModelAndView result = null;
		result = new ModelAndView(location);
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Integer index = WebUtil.getParameterInteger(request, "index");
		String arah = WebUtil.getParameterString(request, "arah", "");
		String searchtext = WebUtil.getParameterString(request, "searchtext", "");
		String searchby = WebUtil.getParameterString(request, "searchby", "");
		Integer searchStatus = WebUtil.getParameterInteger(request, "status");
		
		if (index == null){
			index = new Integer(1);			
		}
		request.setAttribute("index", new Integer(index));
		Collection collection = null;
		Vector vLikeP = new Vector();
		Vector vLikeQ = new Vector();
		Vector vEqP = new Vector();
		Vector vEqQ = new Vector();
		
		vEqP.add("deleted");
		vEqQ.add(0);
		if (searchby!=null && searchtext != null && !searchtext.equals("")) {
			if (searchby.equalsIgnoreCase("username")) {
				vLikeP.add("username");
				vLikeQ.add(searchtext);
			}
			if(searchby.equalsIgnoreCase("customerNumber")){
				vLikeP.add("memberId.customerNumber");
				vLikeQ.add(searchtext);
			}
			if(searchby.equalsIgnoreCase("providerName")){
				vLikeP.add("providerId.providerName");
				vLikeQ.add(searchtext);
			}
			if(searchby.equalsIgnoreCase("customerFullName")){
				vLikeP.add("memberId.fullName");
				vLikeQ.add(searchtext);
			}
			if(searchby.equalsIgnoreCase("telephoneNumber")){
				vLikeP.add("telephoneNumber");
				vLikeQ.add(searchtext);
			}
			if(searchby.equalsIgnoreCase("callcenterUsername")){
				vLikeP.add("callcenterUsername");
				vLikeQ.add(searchtext);
			}
		}
		
		if(searchStatus!=null && searchStatus>=0){
			vEqP.add("status");
			vEqQ.add(searchStatus);
		}
		String sLikeP[] = new String[vLikeP.size()];
		vLikeP.toArray(sLikeP);
		Object sLikeQ[] = new Object[vLikeP.size()];
		vLikeQ.toArray(sLikeQ);
		String sEqP[] = new String[vEqP.size()];
		vEqP.toArray(sEqP);
		Object sEqQ[] = new Object[vEqP.size()];
		vEqQ.toArray(sEqQ);
		
		
		int minIndex = 0;
		int maxIndex = 0;
		int totalIndex = 0;

		Vector vSortP = new Vector();
		vSortP.add("createdTime");
		
		String sSortP[] = new String[vSortP.size()];
		vSortP.toArray(sSortP);
		
		int rowsetint = 0;
		int count = 0;
		try{
			count = registerCallService.getTotal(sLikeP, sLikeQ, sEqP, sEqQ);	
		}catch(Exception e){
			
		}
		if (index == null)
			index = new Integer(1);
		if (arah.equals("kanan"))
			index = new Integer(index.intValue() + 1);
		else if (arah.equals("kiri"))
			index = new Integer(index.intValue() - 1);
		else if (arah.equals("kiribgt"))
			index = new Integer(1);
		else if (arah.equals("kananbgt"))
			index = new Integer(count / countSet.intValue() + 1);
		if (index.compareTo(new Integer(1)) == new Integer(-1).intValue())
			index = new Integer(1);
		else if (index.compareTo(new Integer(count / countSet.intValue() + 1)) == new Integer(1).intValue())
			index = new Integer(count / countSet.intValue() + 1);

		rowsetint = (new Integer((index.intValue() - 1) * countSet.intValue())).intValue();
		if (count % countSet.intValue() > 0) {
			result.addObject("halAkhir", new Integer(count / countSet.intValue() + 1));
		} else {
			result.addObject("halAkhir", new Integer(count / countSet.intValue()));
		}

		minIndex = (index - 1) * countSet;
		maxIndex = index * countSet;

		if (maxIndex > count) {
			maxIndex = count;
		}
		try {
//			collection = infoService.search(sEqP, sEqQ);
			int total = registerCallService.getTotal(sLikeP, sLikeQ, sEqP, sEqQ);
			collection = registerCallService.search(sLikeP, sLikeQ, sEqP, sEqQ,  false, sSortP,rowsetint, countSet.intValue());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println("tes size" + collection.size());
		request.setAttribute("index", new Integer(index));
		request.setAttribute("navigation", navigation);
		request.setAttribute("countSet", countSet);
		request.setAttribute("count", new Integer(count));
		request.setAttribute("alert", request.getParameter("alert"));
		request.setAttribute("minIndex", new Integer(minIndex));
		request.setAttribute("maxIndex", new Integer(maxIndex));
		
//		System.out.println("tes size" + collection.size());s
		result.addObject("RegisterCalls", collection);
		return result;
	}
	
	public Collection<ActionResult> doMobileAnnouncementByUserId(
			RegisterCall registerCall) {

		Collection<User> theUser;
		String username = registerCall.getCreatedBy();
//		try {
//			theUser = userService.search(null, null, "userId", userId, 0, 1);
//		}catch(Exception e){
//			
//		}
		Collection<ActionResult> result = null;
		result = new Vector();
		System.out.println("masuk panic announcement");
		
		Result sentAnnouncementResult = null;
				
		try {
			theUser = userService.search(null, null, "username", username, 0, 1);
			JSONObject obj = new JSONObject();

			obj.put("type","panic");
			obj.put("panicId",""+registerCall.getId());
			obj.put("title", NOTIF_TITLE);
			obj.put("myMessage", NOTIF_MESSAGE);
			System.out.print(obj);
			if(theUser != null){
				Iterator<User> userIterator = theUser.iterator();
				while(userIterator.hasNext()){
					User user = userIterator.next();
					System.out.println("Found " + theUser.size() + " user with mobileDeviceId : "+user.getMobileDeviceId());
					Sender sender = new Sender(GOOGLE_SERVER_KEY);
					com.google.android.gcm.server.Message message = new com.google.android.gcm.server.Message.Builder().timeToLive(30)
							.delayWhileIdle(true).addData(MESSAGE_KEY, obj.toString()).build();
					
					sentAnnouncementResult = sender.send(message, user.getMobileDeviceId(), 1);
					System.out.println("Mobile Announcement Result : " + sentAnnouncementResult.toString());
					
					ActionResult actionResult = new ActionResult();
					actionResult.setResult(true);
					actionResult.setReason("Sent announcement to " + user.getUsername() + " with deviceId : " + user.getMobileDeviceId());
					System.out.println("Sent announcement to " + user.getUsername() + " with deviceId : " + user.getMobileDeviceId());
					
					result.add(actionResult);
				}
			}
//			theUser = userService.search(null, null, "mobileDeviceId", mobileDeviceId, 0, 10);
//			theUser = userService.get(userId);
				
//			User user = userIterator.next();

			 			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return result;
		
	}
	
	private ModelAndView alertGCMPerformed(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		Integer id = WebUtil.getParameterInteger(request, "id");
		try{
			RegisterCall registerCall = registerCallService.get(id);
			doMobileAnnouncementByUserId(registerCall);
		}catch(Exception e){
			
		}
		return searchRegisterCall(request,response,"searchRegisterCall");
	}

}
