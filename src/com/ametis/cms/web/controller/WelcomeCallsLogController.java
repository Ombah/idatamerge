package com.ametis.cms.web.controller;

import java.util.Collection;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.RegisterCall;
import com.ametis.cms.datamodel.WelcomeCallsLog;
import com.ametis.cms.service.SecurityService;
import com.ametis.cms.service.WelcomeCallsLogService;
import com.ametis.cms.util.WebUtil;

public class WelcomeCallsLogController implements Controller{
	
	WelcomeCallsLogService welcomeCallsLogService;
	SecurityService securityService;
	
	public SecurityService getSecurityService() {
		return securityService;
	}

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	public WelcomeCallsLogService getWelcomeCallsLogService() {
		return welcomeCallsLogService;
	}

	public void setWelcomeCallsLogService(WelcomeCallsLogService welcomeCallsLogService) {
		this.welcomeCallsLogService = welcomeCallsLogService;
	}

	@Override
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		String navigation = request.getParameter("navigation");
		if(navigation==null){
			navigation = "searchWelcomeCallsLog";
		}
		ModelAndView result = null;
		if(navigation.equalsIgnoreCase("hapus")){
			result = deletePerformed(request,response);
		}else{
			result = searchWelcomeCallsLog(request,response,"searchWelcomeCallsLog",null);
		}
		return result;
	}

	public ModelAndView deletePerformed(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView result = null;
		try{
			Integer id = WebUtil.getParameterInteger(request, "id");
			ActionUser user = securityService.getActionUser(request);
			WelcomeCallsLog welcomeCallsLog = welcomeCallsLogService.delete(id, user);
			Integer refId = welcomeCallsLog.getWelcomeCallsId().getId();
			result = searchWelcomeCallsLog(request, response, "searchWelcomeCallsLog",refId);
		}catch (Exception e) {
			// nanti kalo sudah OK codenya e.printStackTrace nya di hapus saja
			e.printStackTrace();
			result = new ModelAndView("error");
		}
		return result;
	}
	
	private ModelAndView searchWelcomeCallsLog(HttpServletRequest request, HttpServletResponse response, String location,Integer refId) {
		// TODO Auto-generated method stub
		ModelAndView result = null;
		result = new ModelAndView(location);
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Integer index = WebUtil.getParameterInteger(request, "index");
		if (index == null){
			index = new Integer(1);			
		}
		request.setAttribute("index", new Integer(index));
		Collection collection = null;
		Vector vLikeP = new Vector();
		Vector vLikeQ = new Vector();
		Vector vEqP = new Vector();
		Vector vEqQ = new Vector();
		Integer welcomeCallsId = WebUtil.getParameterInteger(request, "welcomeCallsId");
		if(welcomeCallsId!=null){
			vEqP.add("welcomeCallsId.id");
			vEqQ.add(welcomeCallsId);	
		}else{
			welcomeCallsId = refId;
			vEqP.add("welcomeCallsId.id");
			vEqQ.add(welcomeCallsId);
		}
		
		vEqP.add("deleted");
		vEqQ.add(0);
		String sLikeP[] = new String[vLikeP.size()];
		vLikeP.toArray(sLikeP);
		Object sLikeQ[] = new Object[vLikeP.size()];
		vLikeQ.toArray(sLikeQ);
		String sEqP[] = new String[vEqP.size()];
		vEqP.toArray(sEqP);
		Object sEqQ[] = new Object[vEqP.size()];
		vEqQ.toArray(sEqQ);
		
		try {
			collection = welcomeCallsLogService.search(sEqP, sEqQ);			
			System.out.println("size  = "+collection.size());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println("tes size" + collection.size());
		result.addObject("WelcomeCallsLogs", collection);
		return result;
	}
}
