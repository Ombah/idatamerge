package com.ametis.cms.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.ametis.cms.service.ActivityLogService;
import com.ametis.cms.service.UserMenuService;

public class UserMenuController implements Controller{

	private UserMenuService userMenuService;
	private ActivityLogService logService;
	public UserMenuService getUserMenuService() {
		return userMenuService;
	}
	public void setUserMenuService(UserMenuService userMenuService) {
		this.userMenuService = userMenuService;
	}
	public ActivityLogService getLogService() {
		return logService;
	}
	public void setLogService(ActivityLogService logService) {
		this.logService = logService;
	}
	@Override
	public ModelAndView handleRequest(HttpServletRequest arg0, HttpServletResponse arg1) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
