package com.ametis.cms.web.controller;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.ametis.cms.datamodel.ActionResult;
import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Document;
import com.ametis.cms.datamodel.DocumentCategory;
import com.ametis.cms.service.CaseService;
import com.ametis.cms.service.ClaimService;
import com.ametis.cms.service.DocumentCategoryService;
import com.ametis.cms.service.DocumentService;
import com.ametis.cms.service.MemberService;
import com.ametis.cms.service.SecurityService;
import com.ametis.cms.service.UserService;
import com.ametis.cms.util.StringUtil;
import com.ametis.cms.util.TimeUtils;
import com.ametis.cms.util.WebUtil;
import com.ametis.cms.web.form.DocumentForm;

public class ReceiveDocumentClaimController extends SimpleFormController {

	private Integer countSet;
	private Integer maxPercountSet;
	DocumentService documentService;
	ResourceBundleMessageSource alertProperties;
	private SecurityService securityService;
	private CaseService caseService;
	private String navs = "";
	private String searchText = "";
	private Date searchDateAwal = null;
	private Date searchDateAkhir = null;
	private Integer searchStatus = null;
	private Integer index = null;
	private String arah = "'";
	// foreign affairs

	ClaimService claimService;

	public CaseService getCaseService() {
		return caseService;
	}

	public void setCaseService(CaseService caseService) {
		this.caseService = caseService;
	}

	public void setClaimService(ClaimService obj) {
		this.claimService = obj;
	}

	public ClaimService getClaimService() {
		return this.claimService;
	}

	public void setCountSet(Integer countSet) {
		this.countSet = countSet;
	}

	public Integer getCountSet() {
		return this.countSet;
	}

	public void setMaxPercountSet(Integer maxCount) {
		this.maxPercountSet = maxCount;
	}

	public Integer getMaxPercountSet() {
		return this.maxPercountSet;
	}

	DocumentCategoryService documentCategoryService;

	public void setDocumentCategoryService(DocumentCategoryService obj) {
		this.documentCategoryService = obj;
	}

	public DocumentCategoryService getDocumentCategoryService() {
		return this.documentCategoryService;
	}

	MemberService memberService;

	public void setMemberService(MemberService obj) {
		this.memberService = obj;
	}

	public MemberService getMemberService() {
		return this.memberService;
	}

	// -- foreign affairs end

	public void setDocumentService(DocumentService object) {
		this.documentService = object;
	}

	public DocumentService getDocumentService() {
		return this.documentService;
	}

	// generate by default
	private UserService actionuserService;

	public UserService getActionUserService() {
		return actionuserService;
	}

	public void setActionUserService(UserService userService) {
		this.actionuserService = userService;
	}

	public void setPropertiesUtil(ResourceBundleMessageSource object) {
		this.alertProperties = object;
	}

	public ResourceBundleMessageSource getPropertiesUtil() {
		return this.alertProperties;
	}

	public void setSecurityService(SecurityService object) {
		this.securityService = object;
	}

	public SecurityService getSecurityService() {
		return this.securityService;
	}

	public ReceiveDocumentClaimController() {
		setSessionForm(true);
		setValidateOnBinding(true);
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		Object result = null;
		DocumentForm tmp = new DocumentForm();
		result = tmp;
		return result;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {

		Integer batchClaimId = WebUtil.getParameterInteger(request, "batchClaimId");
		Integer claimId = WebUtil.getParameterInteger(request, "claimId");
		Integer index = WebUtil.getParameterInteger(request, "index");
		Integer caseId = WebUtil.getParameterInteger(request, "caseId");
		String searchby = WebUtil.getParameterString(request, "searchby", "");
		String navigation = WebUtil.getParameterString(request, "navigation", "");

		index = 0;

		String breadcrumb = "";
		breadcrumb = "<a href=\"receivedocumentclaim"
				+ "\" class=\"linkbreadcrumb\">Receive Upload Document Document</a>";

		request.setAttribute("breadcrumb", breadcrumb);
		request.setAttribute("batchClaimId", batchClaimId);
		request.setAttribute("claimId", claimId);
		request.setAttribute("index", index);
		request.setAttribute("searchby", searchby);
		request.setAttribute("caseId", caseId);
		request.setAttribute("navigation", navigation);
	}

	protected Map referenceData(HttpServletRequest request) throws Exception {
		Map result = new HashMap();
		System.out.println("kepanggil references data");
		try {
			System.out.println("Call referenceData");

			String rowset = WebUtil.getParameterString(request, "rowset", "0");

//			Integer index = WebUtil.getParameterInteger(request, "index");

			String navigation = WebUtil.getParameterString(request, "navigation", "");

			String searchby = WebUtil.getParameterString(request, "searchby", "");

			System.out.println("searchtext = "+searchText);
			
			System.out.println(" doc status = "+searchStatus);
			// String sortby = WebUtil.getParameterString(request, "sortby",
			// "");

			int minIndex = 0;
			int maxIndex = 0;
			int totalIndex = 0;

			Collection collection = null;

			int rowsetint = 0;
			int count = 0;

			if (StringUtils.isNumeric(rowset)) {
				rowsetint = Integer.parseInt(rowset);
			}
			Vector vLikeP = new Vector();
			Vector vLikeQ = new Vector();
			Vector vEqP = new Vector();
			Vector vEqQ = new Vector();

			if (navigation.trim().equalsIgnoreCase("gosearch")) {
				if(!searchText.trim().equals("")){
					vLikeP.add("originalDocName");
					vLikeQ.add(this.searchText);
				}if(searchStatus!=null && searchStatus != -1){
					System.out.println("searchStatus = "+searchStatus);
					vEqP.add("status");
					vEqQ.add(searchStatus);
				}
				
				
			} else {
				searchText = "";
			}

			// if (!navs.trim().equals("") && searchDate != null) {
			// vLikeP.add("createdBy");
			// // SimpleDateFormat formater = new
			// // SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			// // String tgl = formater.format(searchDate);
			// vLikeQ.add(searchDate);
			//
			// }

			ActionUser user = securityService.getActionUser(request);
			// vLikeP.add("documentCategoryId.documentCategoryId");
			// vLikeQ.add(6);

			/* remove filter createdby */
			// vLikeP.add("createdBy");
			// vLikeQ.add(user.getUser().getUsername());

			vEqP.add("deletedStatus");
			vEqQ.add(new Integer(0));
			
			vEqP.add("documentType");
			vEqQ.add(new Integer(1));

			String sLikeP[] = new String[vLikeP.size()];
			vLikeP.toArray(sLikeP);
			Object sLikeQ[] = new Object[vLikeP.size()];
			vLikeQ.toArray(sLikeQ);

			String sEqP[] = new String[vEqP.size()];
			vEqP.toArray(sEqP);
			Object sEqQ[] = new Object[vEqP.size()];
			vEqQ.toArray(sEqQ);

			if (searchDateAwal != null && !searchDateAwal.toString().equalsIgnoreCase("1970-01-01")
					&& searchDateAkhir != null && !searchDateAkhir.toString().equalsIgnoreCase("1970-01-01")) {

				Object[] minColumn = { searchDateAwal };
				Object[] maxColumn = { searchDateAkhir };
				String[] betweenCount = { "createdTime" };

				count = documentService.getTotal(sLikeP, sLikeQ, sEqP, sEqQ, betweenCount, minColumn, maxColumn);
			} else {

				count = documentService.getTotal(sLikeP, sLikeQ, sEqP, sEqQ);
			}

//			String arah = WebUtil.getParameterString(request, "arah", "");

			if (index == null)
				index = new Integer(1);

			if (arah.equals("kanan"))
				index = new Integer(index.intValue() + 1);
			else if (arah.equals("kiri"))
				index = new Integer(index.intValue() - 1);
			else if (arah.equals("kiribgt"))
				index = new Integer(1);
			else if (arah.equals("kananbgt"))
				index = new Integer(count / countSet.intValue() + 1);

			if (index.compareTo(new Integer(1)) == new Integer(-1).intValue())
				index = new Integer(1);
			else if (index.compareTo(new Integer(count / countSet.intValue() + 1)) == new Integer(1).intValue())
				index = new Integer(count / countSet.intValue() + 1);

			rowsetint = (new Integer((index.intValue() - 1) * countSet.intValue())).intValue();
			if (count % countSet.intValue() > 0) {
				result.put("halAkhir", new Integer(count / countSet.intValue() + 1));
			} else {
				result.put("halAkhir", new Integer(count / countSet.intValue()));
			}

			minIndex = (index - 1) * countSet;
			maxIndex = index * countSet;

			if (maxIndex > count) {
				maxIndex = count;
			}

			String required[] = new String[] { "Document.ClaimId", "Document.DocumentCategoryId",
					"Document.MemberId", };

			if (searchDateAwal != null && !searchDateAwal.toString().equalsIgnoreCase("1970-01-01")
					&& searchDateAkhir != null && !searchDateAkhir.toString().equalsIgnoreCase("1970-01-01")) {

				Object[] minColumn = { searchDateAwal };
				Object[] maxColumn = { searchDateAkhir };
				String[] betweenColumn = { "createdTime" };

				collection = documentService.search(sLikeP, sLikeQ, sEqP, sEqQ, betweenColumn, minColumn, maxColumn,
						false, "createdTime", required, rowsetint, countSet.intValue());
			} else {
				collection = documentService.search(sLikeP, sLikeQ, sEqP, sEqQ, false, "createdTime", required,
						rowsetint, countSet.intValue());
			}

			if (collection.size() <= 0) {
				index = new Integer(index.intValue() - 1);
				if (index.compareTo(new Integer(1)) == new Integer(-1).intValue())
					index = new Integer(1);
				else if (index.compareTo(new Integer(count / countSet.intValue() + 1)) == new Integer(1).intValue())
					index = new Integer(count / countSet.intValue() + 1);

				rowsetint = (new Integer((index.intValue() - 1) * countSet.intValue())).intValue();
				if (count % countSet.intValue() > 0) {
					result.put("halAkhir", new Integer(count / countSet.intValue() + 1));
				} else {
					result.put("halAkhir", new Integer(count / countSet.intValue()));
				}
				collection = documentService.search(sLikeP, sLikeQ, sEqP, sEqQ, required, rowsetint,
						countSet.intValue());
			}

			request.setAttribute("searchtext", searchText);
			request.setAttribute("searchby", searchby);
			request.setAttribute("navigation", navigation);
			request.setAttribute("countSet", countSet);
			request.setAttribute("index", new Integer(index));
			request.setAttribute("count", new Integer(count));
			request.setAttribute("alert", request.getParameter("alert"));
			request.setAttribute("minIndex", new Integer(minIndex));
			request.setAttribute("maxIndex", new Integer(maxIndex));
			request.setAttribute("Documents", collection);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		System.out.println("kepanggil onsubmit");
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		String searchtext = WebUtil.getParameterString(request, "searchtext", "");

		String temparah = WebUtil.getParameterString(request, "arah", "");
		Integer tempindex = WebUtil.getParameterInteger(request, "index");
		Date searchdate = WebUtil.getParameterDate(request, "upload_date");
		
		Integer searchstatus = WebUtil.getParameterInteger(request, "searchDocumentStatus");
		System.out.println(" doc status = "+searchstatus);
		
		if (tempindex!=null) {
			index = new Integer(tempindex);
		} else {
			arah = null;
		}
		
		if (!temparah.trim().equals("")) {
			arah = temparah;
		} else {
			arah = "";
		}
		
		if (navigation.equalsIgnoreCase("gosearch")) {
			if (searchdate != null) {
				SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat formater2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

				String tglAwal = formater.format(searchdate) + " 00:00:00";
				String tglAkhir = formater.format(searchdate) + " 23:59:59";

				Date tglawal = formater2.parse(tglAwal);
				Date tglakhir = formater2.parse(tglAkhir);
				searchDateAwal = tglawal;
				searchDateAkhir = tglakhir;
			}

			if (!searchtext.trim().equals("")) {
				searchText = searchtext;
			} else {
				searchText = "";
			}
			
			if(searchstatus!=null){
				System.out.println("search status !=null");
				searchStatus = searchstatus;
			}else{
				System.out.println("search status ==null");
				searchStatus = -1;
			}

			// navs = "gosearch";
			System.out.println("#x: " + navigation + ", searchText: " + searchText);
		}else if(navigation.equalsIgnoreCase("updateStatus")){
			
			int docId = WebUtil.getParameterInteger(request, "documentId");
			int newStatus = WebUtil.getParameterInteger(request, "newDocumentStatus");
			Document old = documentService.get(docId);
			old.setStatus(newStatus);
			ActionUser user = securityService.getActionUser(request);
			documentService.update(old, user);
			System.out.println("doc id="+docId+"  new status = "+newStatus);
		}else if(navigation.equalsIgnoreCase("delete")){
			int docId = WebUtil.getParameterInteger(request, "documentId");
			ActionUser user = securityService.getActionUser(request);
			documentService.delete(docId, user);
			System.out.println("doc id="+docId+"  deleted");
		}else {
			System.out.println("#y: " + navigation + ", searchText: " + searchText);
			// navs = "";
			searchText = "";
			searchDateAwal = null;
			searchDateAkhir = null;	
		}
		

		String alertMsg = "";

		String breadcrumb = "";
		breadcrumb = "<a href=\"receivedocumentclaim\">Receive Upload Document Claim</a>";

		request.setAttribute("breadcrumb", breadcrumb);

		return new ModelAndView(new RedirectView("receivedocumentclaim?navigation=gosearch" + alertMsg));
	}

	protected void initBinder(HttpServletRequest req, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(req, binder);
		CustomDateEditor cde = new CustomDateEditor(new SimpleDateFormat("yyyy/MM/dd"), true);
		binder.registerCustomEditor(Date.class, cde);
		CustomNumberEditor num = new CustomNumberEditor(Number.class, true);
		binder.registerCustomEditor(Number.class, num);
	}
}
