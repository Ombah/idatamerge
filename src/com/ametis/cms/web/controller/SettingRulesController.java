package com.ametis.cms.web.controller;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.ametis.cms.service.ProcessNotificationService;
import com.ametis.cms.service.ProductService;
import com.ametis.cms.util.WebUtil;
import com.ametis.cms.web.form.DocumentForm;

public class SettingRulesController extends SimpleFormController {

	ProcessNotificationService processNotificationService;
	ProductService productService;
	private Integer countSet;
	private Integer maxPercountSet;

	public Integer getCountSet() {
		return countSet;
	}

	public void setCountSet(Integer countSet) {
		this.countSet = countSet;
	}

	public Integer getMaxPercountSet() {
		return maxPercountSet;
	}

	public void setMaxPercountSet(Integer maxPercountSet) {
		this.maxPercountSet = maxPercountSet;
	}

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public ProcessNotificationService getProcessNotificationService() {
		return processNotificationService;
	}

	public void setProcessNotificationService(ProcessNotificationService processNotificationService) {
		this.processNotificationService = processNotificationService;
	}

	public SettingRulesController() {
		System.out.println("tes kepanggil constructor di  setting rules controller");
		setSessionForm(true);
		setValidateOnBinding(true);
	}

	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		System.out.println("tes kepanggil form backing object di setting rules controller");
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Integer productId = WebUtil.getParameterInteger(request, "productId");
		System.out.println("nav = " + navigation + "product id = " + productId);
		Object result = null;
		DocumentForm tmp = new DocumentForm();
		result = tmp;
		return result;
	}

	protected void onBindAndValidate(HttpServletRequest request, Object command, BindException errors)
			throws Exception {
		System.out.println("tes kepanggil on bind and validate di  setting rules controller");
		String navigation = WebUtil.getParameterString(request, "navigation", "");
		Integer productId = WebUtil.getParameterInteger(request, "productId");
		System.out.println("nav = " + navigation + "product id = " + productId);
		request.setAttribute("navigation", navigation);
		request.setAttribute("productId", productId);
	}

	protected Map referenceData(HttpServletRequest request) throws Exception {
		Map result = new HashMap();
		try {
			System.out.println("Call referenceData on  setting rules controller");
			String navigation = WebUtil.getParameterString(request, "navigation", "");
			Integer productId = WebUtil.getParameterInteger(request, "productId");
			System.out.println("nav = " + navigation + "product id = " + productId);
			Collection collection = null;
			collection = productService.getAll();
			request.setAttribute("Products", collection);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	protected ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse response, Object command,
			BindException errors) throws Exception {
		System.out.println("kepanggil onsubmit di setting rules controller");
		String navigation = WebUtil.getParameterString(request, "navigation", "");

		if (navigation.equalsIgnoreCase("gosetting")) {
			System.out.println("called nav = gosetting");
			Integer productId = WebUtil.getParameterInteger(request, "productId");
			System.out.println("nav = " + navigation + "product id = " + productId);
			return new ModelAndView(new RedirectView("settingrulesproduct?navigation=gosetting"));
		} else {
			String alertMsg = "";
			String breadcrumb = "";
			breadcrumb = "<a href=\"receivedocumentclaim\">Receive Upload Document Claim</a>";
			request.setAttribute("breadcrumb", breadcrumb);
			return new ModelAndView(new RedirectView("settingrulesproduct" + alertMsg));
		}
	}

	protected void initBinder(HttpServletRequest req, ServletRequestDataBinder binder) throws Exception {
		super.initBinder(req, binder);
		CustomDateEditor cde = new CustomDateEditor(new SimpleDateFormat("yyyy/MM/dd"), true);
		binder.registerCustomEditor(Date.class, cde);
		CustomNumberEditor num = new CustomNumberEditor(Number.class, true);
		binder.registerCustomEditor(Number.class, num);
	}
}
