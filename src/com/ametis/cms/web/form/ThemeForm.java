package com.ametis.cms.web.form;

import java.sql.Timestamp;

import org.springframework.web.multipart.MultipartFile;

import com.ametis.cms.datamodel.Client;
import com.ametis.cms.datamodel.Info;
import com.ametis.cms.datamodel.MemberGroup;
import com.ametis.cms.datamodel.Theme;

public class ThemeForm implements java.io.Serializable {
	private Integer themeId;
	private Integer membergroupId;
	private MultipartFile logo;
	private MultipartFile bg;
	private Timestamp createdTime;
	private Timestamp modifiedTime;
	private Timestamp deletedTime;
	private String createdBy;
	private String modifiedBy;
	private String deletedBy;
	private Integer deleted;
	boolean isNewTheme = false;
	Theme themeBean;
	private String memberGroupName;
	private String originalBgName;
	private String originalLogoName;
	
	public String getOriginalBgName() {
		return originalBgName;
	}
	public void setOriginalBgName(String originalBgName) {
		this.originalBgName = originalBgName;
	}
	public String getOriginalLogoName() {
		return originalLogoName;
	}
	public void setOriginalLogoName(String originalLogoName) {
		this.originalLogoName = originalLogoName;
	}
	public String getMemberGroupName() {
		return memberGroupName;
	}
	public void setMemberGroupName(String memberGroupName) {
		this.memberGroupName = memberGroupName;
	}
	public boolean isNewTheme() {
		return isNewTheme;
	}
	public void setNewTheme(boolean isNewTheme) {
		this.isNewTheme = isNewTheme;
	}
	public Theme getThemeBean() {
		return themeBean;
	}
	public void setThemeBean(Theme themeBean) {
		this.themeBean = themeBean;
	}
	public ThemeForm(){
		this.themeBean = new Theme();
		this.isNewTheme= true;
	}
	public Integer getThemeId() {
		return themeId;
	}
	public void setThemeId(Integer themeId) {
		this.themeId = themeId;
	}

	public Integer getMembergroupId() {
		return membergroupId;
	}
	public void setMembergroupId(Integer membergroupId) {
		this.membergroupId = membergroupId;
	}
	
	
	public MultipartFile getLogo() {
		return logo;
	}
	public void setLogo(MultipartFile logo) {
		this.logo = logo;
	}
	public MultipartFile getBg() {
		return bg;
	}
	public void setBg(MultipartFile bg) {
		this.bg= bg;
	}
	public Timestamp getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}
	public Timestamp getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(Timestamp modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	public Timestamp getDeletedTime() {
		return deletedTime;
	}
	public void setDeletedTime(Timestamp deletedTime) {
		this.deletedTime = deletedTime;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}
	public Integer getDeleted() {
		return deleted;
	}
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}
	
}
