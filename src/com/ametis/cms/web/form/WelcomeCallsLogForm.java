package com.ametis.cms.web.form;

import java.sql.Timestamp;
import java.util.Date;

import com.ametis.cms.datamodel.WelcomeCalls;
import com.ametis.cms.datamodel.WelcomeCallsLog;

public class WelcomeCallsLogForm  implements java.io.Serializable{
	WelcomeCallsLog welcomeCallsLogBean;
	boolean isNewWelcomeCallsLog=false;
	
	private Integer id;
	private Timestamp callTime;
	private String callDescription;
	private Integer callCategory;
	private Integer followupNeeded;
	private Integer statusCall;
	private Integer deleted;
	private String createdBy;
	private WelcomeCalls welcomeCallsId;
	private String modifiedBy;
	private java.sql.Timestamp createdTime;
	private java.sql.Timestamp modifiedTime;
	private String hour;
	private String minute;
	private Date date;
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}

	public String getMinute() {
		return minute;
	}

	public void setMinute(String minute) {
		this.minute = minute;
	}

	public WelcomeCallsLogForm(){
		this.welcomeCallsLogBean = new WelcomeCallsLog();
		this.isNewWelcomeCallsLog = true;
	}
	
	public WelcomeCallsLog getWelcomeCallsLogBean() {
		return welcomeCallsLogBean;
	}

	public void setWelcomeCallsLogBean(WelcomeCallsLog welcomeCallsLogBean) {
		this.welcomeCallsLogBean = welcomeCallsLogBean;
	}

	public boolean isNewWelcomeCallsLog() {
		return isNewWelcomeCallsLog;
	}

	public void setNewWelcomeCallsLog(boolean isNewWelcomeCallsLog) {
		this.isNewWelcomeCallsLog = isNewWelcomeCallsLog;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Timestamp getCallTime() {
		return callTime;
	}
	public void setCallTime(Timestamp callTime) {
		this.callTime = callTime;
	}
	public String getCallDescription() {
		return callDescription;
	}
	public void setCallDescription(String callDescription) {
		this.callDescription = callDescription;
	}
	public Integer getCallCategory() {
		return callCategory;
	}
	public void setCallCategory(Integer callCategory) {
		this.callCategory = callCategory;
	}
	public Integer getFollowupNeeded() {
		return followupNeeded;
	}
	public void setFollowupNeeded(Integer followupNeeded) {
		this.followupNeeded = followupNeeded;
	}
	public Integer getStatusCall() {
		return statusCall;
	}
	public void setStatusCall(Integer statusCall) {
		this.statusCall = statusCall;
	}
	public Integer getDeleted() {
		return deleted;
	}
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public WelcomeCalls getWelcomeCallsId() {
		return welcomeCallsId;
	}
	public void setWelcomeCallsId(WelcomeCalls welcomeCallsId) {
		this.welcomeCallsId = welcomeCallsId;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public java.sql.Timestamp getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(java.sql.Timestamp createdTime) {
		this.createdTime = createdTime;
	}
	public java.sql.Timestamp getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(java.sql.Timestamp modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	
	
}
