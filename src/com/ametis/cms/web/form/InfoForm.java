package com.ametis.cms.web.form;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.mail.Multipart;

import org.springframework.web.multipart.MultipartFile;

import com.ametis.cms.datamodel.Consultation;
import com.ametis.cms.datamodel.Info;

public class InfoForm implements java.io.Serializable{
	boolean isNewInfo = false;
	Info infoBean;
	
	private Integer id;
	private String title;
	private String createdBy;
	private String deletedBy;
	private String modifiedBy;
	private String content;
	private Integer deletedStatus;
	private java.sql.Timestamp createdTime;
	private java.sql.Timestamp deletedTime;
	private java.sql.Timestamp modifiedTime;
	private MultipartFile file;
	private String originalFilename;
	private Date startDate;
	private Date endDate;

	public String getOriginalFilename() {
		return originalFilename;
	}

	public void setOriginalFilename(String originalFilename) {
		this.originalFilename = originalFilename;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}
	public InfoForm(){
		this.infoBean = new Info();
		this.isNewInfo= true;
	}
	
	public boolean isNewInfo() {
		return isNewInfo;
	}
	public void setNewInfo(boolean isNewInfo) {
		this.isNewInfo = isNewInfo;
	}
	public Info getInfoBean() {
		return infoBean;
	}
	public void setInfoBean(Info infoBean) {
		this.infoBean = infoBean;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Integer getDeletedStatus() {
		return deletedStatus;
	}
	public void setDeletedStatus(Integer deletedStatus) {
		this.deletedStatus = deletedStatus;
	}
	public java.sql.Timestamp getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(java.sql.Timestamp createdTime) {
		this.createdTime = createdTime;
	}
	public java.sql.Timestamp getDeletedTime() {
		return deletedTime;
	}
	public void setDeletedTime(java.sql.Timestamp deletedTime) {
		this.deletedTime = deletedTime;
	}
	public java.sql.Timestamp getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(java.sql.Timestamp modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	
	
}
