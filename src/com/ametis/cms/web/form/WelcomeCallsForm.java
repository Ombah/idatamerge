package com.ametis.cms.web.form;

import java.sql.Timestamp;

import com.ametis.cms.datamodel.Info;
import com.ametis.cms.datamodel.Member;
import com.ametis.cms.datamodel.WelcomeCalls;

public class WelcomeCallsForm implements java.io.Serializable {
	private Integer id;
	private Timestamp createdTime;
	private String createdBy;
	private Timestamp modifiedTime;
	private String modifiedBy;
	private Integer memberId;
	private String memberName;
	private Integer deleted;
	private Integer status;
	private String telephoneNumber;
	private Integer logType;
	
	boolean isNewWelcomeCalls = false;
	WelcomeCalls welcomeCallsBean;
	
	public WelcomeCallsForm(){
		this.welcomeCallsBean = new WelcomeCalls();
		this.isNewWelcomeCalls= true;
	}
	
	public boolean isNewWelcomeCalls() {
		return isNewWelcomeCalls;
	}
	public void setNewWelcomeCalls(boolean isNewWelcomeCalls) {
		this.isNewWelcomeCalls = isNewWelcomeCalls;
	}
	public WelcomeCalls getWelcomeCallsBean() {
		return welcomeCallsBean;
	}
	public void setWelcomeCallsBean(WelcomeCalls welcomeCallsBean) {
		this.welcomeCallsBean = welcomeCallsBean;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Timestamp getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(Timestamp modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Integer getMemberId() {
		return memberId;
	}
	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public Integer getDeleted() {
		return deleted;
	}
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	public Integer getLogType() {
		return logType;
	}
	public void setLogType(Integer logType) {
		this.logType = logType;
	}
	
	
}
