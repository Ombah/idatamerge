package com.ametis.cms.web.form;

import org.springframework.web.multipart.MultipartFile;

import com.ametis.cms.datamodel.Case;
import com.ametis.cms.datamodel.Consultation;

public class ConsultationForm implements java.io.Serializable{
	
	boolean isNewConsultation = false;
	Consultation consultationBean;
	private Integer id;
	private String title;
	private String message;
	private String createdBy;
	private Integer createdById;//username tujuan
	private String modifiedBy;
	private java.sql.Timestamp createdTime;
	private java.sql.Timestamp modifiedTime;
	private MultipartFile file;
	private String originalFilename;
	private String memberName;
	private String type;
	public ConsultationForm(){
		this.consultationBean = new Consultation();
		this.isNewConsultation= true;
	}
	public boolean isNewConsultation() {
		return isNewConsultation;
	}
	public void setIsNewConsultation(boolean isNewConsultation) {
		this.isNewConsultation = isNewConsultation;
	}
	public Consultation getConsultationBean() {
		return consultationBean;
	}
	public void setConsultationBean(Consultation consultationBean) {
		this.consultationBean = consultationBean;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getCreatedById() {
		return createdById;
	}
	public void setCreatedById(Integer createdById) {
		this.createdById = createdById;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public java.sql.Timestamp getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(java.sql.Timestamp createdTime) {
		this.createdTime = createdTime;
	}
	public java.sql.Timestamp getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(java.sql.Timestamp modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	public String getOriginalFilename() {
		return originalFilename;
	}
	public void setOriginalFilename(String originalFilename) {
		this.originalFilename = originalFilename;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	
	
}
