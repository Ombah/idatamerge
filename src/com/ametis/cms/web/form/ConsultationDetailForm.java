package com.ametis.cms.web.form;

import com.ametis.cms.datamodel.Case;
import com.ametis.cms.datamodel.ConsultationDetail;

public class ConsultationDetailForm implements java.io.Serializable{
	
	boolean isNewConsultationDetail = false;
	ConsultationDetail consultationDetailBean;
	private Integer id;
	
	private String message;
	private Integer consultationId;
	private Integer deletedStatus;
	private String createdBy;
	private Integer createdById;
	
	private String modifiedBy;
	private java.sql.Timestamp createdTime;
	private java.sql.Timestamp modifiedTime;
	
	
	
	public Integer getConsultationId() {
		return consultationId;
	}

	public void setConsultationId(Integer consultationId) {
		this.consultationId = consultationId;
	}

	public ConsultationDetailForm(){
		this.consultationDetailBean = new ConsultationDetail();
		this.isNewConsultationDetail= true;
	}
	
	public ConsultationDetail getConsultationDetailBean() {
		return consultationDetailBean;
	}

	public void setConsultationDetailBean(ConsultationDetail consultationDetailBean) {
		this.consultationDetailBean = consultationDetailBean;
	}

	public boolean isNewConsultationDetail() {
		return isNewConsultationDetail;
	}

	public void setIsNewConsultationDetail(boolean isNewConsultaion) {
		this.isNewConsultationDetail = isNewConsultaion;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getDeletedStatus() {
		return deletedStatus;
	}
	public void setDeletedStatus(Integer deletedStatus) {
		this.deletedStatus = deletedStatus;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getCreatedById() {
		return createdById;
	}
	public void setCreatedById(Integer createdById) {
		this.createdById = createdById;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public java.sql.Timestamp getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(java.sql.Timestamp createdTime) {
		this.createdTime = createdTime;
	}
	public java.sql.Timestamp getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(java.sql.Timestamp modifiedTime) {
		this.modifiedTime = modifiedTime;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
