package com.ametis.cms.service.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;

import com.ametis.cms.dao.ProcessNotificationDao;
import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.ProcessNotification;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.datamodel.ProcessNotification;
import com.ametis.cms.service.ProcessNotificationService;
import com.ametis.cms.util.dao.DaoSupportUtil;

public class ProcessNotificationServiceImpl implements ProcessNotificationService{

	ProcessNotificationDao processNotificationDao; 
	
	public ProcessNotificationDao getProcessNotificationDao() {
		return processNotificationDao;
	}

	public void setProcessNotificationDao(ProcessNotificationDao processNotificationDao) {
		this.processNotificationDao = processNotificationDao;
	}

	@Override
	public ProcessNotification create(ProcessNotification object, ActionUser actionUser) throws Exception {
		// TODO Auto-generated method stub
		if (actionUser != null){
			User user = actionUser.getUser();					
		}
		ProcessNotification result = processNotificationDao.create(object);
		return result;
	}

	@Override
	public ProcessNotification update(ProcessNotification object, ActionUser actionUser) throws Exception {
		// TODO Auto-generated method stub
		if (actionUser != null) {
			User user = actionUser.getUser();
		}

		processNotificationDao.update(object);
		return object;
	}

	@Override
	public ProcessNotification trash(Serializable pkey) throws Exception {
		// TODO Auto-generated method stub
		ProcessNotification object = processNotificationDao.get(pkey);
		processNotificationDao.delete(object);
		return object;
	}

	@Override
	public ProcessNotification delete(Serializable pkey, ActionUser actionUser) throws Exception {
		// TODO Auto-generated method stub
		ProcessNotification object = processNotificationDao.get(pkey);
		// object.setDeletedDate (new java.sql.Date
		// (System.currentTimeMillis()));
		processNotificationDao.update(object);
		return object;
	}

	@Override
	public ProcessNotification delete(ProcessNotification object, ActionUser actionUser) throws Exception {
		// TODO Auto-generated method stub
		processNotificationDao.update(object);
		return object;
	}

	@Override
	public ProcessNotification get(Serializable pkey) throws Exception {
		// TODO Auto-generated method stub
		ProcessNotification object = null;
		object = processNotificationDao.get(pkey);
		return object;
	}

	@Override
	public ProcessNotification get(Serializable pkey, String[] required) throws Exception {
		// TODO Auto-generated method stub
		ProcessNotification object = null;
		object = processNotificationDao.get(pkey);
		DaoSupportUtil.lazyInit(required, object);
		return object;
	}

	@Override
	public Collection search(String[] eqColumns, Object[] eqParams) throws Exception {
		// TODO Auto-generated method stub
		Criteria c = processNotificationDao.getCriteria();
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		List list = c.list();
		return list;
	}

	@Override
	public int getTotal() throws Exception {
		// TODO Auto-generated method stub
		Criteria c = processNotificationDao.getCriteria();
		int total = DaoSupportUtil.getTotal(c);
		return total;
	}

	@Override
	public Collection getAll(String[] required) throws Exception {
		// TODO Auto-generated method stub
		Criteria c = processNotificationDao.getCriteria();
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			ProcessNotification element = (ProcessNotification) iter.next();
			DaoSupportUtil.lazyInit(required, element);
		}
		return list;
	}

	@Override
	public Collection getAll(String ColumnOrder, boolean Sorting) throws Exception {
		// TODO Auto-generated method stub
		Criteria c = processNotificationDao.getCriteria();
		DaoSupportUtil.setOrderBy(Sorting, ColumnOrder, c);
		List list = c.list();
		return list;
	}

	@Override
	public Collection getAll() throws Exception {
		Criteria c = processNotificationDao.getCriteria();
		List list = c.list();
		return list;
	}

	@Override
	public ProcessNotification searchUnique(String eqColumns, Object eqParams, String[] required) throws Exception {
		// TODO Auto-generated method stub
		Criteria c = processNotificationDao.getCriteria();
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		ProcessNotification obj = (ProcessNotification) c.uniqueResult();
		DaoSupportUtil.lazyInit(required, obj);
		return obj;
	}

	@Override
	public ProcessNotification searchUnique(String eqColumns, Object eqParams) throws Exception {
		// TODO Auto-generated method stub
		Criteria c = processNotificationDao.getCriteria();
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		ProcessNotification obj = (ProcessNotification) c.uniqueResult();
		return obj;
	}

	@Override
	public Criteria getCriteria() throws Exception {
		// TODO Auto-generated method stub
		Criteria c = processNotificationDao.getCriteria();
		return c;
	}

	@Override
	public DetachedCriteria getDetachedCriteria() throws Exception {
		// TODO Auto-generated method stub
		DetachedCriteria dc = processNotificationDao.getDetachedCriteria();
		return dc;
	}

	
}
