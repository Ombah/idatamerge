package com.ametis.cms.service.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.net.ntp.TimeStamp;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;

import com.ametis.cms.dao.RegisterCallDao;
import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Client;
import com.ametis.cms.datamodel.RegisterCall;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.datamodel.RegisterCall;
import com.ametis.cms.service.RegisterCallService;
import com.ametis.cms.util.dao.DaoSupportUtil;

public class RegisterCallServiceImpl implements RegisterCallService{
	
	RegisterCallDao registerCallDao;

	public RegisterCallDao getRegisterCallDao() {
		return registerCallDao;
	}

	public void setRegisterCallDao(RegisterCallDao registerCallDao) {
		this.registerCallDao = registerCallDao;
	}

	@Override
	public RegisterCall create(RegisterCall object, ActionUser actionUser) throws Exception {
		// TODO Auto-generated method stub
		User user = null;
		if (actionUser != null){
			user = actionUser.getUser();					
		}
		object.setCreatedTime(new java.sql.Timestamp(System.currentTimeMillis()));
		object.setUsername(user.getUsername());
		object.setCreatedBy(user.getUsername());
		object.setDeleted(0);
		RegisterCall result = registerCallDao.create (object);
		return result;
	}
	
	@Override
	public RegisterCall create(RegisterCall object) throws Exception {
		// TODO Auto-generated method stub
		object.setDeleted(0);
		object.setCreatedTime(new java.sql.Timestamp(System.currentTimeMillis()));
		RegisterCall result = registerCallDao.create(object);
		return result;
	}

	@Override
	public RegisterCall update(RegisterCall object, ActionUser actionUser) throws Exception {
		// TODO Auto-generated method stub
		if (actionUser != null) {
			User user = actionUser.getUser();
		}

		registerCallDao.update(object);
		return object;
	}

	@Override
	public RegisterCall trash(Serializable pkey) throws Exception {
		// TODO Auto-generated method stub
		RegisterCall object = registerCallDao.get(pkey);
		registerCallDao.delete(object);
		return object;
	}

	@Override
	public RegisterCall delete(Serializable pkey, ActionUser actionUser) throws Exception {
		// TODO Auto-generated method stub
		RegisterCall object = registerCallDao.get(pkey);
		object.setDeletedTime(new java.sql.Timestamp(System.currentTimeMillis()));
		object.setDeleted(new Integer(1));

		if (actionUser != null) {
			User user = actionUser.getUser();
			if (user != null) {
				object.setDeletedBy(user.getUsername());
			}
		}

		registerCallDao.update(object);
		return object;
	}

	@Override
	public RegisterCall delete(RegisterCall object, ActionUser actionUser) throws Exception {
		// TODO Auto-generated method stub
		registerCallDao.update(object);
		return object;
	}

	@Override
	public RegisterCall get(Serializable pkey) throws Exception {
		// TODO Auto-generated method stub
		RegisterCall object = null;
		object = registerCallDao.get(pkey);
		return object;
	}

	@Override
	public RegisterCall get(Serializable pkey, String[] required) throws Exception {
		// TODO Auto-generated method stub
		RegisterCall object = null;
		object = registerCallDao.get(pkey);
		DaoSupportUtil.lazyInit(required, object);
		return object;
	}


	@Override
	public int getTotal() throws Exception {
		// TODO Auto-generated method stub
		Criteria c = registerCallDao.getCriteria();
		int total = DaoSupportUtil.getTotal(c);
		return total;
	}

	@Override
	public Collection getAll(String[] required) throws Exception {
		// TODO Auto-generated method stub
		Criteria c = registerCallDao.getCriteria();
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			RegisterCall element = (RegisterCall) iter.next();
			DaoSupportUtil.lazyInit(required, element);
		}
		return list;
	}

	@Override
	public Collection getAll() throws Exception {
		// TODO Auto-generated method stub
		Criteria c = registerCallDao.getCriteria();
		List list = c.list();
		return list;
	}
	
	@Override
	public int getTotal(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams)
			throws Exception {

		Criteria c = registerCallDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		int total = DaoSupportUtil.getTotal(c);
		return total;

	}
	@Override
	public RegisterCall searchUnique(String eqColumns, Object eqParams, String[] required) throws Exception {
		// TODO Auto-generated method stub
		Criteria c = registerCallDao.getCriteria();
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		RegisterCall obj = (RegisterCall) c.uniqueResult();
		DaoSupportUtil.lazyInit(required, obj);
		return obj;
	}

	@Override
	public RegisterCall searchUnique(String eqColumns, Object eqParams) throws Exception {
		// TODO Auto-generated method stub
		Criteria c = registerCallDao.getCriteria();
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		RegisterCall obj = (RegisterCall) c.uniqueResult();
		return obj;
	}

	@Override
	public Criteria getCriteria() throws Exception {
		Criteria c = registerCallDao.getCriteria();
		return c;
	}

	@Override
	public DetachedCriteria getDetachedCriteria() throws Exception {
		// TODO Auto-generated method stub
		DetachedCriteria dc = registerCallDao.getDetachedCriteria();
		return dc;
	}

	@Override
	public Collection search(String[] eqColumns, Object[] eqParams) throws Exception {
		// TODO Auto-generated method stub
		Criteria c = registerCallDao.getCriteria();
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		List list = c.list();
		return list;
	}
	
	@Override
	public Collection search(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams,
			boolean asc, String columnOrder[], int index, int offset) throws Exception {

		Criteria c = registerCallDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setOrderBy(asc, columnOrder, c);
		List list = c.list();
		return list;

	}

}
