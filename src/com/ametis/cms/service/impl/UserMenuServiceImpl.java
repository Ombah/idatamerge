package com.ametis.cms.service.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;

import com.ametis.cms.dao.UserMenuDao;
import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.UserMenu;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.datamodel.UserMenu;
import com.ametis.cms.service.ActivityLogService;
import com.ametis.cms.service.UserMenuService;
import com.ametis.cms.util.dao.DaoSupportUtil;

public class UserMenuServiceImpl implements UserMenuService {
	
	private UserMenuDao userMenuDao;
	private ActivityLogService activityLogService;

	public UserMenuDao getUserMenuDao() {
		return userMenuDao;
	}

	public void setUserMenuDao(UserMenuDao userMenuDao) {
		this.userMenuDao = userMenuDao;
	}

	public ActivityLogService getActivityLogService() {
		return activityLogService;
	}

	public void setActivityLogService(ActivityLogService activityLogService) {
		this.activityLogService = activityLogService;
	}

	@Override
	public UserMenu create(UserMenu object, ActionUser actionUser) throws Exception {
		if (actionUser != null){
			User user = actionUser.getUser();					
		}
		UserMenu result = userMenuDao.create (object);
		return result;
	}

	@Override
	public UserMenu update(UserMenu object, ActionUser actionUser) throws Exception {

		/*
		 * Gue tambahin mekanisme NULL value checking just in case user nya null
		 */
		if (actionUser != null) {
			User user = actionUser.getUser();
		}

		userMenuDao.update(object);
		return object;
	}

	@Override
	public UserMenu trash(Serializable pkey) throws Exception {
		UserMenu object = userMenuDao.get(pkey);
		userMenuDao.delete(object);
		return object;
	}

	@Override
	public UserMenu delete(Serializable pkey, ActionUser actionUser) throws Exception {
		UserMenu object = userMenuDao.get(pkey);
		// object.setDeletedDate (new java.sql.Date
		// (System.currentTimeMillis()));
		userMenuDao.update(object);
		return object;
	}

	@Override
	public UserMenu delete(UserMenu object, ActionUser actionUser) throws Exception {

		userMenuDao.update(object);
		return object;
	}

	@Override
	public UserMenu get(Serializable pkey) throws Exception {
		UserMenu object = null;
		object = userMenuDao.get(pkey);
		return object;
	}

	@Override
	public UserMenu get(Serializable pkey, String[] required) throws Exception {
		UserMenu object = null;
		object = userMenuDao.get(pkey);
		DaoSupportUtil.lazyInit(required, object);
		return object;
	}

	@Override
	public int getTotal() throws Exception {
		Criteria c = userMenuDao.getCriteria();
		int total = DaoSupportUtil.getTotal(c);
		return total;
	}

	@Override
	public Collection getAll(String[] required) throws Exception {
		Criteria c = userMenuDao.getCriteria();
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			UserMenu element = (UserMenu) iter.next();
			DaoSupportUtil.lazyInit(required, element);
		}
		return list;
	}

	@Override
	public Collection getAll(String ColumnOrder, boolean Sorting) throws Exception {
		Criteria c = userMenuDao.getCriteria();
		DaoSupportUtil.setOrderBy(Sorting, ColumnOrder, c);
		List list = c.list();
		return list;
	}

	@Override
	public Collection getAll() throws Exception {
		Criteria c = userMenuDao.getCriteria();
		List list = c.list();
		return list;
	}

	@Override
	public UserMenu searchUnique(String eqColumns, Object eqParams, String[] required) throws Exception {
		Criteria c = userMenuDao.getCriteria();
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		UserMenu obj = (UserMenu) c.uniqueResult();
		DaoSupportUtil.lazyInit(required, obj);
		return obj;
	}

	@Override
	public UserMenu searchUnique(String eqColumns, Object eqParams) throws Exception {
		Criteria c = userMenuDao.getCriteria();
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		UserMenu obj = (UserMenu) c.uniqueResult();
		return obj;
	}

	@Override
	public Criteria getCriteria() throws Exception {
		Criteria c = userMenuDao.getCriteria();
		return c;
	}

	@Override
	public DetachedCriteria getDetachedCriteria() throws Exception {
		DetachedCriteria dc = userMenuDao.getDetachedCriteria();
		return dc;
	}

	@Override
	public Collection search(String[] eqColumns, Object[] eqParams) throws Exception {
		Criteria c = userMenuDao.getCriteria();
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		List list = c.list();
		return list;

	}

}
