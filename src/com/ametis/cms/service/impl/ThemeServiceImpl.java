package com.ametis.cms.service.impl;

import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;

import com.ametis.cms.dao.ThemeDao;
import com.ametis.cms.dao.ThemeDao;
import com.ametis.cms.datamodel.Action;
import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.ActivityLog;
import com.ametis.cms.datamodel.Info;
import com.ametis.cms.datamodel.Member;
import com.ametis.cms.datamodel.Theme;
import com.ametis.cms.datamodel.SubscriptionStatus;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.service.ActivityLogService;
import com.ametis.cms.service.MemberService;
import com.ametis.cms.service.ThemeService;
import com.ametis.cms.util.dao.DaoSupportUtil;

public class ThemeServiceImpl implements ThemeService{
	ThemeDao themeDao;

	public ThemeDao getThemeDao() {
		return themeDao;
	}

	public void setThemeDao(ThemeDao themeDao) {
		this.themeDao = themeDao;
	}

	@Override
	public Theme create(Theme object, ActionUser actionUser) throws Exception {
		// TODO Auto-generated method stub
		User user = null;
		if (actionUser != null){
			user = actionUser.getUser();					
		}
		object.setCreatedTime(new java.sql.Timestamp(System.currentTimeMillis()));
		object.setCreatedBy(user.getUsername());
		object.setDeleted(0);
		Theme result = themeDao.create (object);
		return result;
	}
	
	@Override
	public Theme create(Theme object) throws Exception {
		// TODO Auto-generated method stub
		object.setDeleted(0);
		object.setCreatedTime(new java.sql.Timestamp(System.currentTimeMillis()));
		Theme result = themeDao.create(object);
		return result;
	}

	@Override
	public Theme update(Theme object, ActionUser actionUser) throws Exception {
		// TODO Auto-generated method stub
		if (actionUser != null) {
			User user = actionUser.getUser();
		}

		themeDao.update(object);
		return object;
	}

	@Override
	public Theme trash(Serializable pkey) throws Exception {
		// TODO Auto-generated method stub
		Theme object = themeDao.get(pkey);
		themeDao.delete(object);
		return object;
	}

	@Override
	public Theme delete(Serializable pkey, ActionUser actionUser) throws Exception {
		// TODO Auto-generated method stub
		Theme object = themeDao.get(pkey);
		object.setDeletedTime(new java.sql.Timestamp(System.currentTimeMillis()));
		object.setDeleted(new Integer(1));

		if (actionUser != null) {
			User user = actionUser.getUser();
			if (user != null) {
				object.setDeletedBy(user.getUsername());
			}
		}

		themeDao.update(object);
		return object;
	}

	@Override
	public Theme delete(Theme object, ActionUser actionUser) throws Exception {
		// TODO Auto-generated method stub
		themeDao.update(object);
		return object;
	}

	@Override
	public Theme get(Serializable pkey) throws Exception {
		// TODO Auto-generated method stub
		Theme object = null;
		object = themeDao.get(pkey);
		return object;
	}

	@Override
	public Theme get(Serializable pkey, String[] required) throws Exception {
		// TODO Auto-generated method stub
		Theme object = null;
		object = themeDao.get(pkey);
		DaoSupportUtil.lazyInit(required, object);
		return object;
	}


	@Override
	public int getTotal() throws Exception {
		// TODO Auto-generated method stub
		Criteria c = themeDao.getCriteria();
		int total = DaoSupportUtil.getTotal(c);
		return total;
	}

	@Override
	public Collection getAll(String[] required) throws Exception {
		// TODO Auto-generated method stub
		Criteria c = themeDao.getCriteria();
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Theme element = (Theme) iter.next();
			DaoSupportUtil.lazyInit(required, element);
		}
		return list;
	}

	@Override
	public Collection getAll() throws Exception {
		// TODO Auto-generated method stub
		Criteria c = themeDao.getCriteria();
		List list = c.list();
		return list;
	}
	
	@Override
	public int getTotal(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams)
			throws Exception {

		Criteria c = themeDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		int total = DaoSupportUtil.getTotal(c);
		return total;

	}
	@Override
	public Theme searchUnique(String eqColumns, Object eqParams, String[] required) throws Exception {
		// TODO Auto-generated method stub
		Criteria c = themeDao.getCriteria();
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		Theme obj = (Theme) c.uniqueResult();
		DaoSupportUtil.lazyInit(required, obj);
		return obj;
	}

	@Override
	public Theme searchUnique(String eqColumns, Object eqParams) throws Exception {
		// TODO Auto-generated method stub
		Criteria c = themeDao.getCriteria();
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		Theme obj = (Theme) c.uniqueResult();
		return obj;
	}

	@Override
	public Criteria getCriteria() throws Exception {
		Criteria c = themeDao.getCriteria();
		return c;
	}

	@Override
	public DetachedCriteria getDetachedCriteria() throws Exception {
		// TODO Auto-generated method stub
		DetachedCriteria dc = themeDao.getDetachedCriteria();
		return dc;
	}

	@Override
	public Collection search(String[] eqColumns, Object[] eqParams) throws Exception {
		// TODO Auto-generated method stub
		Criteria c = themeDao.getCriteria();
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		List list = c.list();
		return list;
	}
	
	@Override
	public Collection search (String[] likeColumns, Object[] likeParams,
			String[] eqColumns, Object[] eqParams,
			boolean asc, String columnOrder[],
			String[] required,
			int index, int offset) throws Exception{

		Criteria c = themeDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns,likeParams,c);
		DaoSupportUtil.setEqParam(eqColumns,eqParams,c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setOrderBy(asc,columnOrder,c);
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Theme element = (Theme) iter.next();
   			DaoSupportUtil.lazyInit(required,element);
		}
		return list;
	}
	
}
