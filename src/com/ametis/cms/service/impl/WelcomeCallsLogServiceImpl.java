package com.ametis.cms.service.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;

import com.ametis.cms.dao.WelcomeCallsLogDao;
import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.WelcomeCallsLog;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.service.WelcomeCallsLogService;
import com.ametis.cms.util.dao.DaoSupportUtil;

public class WelcomeCallsLogServiceImpl implements WelcomeCallsLogService{
public WelcomeCallsLogDao welcomeCallsLogDao;
	
	public WelcomeCallsLogDao getWelcomeCallsLogDao() {
		return welcomeCallsLogDao;
	}

	public void setWelcomeCallsLogDao(WelcomeCallsLogDao welcomeCallsLogDao) {
		this.welcomeCallsLogDao = welcomeCallsLogDao;
	}
	/*
	 * Method create (WelcomeCallsLog object) berfungsi untuk melakukan penambahan
	 * sebuah object kedalam database @param object adalah sebuah object yang
	 * ingin diubah @return object hasil kreasi,lengkap dengan assigned primary
	 * key, exception jika gagal
	 */

	public WelcomeCallsLog create(WelcomeCallsLog object) throws NullPointerException{
		WelcomeCallsLog result = welcomeCallsLogDao.create(object);
		return result;
	}
	@Override
	/*
	 * Method update (WelcomeCallsLog object) berfungsi untuk melakukan perubahan
	 * terhadap sebuah object yang terdapat didalam database @param object
	 * adalah sebuah object yang ingin diubah @return object hasil update,
	 * exception jika gagal
	 */

	public WelcomeCallsLog update(WelcomeCallsLog object, ActionUser user) throws Exception {
		welcomeCallsLogDao.update(object);
		return object;
	}

	/*
	 * Method delete (Object pkey) berfungsi untuk melakukan penghapusan
	 * terhadap sebuah object yang terdapat didalam database @param pkey adalah
	 * sebuah object yang merepresentasikan primary key dari tabel yang
	 * bersangkutan. Object tersebut dapat dalam bentuk single ID maupun
	 * composite ID @return no return value karena objeknya sendiri sudah
	 * dihapus - just for consistency. Again, exception if failure occured
	 * WARNING ! Invalid value for the returned object, better not use it again
	 * in any place
	 */
	public WelcomeCallsLog trash(java.io.Serializable pkey) throws Exception {
		WelcomeCallsLog object = welcomeCallsLogDao.get(pkey);
		welcomeCallsLogDao.delete(object);
		return object;
	}

	/*
	 * Method delete (WelcomeCallsLog object) berfungsi untuk melakukan penghapusan
	 * terhadap sebuah object yang terdapat didalam database @param object
	 * adalah sebuah object yang ingin dihapus, isi dari object tersebut cukup
	 * dengan mengisi field-field primary key @return updated object, exception
	 * if failed
	 */
	public WelcomeCallsLog delete(java.io.Serializable pkey, ActionUser user) throws Exception {
		WelcomeCallsLog object = welcomeCallsLogDao.get(pkey);
		object.setDeleted(1);
		welcomeCallsLogDao.update(object);
		return object;
	}

	/*
	 * Method delete (WelcomeCallsLog object) berfungsi untuk melakukan penghapusan
	 * terhadap sebuah object yang terdapat didalam database @param object
	 * adalah sebuah object yang ingin dihapus, isi dari object tersebut cukup
	 * dengan mengisi field-field primary key @return updated object, exception
	 * if failed
	 */
	public WelcomeCallsLog delete(WelcomeCallsLog object, ActionUser user) throws Exception {
		welcomeCallsLogDao.update(object);
		return object;
	}

	// -- get section - carefull !
	/*
	 * Method get (Object pkey) berfungsi untuk melakukan retrieval terhadap
	 * sebuah object yang terdapat didalam database @param pkey adalah sebuah
	 * object yang merepresentasikan primary key dari tabel yang bersangkutan.
	 * Object tersebut dapat dalam bentuk single ID maupun composite ID @return
	 * Object yang dihasilkan dari proses retrieval, apabila object tidak
	 * ditemukan maka method akan mengembalikan nilai "NULL"
	 */
	public WelcomeCallsLog get(java.io.Serializable pkey) throws Exception {
		WelcomeCallsLog object = null;
		object = welcomeCallsLogDao.get(pkey);
		return object;
	}
	/*
	 * Method get (Object pkey) berfungsi untuk melakukan retrieval terhadap
	 * sebuah object yang terdapat didalam database @param pkey adalah sebuah
	 * object yang merepresentasikan primary key dari tabel yang bersangkutan.
	 * Object tersebut dapat dalam bentuk single ID maupun composite ID @param
	 * required adalah array dari field-field yang dibutuhkan dari hibernate
	 * object @return Object yang dihasilkan dari proses retrieval, apabila
	 * object tidak ditemukan maka method akan mengembalikan nilai "NULL"
	 */

	public WelcomeCallsLog get(java.io.Serializable pkey, String[] required) throws Exception {
		WelcomeCallsLog object = null;
		object = welcomeCallsLogDao.get(pkey);
		DaoSupportUtil.lazyInit(required, object);
		return object;
	}
	// -- get section end here

	// SEARCH SECTION - PALING RUMIT !!
	// * -> plain
	// *b -> with columnOrder
	// -- 1
	public Collection search(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams,
			int index, int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		List list = c.list();
		return list;

	}
	// --req

	public Collection search(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams,
			String[] required, int index, int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			WelcomeCallsLog element = (WelcomeCallsLog) iter.next();
			DaoSupportUtil.lazyInit(required, element);
		}

		return list;

	}
	// --req end

	public Collection search(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams,
			boolean asc, String columnOrder[], int index, int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setOrderBy(asc, columnOrder, c);
		List list = c.list();
		return list;

	}
	// --req

	public Collection search(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams,
			boolean asc, String columnOrder[], String[] required, int index, int offset) throws Exception {
		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setOrderBy(asc, columnOrder, c);
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			WelcomeCallsLog element = (WelcomeCallsLog) iter.next();
			DaoSupportUtil.lazyInit(required, element);
		}

		return list;

	}
	// --req end

	public Collection search(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams,
			boolean asc, String columnOrder, int index, int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setOrderBy(asc, columnOrder, c);
		List list = c.list();
		return list;
	}
	// --req

	public Collection search(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams,
			boolean asc, String columnOrder, String[] required, int index, int offset) throws Exception {
		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setOrderBy(asc, columnOrder, c);
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			WelcomeCallsLog element = (WelcomeCallsLog) iter.next();
			DaoSupportUtil.lazyInit(required, element);
		}

		return list;

	}
	// --req end

	// -- 1b
	public Collection search(String likeColumns, Object likeParams, String eqColumns, Object eqParams, int index,
			int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		List list = c.list();
		return list;

	}
	// --req

	public Collection search(String likeColumns, Object likeParams, String eqColumns, Object eqParams,
			String[] required, int index, int offset) throws Exception {
		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			WelcomeCallsLog element = (WelcomeCallsLog) iter.next();
			DaoSupportUtil.lazyInit(required, element);
		}

		return list;

	}
	// --req end

	public Collection search(String likeColumns, Object likeParams, String eqColumns, Object eqParams, boolean asc,
			String columnOrder[], int index, int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setOrderBy(asc, columnOrder, c);
		List list = c.list();
		return list;

	}
	// req

	public Collection search(String likeColumns, Object likeParams, String eqColumns, Object eqParams, boolean asc,
			String columnOrder[], String[] required, int index, int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setOrderBy(asc, columnOrder, c);
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			WelcomeCallsLog element = (WelcomeCallsLog) iter.next();
			DaoSupportUtil.lazyInit(required, element);
		}
		return list;

	}
	// req end

	public Collection search(String likeColumns, Object likeParams, String eqColumns, Object eqParams, boolean asc,
			String columnOrder, int index, int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setOrderBy(asc, columnOrder, c);
		List list = c.list();
		return list;

	}
	// req

	public Collection search(String likeColumns, Object likeParams, String eqColumns, Object eqParams, boolean asc,
			String columnOrder, String[] required, int index, int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setOrderBy(asc, columnOrder, c);
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			WelcomeCallsLog element = (WelcomeCallsLog) iter.next();
			DaoSupportUtil.lazyInit(required, element);
		}
		return list;

	}

	// req end
	// -- 2 , between
	public Collection search(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams,
			String[] btwnColumns, Object[] btwnParams1, Object[] btwnParams2, int index, int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setBetweenParam(btwnColumns, btwnParams1, btwnParams2, c);
		List list = c.list();
		return list;
	}
	// req

	public Collection search(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams,
			String[] btwnColumns, Object[] btwnParams1, Object[] btwnParams2, String[] required, int index, int offset)
					throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setBetweenParam(btwnColumns, btwnParams1, btwnParams2, c);
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			WelcomeCallsLog element = (WelcomeCallsLog) iter.next();
			DaoSupportUtil.lazyInit(required, element);
		}
		return list;
	}
	// req end

	public Collection search(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams,
			String[] btwnColumns, Object[] btwnParams1, Object[] btwnParams2, boolean asc, String columnOrder[],
			int index, int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setBetweenParam(btwnColumns, btwnParams1, btwnParams2, c);
		DaoSupportUtil.setOrderBy(asc, columnOrder, c);
		List list = c.list();
		return list;
	}
	// req

	public Collection search(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams,
			String[] btwnColumns, Object[] btwnParams1, Object[] btwnParams2, boolean asc, String columnOrder[],
			String[] required, int index, int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setBetweenParam(btwnColumns, btwnParams1, btwnParams2, c);
		DaoSupportUtil.setOrderBy(asc, columnOrder, c);
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			WelcomeCallsLog element = (WelcomeCallsLog) iter.next();
			DaoSupportUtil.lazyInit(required, element);
		}
		return list;
	}
	// req end

	public Collection search(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams,
			String[] btwnColumns, Object[] btwnParams1, Object[] btwnParams2, boolean asc, String columnOrder,
			int index, int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setBetweenParam(btwnColumns, btwnParams1, btwnParams2, c);
		DaoSupportUtil.setOrderBy(asc, columnOrder, c);
		List list = c.list();
		return list;
	}
	// req

	public Collection search(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams,
			String[] btwnColumns, Object[] btwnParams1, Object[] btwnParams2, boolean asc, String columnOrder,
			String[] required, int index, int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setBetweenParam(btwnColumns, btwnParams1, btwnParams2, c);
		DaoSupportUtil.setOrderBy(asc, columnOrder, c);
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			WelcomeCallsLog element = (WelcomeCallsLog) iter.next();
			DaoSupportUtil.lazyInit(required, element);
		}
		return list;
	}

	// req end
	// -- 2b
	public Collection search(String likeColumns, Object likeParams, String eqColumns, Object eqParams,
			String btwnColumns, Object btwnParams1, Object btwnParams2, int index, int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setBetweenParam(btwnColumns, btwnParams1, btwnParams2, c);
		List list = c.list();
		return list;
	}
	// req

	public Collection search(String likeColumns, Object likeParams, String eqColumns, Object eqParams,
			String btwnColumns, Object btwnParams1, Object btwnParams2, String[] required, int index, int offset)
					throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setBetweenParam(btwnColumns, btwnParams1, btwnParams2, c);
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			WelcomeCallsLog element = (WelcomeCallsLog) iter.next();
			DaoSupportUtil.lazyInit(required, element);
		}
		return list;
	}
	// req end

	public Collection search(String likeColumns, Object likeParams, String eqColumns, Object eqParams,
			String btwnColumns, Object btwnParams1, Object btwnParams2, boolean asc, String columnOrder[], int index,
			int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setBetweenParam(btwnColumns, btwnParams1, btwnParams2, c);
		DaoSupportUtil.setOrderBy(asc, columnOrder, c);
		List list = c.list();
		return list;
	}
	// req

	public Collection search(String likeColumns, Object likeParams, String eqColumns, Object eqParams,
			String btwnColumns, Object btwnParams1, Object btwnParams2, boolean asc, String columnOrder[],
			String[] required, int index, int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setBetweenParam(btwnColumns, btwnParams1, btwnParams2, c);
		DaoSupportUtil.setOrderBy(asc, columnOrder, c);
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			WelcomeCallsLog element = (WelcomeCallsLog) iter.next();
			DaoSupportUtil.lazyInit(required, element);
		}
		return list;
	}
	// req end

	public Collection search(String likeColumns, Object likeParams, String eqColumns, Object eqParams,
			String btwnColumns, Object btwnParams1, Object btwnParams2, boolean asc, String columnOrder, int index,
			int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setBetweenParam(btwnColumns, btwnParams1, btwnParams2, c);
		DaoSupportUtil.setOrderBy(asc, columnOrder, c);
		List list = c.list();
		return list;
	}
	// req

	public Collection search(String likeColumns, Object likeParams, String eqColumns, Object eqParams,
			String btwnColumns, Object btwnParams1, Object btwnParams2, boolean asc, String columnOrder,
			String[] required, int index, int offset) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setLimit(index, offset, c);
		DaoSupportUtil.setBetweenParam(btwnColumns, btwnParams1, btwnParams2, c);
		DaoSupportUtil.setOrderBy(asc, columnOrder, c);
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			WelcomeCallsLog element = (WelcomeCallsLog) iter.next();
			DaoSupportUtil.lazyInit(required, element);
		}
		return list;
	}
	// req end

	// -- search end
	// -- get total
	public int getTotal(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams)
			throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		int total = DaoSupportUtil.getTotal(c);
		return total;

	}

	public int getTotal(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams,
			String[] btwnColumns, Object[] btwnParams1, Object[] btwnParams2) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setBetweenParam(btwnColumns, btwnParams1, btwnParams2, c);
		int total = DaoSupportUtil.getTotal(c);
		return total;

	}

	public int getTotal(String likeColumns, Object likeParams, String eqColumns, Object eqParams) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		int total = DaoSupportUtil.getTotal(c);
		return total;

	}

	public int getTotal(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams,
			String btwnColumns, Object btwnParams1, Object btwnParams2) throws Exception {

		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		DaoSupportUtil.setBetweenParam(btwnColumns, btwnParams1, btwnParams2, c);
		int total = DaoSupportUtil.getTotal(c);
		return total;

	}
	// ------------------------------------------------------------------

	@Override
	public int getTotal() throws Exception {
		Criteria c = welcomeCallsLogDao.getCriteria();
		int total = DaoSupportUtil.getTotal(c);
		return total;
	}
	// -- get total end

	// ---------------------------------------------------

	// -------------------------------------------------

	// unique Result
	// -----------------------------------------------

	/*
	 * BASIC IMPLEMENTATION !! USE WITH CAUTION ! USE IT IF NO OTHER OPTION LEFT
	 * 
	 * @return criteria
	 */

	public WelcomeCallsLog getWelcomeCallsLogByName(String bankName) throws Exception {
		// TODO Auto-generated method stub

		String[] eqParam = { "bankName" };
		Object[] eqValue = { bankName };

		int total = getTotal(null, null, eqParam, eqValue);

		Collection<WelcomeCallsLog> bankCollection = search(null, null, eqParam, eqValue, 0, total);

		WelcomeCallsLog bank = null;

		if (bankCollection != null) {
			Iterator<WelcomeCallsLog> iterator = bankCollection.iterator();

			if (iterator.hasNext()) {
				bank = iterator.next();
			}
		}

		return bank;
	}

	// class+
	// class-

	public Collection<WelcomeCallsLog> searchWelcomeCallsLog(String q) throws Exception {
		Collection<WelcomeCallsLog> result = new Vector<WelcomeCallsLog>();
		try {

			String[] likeParam = { "bankName" };
			Object[] likeValue = { q };

			String[] eqParam = { "deletedStatus" };
			Object[] eqValue = { 0 };

			result = search(likeParam, likeValue, eqParam, eqValue, 0, 10);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;

	}

	@Override
	public WelcomeCallsLog searchUnique(String[] eqColumns, Object[] eqParams, int index, int offset) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WelcomeCallsLog create(WelcomeCallsLog object, ActionUser actionUser) throws Exception {
		// TODO Auto-generated method stub
		object.setDeleted(Integer.valueOf(0));
		if (actionUser != null){
			User user = actionUser.getUser();	
					
			if (user != null){
				object.setCreatedBy(user.getUsername());
			}
		}
		WelcomeCallsLog result = welcomeCallsLogDao.create(object);
		return result;
	}

	@Override
	public Collection getAll(String[] required) throws Exception {
		Criteria c = welcomeCallsLogDao.getCriteria();
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			WelcomeCallsLog element = (WelcomeCallsLog) iter.next();
			DaoSupportUtil.lazyInit(required, element);
		}
		return list;
	}

	@Override
	public Collection getAll(String ColumnOrder, boolean Sorting) throws Exception {
		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setOrderBy(Sorting, ColumnOrder, c);
		List list = c.list();
		return list;
	}

	@Override
	public Collection getAll() throws Exception {
		Criteria c = welcomeCallsLogDao.getCriteria();
		List list = c.list();
		return list;
	}

	@Override
	public WelcomeCallsLog searchUnique(String eqColumns, Object eqParams, String[] required) throws Exception {
		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		WelcomeCallsLog obj = (WelcomeCallsLog) c.uniqueResult();
		DaoSupportUtil.lazyInit(required, obj);
		return obj;
	}

	@Override
	public WelcomeCallsLog searchUnique(String eqColumns, Object eqParams) throws Exception {
		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		WelcomeCallsLog obj = (WelcomeCallsLog) c.uniqueResult();
		return obj;
	}

	@Override
	public Criteria getCriteria() throws Exception {
		Criteria c = welcomeCallsLogDao.getCriteria();
		return c;
	}

	@Override
	public DetachedCriteria getDetachedCriteria() throws Exception {
		DetachedCriteria dc = welcomeCallsLogDao.getDetachedCriteria();
		return dc;
	}

	@Override
	public Collection search(String[] eqColumns, Object[] eqParams) throws Exception {
		Criteria c = welcomeCallsLogDao.getCriteria();
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		List list = c.list();
		return list;

	}
}
