package com.ametis.cms.service.impl;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;

import com.ametis.cms.dao.WelcomeCallsDao;
import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.Case;
import com.ametis.cms.datamodel.WelcomeCalls;
import com.ametis.cms.datamodel.User;
import com.ametis.cms.service.WelcomeCallsService;
import com.ametis.cms.util.dao.DaoSupportUtil;

public class WelcomeCallsServiceImpl implements WelcomeCallsService{
	WelcomeCallsDao registerCallDao;

	public WelcomeCallsDao getWelcomeCallsDao() {
		return registerCallDao;
	}

	public void setWelcomeCallsDao(WelcomeCallsDao registerCallDao) {
		this.registerCallDao = registerCallDao;
	}

	@Override
	public WelcomeCalls create(WelcomeCalls object, ActionUser actionUser) throws Exception {
		// TODO Auto-generated method stub
		User user = null;
		if (actionUser != null){
			user = actionUser.getUser();	
			object.setCreatedBy(user.getUsername());
		}
		object.setCreatedTime(new java.sql.Timestamp(System.currentTimeMillis()));
		object.setDeleted(0);
		WelcomeCalls result = registerCallDao.create (object);
		return result;
	}
	
	@Override
	public WelcomeCalls create(WelcomeCalls object) throws Exception {
		// TODO Auto-generated method stub
		object.setDeleted(0);
		object.setCreatedTime(new java.sql.Timestamp(System.currentTimeMillis()));
		WelcomeCalls result = registerCallDao.create(object);
		return result;
	}

	@Override
	public WelcomeCalls update(WelcomeCalls object, ActionUser actionUser) throws Exception {
		// TODO Auto-generated method stub
		if (actionUser != null) {
			User user = actionUser.getUser();
		}

		registerCallDao.update(object);
		return object;
	}

	@Override
	public WelcomeCalls trash(Serializable pkey) throws Exception {
		// TODO Auto-generated method stub
		WelcomeCalls object = registerCallDao.get(pkey);
		registerCallDao.delete(object);
		return object;
	}

	@Override
	public WelcomeCalls delete(Serializable pkey, ActionUser actionUser) throws Exception {
		// TODO Auto-generated method stub
		WelcomeCalls object = registerCallDao.get(pkey);
		object.setDeleted(new Integer(1));

		registerCallDao.update(object);
		return object;
	}

	@Override
	public WelcomeCalls delete(WelcomeCalls object, ActionUser actionUser) throws Exception {
		// TODO Auto-generated method stub
		registerCallDao.update(object);
		return object;
	}

	@Override
	public WelcomeCalls get(Serializable pkey) throws Exception {
		// TODO Auto-generated method stub
		WelcomeCalls object = null;
		object = registerCallDao.get(pkey);
		return object;
	}

	@Override
	public WelcomeCalls get(Serializable pkey, String[] required) throws Exception {
		// TODO Auto-generated method stub
		WelcomeCalls object = null;
		object = registerCallDao.get(pkey);
		DaoSupportUtil.lazyInit(required, object);
		return object;
	}

	@Override
	public int getTotal(String[] likeColumns, Object[] likeParams, String[] eqColumns, Object[] eqParams)
			throws Exception {

		Criteria c = registerCallDao.getCriteria();
		DaoSupportUtil.setLikeParam(likeColumns, likeParams, c);
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		int total = DaoSupportUtil.getTotal(c);
		return total;

	}

	@Override
	public int getTotal() throws Exception {
		// TODO Auto-generated method stub
		Criteria c = registerCallDao.getCriteria();
		int total = DaoSupportUtil.getTotal(c);
		return total;
	}

	@Override
	public Collection getAll(String[] required) throws Exception {
		// TODO Auto-generated method stub
		Criteria c = registerCallDao.getCriteria();
		List list = c.list();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			WelcomeCalls element = (WelcomeCalls) iter.next();
			DaoSupportUtil.lazyInit(required, element);
		}
		return list;
	}

	@Override
	public Collection getAll() throws Exception {
		// TODO Auto-generated method stub
		Criteria c = registerCallDao.getCriteria();
		List list = c.list();
		return list;
	}

	@Override
	public WelcomeCalls searchUnique(String eqColumns, Object eqParams, String[] required) throws Exception {
		// TODO Auto-generated method stub
		Criteria c = registerCallDao.getCriteria();
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		WelcomeCalls obj = (WelcomeCalls) c.uniqueResult();
		DaoSupportUtil.lazyInit(required, obj);
		return obj;
	}

	@Override
	public WelcomeCalls searchUnique(String eqColumns, Object eqParams) throws Exception {
		// TODO Auto-generated method stub
		Criteria c = registerCallDao.getCriteria();
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		WelcomeCalls obj = (WelcomeCalls) c.uniqueResult();
		return obj;
	}

	@Override
	public Criteria getCriteria() throws Exception {
		Criteria c = registerCallDao.getCriteria();
		return c;
	}

	@Override
	public DetachedCriteria getDetachedCriteria() throws Exception {
		// TODO Auto-generated method stub
		DetachedCriteria dc = registerCallDao.getDetachedCriteria();
		return dc;
	}

	@Override
	public Collection search(String[] eqColumns, Object[] eqParams) throws Exception {
		// TODO Auto-generated method stub
		Criteria c = registerCallDao.getCriteria();
		DaoSupportUtil.setEqParam(eqColumns, eqParams, c);
		List list = c.list();
		return list;
	}
}
