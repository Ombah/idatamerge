package com.ametis.cms.service;

import java.util.Collection;

import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;

import com.ametis.cms.datamodel.ActionUser;
import com.ametis.cms.datamodel.ProcessNotification;

public interface ProcessNotificationService {

	public ProcessNotification create(ProcessNotification object, ActionUser actionUser) throws Exception;

	public ProcessNotification update(ProcessNotification object, ActionUser actionUser) throws Exception;

	public ProcessNotification trash(java.io.Serializable pkey) throws Exception;

	public ProcessNotification delete(java.io.Serializable pkey, ActionUser actionUser) throws Exception;

	public ProcessNotification delete(ProcessNotification object, ActionUser actionUser) throws Exception;

	public ProcessNotification get(java.io.Serializable pkey) throws Exception;

	public ProcessNotification get(java.io.Serializable pkey, String[] required) throws Exception;

	// -- get section end here

	// SEARCH SECTION - PALING RUMIT !!

	// -- 1
	/*
	 * Method searching objects from database
	 * 
	 * @param eqColumn kolom equal
	 * 
	 * @param eqParams param equal
	 */

	public Collection search(String[] eqColumns, Object[] eqParams) throws Exception;

	public int getTotal() throws Exception;

	public Collection getAll(String[] required) throws Exception;

	public Collection getAll(String ColumnOrder, boolean Sorting) throws Exception;

	public Collection getAll() throws Exception;

	public ProcessNotification searchUnique(String eqColumns, Object eqParams, String[] required) throws Exception;

	public ProcessNotification searchUnique(String eqColumns, Object eqParams) throws Exception;
	

	public Criteria getCriteria() throws Exception;

	public DetachedCriteria getDetachedCriteria() throws Exception;

}
