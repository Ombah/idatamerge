package com.ametis.cms.datamodel;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import javax.persistence.Entity;

@Entity
@Table(name="welcome_call_category")
public class WelcomeCallCategory {
	private Integer id;
	private String description;
	private Integer deleted;
	private Timestamp modifiedTime;
	private String modifiedBy;
	private Timestamp deletedTime;
	private String deletedBy;
	private Timestamp createdTime;
	private String createdBy;
	private String welcomeCallCategoryName;
	private String welcomeCallCategoryCode;
	
	@Id
	@Column(name="welcome_call_category_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column(name="deleted")
	public Integer getDeleted() {
		return deleted;
	}
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}
	@Column(name="modified_time")
	public Timestamp getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(Timestamp modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	@Column(name="modified_by")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	@Column(name="deleted_time")
	public Timestamp getDeletedTime() {
		return deletedTime;
	}
	public void setDeletedTime(Timestamp deletedTime) {
		this.deletedTime = deletedTime;
	}
	@Column(name="deleted_by")
	public String getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}
	@Column(name="created_time")
	public Timestamp getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(name="welcome_call_category_name")
	public String getWelcomeCallCategoryName() {
		return welcomeCallCategoryName;
	}
	public void setWelcomeCallCategoryName(String welcomeCallCategoryName) {
		this.welcomeCallCategoryName = welcomeCallCategoryName;
	}
	@Column(name="welcome_call_category_code")
	public String getWelcomeCallCategoryCode() {
		return welcomeCallCategoryCode;
	}
	public void setWelcomeCallCategoryCode(String welcomeCallCategoryCode) {
		this.welcomeCallCategoryCode = welcomeCallCategoryCode;
	}
	
}
