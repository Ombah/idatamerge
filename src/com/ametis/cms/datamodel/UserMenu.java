package com.ametis.cms.datamodel;

import java.io.Serializable;
import java.sql.*;
import java.util.*;
import javax.persistence.*;
import org.hibernate.annotations.Generated;

@Entity
@Table(name="tbl_user_menu")
public class UserMenu implements Serializable {
	private int id;
	private Integer roleId;
	private String url;
	
	@Id
	@Column(name="id")
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	@Column(name="url")
	public String getUrl() {
		return url;
	}
	
	public void setUrl(String url) {
		this.url = url;
	}
	
	@Column(name="role_id")
	public Integer getRoleId() {
		return roleId;
	}
	
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	
	
	
}
