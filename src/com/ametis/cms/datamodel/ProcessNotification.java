
package com.ametis.cms.datamodel;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="tbl_process_notification")
public class ProcessNotification implements Serializable{
	private Integer id;
	private Integer processId;
	private Integer productId;
	private Integer ClientId;
	private Integer policyId;
	private Integer isMemberEmail;
	private Integer isClientEmail;
	private Integer isProviderEmail;
	private Integer isMemberSms;
	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="is_member_email")
	public Integer getIsMemberEmail() {
		return isMemberEmail;
	}
	public void setIsMemberEmail(Integer isMemberEmail) {
		this.isMemberEmail = isMemberEmail;
	}
	
	@Column(name="is_client_email")
	public Integer getIsClientEmail() {
		return isClientEmail;
	}
	public void setIsClientEmail(Integer isClientEmail) {
		this.isClientEmail = isClientEmail;
	}
	
	@Column(name="is_provider_email")
	public Integer getIsProviderEmail() {
		return isProviderEmail;
	}
	public void setIsProviderEmail(Integer isProviderEmail) {
		this.isProviderEmail = isProviderEmail;
	}
	
	@Column(name="is_member_sms")
	public Integer getIsMemberSms() {
		return isMemberSms;
	}
	public void setIsMemberSms(Integer isMemberSms) {
		this.isMemberSms = isMemberSms;
	}
	@Column(name="process_id")
	public Integer getProcessId() {
		return processId;
	}
	public void setProcessId(Integer processId) {
		this.processId = processId;
	}
	@Column(name="product_id")
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	@Column(name="client_id")
	public Integer getClientId() {
		return ClientId;
	}
	public void setClientId(Integer clientId) {
		ClientId = clientId;
	}
	@Column(name="policy_id")
	public Integer getPolicyId() {
		return policyId;
	}
	public void setPolicyId(Integer policyId) {
		this.policyId = policyId;
	}
	
}