
package com.ametis.cms.datamodel;


import java.sql.*;
import java.util.*;
import javax.persistence.*;
import org.hibernate.annotations.Generated;


@Entity
@Table(name="info")
public class Info implements java.io.Serializable{
	private static final long serialVersionUID = 1L;

	//Fields
		
	/**data for the column :
	
 --------- message_template.id --------- 
 schema        = null
 tableName     = message_template
 foreignCol    = 
 foreignTab    = 
 catalog       = insura-master
 remarks       = auto_increment
 defaultValue  = null
 decDigits     = 0
 radix         = 10
 nullable      = 0
 ordinal       = 1
 size          = 5
 type          = 4 
 isPrimaryKey  = true

=========================================


*/
	private Integer id;
	private String title;
	private String createdBy;
	private String deletedBy;
	private String modifiedBy;
	private String content;
	private Document documentId;
	private Integer deletedStatus;
	private java.sql.Timestamp createdTime;
	private java.sql.Timestamp deletedTime;
	private Timestamp modifiedTime;
	private java.sql.Timestamp startDate;
	private java.sql.Timestamp endDate;
	@Column(name="start_date")
	public java.sql.Timestamp getStartDate() {
		return startDate;
	}
	public void setStartDate(java.sql.Timestamp startDate) {
		this.startDate = startDate;
	}
	@Column(name="end_date")
	public java.sql.Timestamp getEndDate() {
		return endDate;
	}
	public void setEndDate(java.sql.Timestamp endDate) {
		this.endDate = endDate;
	}
	private Timestamp expiredDate;
	
	@Column(name="expired_date")
	public Timestamp getExpiredDate() {
		return expiredDate;
	}
	public void setExpiredDate(Timestamp expiredDate) {
		this.expiredDate = expiredDate;
	}
	/**data for the column :
	
 --------- message_template.template_name --------- 
 schema        = null
 tableName     = message_template
 foreignCol    = 
 foreignTab    = 
 catalog       = insura-master
 remarks       = 
 defaultValue  = null
 decDigits     = 0
 radix         = 10
 nullable      = 1
 ordinal       = 2
 size          = 150
 type          = 12 
 isPrimaryKey  = false

=========================================

*/

	// PK GETTER SETTER
	@Id
	@Column(name="info_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public java.lang.Integer getId(){
		return id;
	}
	public void setId(java.lang.Integer value){
		id = value;
	}
	@Column(name="title")
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(name="deleted_by")
	public String getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}
	@Column(name="contents")
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@Column(name="created_time")
	public java.sql.Timestamp getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(java.sql.Timestamp createdTime) {
		this.createdTime = createdTime;
	}
	@Column(name="deleted_time")
	public java.sql.Timestamp getDeletedTime() {
		return deletedTime;
	}
	
	@Column(name="deleted_time")
	public void setDeletedTime(java.sql.Timestamp deletedTime) {
		this.deletedTime = deletedTime;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Column(name="deleted_status")
	public Integer getDeletedStatus() {
		return deletedStatus;
	}
	public void setDeletedStatus(Integer deletedStatus) {
		this.deletedStatus = deletedStatus;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public java.sql.Timestamp getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(java.sql.Timestamp modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	@ManyToOne
	@JoinColumn(name="document_id")
	public Document getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Document documentId) {
		this.documentId = documentId;
	}

	

}