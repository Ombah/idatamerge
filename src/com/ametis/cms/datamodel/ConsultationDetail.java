
package com.ametis.cms.datamodel;


import javax.persistence.*;


@Entity
@Table(name="consultation_detail")
public class ConsultationDetail implements java.io.Serializable{

	//Fields
		
	/**
	 * 
	 */
	public static final int STATUS_UNREAD = 0;
	public static final int STATUS_READED = 1;

	private static final long serialVersionUID = 1L;
	/**data for the column :
	
 --------- message_template.id --------- 
 schema        = null
 tableName     = message_template
 foreignCol    = 
 foreignTab    = 
 catalog       = insura-master
 remarks       = auto_increment
 defaultValue  = null
 decDigits     = 0
 radix         = 10
 nullable      = 0
 ordinal       = 1
 size          = 5
 type          = 4 
 isPrimaryKey  = true

=========================================


*/
	private Integer id;
	private Consultation consultationId;
	private String message;

	private Integer deletedStatus;
	private String createdBy;
	private Integer createdById;
	private Integer isRead;
	private String modifiedBy;
	private Document documentId;
	private java.sql.Timestamp createdTime;
	private java.sql.Timestamp modifiedTime;
			
	/**data for the column :
	
 --------- message_template.template_name --------- 
 schema        = null
 tableName     = message_template
 foreignCol    = 
 foreignTab    = 
 catalog       = insura-master
 remarks       = 
 defaultValue  = null
 decDigits     = 0
 radix         = 10
 nullable      = 1
 ordinal       = 2
 size          = 150
 type          = 12 
 isPrimaryKey  = false

=========================================

*/

	// PK GETTER SETTER

	@Id
	@Column(name="consultation_detail_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public java.lang.Integer getId(){
		return id;
	}
	public void setId(java.lang.Integer value){
		id = value;
	}
			// PK GETTER SETTER END

	
	@Column(name="document_id")
	public Document getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Document documentId) {
		this.documentId = documentId;
	}
	@Column(name="modified_by")
	public String getModifiedBy() {
		return modifiedBy;
	}public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	@Column(name="modified_time")
	public java.sql.Timestamp getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(java.sql.Timestamp modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name="created_time")
	public java.sql.Timestamp getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(java.sql.Timestamp createdTime) {
		this.createdTime = createdTime;
	}

	@Column(name = "deleted_status")
	public Integer getDeletedStatus() {
		return deletedStatus;
	}
	public void setDeletedStatus(Integer deletedStatus) {
		this.deletedStatus = deletedStatus;
	}
	
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "consultation_id")	
	public Consultation getConsultationId() {
		return consultationId;
	}
	public void setConsultationId(Consultation consultationId) {
		this.consultationId = consultationId;
	}
	
	@Column(name = "my_message")
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	@Column(name = "created_by_id")
	public Integer getCreatedById() {
		return createdById;
	}
	public void setCreatedById(Integer createdById) {
		this.createdById = createdById;
	}
	
	@Column(name = "is_read")
	public Integer getIsRead() {
		return isRead;
	}
	public void setIsRead(Integer isRead) {
		this.isRead = isRead;
	}
	
	

}