package com.ametis.cms.datamodel;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="welcome_calls_log")
public class WelcomeCallsLog  implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5925089543031288155L;
	private Integer id;
	private Timestamp callTime;
	private String callDescription;
	private WelcomeCallCategory callCategoryId;
	private Integer followupNeeded;
	private WelcomeCallsStatus statusCallId;
	private Integer deleted;
	private String createdBy;
	private WelcomeCalls welcomeCallsId;
	private String modifiedBy;
	private java.sql.Timestamp createdTime;
	private java.sql.Timestamp modifiedTime;
	
	
	@ManyToOne
	@JoinColumn(name = "welcome_calls_id")
	public WelcomeCalls getWelcomeCallsId() {
		return welcomeCallsId;
	}
	public void setWelcomeCallsId(WelcomeCalls welcomeCallsId) {
		this.welcomeCallsId = welcomeCallsId;
	}
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(name="modified_by")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	@Column(name="created_time")
	public java.sql.Timestamp getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(java.sql.Timestamp createdTime) {
		this.createdTime = createdTime;
	}
	@Column(name="modified_time")
	public java.sql.Timestamp getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(java.sql.Timestamp modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	@Column(name="deleted")
	public Integer getDeleted() {
		return deleted;
	}
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="call_time")
	public Timestamp getCallTime() {
		return callTime;
	}
	public void setCallTime(Timestamp callTime) {
		this.callTime = callTime;
	}
	@Column(name="call_description")
	public String getCallDescription() {
		return callDescription;
	}
	public void setCallDescription(String callDescription) {
		this.callDescription = callDescription;
	}
	
	@Column(name="followup_needed")
	public Integer getFollowupNeeded() {
		return followupNeeded;
	}
	public void setFollowupNeeded(Integer followupNeeded) {
		this.followupNeeded = followupNeeded;
	}
	
	@ManyToOne
	@JoinColumn(name = "call_category")
	public WelcomeCallCategory getCallCategoryId() {
		return callCategoryId;
	}
	public void setCallCategoryId(WelcomeCallCategory callCategoryId) {
		this.callCategoryId = callCategoryId;
	}
	@ManyToOne
	@JoinColumn(name = "status_call")
	public WelcomeCallsStatus getStatusCallId() {
		return statusCallId;
	}
	public void setStatusCallId(WelcomeCallsStatus statusCallId) {
		this.statusCallId = statusCallId;
	}
	
	
	
}
