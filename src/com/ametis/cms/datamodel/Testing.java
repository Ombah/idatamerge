package com.ametis.cms.datamodel;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="testing")
public class Testing {
	private Integer id;
	private Integer col1;
	private String col2;
	private Timestamp col3;
	private String col4;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="col1")
	public Integer getCol1() {
		return col1;
	}
	public void setCol1(Integer col1) {
		this.col1 = col1;
	}
	@Column(name="col2")
	public String getCol2() {
		return col2;
	}
	public void setCol2(String col2) {
		this.col2 = col2;
	}
	@Column(name="col3")
	public Timestamp getCol3() {
		return col3;
	}
	public void setCol3(Timestamp col3) {
		this.col3 = col3;
	}
	@Column(name="col4")
	public String getCol4() {
		return col4;
	}
	public void setCol4(String col4) {
		this.col4 = col4;
	}
	
	
	
}
