
package com.ametis.cms.datamodel;


import java.sql.*;
import java.util.*;
import javax.persistence.*;
import org.hibernate.annotations.Generated;


@Entity
@Table(name="consultation")
public class Consultation implements java.io.Serializable{

	//Fields
		
	public static Integer MESSAGE_NOT_READ = 0;
	public static Integer MESSAGE_READ = 1;
	public static Integer MESSAGE_REPLIED = 2;
	
	public static Integer TYPE_CLAIM = 0;
	public static Integer TYPE_COMPLAINT= 1;
	public static Integer TYPE_QUESTION= 2;
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**data for the column :
	
 --------- message_template.id --------- 
 schema        = null
 tableName     = message_template
 foreignCol    = 
 foreignTab    = 
 catalog       = insura-master
 remarks       = auto_increment
 defaultValue  = null
 decDigits     = 0
 radix         = 10
 nullable      = 0
 ordinal       = 1
 size          = 5
 type          = 4 
 isPrimaryKey  = true

=========================================


*/
	private Integer consultationId;
	private String title;
	private String contentPreview;
	private String previewContent;
	private Integer deletedStatus;
	private Integer type;
	private String createdBy;
	//private Integer createdById;
	private Integer status;
	private String modifiedBy;
	private java.sql.Timestamp createdTime;
	private java.sql.Timestamp modifiedTime;
	private Member memberId;
	private Document documentId;
	private User userId;
	private String originalDocName;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="document_id")	
	public Document getDocumentId() {
		return documentId;
	}
	public void setDocumentId(Document documentId) {
		this.documentId = documentId;
	}
	
	@ManyToOne
	@JoinColumn(name="user_id")
	public User getUserId() {
		return userId;
	}
	public void setUserId(User userId) {
		this.userId = userId;
	}
	@Column(name="content_preview")
	public String getContentPreview() {
		return contentPreview;
	}
	public void setContentPreview(String contentPreview) {
		this.contentPreview = contentPreview;
	}
	@ManyToOne
	@JoinColumn(name="member_id")
	public Member getMemberId() {
		return memberId;
	}
	public void setMemberId(Member memberId) {
		this.memberId = memberId;
	}
	/**data for the column :
	
 --------- message_template.template_name --------- 
 schema        = null
 tableName     = message_template
 foreignCol    = 
 foreignTab    = 
 catalog       = insura-master
 remarks       = 
 defaultValue  = null
 decDigits     = 0
 radix         = 10
 nullable      = 1
 ordinal       = 2
 size          = 150
 type          = 12 
 isPrimaryKey  = false

=========================================

*/

	// PK GETTER SETTER
	
	@Id
	@Column(name="consultation_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public java.lang.Integer getConsultationId(){
		return consultationId;
	}
	public void setConsultationId(Integer consultationId) {
		this.consultationId = consultationId;
	}
	@Column(name="status")
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	@Column(name="type")
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
			// PK GETTER SETTER END

	
	@Column(name="modified_by")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	@Column(name="modified_time")
	public java.sql.Timestamp getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(java.sql.Timestamp modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name="title")
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name="created_time")
	public java.sql.Timestamp getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(java.sql.Timestamp createdTime) {
		this.createdTime = createdTime;
	}

	@Column(name = "deleted_status")
	public Integer getDeletedStatus() {
		return deletedStatus;
	}
	public void setDeletedStatus(Integer deletedStatus) {
		this.deletedStatus = deletedStatus;
	}
	
	//@Column(name = "created_by_id")
//	public Integer getCreatedById() {
//		return createdById;
//	}
//	public void setCreatedById(Integer createdById) {
//		this.createdById = createdById;
//	}
	@Column(name = "preview_content")
	public String getPreviewContent() {
		return previewContent;
	}
	public void setPreviewContent(String previewContent) {
		this.previewContent = previewContent;
	}
	@Column(name = "original_doc_name")
	public String getOriginalDocName() {
		return originalDocName;
	}
	public void setOriginalDocName(String originalDocName) {
		this.originalDocName = originalDocName;
	}
	
	

}