package com.ametis.cms.datamodel;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.web.multipart.MultipartFile;

@Entity
@Table(name="theme")
public class Theme {
	private static final long serialVersionUID = 1L;
	private Integer themeId;
	
	private MemberGroup membergroupId;
	private Timestamp createdTime;
	private Timestamp modifiedTime;
	private Timestamp deletedTime;
	private String createdBy;
	private String modifiedBy;
	private String deletedBy;
	private Integer deleted;
	
	private Document logoId;
	private Document bgId;
	
	@ManyToOne
	@JoinColumn(name="logo_id")
	public Document getLogoId() {
		return logoId;
	}
	public void setLogoId(Document logoId) {
		this.logoId = logoId;
	}
	@ManyToOne
	@JoinColumn(name="bg_id")
	public Document getBgId() {
		return bgId;
	}
	public void setBgId(Document bgId) {
		this.bgId = bgId;
	}
	@Column(name="deleted")
	public Integer getDeleted() {
		return deleted;
	}
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}
	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(name="modified_by")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	@Column(name="deleted_by")
	public String getDeletedBy() {
		return deletedBy;
	}
	public void setDeletedBy(String deletedBy) {
		this.deletedBy = deletedBy;
	}
	@Column(name="created_time")
	public Timestamp getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}
	@Column(name="modified_time")
	public Timestamp getModifiedTime() {
		return modifiedTime;
	}
	public void setModifiedTime(Timestamp modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	@Column(name="deleted_time")
	public Timestamp getDeletedTime() {
		return deletedTime;
	}
	public void setDeletedTime(Timestamp deletedTime) {
		this.deletedTime = deletedTime;
	}
	@Id
	@Column(name="theme_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getThemeId() {
		return themeId;
	}
	public void setThemeId(Integer themeId) {
		this.themeId = themeId;
	}
	@ManyToOne
	@JoinColumn(name="membergroup_id")
	public MemberGroup getMembergroupId() {
		return membergroupId;
	}
	public void setMembergroupId(MemberGroup membergroupId) {
		this.membergroupId = membergroupId;
	}
	
}
